
#
# RxDOS Vers 6.00 (c) Mike Podanoffsky
#
TARGETS = prepdirs rxdos.sys rxdosbio.sys rxdoscmd.exe rxd_boot.exe rxvdisk.sys

SOURCE_DIR = sources
OBJECTS_DIR = objs
ERROR_DIR = errors

INCLUDE_SW = -I$(SOURCE_DIR)

!if "$(RxDOS_RELEASE)" == "1"
MASM_SW = -DRxDOS_RELEASE
LINK_SW =
!else

MASM_SW = -Zi
LINK_SW = /co
!endif

!if "$(INCLUDE_DATACHECK)" == "1"
MASM_SW = -DRxDOS_RELEASE -DINCLUDE_DATACHECK
LINK_SW =
!endif

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# All
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

all: $(TARGETS)

prepdirs:
   @md $(OBJECTS_DIR)
   @md $(ERROR_DIR)

rem-objs: 
   @md $(ERROR_DIR)
   @md $(OBJECTS_DIR)
   del $(OBJECTS_DIR)\*.obj
   del $(ERROR_DIR)\*.err
   del *.map
   del rxdos.exe 
   del rxdosbio.exe 
   del rxdoscmd.exe 
   del rxd_boot.exe 
   del rxdos.sys
   del rxdosbio.sys
   del rxvdisk.exe
   del rxvdisk.sys

#   del rxmouse.exe
#   del rxmouse.sys

xdata:
   $(MAKE) "INCLUDE_DATACHECK=1"

release:
   $(MAKE) rem-objs
   $(MAKE) "RxDOS_RELEASE=1"

debug:
   $(MAKE)

clean:
   $(MAKE) rem-objs

# zip:
#    pkzip rxdos $(SOURCE_DIR)\rx*.asm makefile make*.bat zero.res *.def
#    $(MAKE) release
#    pkzip rxd_boot rx*.sys rxdoscmd.exe rxd_boot.exe
#    pkzip rxd_boot config.def autoexec.def
#    @del $(OBJECTS_DIR)\*.obj
#    @del rxdos.exe 
#    @del rxdosbio.exe 
#    @del rxdoscmd.exe 
#    @del rxd_all.zip
#    $(MAKE)
#    pkzip rxd_winx rx*.exe
#    pkzip rxd_all rx*.zip

zip:
   $(MAKE) release
   pkzip rxdos $(SOURCE_DIR)\rx*.asm makefile make*.bat zero.res *.def
   pkzip rxd_boot rx*.sys rxdoscmd.exe rxd_boot.exe config.def autoexec.def


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Shortcuts
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

rxdos: rxdos.sys

rxdosbio: rxdosbio.sys

rxdoscmd: rxdoscmd.exe

rxd_boot: rxd_boot.exe

rxvdisk:  rxvdisk.sys

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Overwrite default rules
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

.asm.exe:
   masm $(MASM_SW) $(INCLUDE_SW) $*.asm,$(OBJECTS_DIR)\; > $*.err
 
.asm.obj:
   masm $(MASM_SW) $(INCLUDE_SW) $*.asm,$(OBJECTS_DIR)\; > $*.err

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# boot program
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

rxd_boot.exe : $(SOURCE_DIR)\rxd_boot.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxd_boot.exe
   @del $(OBJECTS_DIR)\rxd_boot.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxd_boot.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxd_boot.err
   type $(ERROR_DIR)\rxd_boot.err
   link $(LINK_SW) $(OBJECTS_DIR)\rxd_boot,rxd_boot;

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Bios load program - rxdosbio
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(OBJECTS_DIR)\rxdosbio.obj : $(SOURCE_DIR)\rxdosbio.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdosbio.exe
   @del rxdosbio.sys
   @del $(OBJECTS_DIR)\rxdosbio.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosbio.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosbio.err
   type $(ERROR_DIR)\rxdosbio.err

rxdosbio.exe : $(OBJECTS_DIR)\rxdosbio.obj
   link $(LINK_SW) $(OBJECTS_DIR)\rxdosbio,rxdosbio;

rxdosbio.sys : rxdosbio.exe
   exe2bin rxdosbio.exe rxdosbio.sys <zero.res

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# DOS kernel
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(OBJECTS_DIR)\rxdos.obj: $(SOURCE_DIR)\rxdos.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   del $(OBJECTS_DIR)\rxdos.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdos.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdos.err
   type $(ERROR_DIR)\rxdos.err

$(OBJECTS_DIR)\rxdosccb.obj: $(SOURCE_DIR)\rxdosccb.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosccb.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosccb.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosccb.err
   type $(ERROR_DIR)\rxdosccb.err

$(OBJECTS_DIR)\rxdosdev.obj: $(SOURCE_DIR)\rxdosdev.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosdev.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosdev.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosdev.err
   type $(ERROR_DIR)\rxdosdev.err

$(OBJECTS_DIR)\rxdosexe.obj: $(SOURCE_DIR)\rxdosexe.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosexe.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosexe.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosexe.err
   type $(ERROR_DIR)\rxdosexe.err

$(OBJECTS_DIR)\rxdosfat.obj: $(SOURCE_DIR)\rxdosfat.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosfat.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosfat.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosfat.err
   type $(ERROR_DIR)\rxdosfat.err

$(OBJECTS_DIR)\rxdosfcb.obj: $(SOURCE_DIR)\rxdosfcb.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosfcb.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosfcb.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosfcb.err
   type $(ERROR_DIR)\rxdosfcb.err

$(OBJECTS_DIR)\rxdosfil.obj: $(SOURCE_DIR)\rxdosfil.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosfil.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosfil.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosfil.err
   type $(ERROR_DIR)\rxdosfil.err

$(OBJECTS_DIR)\rxdoslfn.obj: $(SOURCE_DIR)\rxdoslfn.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdoslfn.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdoslfn.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdoslfn.err
   type $(ERROR_DIR)\rxdoslfn.err

$(OBJECTS_DIR)\rxdosf32.obj: $(SOURCE_DIR)\rxdosf32.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosf32.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosf32.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosf32.err
   type $(ERROR_DIR)\rxdosf32.err

$(OBJECTS_DIR)\rxdosifs.obj: $(SOURCE_DIR)\rxdosifs.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.obj
   @del $(OBJECTS_DIR)\rxdosifs.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosifs.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosifs.err
   type $(ERROR_DIR)\rxdosifs.err

$(OBJECTS_DIR)\rxdosctb.obj: $(SOURCE_DIR)\rxdosctb.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosctb.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosctb.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosctb.err
   type $(ERROR_DIR)\rxdosctb.err

$(OBJECTS_DIR)\rxdoserr.obj: $(SOURCE_DIR)\rxdoserr.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdoserr.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdoserr.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdoserr.err
   type $(ERROR_DIR)\rxdoserr.err

$(OBJECTS_DIR)\rxdosini.obj: $(SOURCE_DIR)\rxdosini.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosini.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosini.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosini.err
   type $(ERROR_DIR)\rxdosini.err

$(OBJECTS_DIR)\rxdosmem.obj: $(SOURCE_DIR)\rxdosmem.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosmem.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosmem.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosmem.err
   type $(ERROR_DIR)\rxdosmem.err

$(OBJECTS_DIR)\rxdossft.obj: $(SOURCE_DIR)\rxdossft.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdossft.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdossft.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdossft.err
   type $(ERROR_DIR)\rxdossft.err

$(OBJECTS_DIR)\rxdosstr.obj: $(SOURCE_DIR)\rxdosstr.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc
   @del rxdos.exe
   @del rxdos.sys
   @del $(OBJECTS_DIR)\rxdosstr.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosstr.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosstr.err
   type $(ERROR_DIR)\rxdosstr.err

rxdos.exe : $(OBJECTS_DIR)\rxdos.obj \
            $(OBJECTS_DIR)\rxdosccb.obj \
            $(OBJECTS_DIR)\rxdosdev.obj \
            $(OBJECTS_DIR)\rxdosexe.obj \
            $(OBJECTS_DIR)\rxdosfat.obj \
            $(OBJECTS_DIR)\rxdosfcb.obj \
            $(OBJECTS_DIR)\rxdosfil.obj \
            $(OBJECTS_DIR)\rxdoslfn.obj \
            $(OBJECTS_DIR)\rxdosf32.obj \
            $(OBJECTS_DIR)\rxdosifs.obj \
            $(OBJECTS_DIR)\rxdosmem.obj \
            $(OBJECTS_DIR)\rxdossft.obj \
            $(OBJECTS_DIR)\rxdosstr.obj \
            $(OBJECTS_DIR)\rxdosctb.obj \
            $(OBJECTS_DIR)\rxdoserr.obj \
            $(OBJECTS_DIR)\rxdosini.obj 
   link $(LINK_SW) @rxdos.lnk

rxdos.sys : rxdos.exe
   exe2bin rxdos.exe rxdos.sys <zero.res

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# DOS Command - rxdoscmd.exe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(OBJECTS_DIR)\rxdoscmd.obj: $(SOURCE_DIR)\rxdoscmd.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc $(SOURCE_DIR)\rxdoscin.inc
   @del rxdoscmd.exe
   @del $(OBJECTS_DIR)\rxdoscmd.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdoscmd.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdoscmd.err
   type $(ERROR_DIR)\rxdoscmd.err

$(OBJECTS_DIR)\rxdoscpy.obj: $(SOURCE_DIR)\rxdoscpy.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc $(SOURCE_DIR)\rxdoscin.inc
   @del rxdoscmd.exe
   @del $(OBJECTS_DIR)\rxdoscpy.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdoscpy.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdoscpy.err
   type $(ERROR_DIR)\rxdoscpy.err

$(OBJECTS_DIR)\rxdosdir.obj: $(SOURCE_DIR)\rxdosdir.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc $(SOURCE_DIR)\rxdoscin.inc
   @del rxdoscmd.exe
   @del $(OBJECTS_DIR)\rxdosdir.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosdir.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosdir.err
   type $(ERROR_DIR)\rxdosdir.err

$(OBJECTS_DIR)\rxdosfor.obj: $(SOURCE_DIR)\rxdosfor.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc $(SOURCE_DIR)\rxdoscin.inc
   @del rxdoscmd.exe
   @del $(OBJECTS_DIR)\rxdosfor.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosfor.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosfor.err
   type $(ERROR_DIR)\rxdosfor.err

$(OBJECTS_DIR)\rxdosprm.obj: $(SOURCE_DIR)\rxdosprm.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc $(SOURCE_DIR)\rxdoscin.inc
   @del rxdoscmd.exe
   @del $(OBJECTS_DIR)\rxdosprm.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosprm.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosprm.err
   type $(ERROR_DIR)\rxdosprm.err

$(OBJECTS_DIR)\rxdosren.obj: $(SOURCE_DIR)\rxdosren.asm $(SOURCE_DIR)\rxdosdef.inc $(SOURCE_DIR)\rxdosmac.inc $(SOURCE_DIR)\rxdoscin.inc
   @del rxdoscmd.exe
   @del $(OBJECTS_DIR)\rxdosren.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxdosren.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxdosren.err
   type $(ERROR_DIR)\rxdosren.err

#  rxdosren MUST be last file

rxdoscmd.exe : $(OBJECTS_DIR)\rxdoscmd.obj \
            $(OBJECTS_DIR)\rxdoscpy.obj \
            $(OBJECTS_DIR)\rxdosdir.obj \
            $(OBJECTS_DIR)\rxdosfor.obj \
            $(OBJECTS_DIR)\rxdosprm.obj \
            $(OBJECTS_DIR)\rxdosren.obj
   link $(LINK_SW) @rxdoscmd.lnk

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Mouse drived - rxmouse
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#$(OBJECTS_DIR)\rxmouse.obj: $(SOURCE_DIR)\rxmouse.asm
#   @del rxmouse.exe
#   @del $(OBJECTS_DIR)\rxmouse.obj
#   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxmouse.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxmouse.err
#   type $(ERROR_DIR)\rxmouse.err
   
#rxmouse.exe : $(OBJECTS_DIR)\rxmouse.obj
#   link $(LINK_SW) $(OBJECTS_DIR)\rxmouse.obj, rxmouse.exe;
#   exe2bin rxmouse.exe rxmouse.sys <zero.res
#   exe2bin rxmouse.exe rxmouse.com <zero.res

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# RAM DISK
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(OBJECTS_DIR)\rxvdisk.obj: $(SOURCE_DIR)\rxvdisk.asm
   @del rxvdisk.exe
   @del $(OBJECTS_DIR)\rxvdisk.obj
   masm $(MASM_SW) $(INCLUDE_SW) $(SOURCE_DIR)\rxvdisk.asm,$(OBJECTS_DIR)\; > $(ERROR_DIR)\rxvdisk.err
   type $(ERROR_DIR)\rxvdisk.err
   
rxvdisk.sys : $(OBJECTS_DIR)\rxvdisk.obj
   link $(LINK_SW) $(OBJECTS_DIR)\rxvdisk.obj, rxvdisk.exe;
   exe2bin rxvdisk.exe rxvdisk.sys

