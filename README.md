F O R    I M M E D I A T E   R E L E A S E

7 August 1999

RxDOS is now Free Software

RxDOS is now freely available under the GNU Public License. Distribution and use of RxDOS is free so long as you acknowledge the original source of the software and freely make available to others the sources to any derived work. The FreeDOS project web site, http://www.freedos.org will be the initial distribution site for downloading, discussing, and evolving RxDOS.

RxDOS is a DOS replacement operating system.  It is a stable, high performance system designed for embedded application use. It was originally released in book form, Dissecting DOS (1994 Addison-Wesley). You can find out more about the book at

http://www.amazon.com/exec/obidos/ASIN/020162687X/o/qid=934042853/sr=8-1/002-1027548-5689804

The full GNU GPL can be found on the web at

http://www.gnu.org/copyleft/gpl.html

RxDOS features:

   * Internal data structure compatibility with MSDOS
   * Supports all commands
   * Supports FAT32 and Long filenames (including int 21 fct 71 APIs)

RxDOS does not support:

   * Himem.sys
   * NewDeal Office
   * Is not supplied with utilities like format, fdisk and others

Requirements:

   * RxDOS is written in assembly language and requires MASM 5.x

Interesting Projects:

   * Move RxDOS to NASM and/or MASM 6.x
   * Add support for Himem.sys and NewDeal Office
   * Package RxDOS with utilities
   * Fix assorted small bugs

Really Interesting Projects

We should open a discussion on how to merge FreeDOS and RxDOS to create either two distributions or to create a single FreeDOS/RxDOS distribution. We should solicit opinions, commentary and suggestions.  I am open to any and all ideas.

mike.podanoffsky@mindspring.com
