        TITLE   'FCB - Support FCB Processing'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Support FCB Processing                                       ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc

RxDOS   SEGMENT PUBLIC 'CODE'
        assume cs:RxDOS, ds:RxDOS, es:RxDOS, ss:RxDOS

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Support FCB Processing                                       ;
        ;...............................................................;

        public initFCBfromSFT
        public buildFindFromFCB
        public buildDTAfcbFind

        extrn copyBlock                         : near
        extrn getActualDrive                    : near
        extrn getCurrDirCluster                 : near
        extrn locateCCBPHeader                  : near

        extrn _RxDOS_pDTA                       : dword

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Init FCB from an opened SFT                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ds:si  fcb pointer                                          ;
        ;   es:di  sft pointer                                          ;
        ;   ax     handle assigned                                      ;
        ;...............................................................;

initFCBfromSFT:
        mov byte ptr [ fcbSFN          ][ si ], al
        mov word ptr [ fcbRecordSize   ][ si ], 128

        xor dx, dx
        mov word ptr [ fcbRandomRecNo. _low  ][ si ], dx
        mov word ptr [ fcbRandomRecNo. _high ][ si ], dx
        mov word ptr [ fcbCurrBlockNo       ][ si ], dx
        mov word ptr [ fcbCurrRecNo         ][ si ], dx
        mov byte ptr [ fcbFlags             ][ si ], dl

        mov dx, word ptr es:[ sftFileSize. _high ][ di ]
        mov ax, word ptr es:[ sftFileSize. _low  ][ di ]
        mov word ptr [ fcbFileSize. _high ][ si ], dx
        mov word ptr [ fcbFileSize. _low  ][ si ], ax

        mov dx, word ptr es:[ sftDate ][ di ]
        mov ax, word ptr es:[ sftTime ][ di ]
        mov word ptr [ fcbDate ][ si ], dx
        mov word ptr [ fcbTime ][ si ], ax

        mov dx, word ptr es:[ sftDCB. _high ][ di ]
        mov ax, word ptr es:[ sftDCB. _low  ][ di ]
        mov word ptr [ fcbDHD. _high ][ si ], dx
        mov word ptr [ fcbDHD. _low  ][ si ], ax

        mov ax, word ptr es:[ sftBegCluster ][ di ]
        mov word ptr [ fcbBegClusterNo ][ si ], ax

        or word ptr es:[ sftMode ][ di ], sftENTRY_ISFCB
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Init Find Record from an FCB                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   stack  pointer to fcb                                       ;
        ;   stack  pointer to find record                               ;
        ;   dx     dir offset to insert                                 ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   es:di  points to find record                                ;
        ;...............................................................;

buildFindFromFCB:

        Entry 4
        darg _fcb
        darg _findEntry
        def _dirOffset, dx
        def _attributes, 0000

        saveRegisters ds, si, bx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if we have an extended FCB 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        getdarg es, bx, _fcb
        cmp byte ptr es:[ extSignature ][ bx ], -1      ; do we have an extended fcb ?
        jnz buildFindFromFCB_08                         ; no -->

        mov al, byte ptr es:[ extAttribute ][ bx ]      ; get attributes from
        mov byte ptr [ _attributes ][ bp ], al
        add bx, offset sizeExtendedFCBHdr
        mov word ptr [ _fcb. _pointer ][ bp ], bx       ; adjust to fcb address

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if we have a 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
buildFindFromFCB_08:
        push ds
        setDS es        
        lea si, offset fcbName [ bx ]

        getdarg es, di, _findEntry
        lea di, offset findSrchName [ di ]
        mov cx, sizeFILENAME

buildFindFromFCB_12:
        lodsb                                           ; get character
        _upperCase al                                   ; upper case
        stosb
        loop buildFindFromFCB_12

        pop ds
        getdarg es, bx, _fcb
        push word ptr es:[ fcbDrive ][ bx ]
        call getActualDrive                             ; correct drive
        call getCurrDirCluster

        getdarg es, di, _findEntry
        mov word ptr es:[ findDirBegCluster. _low ][ di ], dx
        mov word ptr es:[ findDirBegCluster. _high ][ di ], cx
        mov byte ptr es:[ findSrchDrive ][ di ], al     ; store drive

        mov al, byte ptr [ _attributes ][ bp ]
        mov byte ptr es:[ findSrchAttributes ][ di ], al

        mov ax, word ptr [ _dirOffset ][ bp ]
        mov word ptr es:[ findDirEntry ][ di ], ax

        restoreRegisters bx, si, ds
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  build DTA record from fcb/ find entry                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   stack  pointer to fcb                                       ;
        ;   stack  pointer to find record                               ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   es:di  points to find record                                ;
        ;...............................................................;

buildDTAfcbFind:
        Entry 4
        darg _fcb
        darg _findEntry

        currSegment ds                          ; point to current segment
        lds di, dword ptr [ _RxDOS_pDTA ]       ; build DTA area

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  extended fcb passed ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getdarg es, bx, _fcb                    ; fcb address
        cmp byte ptr es:[ extSignature ][ bx ], -1
        jnz _buildDTAfcbFind_12                 ; if not an extended fcb -->

        push es
        push bx                                 ; extended fcb
        push ds
        push di                                 ; dta
        mov cx, sizeExtendedFCBHdr
        call CopyBlock                          ; copy 
        add di, sizeExtendedFCBHdr              ; dta pointer
        add bx, sizeExtendedFCBHdr              ; fcb pointer

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy directory entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_buildDTAfcbFind_12:
        push es
        push bx                                 ; address of fcb (adjusted)

        getdarg es, si _findEntry
        mov al, byte ptr es:[ findSrchDrive ][ si ]
        mov byte ptr [ findSrchDrive ][ di ], al
        inc byte ptr [ findSrchDrive ][ di ]    ; fcb drive is 01 ...

        lea di, offset findSrchName [ di ]
        push di                                 ; where to copy dir structure
        mov si, word ptr es:[ findCCBPointer ][ si ]
        call locateCCBPHeader                   ; get buffers segment
        pop di                                  ; restore di (dta pointer)

        push es
        push si                                 ; from directory entry
        push ds
        push di                                 ; to dta
        mov cx, sizeDIRENTRY
        call CopyBlock                          ; copy dir entry

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; update fcb for search next
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        pop bx
        pop ds
        getdarg es, si _findEntry
        mov cx, word ptr es:[ findDirEntry      ][ si ]
        mov word ptr [ fcbCurrRecNo          ][ bx ], cx
        mov dx, word ptr es:[ findDirBegCluster. _low ][ si ]
        mov word ptr [ fcbBegClusterNo       ][ bx ], dx

        currSegment ds                          ; point to current segment
        clc

        Return

RxDOS   ENDS
        END

