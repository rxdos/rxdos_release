



        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell Data Structures and Definitions          ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Switches                                                     ;
        ;...............................................................;

        SWITCHENTRY struc

swChar                          db ?
swFlags                         dw ?
swMinValue                      dw ?
swMaxValue                      dw ?
swActualValue                   dw ?

        SWITCHENTRY ends

SW_MINMAXVALUE                  equ 0001h
SW_LETTERCHOICE                 equ 0002h

SW_SWITCHSET                    equ 8000h
sizeSWITCHENTRY                 equ size SWITCHENTRY

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Sprintf flags                                                ;
        ;...............................................................;

SPRINTF_COMMADELIM              equ 8000h
SPRINTF_LONGFLAG                equ 4000h
SPRINTF_LEFTALIGN               equ 2000h
SPRINTF_ZEROFIELDFILL           equ 1000h

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Replace Macro                                                ;
        ;...............................................................;

Translate macro c1, c2, goto

        mov ah, c2
        cmp al, c1
        jz goto 
        endm

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Define Date/Time Template                                    ;
        ;...............................................................;

intldate macro country, template

        dw country
        db template, 0
        endm

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Extended Open                                                ;
        ;...............................................................;

ExOpenFile macro filename, opt_access

    filenameassigned = no

    ifnb <opt_access>
        mov bx, opt_access                    ; open read only
    else
        mov bx, OPEN_ACCESS_READWRITE                   ; default mode: read/ write
        endif

    ifnb <filename>
     irp treg, <ax, bx, cx, dx, di>
      ifidn <filename>, <treg>
        filenameassigned = yes
        mov si, filename
        endif
        endm

     ifidn <filename>, <si>

     else
      ife (filenameassigned EQ yes)
        mov si, offset filename
        endif
        endif
        endif

        xor cx, cx
        mov dx, EXTENDEDACTION_OPEN
        Int21 LFNExtendedOpenCreate                     ; try to open file
        endm

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Extended Create                                              ;
        ;...............................................................;

ExCreateFile macro filename, opt_attrib

    filenameassigned = no

    ifnb <opt_attrib>
        mov cx, opt_attrib                    ; open read only
    else
        mov cx, ATTR_NORMAL                   ; default mode: read/ write
        endif

    ifnb <filename>
     irp treg, <ax, bx, cx, dx, di>
      ifidn <filename>, <treg>
        filenameassigned = yes
        mov si, filename
        endif
        endm

     ifidn <filename>, <si>

     else
      ife (filenameassigned EQ yes)
        mov si, offset filename
        endif
        endif
        endif

        mov dx, (EXTENDEDACTION_CREATE + EXTENDEDACTION_TRUNCATE)
        Int21 LFNExtendedOpenCreate                     ; try to open file
        endm

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  File Attributes                                              ;
        ;...............................................................;

FileAttrib macro Must, Search

    ifb <Must>
        xor cx, cx
    else
      ifidn <Must>, <ATTR_NORMAL>
        xor cx, cx
      else
        mov ch, Must
        endif
 
    ifb <Search>
        mov cl, ch
    else
        mov cl, Search

        endif
        endif
        endm

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  For Argument List                                            ;
        ;...............................................................;

        FORARGS struc

forVarIdent                     dw ?, ?
forVarLetter                    dw ?, ?
forInArg                        dw ?, ?
forInStartParen                 dw ?, ?

        FORARGS ends

_IN                             equ 0
_DO                             equ 1

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Date Control Block                                           ;
        ;...............................................................;

        DATE struc

_DAY                            dw ?
_MONTH                          dw ?
_YEAR                           dw ?

        DATE ends

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Time Control Block                                           ;
        ;...............................................................;

        TIME struc

_HOURS                          dw ?
_MINUTES                        dw ?
_SECONDS                        dw ?
_HUNDREDTHS                     dw ?

        TIME ends

sizeDATE                        equ size DATE
sizeTIME                        equ size TIME        
        
        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Arg Table                                                    ;
        ;...............................................................;

        CMDARGUMENT struc

argpointer                      dw ?                    ; argument pointer
arglength                       dw ?                    ; argument length

        CMDARGUMENT ends

maxArgArray                     equ 64 
sizeArgCmd                      equ size CMDARGUMENT
sizemaxArgArray                 equ ( maxArgArray * sizeArgCmd )

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Parsed Filename Struct                                       ;
        ;...............................................................;

        PARSEDFILENAME struc

parsedrive                      dw ?                    ; drive value
parsewildcharflag               dw ?                    ; wild char flag
parsestringlength               dw ?                    ; length of string
parsepathlength                 dw ?                    ; length of drive/path string
parsefilenamelength             dw ?                    ; length of filename string
parseextensionlength            dw ?                    ; length of extension string

parseptrstartstring             dd ?                    ; start of string
parseptrfilename                dw ?                    ; ptr to filename (after last \)
parseptrextension               dw ?                    ; ptr to extension (after .)

        PARSEDFILENAME ends

sizeParsedFilename              equ size PARSEDFILENAME
SAFE_ALLOCATION                 equ ( 8 * 1024)
