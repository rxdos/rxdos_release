        TITLE   'ERR - RxDOS Error Info'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  ERR - RxDOS Error Info                                       ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc

RxDOS   SEGMENT PUBLIC 'CODE'
        assume cs:RxDOS, ds:RxDOS, es:RxDOS, ss:RxDOS

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  ERR - RxDOS Error Info                                       ;
        ;...............................................................;

        public pexterrInvalidFunction
        public pexterrFileNotFound
        public pexterrPathNotFound
        public pexterrIllegalName
        public pexterrNoHandlesAvailable
        public pexterrAccessDenied
        public pexterrInvalidHandle
        public pexterrArenaTrashed
        public pexterrNotEnoughMemory
        public pexterrInvalidBlock
        public pexterrInvalidAccess
        public pexterrInvalidDrive
        public pexterrCurrentDirectory
        public pexterrNoMoreFiles
        public pexterrFileExists

        public GetExtErrorCodeValue
        public GetAllExtErrorInfo

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Extended Error Tables                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  macro requires error code, locus, sug-action, class.         ;
        ;                                                               ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  EXTERRORCODE Structure                                       ;
        ;...............................................................;

                        EXTERRORCODE struc
ExtErrorCodeValue       dw ?
Locus                   db ?
SuggestedAction         db ?
ErrorClass              db ?
                        db ?
                        EXTERRORCODE ends

pexterrInvalidFunction    EXTERRORCODE  <errInvalidFunction,          \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrFileNotFound       EXTERRORCODE  <errFileNotFound,             \
                                             ERRLOC_DISK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrPathNotFound       EXTERRORCODE  <errPathNotFound,             \
                                             ERRLOC_DISK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrIllegalName        EXTERRORCODE  <errIllegalName,              \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrNoHandlesAvailable EXTERRORCODE  <errNoHandlesAvailable,       \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_OUTRES >

pexterrAccessDenied       EXTERRORCODE  <errAccessDenied,             \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrInvalidHandle      EXTERRORCODE  <errInvalidHandle,            \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrArenaTrashed       EXTERRORCODE  <errArenaTrashed,             \
                                             ERRLOC_MEM, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrNotEnoughMemory    EXTERRORCODE  <errNotEnoughMemory,          \
                                             ERRLOC_MEM, ERRACT_RETRY, ERRCLASS_NOTFND >

pexterrInvalidBlock       EXTERRORCODE  <errInvalidBlock,             \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrInvalidAccess      EXTERRORCODE  <errInvalidAccess,            \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrInvalidDrive       EXTERRORCODE  <errInvalidDrive,             \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrCurrentDirectory   EXTERRORCODE  <errCurrentDirectory,         \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_AUTH   >

pexterrNoMoreFiles        EXTERRORCODE  <errNoMoreFiles,              \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_NOTFND >

pexterrFileExists         EXTERRORCODE  <errFileExists,               \
                                             ERRLOC_UNK, ERRACT_USER,  ERRCLASS_NOTFND >

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Extended Error Info                                      ;
        ;...............................................................;

GetExtErrorCodeValue:

        mov ax, word ptr cs:[ ExtErrorCodeValue ][ si ] ; get actual error value
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get All Extended Error Info                                  ;
        ;...............................................................;

GetAllExtErrorInfo:
        mov ax, word ptr cs:[ ExtErrorCodeValue ][ bx ] ; else get error values
        mov ch, byte ptr cs:[ Locus             ][ bx ]
        mov dl, byte ptr cs:[ SuggestedAction   ][ bx ]
        mov dh, byte ptr cs:[ ErrorClass        ][ bx ]
        ret

RxDOS   ENDS
        END





