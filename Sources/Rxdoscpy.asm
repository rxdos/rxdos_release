        TITLE   'Copy - RxDOS Command Shell Copy'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell Copy                                     ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Programmer's Notes:                                          ;
        ;                                                               ;
        ;  Command Shell consists of  two parts bound  together into a  ;
        ;  single executable load.  There  exists  a  single  resident  ;
        ;  command shell which is accessible by an Int 2Eh.             ;
        ;                                                               ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc
        include rxdoscin.inc

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;...............................................................;

RxDOSCMD SEGMENT PUBLIC 'CODE'
         assume cs:RxDOSCMD, ds:RxDOSCMD, es:RxDOSCMD, ss:RxDOSCMD

        public _Copy
        public allocateCopyBuffer
        public freeCopyBuffer

        extrn CmndError_BadSwitch                       : near
        extrn CmndError_CannotCopyOverSelf              : near
        extrn CmndError_CannotCreateFile                : near
        extrn CmndError_ContentsLostBeforeCopy          : near
        extrn CmndError_InvalidNumberArguments          : near
        extrn CmndError_FileNotFound                    : near
        extrn CmndError_ReadErrorAbort                  : near

        extrn _Copy_FilesCopied                         : near
        extrn CountArgs                                 : near
        extrn CRLF                                      : near
        extrn deleteArg                                 : near
        extrn insertArg                                 : near
        extrn removeLooseSwitches                       : near
        extrn DisplayErrorMessage                       : near
        extrn DisplayLine                               : near
        extrn RxDOS_AllFiles                            : near
        extrn RxDOS_TruenameCurrDir                     : near
        extrn fixUpArguments                            : near

        extrn CopyString                                : near
        extrn _endofString                              : near
        extrn _Copy_FilesCopied                         : near
        extrn _CmndParse_SeparatorCheck                 : near
        extrn _SwitchChar                               : near
        extrn _sprintf                                  : near

        extrn CheckFileClose                            : near
        extrn BuildFullPathFirstFile                    : near
        extrn BuildFullPathNextFile                     : near
        extrn BuildFullPathClose                        : near
        extrn LFNReturnPreferedFilenamePtr              : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Cluster Record                                               ;
        ;...............................................................;

        CLUSTERRECORD struc

handle                  dw  ?                           ; file handle
clusterDrive            dw  ?                           ; drive
clusterValue            dd  ?                           ; value

        CLUSTERRECORD ends

sizeClusterRecord       equ size CLUSTERRECORD
FASTREAD_BUFFERSIZE     equ 8192                        ; 8k

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Copy filenameA+filenameB+filenameC filenameDest              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_Copy:

        Entry
        def __argarray, di                              ; args array
        def __Mode, 0000                                ; non-z if ascii/ z if binary
        def _overide, FALSE                             ; no automatic overide
        def __AddMode                                   ; 0000 not add mode
        def __bytesRead                                 ; bytes actually read
        def _sameName, TRUE                             ; if take names from source
        def _wildChars, FALSE                           ; if wild char name replacement required
        def _filesCopied, 0000
        def _findHandle, -1
        def _ptrName

        def _buffersize, SAFE_ALLOCATION
        ddef _pBuffer, 0000, 0000

        defbytes _destCluster   , sizeClusterRecord
        defbytes _srcCluster    , sizeClusterRecord

        defbytes _destFilename  , sizeLFNPATH           ; destination work area (build path, wild chars)
        defbytes _createFilename, sizeLFNPATH           ; name to create
        defbytes _copyFilename  , sizeLFNPATH           ; do I really need this ?????
        defbytes _finddata      , sizeFINDDATA

        defbytes _buffer, SAFE_ALLOCATION

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  allocate copy buffer
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        push es
        lea bx, offset [ _buffer ][ bp ]                ; default buffer 
        call allocateCopyBuffer                         ; allocate copy buffer
        stordarg _pBuffer, es, bx                       ; store copy buffer
        storarg  _buffersize, cx                        ; buffer size
        pop es

        mov bx, -1                                      ; handle not init
        lea di, offset [ _destCluster ][ bp ]           ; cluster record 
        call _initClusterRecord

        lea di, offset [ _copyFilename ][ bp ]
        mov word ptr [ di ], '.'                        ; default to current directory if arg missing

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get/test destination filename
;-----------------------------------------------------------------------
;
;  destination argument may be:
;
;    blank              - replace with drive:\current directory
;    drive:             - replace with drive:\current directory
;    [drive:][\]path    - detect, set same name = TRUE
;    [path]filename     - detect, set same name = FALSE, set wild chars = FALSE
;    [path]file*name    - detect, set same name = FALSE, set wild chars = TRUE
;
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, __argarray                           ; args array
        call fixUpArguments                             ; removes "..." etc...

        getarg di, __argarray                           ; args array
        call removeLooseSwitches                        ; remove any extra args (switches)

        cmp ax, 1                                       ; must have at least one arg
        ifc _copyInvalidNumberArguments                 ; any less means error -->
        jz _Copy_14                                     ; if only one arg, then same as drive: case ->

        dec ax                                          ; may not have more than two args
        mov cx, sizeArgCmd                              ; 
        mul cl                                          ; ignore dx
        add di, ax                                      ; point to last argument

        push word ptr [ argpointer ][ di ]              ; get last arg address
        push word ptr [ arglength ][ di ]               ; length
        call deleteArg                                  ; delete

        pop cx
        pop si
        lea di, offset [ _copyFilename ][ bp ]
        call _copyArgument                              ; copy argument

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if dest is just a directory name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea di, offset [ _copyFilename ][ bp ]
        call isitJustDriveLetter                        ; test for x:
        jz _Copy_14                                     ; if just drive letter -->

        mov dx, di                                      ; _copyFilename
        lea di, offset [ _finddata ][ bp ]
        call seeIfDirectoryName                         ; is it a directory ?
        jc _Copy_20                                     ; if not a directory -->
        jz _Copy_20                                     ; if not a dir entry -->

_Copy_14:
        storarg _sameName, TRUE                         ; same name
        storarg _wildChars, FALSE                       ; no wild chars replacement

        lea si, offset [ _copyFilename ][ bp ]
        lea di, offset [ _destFilename ][ bp ]          ; expansion area
        mov byte ptr [ di ], '\'                        ; (in case no output generated)

        mov cl, LFNEXPAND_GETFULLPATHNAME
        Int21 LFNGetActualFileName                      ; expand name
        ifc _copyCannotCreateFile                       ; destination doesn't exist -->

        lea di, offset [ _destFilename ][ bp ]          ; destination path
        call _endofString                               ; locate end
        cmp byte ptr [ di ], '\'                        ; already a trailing \ ?
        jz _Copy_30                                     ; yes -->

        mov word ptr [ di ], '\'                        ; add trailing \ + null char
        jmp short _Copy_30                              ; start copy -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  not a directory
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_20:
        storarg _sameName, FALSE                        ; same name not true
        storarg _wildChars, FALSE                       ; no wild chars replacement

        lea si, offset [ _copyFilename ][ bp ]
        lea di, offset [ _destFilename ][ bp ]          ; expansion area
        mov byte ptr [ di ], '\'                        ; (in case no output generated)

        mov cl, LFNEXPAND_GETFULLPATHNAME
        Int21 LFNGetActualFileName                      ; expand name
        ifc _copyCannotCreateFile                       ; destination doesn't exist -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  main loop through all args
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_30:
        mov word ptr [ __AddMode  ][ bp ], 0000         ; not add mode

_Copy_32:
        setDS ss
        getarg di, __argarray                           ; get arg pointer to next arg
        add word ptr [ __argarray ][ bp ], sizeArgCmd   ; skip this argument next time

        mov cx, word ptr [ arglength ][ di ]            ; argument length
        mov si, word ptr [ argpointer ][ di ]           ; point to text 
        or si, si                                       ; null entry ?
        ifz _Copy_ReturnClose                           ; yes, return -->

        mov ax, word ptr [ si ]
        call _Copy_TestSwitch                           ; see if /a, /b ...
        jz _Copy_32
        call _Copy_AddMode                              ; see if + add
        jz _Copy_32

        fileAttrib ATTR_NORMAL
        lea di, offset [ _finddata ][ bp ]
        lea dx, offset [ _copyFilename ][ bp ]          ; expanded path (sizeLFNPATH)
        call BuildFullPathFirstFile                     ; filename found for arg ?
        storarg _findHandle, bx                         ; save find handle
        storarg _ptrName, dx                            ; present name to next
        jnc _Copy_44                                    ; if file found -->

        mov dx, offset CmndError_FileNotFound
        call DisplayLine
        jmp _copyErrorCleanUp

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  open source file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_44:
        lea di, offset [ _copyFilename ][ bp ]          ; expanded path (sizeLFNPATH)
        ExOpenFile di, OPEN_ACCESS_READONLY             ; open read only
        ifc _copyCannotOpenFile                         ; can't, display error -->

        mov bx, ax
        lea di, offset [ _srcCluster ][ bp ]            ; cluster record
        call GetClusterValue                            ; src cluster
        jz _Copy_50                                     ; if not a file -->

        lea si, offset [ _destCluster ][ bp ]           ; cluster record
        call _compareClusters                           ; compare src and dest Clusters
        jnz _Copy_50                                    ; no -->

    ; error: file destroyed

        mov dx, offset CmndError_ContentsLostBeforeCopy
        call DisplayErrorMessage                        ; dest file same as source
        jmp _copyErrorCleanUp

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  attempt create file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_50:
        cmp word ptr [ _destCluster. handle ][ bp ], -1 ; any dest file ?
        jz _Copy_52                                     ; no, MUST create -->
        cmp word ptr [ __AddMode  ][ bp ], 0000         ; add mode ?
        ifnz _Copy_68                                   ; yes, append to existing file -->

_Copy_52:
        lea si, offset [ _destFilename ][ bp ]          ; destination filename
        lea di, offset [ _createFilename ][ bp ]        ; make destination name
        call CopyString                                 ; copy string

        cmp word ptr [ _sameName ][ bp ], TRUE
        jnz _Copy_54                                    ; different name available -->

        lea di, offset [ _copyFilename ][ bp ]
        call _PtrTo_Filename                            ; get pointer to filename
        push di

        lea di, offset [ _createFilename ][ bp ]        ; make destination name
        call _PtrTo_Filename                            ; get pointer to filename

        pop si                                          ; source name
        call CopyString                                 ; copy string

_Copy_54:
        lea si, offset [ _createFilename ][ bp ]        ; destination filename
        ExOpenFile si, OPEN_ACCESS_READONLY             ; open read only
        jc _Copy_62                                     ; MUST create -->

        mov bx, ax
        lea di, offset [ _destCluster ][ bp ]           ; cluster record
        call GetClusterValue                            ; cluster of dest
        Int21 CloseFile                                 ; release file

        lea si, offset [ _srcCluster ][ bp ]            ; cluster record
        call _compareClusters                           ; compare src and dest Clusters
        ifz _copyCannotCopyOverSelf                     ; cannot copy over self -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  create file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_62:
        getarg ax, _overide                             ; previous overide
        lea si, offset [ _createFilename ][ bp ]        ; destination filename
      ; call _copyOverideCheck                          ; see if overide is ok
      ; storarg _overide, ax                            ; store for future reference

        ExCreateFile si, ATTR_NORMAL
        ifc _copyCannotCreateFile                       ; if cannot be created or nulled -->

        mov bx, ax
        lea di, offset [ _destCluster ][ bp ]           ; cluster record
        call GetClusterValue                            ; cluster of dest

        lea si, offset [ _srcCluster ][ bp ]            ; cluster record
        call _compareClusters                           ; compare src and dest Clusters
        ifz _copyCannotCopyOverSelf                     ; camnnot copy over self -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  display filename
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_68:
        mov bx, word ptr [ _srcCluster. handle ][ bp ]
        Int21 IoControl, 00                             ; is source a char device ?
        test dx, sftIsDevice                            ; test device flag
        jnz _Copy_74                                    ; if device, skip display filename -->

        lea di, offset [ _finddata ][ bp ]
        call LFNReturnPreferedFilenamePtr
        call DisplayLine                                ; display line
        call CRLF                                       ; cr/lf

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy loop
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_74:
        push ds
        getarg  cx, _buffersize                         ; buffer size
        getdarg ds, dx, _pBuffer                        ; allocated buffer
        mov bx, word ptr [ _srcCluster. handle ][ bp ]
        Int21 ReadFile                                  ; read 
        pop ds
        ifc _copyReadErrorAbort                         ; if error -->

        mov cx, ax                                      ; bytes read
        or cx, cx                                       ; null chars returned ?
        jz _Copy_82                                     ; must be end of file -->

        push ds
        getdarg ds, dx, _pBuffer                        ; allocated buffer
        mov bx, word ptr [ _destCluster. handle ][ bp ]
        Int21 WriteFile                                 ; write
        pop ds
        jmp _Copy_74                                    ; read more -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  close source file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_82:
        mov bx, word ptr [ _srcCluster. handle ][ bp ]
        call _CopyCloseFile
        mov word ptr [ _srcCluster. handle ][ bp ], -1  ; change handle to reflect this

        setDS ss
        getarg di, __argarray                           ; get arg pointer to next arg
        mov si, word ptr [ argpointer ][ di ]           ; point to text 
        jnz _Copy_92                                    ; yes, go to next -->

        mov bx, word ptr [ _destCluster. handle ][ bp ]
        call _CopyCloseFile                             ; if no adds left ...
        mov word ptr [ _destCluster. handle ][ bp ], -1
        inc word ptr [ _filesCopied ][ bp ]             ; # files copied.

        getarg dx, _ptrName                             ; name field
        getarg bx, _findHandle                          ; find handle
        lea di, offset [ _finddata ][ bp ]              ; name area
        call BuildFullPathNextFile                      ; filename found for arg ?
        ifnc _Copy_44                                   ; if file found -->

_Copy_92:
        getarg bx, _findHandle                          ; find handle
        call BuildFullPathClose                         ; close find search
        storarg _findHandle, -1                         ; clear find handle
        jmp _Copy_30

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_ReturnClose:
        mov bx, word ptr [ _destCluster. handle ][ bp ]
        cmp bx, -1                                      ; destination still open ?
        jz _Copy_ReturnClose_08                         ; no, don't close -->
        call _CopyCloseFile
        mov word ptr [ _destCluster. handle ][ bp ], -1
        inc word ptr [ _filesCopied ][ bp ]             ; # files copied.

_Copy_ReturnClose_08:
        lea di, offset [ _filesCopied ][ bp ]
        push di                                         ; first arg encountered
        mov di, offset _Copy_FilesCopied
        push di

        lea di, offset [ _buffer ][ bp ]
        push di
        call _sprintf
        add sp, ax                                      ; # args passed
        call DisplayLine

_Copy_Return:
        mov es, word ptr [ _pBuffer. _segment ][ bp ]
        call freeCopyBuffer                             ; free copy buffer

        getarg bx, _findHandle                          ; find handle
        call BuildFullPathClose                         ; close find search
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  test switches
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_TestSwitch:
        cmp al, byte ptr [ _SwitchChar ]                ; switch ?
        jz _Copy_TestSwitch_08
        ret

_Copy_TestSwitch_08:
        mov al, byte ptr [ si+1 ]
        _lowerCase al                                   ; test switch option

        storarg __Mode, 01                              ; ascii mode
        cmp al, 'a'
        jz _Copy_TestSwitch_12

        storarg __Mode, 00                              ; binary mode

_Copy_TestSwitch_12:
        xor ax, ax
        ret

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Add mode
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Copy_AddMode:
        cmp al, '+'
        jnz _Copy_AddMode_08
        mov word ptr [ __AddMode  ][ bp ], -1           ; set add mode

_Copy_AddMode_08:
        ret

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  error in rename command
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_copyInvalidNumberArguments:
        mov dx, offset CmndError_InvalidNumberArguments
        jmp short _copyDisplayError

_copyCannotCopyOverSelf:
        mov dx, offset CmndError_CannotCopyOverSelf
        jmp short _copyDisplayError

_copyCannotOpenFile:
        mov dx, offset CmndError_FileNotFound
        jmp short _copyDisplayError

_copyCannotCreateFile:
        mov dx, offset CmndError_CannotCreateFile
        jmp short _copyDisplayError

_copyReadErrorAbort:
        mov dx, offset CmndError_ReadErrorAbort
      ; jmp short _copyDisplayError

_copyDisplayError:
        call DisplayErrorMessage
      ; jmp short _copyErrorCleanUp

_copyErrorCleanUp:
        mov bx, word ptr [ _destCluster. handle ][ bp ]
        cmp bx, -1                                      ; if not assigned, 
        jz _copyErrorCleanUp_08                         ; don't delete -->
        call _CopyCloseFile

_copyErrorCleanUp_08:
        jmp _Copy_Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Close File                                                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Inputs:                                                      ;
        ;   bx     file Handle                                          ;
        ;                                                               ;
        ;...............................................................;

_CopyCloseFile:

        cmp bx, -1                                      ; if not assigned, 
        jz _CopyClose_Exit                              ; exit -->
        or bx, bx
        jz _CopyClose_Exit                              ; exit -->

        Int21 IoControl, 00                             ; is source a char device ?
        test dx, sftIsDevice                            ; test device flag
        jnz _CopyClose_Exit                             ; if device, skip close -->

        Int21 CloseFile                                 ; close source file

_CopyClose_Exit:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get ptrto filename after drive and path info                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Inputs:                                                      ;
        ;   ss:di  pointer to null terminated path                      ;
        ;                                                               ;
        ;  Outputs:                                                     ;
        ;   di     returns pointer within name                          ;
        ;   cy     set if no filename found                             ;
        ;                                                               ;
        ;...............................................................;

_PtrTo_Filename:

        xor ax, ax
        mov cx, -1
        repnz scasb                                     ; scan for null terminator
        not cx                                          ; actual size in cx

_PtrTo_Filename_08:
        cmp byte ptr ss:[ di - 1 ], '\'                 ; previous char a '\' ?
        jz _PtrTo_Filename_12                           ; if found -->

        dec di                                          ; scan prev
        loop _PtrTo_Filename_08                         ; continue until start -->

        stc

_PtrTo_Filename_12:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Test For Drive: Input                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Inputs:                                                      ;
        ;   di     input text string                                    ;
        ;                                                               ;
        ;  Outputs:                                                     ;
        ;   zr     input text string contains only drive letter (x:)    ;
        ;                                                               ;
        ;...............................................................;

isitJustDriveLetter:

        cmp byte ptr [ di ], 00                         ; null string ?
        jz isitJustDriveLetter_20                       ; yes, say yes to just drive -->

        cmp byte ptr [ di + 1 ], ':'                    ; only drive /colon entered ?
        jnz isitJustDriveLetter_20                      ; no -->
        cmp byte ptr [ di + 2 ], 0                      ; must be followed by null

isitJustDriveLetter_20:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Allocate Copy Buffer                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Inputs:                                                      ;
        ;   ss:bx  address of default buffer                            ;
        ;                                                               ;
        ;  Outputs:                                                     ;
        ;   es:bx  actual copy buffer                                   ;
        ;                                                               ;
        ;...............................................................;

allocateCopyBuffer:

        push ax
        push di
        push bx

        mov bx, ( FASTREAD_BUFFERSIZE ) / 16
        Int21 AllocateMemory                            ; 60 k
        jnc allocateCopyBuffer_12                       ; segment in ax

        push ss
        pop es                                          ; segment to ss:
        pop bx                                          ; use default address
        mov cx, FASTREAD_BUFFERSIZE                     ; use default size
        jmp short allocateCopyBuffer_20                 ; return -->

allocateCopyBuffer_12:
        pop bx                                          ; ignore default buffer
        xor bx, bx                                      ; new buffer at es: 0000
        mov es, ax                                      ; segment to es
        mov cx, FASTREAD_BUFFERSIZE

allocateCopyBuffer_20:
        pop di
        pop ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Free Copy Buffer                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Inputs:                                                      ;
        ;   es     copy buffer address                                  ;
        ;                                                               ;
        ;...............................................................;

freeCopyBuffer:

        push ax
        push cx
        mov ax, es
        mov cx, ss

        or ax, ax
        jz freeCopyBuffer_18                            ; if not init -->
        cmp ax, cx
        jz freeCopyBuffer_18                            ; if allocated on stack -->
        Int21 FreeAllocatedMemory                       ; free mem block

freeCopyBuffer_18:
        pop cx
        pop ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Insure OK to overwrite file                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Inputs:                                                      ;
        ;   ax     previous overide                                     ;
        ;   dx     filename pointer                                     ;
        ;   di     find data structure                                  ;
        ;                                                               ;
        ;  Outputs:                                                     ;
        ;   dx     filename pointer                                     ;
        ;   ax     action code                                          ;
        ;          'A'  overide all                                     ;
        ;          0000 no                                              ;
        ;          0001 yes                                             ;
        ;                                                               ;
        ;...............................................................;

_copyOverideCheck:

        cmp al, 'A'                                     ; all ?
        jnz _copyOverideCheck_06                        ; no -->
        ret

_copyOverideCheck_06:
        push dx
        ExOpenFile dx, OPEN_ACCESS_READONLY             ; open read only
        ifc _copyOverideCheck_OK                        ; doesn't, so exit with OK to overide

        mov bx, ax
        Int21 CloseFile

      ; display message, get overide ..  



_copyOverideCheck_OK:
        mov ax, TRUE

_copyOverideCheck_Exit:
        pop dx
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Init Cluster Value                                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   bx     file handle                                          ;
        ;   ss:di  cluster record                                       ;
        ;...............................................................;

_initClusterRecord:

        mov word ptr ss:[ handle ][ di ], bx
        mov word ptr ss:[ clusterDrive ][ di ], 0000
        mov word ptr ss:[ clusterValue. _low  ][ di ], 0000
        mov word ptr ss:[ clusterValue. _high ][ di ], 0000
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Starting Cluster Value for a given Handle                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   bx     file handle                                          ;
        ;   ss:di  cluster record                                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster value                                        ;
        ;...............................................................;

GetClusterValue:

        Entry
        def _handle, bx
        def _pClusterRecord, di

        push es
        push di
        push bx                                         ; file handle
        call _initClusterRecord                         ; init to zeros
        Int21 GetPSPAddress                             ; returns bx with segment
        mov es, bx                                      ; set PSP address

        les bx, dword ptr es:[ pspFileHandlePtr ]       ; point to file handles
        add bx, word ptr [ _handle ][ bp ]              ; offset to handle
        mov bl, byte ptr es:[ bx ]                      ; get real sft offset
        xor bh, bh
        push bx                                         ; save real SFT handle
        Int21 GetDosDataTablePtr                        ; UNDOCUMENTED DOS CALL

        pop ax                                          ; restore real SFT handle
        les si, dword ptr es:[ bx + 4 ]                 ; get address of FT start 

GetClusterValue_08:
        sub ax, word ptr es:[ numberSFTEntries ][ si ]
        jc GetClusterValue_14                           ; if in current FT -->
        cmp word ptr es:[ nextFTPointer. _pointer ][ si ], -1
        jz GetClusterValue_22                           ; if error -->

        les si, dword ptr es:[ nextFTPointer ][ si ]    ; get address of FT start 
        jmp GetClusterValue_08                          ; got to next FT -->
        
GetClusterValue_14:
        add ax, word ptr es:[ numberSFTEntries ][ si ]  ; restore offset
        mov cx, sizeSFT
        mul cx                                          ; offset times size of entries
                
        add si, sizeFT                                  ; move past header
        add si, ax                                      ; position at sft

        test word ptr es:[ sftDevInfo    ][ si ], sftIsDevice
        jz GetClusterValue_18                           ; if not device, go get values -->
        xor ax, ax                                      ; set zero flag if device
        jmp short GetClusterValue_26                    ; exit -->

GetClusterValue_18:
        mov cx, word ptr es:[ sftBegCluster. _high ][ si ]
        mov dx, word ptr es:[ sftBegCluster. _low ][ si ]
        mov ax, word ptr es:[ sftDevInfo    ][ si ]
        and ax, sftDrivemask

        getarg di, _pClusterRecord
        mov word ptr ss:[ clusterDrive ][ di ], ax
        mov word ptr ss:[ clusterValue. _low  ][ di ], dx
        mov word ptr ss:[ clusterValue. _high ][ di ], cx

        or ax, cx
        or ax, dx                                       ; test for all zeroes
        jmp short GetClusterValue_26                    ; exit -->

GetClusterValue_22:
        stc

GetClusterValue_26:
        pop bx
        pop di
        pop es
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compare Drive/Cluster Info                                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:si  cluster record                                       ;
        ;   ss:di  cluster record                                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   nz     not equal                                            ;
        ;...............................................................;

_compareClusters:

        mov ax, word ptr ss:[ clusterValue. _low  ][ si ]
        or ax, word ptr ss:[ clusterValue. _high  ][ si ]
        jz _compareClusters_NotEqual
        mov ax, word ptr ss:[ clusterValue. _low  ][ di ]
        or ax, word ptr ss:[ clusterValue. _high  ][ di ]
        jz _compareClusters_NotEqual

        mov ax, word ptr ss:[ clusterValue. _high ][ si ]
        cmp ax, word ptr ss:[ clusterValue. _high ][ di ]  ; (cluster: high )
        jnz _compareClusters_NotEqual

        mov ax, word ptr ss:[ clusterValue. _low  ][ si ]
        cmp ax, word ptr ss:[ clusterValue. _low  ][ di ]  ; (cluster: low )
        jnz _compareClusters_NotEqual

        mov ax, word ptr ss:[ clusterDrive ][ si ]
        cmp ax, word ptr ss:[ clusterDrive ][ di ]
        jnz _compareClusters_NotEqual
        ret

_compareClusters_NotEqual:
        or si, si                                       ; say not equal
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Copy Argument                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   si     points to argument start                             ;
        ;   di     points to destination                                ;
        ;   cx     length                                               ;
        ;...............................................................;

_copyArgument:

        push di
        push cx
        rep movsb                                       ; copy
        
        xor ax, ax
        stosb                                           ; place null terminator
        pop cx
        pop di
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Replace with Real Name                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   bx     finddata                                             ;
        ;   di     full expanded name expected                          ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   dx     full expanded name expected                          ;
        ;...............................................................;

_replaceWithRealName:

        push di
        xor ax, ax
        mov cx, -1
        repnz scasb                                     ; length of string to scan
        not cx

_replaceWithRealName_08:
        mov al, byte ptr es:[ di - 1 ]
        cmp al, '\'                                     ; scan back until : or \
        jz _replaceWithRealName_12                      ; if found -->
        cmp al, ':'                                     ; scan back until : or \
        jz _replaceWithRealName_12                      ; if found -->
        dec di
        loop _replaceWithRealName_08

_replaceWithRealName_12:
        push di
        mov di, bx
        call LFNReturnPreferedFilenamePtr
        mov si, dx

        pop di
        call CopyString

        pop di
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  See If Directory Name                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   dx     filename                                             ;
        ;   di     find data                                            ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   dx     full expanded name expected                          ;
        ;...............................................................;

seeIfDirectoryName:

        Entry
        def _finddata, di

        fileAttrib ATTR_DIRECTORY
        mov si, FINDDATA_MSDOSDATEFORMAT
        Int21 LFNFindFirstFile                          ; locate file with name
        jc seeIfDirectoryName_24                        ; if not a directory -->

        mov bx, ax                                      ; search handle

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  handle . or .. special cases
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

seeIfDirectoryName_16:
        getarg di, _finddata
        test byte ptr [ findDataAttributes ][ di ], ATTR_DIRECTORY
        jz seeIfDirectoryName_20                        ; if not a dir entry -->

        cmp byte ptr [ findDataLongFilename ][ di ], '.'
        jnz seeIfDirectoryName_20                       ; if dir and not . or .. -->

        mov si, FINDDATA_MSDOSDATEFORMAT
        lea di, offset [ _finddata ][ bp ]
        Int21 LFNFindNextFile                           ; locate next file
        jnc seeIfDirectoryName_16                       ; see if also a dir -->

seeIfDirectoryName_20:
        pushf

        Int21 LFNFindClose                              ; close handle
        popf

seeIfDirectoryName_24:
        Return

RxDOSCMD                        ENDS
                                END
