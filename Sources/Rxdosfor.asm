        TITLE   'For - RxDOS Command Shell For Function'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell For                                      ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Programmer's Notes:                                          ;
        ;                                                               ;
        ;  Command Shell consists of  two parts bound  together into a  ;
        ;  single executable load.  There  exists  a  single  resident  ;
        ;  command shell which is accessible by an Int 2Eh.             ;
        ;                                                               ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc
        include rxdoscin.inc

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;...............................................................;

RxDOSCMD SEGMENT PUBLIC 'CODE'
         assume cs:RxDOSCMD, ds:RxDOSCMD, es:RxDOSCMD, ss:RxDOSCMD

        public _For

        extrn CheckOptOneArg                            : near
        extrn CountArgs                                 : near
        extrn CmndLookup                                : near
        extrn CmndError_SyntaxError                     : near
        extrn CRLF                                      : near
        extrn DisplayErrorMessage                       : near
        extrn DisplayLine                               : near
        extrn DisplayPrompt                             : near
        extrn CopyString                                : near
        extrn ReplaceForVariables                       : near
        extrn _CommandParser                            : near
        extrn CheckFileClose                            : near

        extrn RxDOS_ForArgs                             : near
        extrn _lowerCaseString                          : near

        extrn _computeLength                            : near
        extrn _Seeif_WildCharacter                      : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  FOR %variable IN (set) DO command [command-parameters]       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Replace %var in command.                                     ;
        ;                                                               ;
        ;...............................................................;

_For:

        Entry
        def __argarray, di
        def __beginrepeatarg
        def __endrepeatarg
        def __cmdline

        def _letter
        def _length
        def _WildChars
        def _filehandle, -1

        defbytes _commandLine, 128
        defbytes _replacement, 128
        defbytes _finddata, sizeFINDDATA

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get variable arg letter 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        call CountArgs                                  ; must have min of 8 args
        cmp ax, 8                                       ; min args 
        jge _For_08                                     ; if at leats 8 -->
        jmp _ForError                                   ; else if less, syntax error -->

_For_08:
        mov si, word ptr [ forVarIdent ][ di ]          ; point to arg
        cmp byte ptr [ si ], '%'                        ; is arg a % variable ?
        ifnz _ForError                                  ; if not, then syntax error -->

        mov al, byte ptr [ si + 1 ]                     ; get letter
        _lowerCase al
        cbw
        storarg _letter, ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  check for 'in' argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov si, word ptr [ forInArg. argpointer ][ di ] ; point to next arg
        mov cx, word ptr [ forInArg. arglength ][ di ]  ; get arg length
        mov di, offset RxDOS_ForArgs
        call CmndLookup                                 ; lookup command
        ifc _ForError                                   ; if not, then syntax error -->

        cmp bx, _IN                                     ; 'in' returns a zero
        ifnz _ForError                                  ; if not 'in', then syntax error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  check for ( argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, __argarray
        add di, forInStartParen + sizeArgCmd
        mov si, word ptr [ argpointer - sizeArgCmd ][ di ]
        cmp byte ptr [ si ], '('                        ; is arg a ( variable ?
        ifnz _ForError                                  ; if not, then syntax error -->

        storarg __beginrepeatarg, di                    ; di points to an argument

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  search for ) do ...
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ForSearchForParen:
        storarg __endrepeatArg, di                      ; end repeat arg

        add di, sizeArgCmd
        mov si, word ptr [ argpointer - sizeArgCmd ][ di ]
        or si, si                                       ; end of args ?
        ifz _ForError                                   ; if not, then syntax error -->

        cmp byte ptr [ si ], ')'                        ; is arg a ) variable ?
        jnz _ForSearchForParen                          ; keep looking -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  search for last ) in case ))) ...
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ForSearchForDoLastParen:
        add di, sizeArgCmd
        mov si, word ptr [ argpointer - sizeArgCmd ][ di ]
        or si, si                                       ; end of args ?
        ifz _ForError                                   ; if not, then syntax error -->

        cmp byte ptr [ si ], ')'                        ; is arg a ) variable ?
        jz _ForSearchForDoLastParen                     ; keep looking -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  test for do argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ForSearchForDo:
        push di
        mov si, word ptr [ argpointer - sizeArgCmd ][ di ]
        mov di, offset RxDOS_ForArgs
        call CmndLookup                                 ; lookup command
        pop di                                          ; restore arg pointer
        ifc _ForError                                   ; if not, then syntax error -->

        cmp bx, _DO                                     ; 'do' returns a 1
        ifnz _ForError                                  ; if not, then syntax error -->

        storarg __cmdline, di                           ; where command line is
        mov si, word ptr [ argpointer ][ di ]
        or si, si                                       ; end of args ?
        ifz _ForError                                   ; if not, then syntax error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  sequentially walk through each arg
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ForNext:
        getarg bx, __beginrepeatarg                     ; point to arg list
        cmp bx, word ptr [ __endrepeatArg ][ bp ]       ; end repeat arg ?
        ifz _ForReturn                                  ; if at end, exit -->

        storarg _WildChars, 0000
        mov si, word ptr [ argpointer ][ bx ]
        mov cx, word ptr [ arglength ][ bx ]
        storarg _length, cx                             ; save length

        call _Seeif_WildCharacter                       ; see if wild characters in name.
        jc _ForNext_30                                  ; if no -->

        lea di, offset _replacement [ bp ]              ; where to copy
        rep movsb                                       ; copy argument
        xor ax, ax                                      ; null terminate
        stosb                                           ; null terminate
        storarg _WildChars, -1                          ; wild characters used.

        fileAttrib ATTR_NORMAL
        mov si, FINDDATA_MSDOSDATEFORMAT
        lea dx, offset _replacement [ bp ]              ; search for argument
        lea di, offset [ _finddata ][ bp ]
        Int21 LFNFindFirstFile                          ; any found ?
        jc _ForNext_36                                  ; if none -->

        storarg _filehandle, ax                         ; search next handle

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  create replacement variable
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ForNext_14:
        lea si, offset [ _finddata. findFileName ][ bp ]
        lea di, offset _replacement [ bp ]              ; search for argument
        call CopyString                                 ; get command line

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  build command line to execute
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ForNext_30:
        getarg di, __cmdline                            ; ptr->arg array where command line begins
        mov si, word ptr [ argpointer ][ di ]           ; ptr-> text
        lea di, offset [ _commandLine + 1 ][ bp ]
        call CopyString                                 ; get command line

        mov al, byte ptr [ _letter ][ bp ]
        lea bx, offset [ _replacement ][ bp ]
        lea si, offset [ _commandLine + 1 ][ bp ]
        call ReplaceForVariables                        ; replace variable letter

        call DisplayPrompt                              ; display prompt

        lea dx, offset [ _commandLine + 1 ][ bp ]
        call DisplayLine                                ; echo line
        call CRLF

        lea si, offset [ _commandLine ][ bp ]
        mov byte ptr [ si ], 128
        call _CommandParser                             ; reparse remainder of line

        cmp word ptr [ _WildChars ][ bp ], 0000         ; wild character search ?
        jz _ForNext_36                                  ; no, skip to next arg in set -->

        getarg bx, _filehandle
        lea di, offset [ _finddata ][ bp ]
        mov si, FINDDATA_MSDOSDATEFORMAT
        Int21 LFNFindNextFile                           ; search for next matching file
        jnc _ForNext_14                                 ; continue -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get next argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ForNext_36:
        getarg bx, _filehandle                          ; search next handle
        call CheckFileClose

        getarg di, __beginrepeatarg                     ; get current pointer to arg array
        mov dx, word ptr [ argpointer ][ di ]           ; get current reference
        add dx, word ptr [ _length ][ bp ]              ; offset to past current arg
        sub di, sizeArgCmd

_ForNext_38:
        add di, sizeArgCmd
        mov si, word ptr [ argpointer ][ di ]           ; get current reference
        cmp si, dx                                      ; beyond or equal to current ?
        jc _ForNext_38                                  ; not yet -->

        storarg __beginrepeatarg, di                    ; remember reference
        jmp _ForNext

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  error
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ForError:
        mov dx, offset CmndError_SyntaxError
        call DisplayErrorMessage

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ForReturn:
        Return

RxDOSCMD                        ENDS
                                END
