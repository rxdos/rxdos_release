        TITLE   'exe - load and execute .com and .exe programs'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Load EXE/COM Programs                                        ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc

RxDOS   SEGMENT PUBLIC 'CODE'
        assume cs:RxDOS, ds:RxDOS, es:RxDOS, ss:RxDOS

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Load EXE/COM Programs                                        ;
        ;...............................................................;

        public copyCurrentPSP
        public loadProgram

        extrn LocateFile                        : near
        extrn ExpandFileName                    : near
        extrn initdiskAccess                    : near
        extrn readLogicalBuffer                 : near
        extrn allocateMemBlock                  : near
        extrn _allocateMinMaxMemBlock           : near

        extrn stdDeviceAssignTable              : near
        extrn _RetCallersStackFrame             : near
        extrn _freeMemBlock                     : near
        extrn MapAppToSFTHandle                 : near
        extrn FindSFTbyHandle                   : near

        extrn _RxDOS_AllocStrategy              : word
        extrn _RxDOS_CurrentPSP                 : word
        extrn _RxDOS_CurrentInstance            : word
        extrn _RxDOS_MaxMemory                  : word
        extrn _RxDOS_DOSVersion                 : word
        extrn _RxDOS_pDTA                       : dword
        extrn _CallDOS                          : far
        extrn RxDOS_CMDLINE                     : byte

        extrn pexterrNotEnoughMemory            : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Load Program Envelope                                        ;
        ;...............................................................;

        LOADENVELOPE struc 

loadEnvOperationMode       dw ?
loadEnvStartSegment        dw ?
loadEnvRelocFactor         dw ?
loadEnvExecBlock           dd ?                         ; exec block

loadEnvExtraEnvSizeParas   dw ?                         ; addtl env size required (paras)
loadEnvProgramPSP          dw ?
loadEnvEnvBlock            dw ?

        LOADENVELOPE ends

sizeloadEnvelope           equ size LOADENVELOPE

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Load Program Image (COM, SYS, etc... )                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ds:dx  asciz name of program to load                        ;
        ;   es:bx  address of LOADEXEC structure                        ;
        ;   al     mode (subfunction)                                   ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   dx     PSP segment address                                  ;
        ;   cy     load failed                                          ;
        ;...............................................................;

loadProgram:

        Entry
        def  _Mode, ax
        def  _ptrDirAccess, di
        ddef _ExecBlock, es, bx
        def  _SizePara

        ddef _filesize
        ddef _PrevInstance
        ddef _DebuggerReturnTo

        defbytes _relocitem,  4
        defbytes _exeHeader,  sizeEXEHEADER
        defbytes _diskAccess, sizeDISKACCESS
        defbytes _loadEnvelope, sizeloadEnvelope

        setES ss
        lea di, offset [ _loadEnvelope ][ bp ]
        clearMemory sizeloadEnvelope

        getdarg es, bx, _ExecBlock
        mov word ptr [ _loadEnvelope. loadEnvExecBlock. _segment ][ bp ], es
        mov word ptr [ _loadEnvelope. loadEnvExecBlock. _pointer ][ bp ], bx
        mov word ptr [ _loadEnvelope. loadEnvOperationMode ][ bp ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Debugger Info
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setDS ss
        mov bx, word ptr ss:[ _RxDOS_CurrentInstance ]
        les bx, dword ptr ss:[ bx ]
        stordarg _PrevInstance, es, bx                  ; instance before 

        mov dx, word ptr es:[ _CS ][ bx ]
        mov ax, word ptr es:[ _IP ][ bx ]
        stordarg _DebuggerReturnTo, dx, ax              ; return address

        getarg ax, _Mode                                ; get mode
        Goto   execSetExecuteMode, _loadProgram_SetExec
        Goto   execLoadOverlay,    _loadProgram_Overlay

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Execute Program
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getdarg es, bx, _ExecBlock
        mov ax, word ptr es:[ lprogCommandTail. _pointer ][ bx ]
        or ax, word ptr es:[ lprogCommandTail. _segment ][ bx ]
        jz _loadProgram_08

        mov si, word ptr es:[ lprogCommandTail. _pointer ][ bx ]
        mov es, word ptr es:[ lprogCommandTail. _segment ][ bx ]
        mov cx, si

_loadProgram_04:
        inc si
        cmp byte ptr es:[ si - 1 ], 00
        jnz _loadProgram_04

        sub cx, si
        neg cx                                          ; length command line
        add cx, 8                                       ; cmdline= overhead
        mov word ptr [ _loadEnvelope. loadEnvExtraEnvSizeParas ][ bp ], cx

_loadProgram_08:
        getarg si, _ptrDirAccess
        lea si, offset [ fileAcExpandedName ][ si ]     ; program to load 
        mov cx, si

_loadProgram_12:
        inc si
        cmp byte ptr [ si - 1 ], 00
        jnz _loadProgram_12

        sub cx, si
        neg cx                                          ; length command line
        add cx, 4                                       ; overhead bytes
        add cx, (PARAGRAPH - 1)                         ; round up
        add cx, word ptr [ _loadEnvelope. loadEnvExtraEnvSizeParas ][ bp ]

        shr cx, 1
        shr cx, 1
        shr cx, 1
        shr cx, 1                                       ; express bytes as paragraphs
        mov word ptr [ _loadEnvelope. loadEnvExtraEnvSizeParas ][ bp ], cx

        jmp short _loadProgram_22

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  initialize mode
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_SetExec:
        call _SetExecuteMode
        jmp _loadProgram_80

_loadProgram_Overlay:
        getdarg es, bx, _ExecBlock
        mov ax, word ptr es:[ loverSegLoadAddress   ][ bx ]
        mov dx, word ptr es:[ loverRelocFactor      ][ bx ]
        mov word ptr [ _loadEnvelope. loadEnvStartSegment ][ bp ], ax
        mov word ptr [ _loadEnvelope. loadEnvRelocFactor ][ bp ], dx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  initialize disk access block
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_22:
        setES ss
        getarg bx, _ptrDirAccess                        ; build access control block
        mov ax, word ptr [ fileAcDrive ][ bx ]          ; drive 
        mov cx, word ptr [ fileAcCluster. _high ][ bx ] ; cluster
        mov dx, word ptr [ fileAcCluster. _low  ][ bx ] ; cluster

        lea bx, offset [ _diskAccess ][ bp ]            ; build access control block
        call initdiskAccess                             ; [ax] is drive, [cx:dx] is cluster
        call checkEXEHeader                             ; exe file ?
        jnz _loadProgram_30                             ; no, COM or SYS type -->

        getarg ax, _Mode                                ; get mode
        getarg di, _ptrDirAccess                        ; dir information
        lea bx, offset [ _loadEnvelope ][ bp ]          ; load block
        call loadExe_Program                            ; pass off control to load EXE
        ifc _loadProgram_80                             ; if cannot -->
        jmp _loadProgram_54                             ; switch stack -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  load COM or SYS 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_30:
        getarg bx, _ptrDirAccess                        ; dir information
        les di, dword ptr [ fileAcBufferPtr ][ bx ]
        add di, word ptr [ fileAcDirOffset ][ bx ]
        mov ax, word ptr es:[ deFileSize. _low  ][ di ]
        mov dx, word ptr es:[ deFileSize. _high ][ di ]
        stordarg _filesize, dx, ax                      ; save file size

        add ax, (PARAGRAPH - 1)
        adc dx, 0                                       ; just in case carry

        shr dx, 1
        rcr ax, 1
        shr dx, 1
        rcr ax, 1
        shr dx, 1
        rcr ax, 1
        shr dx, 1
        rcr ax, 1
        storarg _SizePara, ax                           ; save as paragraphs needed

        cmp byte ptr [ _Mode  ][ bp ], execLoadOverlay  ; if load overlay 
        jz _loadProgram_34                              ; no need to allocate memeory -->

        mov dx, 0FFFFh                                  ; max request (all of memory)
        lea bx, offset [ _loadEnvelope ][ bp ]
        call _LoaderAllocMemory                         ; alloc memory for env block/ PSP
        ifc _loadProgram_80                             ; if cannot allocate -->

        mov word ptr [ _loadEnvelope. loadEnvProgramPSP ][ bp ], ax
        mov word ptr [ _loadEnvelope. loadEnvStartSegment ][ bp ], es

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  load module (at es: 0000)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_34:
        xor di, di                                      ; load at es:0000
        mov es, word ptr [ _loadEnvelope. loadEnvStartSegment ][ bp ]
        lea bx, offset _diskAccess [ bp ]               ; build access control block
        mov word ptr ss:[ diskAcPosition. _high ][ bx ], di
        mov word ptr ss:[ diskAcPosition. _low  ][ bx ], di

_loadProgram_36:
        mov cx, 60 * 1024                               ; read 60k at a time
        mov ax, word ptr [ _filesize. _low  ][ bp ]
        mov dx, word ptr [ _filesize. _high ][ bp ]
        sub ax, word ptr ss:[ diskAcPosition. _low  ][ bx ]
        sbb dx, word ptr ss:[ diskAcPosition. _high ][ bx ]  
        or dx, dx                                       ; more than 60k ?
        jnz _loadProgram_38                             ; yes -->
        cmp ax, cx                                      ; more than 60k ?
        jnc _loadProgram_38                             ; yes -->
        mov cx, ax                                      ; exact bytes to read

_loadProgram_38:
        or cx, cx                                       ; more to read ?
        jz _loadProgram_44                              ; if no more to read -->

        mov dx, word ptr ss:[ diskAcPosition. _high ][ bx ]  
        mov ax, word ptr ss:[ diskAcPosition. _low  ][ bx ]
        lea bx, offset _diskAccess [ bp ]               ; build access control block
        call readLogicalBuffer                          ; Access buffer: ss: bx
        or cx, cx                                       ; bytes actually read
        jz _loadProgram_44                              ; if no more to read -->

        push cx
        shr cx, 1
        shr cx, 1
        shr cx, 1
        shr cx, 1                                       ; chars read in paras
        mov ax, es
        add ax, cx
        mov es, ax
        pop cx
        and cx, (PARAGRAPH - 1)
        add di, cx
        jmp _loadProgram_36

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  switch to new process
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_44:
        mov al, byte ptr [ _Mode  ][ bp ]               ; if load overlay 
        getdarg cx, dx, _filesize
        call ifOverlayLoader                            ; set up ret args if overlay
        ifz _loadProgram_62                             ; no need to build return stack -->

        mov es, word ptr [ _loadEnvelope. loadEnvProgramPSP ][ bp ]
        mov word ptr [ _RxDOS_CurrentPSP ], es          ; change running PSP

        call _getSizeOfEnvString                        ; get size of env string

        xor si, si
     ;  cmp cx, (1024 /PARAGRAPH) * 64                  ; greater than 64k ?
     ;  jnc _loadProgram_46                             ; yes, use max value for seg
     ;  mov si, cx
     ;  shl si, 1
     ;  shl si, 1
     ;  shl si, 1                                       ; actual words
        
_loadProgram_46:
        mov word ptr es:[ si - 2 ], 0000                ; place a null return address at top of stack

        sub si, 4                                       ; top value minus 2 words
        mov word ptr es:[ pspUserStack. _pointer ], si
        mov word ptr es:[ pspUserStack. _segment ], es  ; child process psp

        sub si, _Flags
        mov bx, word ptr ss:[ _RxDOS_CurrentInstance ]
        mov word ptr ss:[ _pointer ][ bx ], si
        mov word ptr ss:[ _segment ][ bx ], es          ; where new process' startup stack will exist (COM)

        or si, si
        pushf
        pop word ptr es:[ _Flags        ][ si ]         ; Nz Nc ...

        mov word ptr es:[ _IP           ][ si ], 100h
        mov word ptr es:[ _CS           ][ si ], es
        mov word ptr es:[ _ExtraSegment ][ si ], es
        mov word ptr es:[ _DataSegment  ][ si ], es
        mov word ptr es:[ _BP           ][ si ], 0000
        mov word ptr es:[ _DI           ][ si ], 0000
        mov word ptr es:[ _SI           ][ si ], 0000
        mov word ptr es:[ _DX           ][ si ], 0000

        getdarg bx, cx, _filesize
        mov word ptr es:[ _CX           ][ si ], cx
        mov word ptr es:[ _BX           ][ si ], bx
        mov word ptr es:[ _AX           ][ si ], 0000

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  set DTA on the way out; switch stacks.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_54:
        cmp byte ptr [ _Mode  ][ bp ], execLoadOverlay  ; if load overlay 
        jz _loadProgram_62                              ; no need to allocate memeory -->

        mov es, word ptr [ _RxDOS_CurrentPSP ]          ; new PSP
        mov word ptr [ _RxDOS_pDTA. _segment ], es
        mov word ptr [ _RxDOS_pDTA. _pointer ], pspCommandTail

        cli
        mov bx, word ptr ss:[ _RxDOS_CurrentInstance ]
        mov ax, word ptr es:[ pspParentId    ]          ; parent PSP address
        sti                                             ; re-enable interrupts
        or ax, ax                                       ; if no current PSP
        jz _loadProgram_56                              ; none -->

        push word ptr ss:[ _segment ][ bx ]
        push word ptr ss:[ _pointer ][ bx ]
        pop word ptr es:[ pspUserStack. _pointer ]
        pop word ptr es:[ pspUserStack. _segment ]      ; child process

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy program name to Environment Block
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_56:
        getarg bx, _ptrDirAccess                        ; build access control block
        mov es, word ptr [ _RxDOS_CurrentPSP ]          ; new PSP
        mov dx, word ptr [ fileAcNameOffset ][ bx ]     ; offset to short name
        call _setProgramNameInMemBlock

        mov ax, word ptr es:[ pspEnvironment ]          ; get environment block
        or ax, ax                                       ; if no environment block
        jz _loadProgram_62                              ; -->

        push ds
        mov es, ax                                      ; get environment block
        call _setProgramNameInMemBlock

        lea bx, offset [ _loadEnvelope ][ bp ]          ; load envelope
        call AppendCmdLineToEnv

        setDS ss
        getarg bx, _ptrDirAccess                        ; build access control block
        lea dx, offset [ fileAcExpandedName ][ bx ]     ; expanded name
        call appendProgramNametoEnv
        pop ds             

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if DEBUGGER support request
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_62:
        cmp byte ptr [ _Mode ][ bp ], execLoadAndReturnDebug
        clc
        jnz _loadProgram_80                             ; if not debugger mode -->

        SaveSegments
        mov es, word ptr [ _RxDOS_CurrentPSP ]          ; new PSP
        mov dx, word ptr es:[ pspUserStack. _segment ]
        mov ax, word ptr es:[ pspUserStack. _pointer ]

      ; get CS: IP

        mov es, word ptr [ _RxDOS_CurrentPSP ]          ; new PSP
        mov bx, word ptr es:[ pspUserStack. _pointer ]
        mov es, word ptr es:[ pspUserStack. _segment ]  ; es:bx = current stack
        mov dx, word ptr es:[ _CS           ][ bx ]     ; get new prog cs:ip
        mov ax, word ptr es:[ _IP           ][ bx ]

        getdarg ds, si, _ExecBlock
        mov word ptr [ lprogCSIP. _segment ][ si ], dx
        mov word ptr [ lprogCSIP. _pointer ][ si ], ax  ; save in exec block

      ; save SS: SP in exec block

        mov word ptr [ lprogSSSP. _segment ][ si ], es
        mov word ptr [ lprogSSSP. _pointer ][ si ], bx  ; stack to exec block

      ; return to CS:IP of previous instance with new stack

        getdarg dx, ax, _DebuggerReturnTo
        mov word ptr es:[ _CS      ][ bx ], dx
        mov word ptr es:[ _IP      ][ bx ], ax

        mov bx, word ptr ss:[ _RxDOS_CurrentInstance ]
        getdarg dx, ax, _PrevInstance
        mov word ptr ss:[ _segment ][ bx ], dx
        mov word ptr ss:[ _pointer ][ bx ], ax

        RestoreSegments
        clc

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  exit.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_80:
        jnc _loadProgram_86

        pushf
        SaveAllRegisters
        mov es, word ptr [ _loadEnvelope. loadEnvProgramPSP ][ bp ]
        call _freeMemBlock

        mov es, word ptr [ _loadEnvelope. loadEnvEnvBlock ][ bp ]
        call _freeMemBlock
        RestoreAllRegisters
        popf

_loadProgram_86:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Load Exe Program                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:di  pointer to directory block                           ;
        ;   es:bx  address of EXEC structure                            ;
        ;   dx     load segment address if mode (al) is load Overlay    ;
        ;   cx     code relocation factor                               ;
        ;   ax     subfunction code                                     ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   dx     PSP segment address                                  ;
        ;   cy     load failed                                          ;
        ;...............................................................;

loadExe_Program:

        Entry
        def  _Mode, ax
        def  _ptrDirAccess, di
        def  _pLoadEnvelope, bx

        def  _SizePara
        def  _CheckSum, 0000
        ddef _returnSize
        defbytes _exeHeader, sizeEXEHEADER
        defbytes _diskAccess, sizeDISKACCESS
        defbytes _relocitem, 4

        setDS ss
        mov word ptr [ loadEnvProgramPSP ][ bx ], 0000  ; PSP

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  initialize disk access block
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ fileAcDrive    ][ di ]
        mov dx, word ptr [ fileAcCluster. _low  ][ di ]
        mov cx, word ptr [ fileAcCluster. _high ][ di ]

        lea bx, offset _diskAccess [ bp ]               ; build access control block
        call initdiskAccess                             ; [ax] is drive, [dx] is cluster

        les si, dword ptr [ fileAcBufferPtr ][ di ]
        add si, word ptr [ fileAcDirOffset ][ di ]
        mov ax, word ptr es:[ deFileSize. _low  ][ si ]
        mov dx, word ptr es:[ deFileSize. _high ][ si ]
        mov word ptr [ _diskAccess. diskAcFileSize. _low  ][ bp ], ax
        mov word ptr [ _diskAccess. diskAcFileSize. _high ][ bp ], dx

        sub ax,  200h
        sbb dx, 0000h                                   ; return size
        mov word ptr [ _returnSize. _low  ][ bp ], ax
        mov word ptr [ _returnSize. _high ][ bp ], dx

        xor dx, dx
        xor ax, ax                                      ; position to beg of file
        setES ss
        lea di, offset [ _exeHeader ][ bp ]
        mov cx, sizeEXEHEADER
        call readLogicalBuffer                          ; Access buffer: ss: bx
        call computeChecksum
        mov word ptr [ _CheckSum ][ bp ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compute size in pages of load module
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov dx, word ptr [ _exeHeader. exeExtraBytes ][ bp ]
        add dx, (PARAGRAPH - 1)                         ; round up parags needed
        shr dx, 1
        shr dx, 1
        shr dx, 1
        shr dx, 1                                       ; paragraphs at end needed

        mov ax, word ptr [ _exeHeader. exePages ][ bp ]
     ;  dec ax                                          ; adjust for last block
        shl ax, 1                                       ; conv pages to paragraphs
        shl ax, 1                                       ;  4
        shl ax, 1                                       ;  8
        shl ax, 1                                       ; 16
        shl ax, 1                                       ; 32
        sub ax, word ptr [ _exeHeader. exeHeaderSize ][ bp ]
        add ax, dx                                      ; actual paragr needed
        add ax, word ptr [ _exeHeader. exeMinAlloc ][ bp ]
        storarg _SizePara, ax                           ; save as paragraphs needed

        getarg bx, _pLoadEnvelope
        cmp byte ptr [ _Mode  ][ bp ], execLoadOverlay  ; if load overlay 
        jz loadExe_Program06                            ; no need to build return stack -->

        mov dx, word ptr [ _exeHeader. exeMaxAlloc ][ bp ]
        call _LoaderAllocMemory                         ; alloc memory for env block/ PSP
        ifc loadExe_Program50                           ; if cannot allocate -->

        getarg bx, _pLoadEnvelope
        mov word ptr [ loadEnvStartSegment ][ bx ], es
        mov word ptr [ loadEnvRelocFactor ][ bx ], es
        mov word ptr [ loadEnvProgramPSP ][ bx ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  load module
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

loadExe_Program06:
        mov es, word ptr [ loadEnvStartSegment ][ bx ]
        mov ax, word ptr [ _exeHeader. exeHeaderSize ][ bp ]
        shl ax, 1                                       ;  2
        shl ax, 1                                       ;  4
        shl ax, 1                                       ;  8
        shl ax, 1                                       ; 16 byte offset
        xor dx, dx

loadExe_Program08:
        push es
        push dx
        push ax                                         ; save position in file

        mov cx, word ptr [ _SizePara ][ bp ]            ; get paras to read
        test cx, 0F000h                                 ; over 65k ?
        jz loadExe_Program12                            ; no, read whole file -->
        mov cx, 0FFFh                                   ; read as much as possible.

loadExe_Program12:
        push cx                                         ; save paras to read

        shl cx, 1
        shl cx, 1
        shl cx, 1
        shl cx, 1
        push cx                                         ; save bytes actually to read

        xor di, di                                      ; load at _StartSeg:0000
        lea bx, offset _diskAccess [ bp ]               ; build access control block
        call readLogicalBuffer                          ; Access buffer: ss: bx
        call computeChecksum
        add word ptr [ _CheckSum ][ bp ], ax

        pop cx                                          ; bytes read
        pop bx                                          ; paras read
        pop ax                                          ; previous file offset
        pop dx
        add ax, cx                                      ; incr position by bytes read
        adc dx, 0000

        pop cx                                          ; segment loaded
        add cx, bx                                      ; increment by paras read
        mov es, cx                                      ; set new read segment

        sub word ptr [ _SizePara ][ bp ], bx            ; subtract paragraphs used.
        jnz loadExe_Program08

        xor di, di                                      ; load at _StartSeg:0000
        mov cx, word ptr [ _exeHeader. exeExtraBytes ][ bp ]
        and cx, (PARAGRAPH - 1)                         ; last 15 bytes
        jz loadExe_Program20                            ; if none to read -->
        
        lea bx, offset _diskAccess [ bp ]               ; build access control block
        call readLogicalBuffer                          ; Access buffer: ss: bx
        call computeChecksum
        add word ptr [ _CheckSum ][ bp ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  address relocation blocks
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

loadExe_Program20:
        mov ax, word ptr [ _exeHeader. exeRelocItems ][ bp ]
        or ax, ax
        jz loadExe_Program32

loadExe_Program28:
        setES ss
        mov cx, 4
        xor dx, dx
        mov ax, word ptr [ _exeHeader. exeRelocTable ][ bp ]
        lea di, offset _relocitem [ bp ]
        lea bx, offset _diskAccess [ bp ]               ; build access control block
        call readLogicalBuffer                          ; Access buffer: ss: bx
        call computeChecksum
        add word ptr [ _CheckSum ][ bp ], ax

        getarg bx, _pLoadEnvelope
        mov ax, word ptr [ loadEnvRelocFactor ][ bx ]   ; starting segment

        getdarg dx, di, _relocitem                      ; get reloc item
        add dx, ax
        mov es, dx
        add word ptr es:[ di ], ax                      ; relocate load module

        add word ptr [ _exeHeader. exeRelocTable ][ bp ], 4
        dec word ptr [ _exeHeader. exeRelocItems ][ bp ]
        jnz loadExe_Program28

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  execute
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

loadExe_Program32:
        mov ax, word ptr [ _CheckSum ][ bp ]
        add ax, word ptr [ _exeHeader. exeChecksum   ][ bp ]

        mov al, byte ptr [ _Mode  ][ bp ]               ; if load overlay 
        getdarg cx, dx, _returnSize                     ; return size
        call ifOverlayLoader                            ; set up ret args if overlay
        ifz loadExe_Program50                           ; no need to build return stack -->

        getarg bx, _pLoadEnvelope
        mov es, word ptr [ loadEnvProgramPSP      ][ bx ]
        mov dx, word ptr [ loadEnvStartSegment    ][ bx ]
        add dx, word ptr [ _exeHeader. exeInitSS  ][ bp ]
        mov si, word ptr [ _exeHeader. exeInitSP  ][ bp ]

        sub si, 2                                       ; top value minus 1 word
        mov word ptr es:[ pspUserStack. _pointer ], si
        mov word ptr es:[ pspUserStack. _segment ], dx

        setES dx                                        ; child program's stack seg
        sub si, _Flags
        mov bx, word ptr ss:[ _RxDOS_CurrentInstance ]
        mov word ptr ss:[ _pointer ][ bx ], si
        mov word ptr ss:[ _segment ][ bx ], dx          ; where new process' startup stack will exist (COM)

        or si, si
        pushf
        pop word ptr es:[ _Flags        ][ si ] 

        getarg bx, _pLoadEnvelope
        mov dx, word ptr [ loadEnvProgramPSP      ][ bx ]
        mov word ptr es:[ _ExtraSegment ][ si ], dx
        mov word ptr es:[ _DataSegment  ][ si ], dx

        mov dx, word ptr [ loadEnvStartSegment    ][ bx ]
        add dx, word ptr [ _exeHeader. exeInitCS  ][ bp ]
        mov ax, word ptr [ _exeHeader. exeInitIP  ][ bp ]
        mov word ptr es:[ _IP           ][ si ], ax
        mov word ptr es:[ _CS           ][ si ], dx

        mov word ptr es:[ _BP           ][ si ], 0000
        mov word ptr es:[ _DI           ][ si ], 0000
        mov word ptr es:[ _SI           ][ si ], 0000
        mov word ptr es:[ _DX           ][ si ], 0000

        getdarg dx, ax, _returnSize
        mov word ptr es:[ _CX           ][ si ], ax
        mov word ptr es:[ _BX           ][ si ], dx
        mov word ptr es:[ _AX           ][ si ], 0000

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy startup command line to _PSP
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg bx, _pLoadEnvelope
        mov ax, word ptr [ loadEnvProgramPSP ][ bx ]
        mov word ptr [ _RxDOS_CurrentPSP ], ax          ; change running PSP
        clc

loadExe_Program50:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Determine if file is EXE formatted                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:bx  disk access block                                    ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   zr     file is an EXE                                       ;
        ;   nz     file is not an EXE                                   ;
        ;...............................................................;

checkEXEHeader:
        Entry
        defbytes _exeHeader, sizeEXEHEADER

        xor dx, dx
        xor ax, ax                                      ; file pointer to beg of file
        mov cx, sizeEXEHEADER
        lea di, offset _exeHeader [ bp ]
        call readLogicalBuffer                          ; Access buffer: ss: bx

        cmp word ptr [ _exeHeader. exeSignature ][ bp ], EXE_SIGNATURE
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Copy Current PSP                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   es     PSP segment address to update                        ;
        ;...............................................................;

copyCurrentPSP:

        SaveSegments
        mov ax, word ptr ss:[ _RxDOS_CurrentPSP ]       ; current PSP
        or ax, ax                                       ; any current PSP ?
        jz copyCurrentPSP_36                            ; no -->

        xor si, si
        xor di, di
        mov ds, ax                                      ; current PSP
        mov cx, ( sizePSP / 2 )
        rep movsw

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; duplicate all file handles (except for no inherit)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr ss:[ _RxDOS_CurrentPSP ]
        mov word ptr es:[ pspParentId    ], ax          ; parent PSP address

        mov cx, sizePSPHandleTable 
        mov bx, offset PSPHandleTable 
        mov word ptr es:[ pspFileHandleCount ], cx
        mov word ptr es:[ pspFileHandlePtr. _segment ], es
        mov word ptr es:[ pspFileHandlePtr. _pointer ], bx

copyCurrentPSP_08:
        xor ax, ax
        mov al, byte ptr es:[ bx ]                      ; get old handle
        cmp al, -1                                      ; if entry not set
        jz copyCurrentPSP_14                            ; skip around -->

        push es
        push bx
        call FindSFTbyHandle                            ; get corresponding SFT (es: di )
        test word ptr es:[ sftDevInfo ][ di ], sftNoInherit
        jnz copyCurrentPSP_12                           ; if no inherit -->

        inc word ptr es:[ sftRefCount ][ di ]           ; bump in use count

        pop bx
        pop es
        jmp short copyCurrentPSP_14

copyCurrentPSP_12:
        pop bx
        pop es
        mov byte ptr es:[ bx ], -1                      ; free SFT in copy of psp

copyCurrentPSP_14:
        inc bx
        cmp bx, offset (PSPHandleTable + sizePSPHandleTable)
        jl copyCurrentPSP_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

copyCurrentPSP_36:
        RestoreSegments
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Create New Program Seg Prefix                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es     start address of PSP                                 ;
        ;   cx     allocate size                                        ;
        ;   dx:ax  EXEC BLOCK address                                   ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es     updated es:                                          ;
        ;...............................................................;

createPSP:

        Entry
        def  _AllocSize, cx
        ddef _ExecBlock, dx, ax

        push ds
        mov ax, es
        add ax, (sizePSP / PARAGRAPH)                   ; always a fixed size
        push ax                                         ; address of PSP to return

        xor di, di
        mov ax, word ptr ss:[ _RxDOS_CurrentPSP ]
        or ax, ax                                       ; any parent ?
        jnz createPSP_08                                ; yes, copy parent -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init a blank PSP
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor ax, ax
        mov cx, (sizePSP / 2)                           ; always a fixed size
        rep stosw                                       ; clear/ zero.

        mov ax, -1
        mov di, offset pspHandleTable
        mov cx, (sizePSPHandleTable / 2)
        rep stosw                                       ; init handle table

        mov dx, 1
        mov al, byte ptr ss:[ stdRedirec_Aux * sizeStdRedirec ]. stdDeviceAssignTable. stdIOHandle
        mov byte ptr es:[ pspHandleTable.STDAUX ], al
        call incrRefCount

        mov al, byte ptr ss:[ stdRedirec_Prn * sizeStdRedirec ]. stdDeviceAssignTable. stdIOHandle
        mov byte ptr es:[ pspHandleTable.STDPRN ], al
        call incrRefCount

        mov al, byte ptr ss:[ stdRedirec_Con * sizeStdRedirec ]. stdDeviceAssignTable. stdIOHandle
        mov byte ptr es:[ pspHandleTable.STDIN  ], al
        mov byte ptr es:[ pspHandleTable.STDOUT ], al
        mov byte ptr es:[ pspHandleTable.STDERR ], al

        mov dx, 3
        call incrRefCount

        mov al, ' '
        mov cx, (sizefnName + sizefnExtension)
        mov di, offset pspFCB_1
        rep stosb                                       ; init fcb 1

        mov cx, (sizefnName + sizefnExtension)
        mov di, offset pspFCB_2
        rep stosb                                       ; init fcb 2

        mov word ptr es:[ pspFileHandlePtr. _segment ], es
        mov word ptr es:[ pspFileHandlePtr. _pointer ], pspHandleTable
        mov word ptr es:[ pspFileHandleCount         ], sizepspHandleTable

        mov ax, word ptr [ _RxDOS_DOSVersion ]          ; get DOS Version
        mov word ptr es:[ pspVersion ], ax              ; Major, Minor version (VERS)
        jmp short createPSP_12

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy an existing PSP
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

createPSP_08:
        call copyCurrentPSP                             ; copy current PSP

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  other specific initialization
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

createPSP_12:
        mov ax, word ptr ss:[ _RxDOS_CurrentPSP ]
        mov word ptr es:[ pspParentId    ], ax          ; parent PSP address

        mov ax, es                                      ; compute next alloc
        add ax, word ptr [ _AllocSize ][ bp ]
        mov word ptr es:[ pspNextParagraph ], ax

        mov word ptr es:[ pspInt20         ], 20CDh     ; Int 20 instruction
        mov word ptr es:[ pspDosCall       ], 21CDh     ; Int 21 instruction
        mov word ptr es:[ pspDosCall + 2   ], 0CBh      ; retf

        mov byte ptr es:[ pspDispatcher    ], 9Ah       ; call instruction
        mov word ptr es:[ 1+(pspDispatcher. _segment) ], cs
        mov word ptr es:[ 1+(pspDispatcher. _pointer) ], offset _CallDOS
        mov word ptr es:[ pspShareChain. _pointer ], -1 ; share vector 
        mov word ptr es:[ pspShareChain. _segment ], -1

        push ds
        xor ax, ax
        mov ds, ax

        mov bx, offset ( intCONTROLC * 4 )              ; Int23 control-C vector
        mov ax, word ptr [ _pointer ][ bx ]				  ; existing value copied to PSP
        mov dx, word ptr [ _segment ][ bx ]
        mov word ptr es:[ pspControlCVect. _pointer ], ax
        mov word ptr es:[ pspControlCVect. _segment ], dx

        mov bx, offset ( intCRITICALERROR * 4 )         ; Int24 criterror vector
        mov ax, word ptr [ _pointer ][ bx ]				  ; existing value copied to PSP
        mov dx, word ptr [ _segment ][ bx ]
        mov word ptr es:[ pspCritErrorVect. _pointer ], ax
        mov word ptr es:[ pspCritErrorVect. _segment ], dx

        RetCallersStackFrame ds, bx
        mov dx, word ptr [ _CS ][ bx ]					     ; set terminate vector as return address
        mov ax, word ptr [ _IP ][ bx ]
        mov word ptr es:[ pspTerminateVect. _pointer ], ax
        mov word ptr es:[ pspTerminateVect. _segment ], dx
        pop ds

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy command line
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov di, offset pspCommandTail					     ; build dummy command line
        mov word ptr ss:[ di ], 0D00h                   ; pre-init command line

        mov ax, word ptr [ _ExecBlock. _segment ][ bp ]
        or ax, word ptr [ _ExecBlock. _pointer ][ bp ]
        jz createPSP_56                                 ; if no load execute block -->
        
        getdarg ds, di, _ExecBlock
        cmp word ptr [ lexecCommandTail. _segment ][ di ], -1
        jz createPSP_36                                 ; if no command tail to copy -->
        mov ax, word ptr [ lexecCommandTail. _segment ][ di ]
        or ax, word ptr [ lexecCommandTail. _pointer ][ di ]
        jz createPSP_36                                 ; if no command tail to copy -->

        SaveRegisters es, si
        lds si, dword ptr [ lexecCommandTail ][ di ]
        mov di, offset pspCommandTail

        mov cx, 0001                                    ; add one for length byte  (thanks BillW)
        add cl, byte ptr [ si ]                         ; get character count
        rep movsb                                       ; copy
        mov byte ptr es:[ di ], cr                      ; store trailing cr

        RestoreRegisters si, es                         ; address of PSP
        
createPSP_36:
        getdarg ds, di, _ExecBlock
        cmp word ptr [ lexecFCB_1. _segment ][ di ], -1
        jz createPSP_42                                 ; if no FCB 1 to copy -->
        mov ax, word ptr [ lexecFCB_1. _segment ][ di ]
        or ax, word ptr [ lexecFCB_1. _pointer ][ di ]
        jz createPSP_42                                 ; if no FCB 1 to copy -->

        lds si, dword ptr [ lexecFCB_1 ][ di ]
        mov di, offset pspFCB_1
        mov cx, (pspFCB_2 - pspFCB_1)/ 2
        rep movsw
        
createPSP_42:
        getdarg ds, di, _ExecBlock
        cmp word ptr [ lexecFCB_2. _segment ][ di ], -1
        jz createPSP_56                                 ; if no FCB 2 to copy -->
        mov ax, word ptr [ lexecFCB_2. _segment ][ di ]
        or ax, word ptr [ lexecFCB_2. _pointer ][ di ]
        jz createPSP_56                                 ; if no FCB 2 to copy -->

        lds si, dword ptr [ lexecFCB_2 ][ di ]
        mov di, offset pspFCB_2
        mov cx, (pspFCB_2 - pspFCB_1)/ 2
        rep movsw

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  exit
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

createPSP_56:
        pop es
        pop ds
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Increment Ref Count                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   al     SFT handle                                           ;
        ;   dx     increment value                                      ;
        ;...............................................................;

incrRefCount:

        push es
        push di
        push dx
        xor ah, ah
        call FindSFTbyHandle                            ; get corresponding SFT (es: di )

        pop dx
        test word ptr es:[ sftDevInfo ][ di ], sftNoInherit
        jnz incrRefCount_08                             ; if no inherit -->
        add word ptr es:[ sftRefCount ][ di ], dx       ; bump in use count

incrRefCount_08:
        pop di
        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Allocate Loader Memory, copy Env block, init PSP             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:bx  load envelope                                        ;
        ;   ax     paragraphs of program expected                       ;
        ;   cx     additional env variable paras required               ;
        ;   dx     max requested paragraphs                             ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     PSP segment address                                  ;
        ;   es     Data segment address                                 ;
        ;   cx     size in paras of area allocated                      ;
        ;   cy     if cannot create                                     ;
        ;...............................................................;

_LoaderAllocMemory:

        Entry
        def  _pLoadEnvelope, bx                         ; load envelope
        def  _requiredParas, ax
        def  _maxRequestedParas, dx
        def  _allocatedSize, 0000
        def  _envSeg, 0000
        def  _AllocEnv, 0000
        def  _programPSP, 0000
        def  _loadHigh, FALSE
        def  _StartSegment
        def  _envSize

        add word ptr [ _requiredParas ][ bp ], (sizePSP / PARAGRAPH)
        ifc _loaderAllocMemory_50

        or dx, dx                                       ; special 0000 request ?
        jnz _loaderAllocMemory_04                       ; no -->
        mov word ptr [ _maxRequestedParas ][ bp ], 0FFFFh
        mov word ptr [ _loadHigh          ][ bp ], TRUE ; if load HIGH

_loaderAllocMemory_04:
        mov ax, word ptr [ _requiredParas ][ bp ]
        cmp ax, word ptr [ _maxRequestedParas ][ bp ]
        jc _loaderAllocMemory_08
        jz _loaderAllocMemory_08
        mov word ptr [ _maxRequestedParas ][ bp ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  determine the size of the environment block, if any.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loaderAllocMemory_08:
        push word ptr [ _RxDOS_AllocStrategy ]          ; save current alloc policy

        getarg bx, _pLoadEnvelope
        les bx, dword ptr [ loadEnvExecBlock ][ bx ]    ; load exec module
        mov ax, word ptr es:[ lexecEnvironment ][ bx ]
        or ax, ax                                       ; env block address passed ?
        jnz _loaderAllocMemory_14                       ; if segment passed -->

        mov ax, word ptr ss:[ _RxDOS_CurrentPSP ]
        or ax, ax                                       ; current PSP available ?
        jz _loaderAllocMemory_12                        ; no, go figure something -->

        mov es, ax
        mov ax, word ptr es:[ pspEnvironment ]          ; get environment block address
        or ax, ax                                       ; available ?
        jnz _loaderAllocMemory_14                       ; yes, use it -->

_loaderAllocMemory_12:
        mov word ptr [ _RxDOS_AllocStrategy ], _MEM_FIRSTFIT_LOW
        mov cx, sizeParasPerPage                        ; if no exec block, default to this for now
        jmp short _loaderAllocMemory_24                 ; go allocate default size

_loaderAllocMemory_14:
        mov es, ax                                      ; memory block address
        storarg _envSeg, ax                             ; save env segment to copy
        call _getSizeOfEnvString                        ; get size of env string

_loaderAllocMemory_24:
        getarg bx, _pLoadEnvelope
        add cx, word ptr [ loadEnvExtraEnvSizeParas ][ bx ]   ; addtl env size required (paras)
        storarg _envSize, cx                            ; save size in paras
        call allocateMemBlock                           ; return seg pointer to avail memory

        pop word ptr [ _RxDOS_AllocStrategy ]           ; restore current alloc policy
        jc _loaderAllocMemory_50                        ; if insufficient space to load -->

        storarg _AllocEnv, es                           ; store alloc environment

        xor ax, ax
        xor di, di                                      ; start of area
        getarg cx, _envSize                             ; get size in paras
        shl cx, 1                                       ; convert paras to words
        shl cx, 1                                       ; convert paras to words
        shl cx, 1                                       ; convert paras to words
        rep stosw                                       ; clear/ zero.

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy environment
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        cmp word ptr [ _envSeg ][ bp ], 0000            ; no default segment ?
        jz _loaderAllocMemory_42                        ; skip copy -->

        push ds
        xor di, di
        xor si, si
        getarg ds, _envSeg
        getarg cx, _envSize                             ; env size in paras
        shl cx, 1                                       ; convert to words !
        shl cx, 1
        shl cx, 1
        rep movsw                                       ; copy env block

        pop ds

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  allocate block
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loaderAllocMemory_42:
        getarg ax, _requiredParas
        getarg dx, _maxRequestedParas
        call _allocateMinMaxMemBlock                    ; return seg pointer to avail memory
        jc _loaderAllocMemory_50                        ; if insufficient space to load -->
        storarg _programPSP, es                         ; save PSP seg address
        storarg _allocatedSize, cx                      ; allocated size

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  create child PSP
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg bx, _pLoadEnvelope
        mov dx, word ptr [ loadEnvExecBlock. _segment ][ bx ] ; exec block
        mov ax, word ptr [ loadEnvExecBlock. _pointer ][ bx ]
        call createPSP                                  ; build PSP (cx contains alloc size)
        storarg _StartSegment, es                       ; PSP here

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  set Env Block in PSP
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg es, _programPSP
        getarg ax, _AllocEnv                            ; alloc environment
        mov word ptr es:[ pspEnvironment ], ax          ; environment block

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  insure parent's handle are set for both blocks
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov dx, es                                      ; PSP address
        mov ax, es
        sub ax, (sizeMEMBLOCK/ PARAGRAPH)
        mov es, ax                                      ; point to allocation block
        mov word ptr es:[ _memParent ], dx              ; assign parent

        getarg ax, _AllocEnv
        or ax, ax                                       ; none required ?
        jz _loaderAllocMemory_56                        ; no -->

        sub ax, (sizeMEMBLOCK/ PARAGRAPH)
        mov es, ax                                      ; point to allocation block
        mov word ptr es:[ _memParent ], dx              ; assign parent
        jmp short _loaderAllocMemory_56                 ; if everything is ok -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  free env block if error
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loaderAllocMemory_50:
        getarg es, _AllocEnv
        call _freeMemBlock
        SetError pexterrNotEnoughMemory

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loaderAllocMemory_56:
        getarg es, _StartSegment
        getarg ax, _programPSP
        getarg cx, _allocatedSize
        getarg dx, _AllocEnv

        getarg bx, _pLoadEnvelope
        mov word ptr [ loadEnvStartSegment ][ bx ], es
        mov word ptr [ loadEnvProgramPSP   ][ bx ], ax
        mov word ptr [ loadEnvEnvBlock     ][ bx ], dx

_loaderAllocMemory_66:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compute Checksum                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:di  start address of block                               ;
        ;   cx     number of bytes in block                             ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     sum, ignoring carry, of all words in block           ;
        ;...............................................................;

computeChecksum:
        push di
        push cx
        shr cx, 1
        xor ax, ax

computeChecksum_04:
        add ax, word ptr es:[ di ]
        add di, 2
        loop computeChecksum_04

        pop cx
        pop di
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Set Execution State                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ds:dx  address of SETEXEC block                             ;
        ;                                                               ;
        ;...............................................................;

_SetExecuteMode:

        clc
        ret  

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Append CMDLINE to Env block                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es     env block                                            ;
        ;   bx     load envelope                                        ;
        ;                                                               ;
        ;...............................................................;

AppendCmdLineToEnv:

        SaveSegments bx

        lds si, dword ptr ss:[ loadEnvExecBlock ][ bx ]
        lds si, dword ptr [ lexecCommandTail ][ si ]
        mov ax, ds
        or ax, si
        jz appendCmdLine_16

        push ds
        push si
        setDS cs
        mov si, offset RxDOS_CMDLINE
        call RemoveEnvString
        call AppendEnvString

        pop si
        pop ds
        call AppendEnvString

        stosb                                           ; add terminating null

appendCmdLine_16:
        RestoreSegments bx
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Remove Env String                                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es     env block                                            ;
        ;   si     key to match                                         ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   di     points to second terminating null at end of env      ;
        ;...............................................................;

RemoveEnvString:

        xor di, di                                      ; start of env space

RemoveEnvString_08:
        push si

RemoveEnvString_12:
        mov al, byte ptr es:[ di ]
        or al, al                                       ; unexpected end char
        jz RemoveEnvString_22                           ; go scan next key -->

        cmp al, byte ptr [ si ]                         ; matches search key ?
        jnz RemoveEnvString_20                          ; no, skip to next key -->

        cmp al, '='                                     ; equal sign ?
        jz RemoveEnvString_36                           ; we have a match -->

        inc di
        inc si
        jmp RemoveEnvString_12

RemoveEnvString_20:
        inc di                                          ; scan to end of key
        cmp byte ptr es:[ di ], 00
        jnz RemoveEnvString_20

RemoveEnvString_22:
        pop si
        inc di
        cmp byte ptr es:[ di ], 00                      ; at end of env space ?
        jnz RemoveEnvString_08                          ; no -->

        ret

RemoveEnvString_36:
        pop ax                                          ; original si
        sub si, ax                                      ; si will contain length of key word
        xchg si, ax                                     ; si points to lookup key

        push si
        mov si, di
        sub si, ax                                      ; must delete from 

RemoveEnvString_38:
        inc di                                          ; scan to end of key
        cmp byte ptr es:[ di ], 00
        jnz RemoveEnvString_38

        inc di
        xchg si, di

RemoveEnvString_44:
        mov al, byte ptr es:[ si ]                      ; copy down
        mov byte ptr es:[ di ], al
        inc si
        inc di
        or al, al
        jnz RemoveEnvString_44

        cmp byte ptr es:[ si ], 00                      ; end of env buffer ?
        jnz RemoveEnvString_44                          ; no -->

        mov byte ptr es:[ di ], al                      ; append a second null

        pop si
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Append Env String                                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es     env block                                            ;
        ;   ds:si  text to insert                                       ;
        ;...............................................................;

AppendEnvString:

        lodsb                                           ; copy arg
        stosb
        or al, al
        jnz AppendEnvString

        dec di                                          ; ptr does not include 0
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Append Program Name to Environment Block                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   es     environment block                                    ;
        ;   ds:dx  program name                                         ;
        ;                                                               ;
        ;...............................................................;

appendProgramNametoEnv:

        mov si, dx                                      ; save name to program

        xor al, al                                      ; byte to search for
        xor di, di                                      ; byte offset
        mov cx, 0FFFFh                                  ; maximum length

appendPgmName_10:
        repnz scasb                                     ; scan for end of string
        scasb                                           ; test if double null
        jne appendPgmName_10                            ;

        mov ax, 0001                                    ; # strings to append
        stosw                                           ;

appendPgmName_14:
        lodsb
        stosb
        or al, al                                       ; null byte copied ?
        jnz appendPgmName_14                            ; not yet -->

        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Size of Envirnment String                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es     environment block string (must be non-zero)          ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   cx     # paragraphs requested. (if zero, allocate none).    ;
        ;                                                               ;
        ;...............................................................;

_getSizeOfEnvString:

        push di

        xor al, al                                      ; byte to search for
        xor di, di                                      ; byte offset
        mov cx, 0FFFFh                                  ; maximum length

getSizeEnv_10:
        repnz scasb                                     ; scan for end of string
        scasb                                           ; test if double null
        jne getSizeEnv_10                               ;

        mov cx, di                                      ; get length
        add cx, (sizeEXPANDNAME + sizeParagraph - 1)/2  ; include size of trailing filename
        add cx, 2                                       ; include count word
        shr cx, 1                                       ; size to paragraphs
        shr cx, 1
        shr cx, 1
        shr cx, 1
        or cx, cx

        pop di
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Set Program Name in Memory Block                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es     block to change (not header address)                 ;
        ;   dx     full or partial filename (null terminated)           ;
        ;                                                               ;
        ;...............................................................;

_setProgramNameInMemBlock:

        SaveSegments
        mov ax, es
        or ax, ax                                       ; no destination mem block ?
        jz _setProgName_22                              ; ignore set name -->

        sub ax, (sizeMEMBLOCK/ PARAGRAPH)
        push ax                                         ; save seg address

        mov ax, ss
        mov ds, ax                                      ; just in case it's not
        mov es, ax                                      ; scan expanded name

        xor ax, ax
        mov cx, -1
        mov di, dx                                      ; scan name for null terminator
        repnz scasb                                     ; scan name
        not cx                                          ; valid count

_setProgName_08:
        cmp byte ptr es:[ di - 1 ], '\'
        jz _setProgName_12                              ; we have start of name -->
        dec di
        loop _setProgName_08

        mov di, dx                                      ; name contained no path

_setProgName_12:
        mov si, di                                      ; save filename 
        mov dx, di                                      ; save filename 
        pop es                                          ; restore memblock seg address

        mov ax, 2020h
        mov di, offset _memPgmName                      ; where to init
        mov cx, sizefnName / 2                          ; size filename in words
        rep stosw                                       ; initialize to blanks

        mov di, offset _memPgmName                      ; where to init
        mov cx, sizefnName                              ; size filename in bytes

_setProgName_16:
        lodsb                                           ; get byte
        or al, al                                       ; end of name ?
        jz _setProgName_22                              ; if end of name -->
        cmp al, '.'                                     ; extension ?
        jz _setProgName_22                              ; if end of name -->

        stosb                                           ; save character in mem block
        loop _setProgName_16

_setProgName_22:
        RestoreSegments
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  If Overlay Loader, return size                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   al     mode flag                                            ;
        ;   cx:dx  file size to return                                  ;
        ;                                                               ;
        ;...............................................................;

ifOverlayLoader:
        push es
        push si
        RetCallersStackFrame es, si                     ; save size only if non-Overlay
        mov word ptr es:[ _CX           ][ si ], dx
        mov word ptr es:[ _BX           ][ si ], cx
        mov word ptr es:[ _AX           ][ si ], 0000
        pop si
        pop es

        cmp al, execLoadOverlay                         ; load overlay ?
        clc
        ret

RxDOS   ENDS
        END


