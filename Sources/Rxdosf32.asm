        TITLE   'f32 - DOS Long Filename Mapping Services'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  DOS FAT32 Support                                            ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc

RxDOS   SEGMENT PARA PUBLIC 'CODE'
        assume cs:RxDOS, ds:RxDOS, es:RxDOS, ss:RxDOS

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  DOS FAT32 Support                                            ;
        ;...............................................................;

        public _Int21Function73

        extrn getDrive                          : near
        extrn getDPB                            : near
        extrn _mul32                            : near
        extrn _RetCallersStackFrame             : near
        extrn AmountFreeSpace                   : near

        extrn pexterrInvalidFunction            : near
        extrn pexterrFileNotFound               : near
        extrn pexterrPathNotFound               : near
        extrn pexterrIllegalName                : near
        extrn pexterrNoHandlesAvailable         : near
        extrn pexterrAccessDenied               : near
        extrn pexterrInvalidHandle              : near
        extrn pexterrArenaTrashed               : near
        extrn pexterrNotEnoughMemory            : near
        extrn pexterrInvalidBlock               : near
        extrn pexterrInvalidAccess              : near
        extrn pexterrInvalidDrive               : near
        extrn pexterrCurrentDirectory           : near
        extrn pexterrNoMoreFiles                : near
        extrn pexterrFileExists                 : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  73xxh FAT32 Get/Set Structures                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Dispatch on function code services                           ;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;...............................................................;

_Int21Function73:

        Goto 02h,      Get_ExtDPB                              ; 02h
        Goto 03h,      Get_ExtFreeSpace                        ; 03h

      ; ** Not Supported **
      ; Goto 04h,      Extended Drive Format                   ; 04h 

        mov ax, offset pexterrInvalidFunction
        stc
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  7302h Get Extended DPB                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Get a copy of an Extended DPB (FAT32) drive                  ;
        ;                                                               ;
        ;  cx    buffer size (should equal sizeDPB + size WORD)         ;
        ;  dl    drive                                                  ;
        ;  es:di pointer to buffer                                      ;
        ;                                                               ;
        ;  When calling this  function  with  DeviceIoControl,  it  is  ;
        ;  recommended that the  dwloControlCode parameter  be set  to  ;
        ;  VWIN32_DIOC_DOS_IOCTL (see VWIN32.H). For additional infor-  ;
        ;  mation, see Using VWIN32 to Carry Out MS-DOS Functions.      ;
        ;...............................................................;

Get_ExtDPB:

        Entry
        ddef _ptrtoBuffer
        def  _size, cx

        mov ax, offset pexterrInvalidFunction
        cmp cx, sizeExtDPB
        stc
        jnz Get_ExtDPB_return

        xor dh, dh
        mov ax, dx
        call getDPB                                     ; get DPB
        jc Get_ExtDPB_return                            ; if error -->
        stordarg _ptrtoBuffer, es, bx                   ; save pointer

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; es:bx points to DPB for drive
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        RetCallersStackFrame es, bx
        mov di, word ptr es:[ _DI ][ bx ]
        mov es, word ptr es:[ _ExtraSegment ][ bx ]     ; buffer address

        getdarg ds, si, _ptrtoBuffer                    ; save pointer
        getarg cx, _size

        sub cx, 2
        mov es:[ di ], cx                               ; set length in WORD
        add di, 2                                       ; bump pointer
        rep movsb                                       ; copy DPB to return address

        or ax, ax                                       ; clear carry

Get_ExtDPB_return:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  7303h Get_ExtFreeSpace                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Returns the total disk space and the free disk space         ;
        ;                                                               ;
        ;  cx    buffer size                                            ;
        ;  es:di pointer to EXTENDED FREE SPACE table                   ;
        ;  ds:dx pointer to ASCIZ path name. Must include drive.        ;
        ;                                                               ;
        ;...............................................................;

Get_ExtFreeSpace:

        Entry
        ddef _pExtendedFreeSpaceTable

        mov ax, offset pexterrInvalidFunction
        cmp cx, sizeEXTENDEDFREESPACE
        stc
        ifnz Get_ExtFreeSpaceReturn

        RetCallersStackFrame ds, bx
        mov di, word ptr [ _DI           ][ bx ]
        mov es, word ptr [ _ExtraSegment ][ bx ]        ; buffer address
        stordarg _pExtendedFreeSpaceTable, es, di

        mov si, word ptr [ _DX           ][ bx ]
        mov es, word ptr [ _DataSegment  ][ bx ]        ; asciz drive
        call getDrive                                   ; extract drive info
        ifc Get_ExtFreeSpaceReturn                      ; if error -->

        setDS ss
        call AmountFreeSpace                            ; dpb in es:bx
        setDS es                                        ; ds:bx

        getdarg es, di, _pExtendedFreeSpaceTable
        clearMemory sizeEXTENDEDFREESPACE               ; init return

        mov word ptr es:[ _extFreeSize  ][ di ], sizeEXTENDEDFREESPACE
        mov word ptr es:[ _extFreeLevel ][ di ], 0000

        mov ax, word ptr [ _dpbBytesPerSector ][ bx ]
        mov word ptr es:[ _extFreeBytesPerSector. _low  ][ di ], ax              ; num bytes per sector

        mov cx, word ptr [ _dpbFreeCount. _high ][ bx ]
        mov dx, word ptr [ _dpbFreeCount. _low  ][ bx ]
        mov word ptr es:[ _extFreeAvailableClusters. _low  ][ di ], dx           ; num available clusters
        mov word ptr es:[ _extFreeAvailableClusters. _high ][ di ], cx           ; num available clusters
        mov word ptr es:[ _extFreeAvailableAllocationUnits. _low ][ di ], dx     ; num free allocation units, w/o compression
        mov word ptr es:[ _extFreeAvailableAllocationUnits. _high ][ di ], cx    ; num free allocation units, w/o compression

        mov dx, word ptr [ _dpbxMaxCluster. _low   ][ bx ]
        mov cx, word ptr [ _dpbxMaxCluster. _high  ][ bx ]
        cmp word ptr [ _dpbSectorsPerFat ][ bx ], 0000                           ; fat32 drive ?
        jz Get_ExtFreeSpace22                                                    ; yes -->
        mov dx, word ptr [ _dpbMaxClusterNumber ][ bx ]
        xor cx, cx

Get_ExtFreeSpace22:
        mov word ptr es:[ _extFreeTotalClusters. _low      ][ di ], dx           ; total num clusters for drive
        mov word ptr es:[ _extFreeTotalClusters. _high     ][ di ], cx           ; total num clusters for drive
        mov word ptr es:[ _extFreeTotalAllocationUnits. _low   ][ di ], dx       ; num total allocation units, w/o compression
        mov word ptr es:[ _extFreeTotalAllocationUnits. _high  ][ di ], cx       ; num total allocation units, w/o compression

        mov ax, dx
        mov dx, cx

        xor cx, cx
        mov cl, byte ptr [ _dpbClusterSizeMask     ][ bx ]
        inc cx
        mov word ptr es:[ _extFreeSectorsPerCluster. _low  ][ di ], cx           ; num sectors per cluster

        call _mul32
        mov word ptr es:[ _extFreeTotalPhysSectors. _low       ][ di ], ax       ; num total physical sectors, w/o compression
        mov word ptr es:[ _extFreeTotalPhysSectors. _high      ][ di ], dx       ; num total physical sectors, w/o compression

        mov ax, word ptr es:[ _extFreeAvailableClusters. _low  ][ di ]           ; num available clusters
        mov dx, word ptr es:[ _extFreeAvailableClusters. _high ][ di ]
        call _mul32

        mov word ptr es:[ _extFreeAvailablePhysSectors. _low   ][ di ], ax       ; num free physical sectors, w/o compression
        mov word ptr es:[ _extFreeAvailablePhysSectors. _high  ][ di ], dx       ; num free physical sectors, w/o compression
        clc

Get_ExtFreeSpaceReturn:
        Return

RxDOS   ENDS
        END

