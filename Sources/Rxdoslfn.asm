        TITLE   'lfn - DOS Long Filename Mapping Services'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  DOS Long Filename Mapping Services                           ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc

RxDOS   SEGMENT PARA PUBLIC 'CODE'
        assume cs:RxDOS, ds:RxDOS, es:RxDOS, ss:RxDOS

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  DOS Long Filename Mapping Services                           ;
        ;...............................................................;

        public _Int21Function71
        public LFNReleaseFindData

        extrn _DiskReset                        : near
        extrn initdiskAccess                    : near
        extrn CtrlC_Check                       : near
        extrn VerifyAvailableHandle             : near
        extrn _RetCallersStackFrame             : near

        extrn _SFTOpenFile                      : near
        extrn ReleaseSFT                        : near
        extrn MapSFTtoAppHandle                 : near
        extrn convFCBNametoASCIZ                : near

        extrn getCurrDirCluster                 : near
        extrn scanInvalidFilenameChars          : near
        extrn ifPathSeparator                   : near
        extrn getDrive                          : near
        extrn GetActualDrive                    : near
        extrn checkforDeviceName                : near
        extrn getAddrDPB                        : near
        extrn getDPB                            : near

        extrn _div32                            : near
        extrn CCBChanged                        : near
        extrn convFilenametoFCBString           : near
        extrn locateCCBPHeader                  : near
        extrn SetCCBChanged                     : near
        extrn selBuffer                         : near
        extrn updateAllChangedCCBBuffers        : near
        extrn IsRootExtendable                  : near

        extrn RxDOS_LFNTempStoreCount           : word
        extrn RxDOS_LFNTempStorage              : word
        extrn _RxDOS_CurrentPSP                 : word

        extrn AllocateInitCluster               : near
        extrn updateClusterValue                : near
        extrn _FATReadRandom                    : near
        extrn CompareString                     : near
        extrn StringLength                      : near
        extrn convDirEntrytoASCIZ               : near

        extrn initSFTfromDirEntry               : near
        extrn FindAvailableSFThandle            : near
        extrn InitSFTentry                      : near

        extrn _chgErrorToPathNotFound           : near
        extrn getSysDateinDirFormat             : near
        extrn blankinitDirName                  : near
        extrn ReleaseClusterChain               : near
        extrn updateDriveBuffers                : near

        extrn _GetCurrentDirectory              : near
        extrn _ExecuteProgram                   : near
        extrn LoadProgram                       : near
        extrn CopyStringLowercase               : near
        extrn CopyString                        : near

        extrn _RxDOS_pCDS                       : dword
        extrn _RxDOS_pLFNCDS                    : dword
        extrn _RxDOS_bLastDrive                 : byte
        extrn _RxDOS_CurrentDrive               : byte
        extrn _bitShiftTable                    : byte
        extrn _invalidFnCharacters              : byte
        extrn RxDOS_Heap                        : near

        extrn pexterrInvalidFunction            : near
        extrn pexterrFileNotFound               : near
        extrn pexterrPathNotFound               : near
        extrn pexterrIllegalName                : near
        extrn pexterrNoHandlesAvailable         : near
        extrn pexterrAccessDenied               : near
        extrn pexterrInvalidHandle              : near
        extrn pexterrArenaTrashed               : near
        extrn pexterrNotEnoughMemory            : near
        extrn pexterrInvalidBlock               : near
        extrn pexterrInvalidAccess              : near
        extrn pexterrInvalidDrive               : near
        extrn pexterrCurrentDirectory           : near
        extrn pexterrNoMoreFiles                : near
        extrn pexterrFileExists                 : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Scan Directory Entry                                         ;
        ;...............................................................;

        SCANDIR struc

_scandirStringLength     dw ?                           ; string length
_scandirStringPointer    dd ?                           ; string pointer

_scandirDrive            dw ?                           ; drive directory
_scandirCluster          dd ?                           ; cluster directory
_scandirFileOffset       dd ?                           ; where to start searching in dir

_scandirFileAcLongEntry  dd ?                           ; file offset, long dir entry
_scandirFileAcShortEntry dd ?                           ; file offset, short dir entry

_scandirLongFilenameStr  dw ?                           ; ptr to store LFN (ss:)
_scandirShortFilenameStr dw ?                           ; ptr to store SFN (ss:)

        SCANDIR ends

sizeSCANDIR              equ size SCANDIR

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compare Wild Characters                                      ;
        ;...............................................................;

maxEntriesCOMPARELIST   equ 32
_csStringLength         equ  _segment

        COMPARESTRUCT struc

_csFlags                dw ?                            ; flags
_cspStartStringSegment  dw ?                            ; string base segment
_cspStartString         dd ?                            ; must start with (ptr / count)
_cspEndString           dd ?                            ; must end with   (ptr / count) 

_csCountEntries         dw ?                            ; entries in compare list
_csCompareList          dd maxEntriesCOMPARELIST dup(?) ; compare list    (ptr / count)

        COMPARESTRUCT ends

sizeCompareListItem     equ  (size _csCompareList) / maxEntriesCOMPARELIST
sizeCompareListStruct   equ  size COMPARESTRUCT

CSMATCHES_ALL           equ 8000h                       ; *.*
CSNOWILD_CHARS          equ 4000h                       ; no * or ? (must match exact )

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Macro. Ptr to Entry                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  base_reg, offset_reg                                         ;
        ;                                                               ;
        ;  base_reg     reg points to canonical list struct             ;
        ;  offset_reg   reg contains offset #                           ;
        ;                                                               ;
        ;...............................................................;

_CANPtrToEntry    macro base_reg, offset_reg

        mov ax, sizeCANONICALPOINTER
        mul offset_reg
        lea base_reg, offset [ _canpArray ][ base_reg ]  ; offset to next entry to store
        add base_reg, ax                                 ; offset to next entry to store
        endm

_CANAppendEntry    macro base_reg, offset_reg

        mov ax, sizeCANONICALPOINTER
        mul word ptr [ _canpNumEntries ][ base_reg ]     ; get current entries
        lea base_reg, offset [ _canpArray ][ base_reg ]  ; offset to next entry to store
        add base_reg, ax                                 ; offset to next entry to store
        endm

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Canonical Pointer Entry                                      ;
        ;...............................................................;

        CANONICALPOINTER struc

_canpFlags              dw ?                            ; flags
_canpStringLength       dw ?                            ; string length
_canpStringPointer      dd ?                            ; string pointer

        CANONICALPOINTER ends

maxEntriesCANONICALLIST  equ 64
CANONICALENTRY_WILDCHARS equ 0001h                      ; entry contains ? or *
sizeCANONICALPOINTER     equ size CANONICALPOINTER

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Canonical Pointer List                                       ;
        ;...............................................................;

        CANONICALPOINTERLIST struc

_canpNumEntries         dw ?                            ; num entries
_canpOptSearchEntry     dw ?                            ; opt search entry
_canpDrive              dw ?                            ; drive
_canpCluster            dd ?                            ; cluster
_canpArray              db maxEntriesCANONICALLIST * sizeCANONICALPOINTER dup(?)

        CANONICALPOINTERLIST ends

sizeCANONICALPOINTERLIST equ size CANONICALPOINTERLIST

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  LFN Directory Entries                                        ;
        ;...............................................................;

        LFNDIRENTRY struc

LFN_SequenceByte        db ?
LFN_Part1               db 10 dup(?)                    ; (UNICODE ) Name, part 1
LFN_Attributes          db ?                            ;            ATTR_LONGFILENAME
LFN_EntryType           db ?                            ;            Type
LFN_Checksum            db ?                            ;            Checksum
LFN_Part2               db 12 dup(?)                    ; (UNICODE ) Name, part 2
LFN_Unknown             dw ?                            ; (0000's  ) Unknown
LFN_Part3               db  4 dup(?)                    ; (UNICODE ) Name, part 3

        LFNDIRENTRY ends

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Offsets in LFN directory entry for match/ copy purposes
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_DefaultAll:            db '*', 0
LFN_DirEntry_MatchTable:db 1, 3, 5, 7, 9, 14, 16, 18, 20, 22, 24, 28, 30, -1
LFN_NAMECHARPERENTRY    equ (size LFN_Part1 + size LFN_Part2 + size LFN_Part3) / 2
LFN_START_LFNENTRY      equ 40h
LFN_SEQUENCEMASK        equ (LFN_START_LFNENTRY-1)

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Long Filename Structure                                      ;
        ;...............................................................;

        STRUCTLONGFILENAME struc

_lfnFlags               dw ?                            ; flags
_lfnOptions             dw ?                            ; options

_lfnDrive               dw ?                            ; drive
_lfnCluster             dd ?                            ; cluster
_lfnFileSize            dd ?                            ; file size

_lfnDirCluster          dd ?                            ; directory cluster
_lfnOffsetLongEntry     dd ?                            ; offset in dir to start of long entry
_lfnOffsetShortEntry    dd ?                            ; offset in dir to start of short entry

_lfnSearchAttribs       db ?                            ; search attributes
_lfnMustMatchAttribs    db ?                            ; must match attributes

_lfnDirEntriesReqd      dw ?                            ; required dir entries
_lfnNameRemainder       dw ?                            ; name remainder
_lfnChecksum            dw ?                            ; checksum
_lfnSearchStringLength  dw ?                            ; length of provided long filename
_lfnSearchLongNameText  dd ?                            ; ptr to provided long filename
_lfnShortFCBName        db (sizeFILENAME + 1) dup(?)    ; short filename
_lfnCanonicalList       db sizeCANONICALPOINTERLIST dup(?)

        STRUCTLONGFILENAME ends

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Flags
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_lfnFlag_IsShortName    equ 8000h                       ; long name entry points to short name
sizeSTRUCTLONGFILENAME  equ size STRUCTLONGFILENAME

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  LFN Temp Store                                               ;
        ;...............................................................;

        LFNTEMPSTORE struc

_lfnfindstoreOwner      dw ?                            ; PSP value of owner
_lfnfindstoreFindText   db sizeLFNPATH dup(?)           ; search text
_lfnfindstoreLFNStruc   db sizeSTRUCTLONGFILENAME dup(?); lfn struct 
_lfnfindstoreDirAccess  db sizeDIRACCESS dup(?)         ; dir access

        LFNTEMPSTORE ends

sizeLFNTEMPSTORE         equ size LFNTEMPSTORE

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  71xxh Long Filename Services                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Dispatch on function code services                           ;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;...............................................................;

_Int21Function71:

        Goto DiskReset,               _DiskReset                    ; 0Dh
        Goto CreateSubdirectory,      Int71LFNCreateSubdirectory    ; 39h
        Goto RemoveSubdirectory,      Int71LFNRemoveSubdirectory    ; 3Ah
        Goto ChangeSubdirectory,      Int71LFNChangeSubdirectory    ; 3Bh 
        Goto DeleteFile,              Int71LFNDeleteFile            ; 41h

        Goto ChangeFileMode,          Int71LFNChangeFileMode        ; 43h
        Goto GetCurrentDirectory,     Int71LFNGetCurrentDirectory   ; 47h
        Goto ExecuteProgram,          Int71LFNExecuteProgram        ; 4Bh
        Goto FindFirstFile,           Int71LFNFindFirstFile         ; 4Eh
        Goto FindNextFile,            Int71LFNFindNextFile          ; 4Fh
        Goto RenameFile,              Int71LFNRenameFile            ; 56h
        Goto GetActualFileName,       Int71LFNGetActualFileName     ; 60h
        Goto ExtendedOpenCreate,      Int71LFNExtendedOpenCreate    ; 6Ch

        Goto GetVolumeInformation,    Int71LFNGetVolumeInformation  ; A0h
        Goto FindClose,               Int71LFNFindClose             ; A1h
        Goto GetFileInfoByHandle,     Int71LFNGetFileInfoByHandle   ; A6h
        Goto FileTime,                Int71LFNFileTime              ; A7h
        Goto GenerateShortName,       Int71LFNGenerateShortName     ; A8h
      ; Goto ServerOpenFile,          __ServerOpenFile              ; A9h
      ; Goto Subst,                   __Subst                       ; AAh

        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  7139h Create Subdirectory                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  ds:dx pointer to subdirectory                                ;
        ;                                                               ;
        ;...............................................................;

Int71LFNCreateSubdirectory:

        Entry
        ddef _pathname, es, dx                          ; arg passed internally as es:dx
        ddef _ptrParentDirEntry                         ; dir reference
        ddef _alloccluster                              ; allocated cluster
        ddef _datetime

        defbytes _dirAccess, sizeDIRACCESS
        defbytes _structLongFilename, sizeSTRUCTLONGFILENAME

        call CtrlC_Check                                ; look-ahead CtrlC Check

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  does entry already exist ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        mov ax, (FILE_NODEVICENAME + FILEHAS_NOFILENAME + FILECANNOT_BEDEFINED)
        lea di, offset [ _dirAccess ][ bp ]             ; dir access
        getdarg es, si, _pathname                       ; arg passed internally as es:dx
        call LFNLocateFile                              ; check file path
        jnc LFNMakeSubDir_12                            ; if path is valid -->

        call _chgErrorToPathNotFound                    ; if name referenced path
        jmp LFNMakeSubDir_40                            ; if path invalid -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  find next empty directory entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNMakeSubDir_12:
        and word ptr [ _dirAccess. fileAcDrive   ][ bp ], 7FFFh

        mov ax, word ptr [ _dirAccess. fileAcDrive   ][ bp ]
        mov dx, word ptr [ _dirAccess. fileAcDirCluster. _low  ][ bp ]
        mov cx, word ptr [ _dirAccess. fileAcDirCluster. _high ][ bp ]
        lea di, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call LFNBuildDirectoryEntry                     ; valid empty entry ?
        stordarg _ptrParentDirEntry, es, bx             ; save ptr to directory reference
        ifc LFNMakeSubDir_40                            ; if can't find free slot -->

        call getSysDateinDirFormat
        stordarg _datetime, dx, ax                      ; make sure we get these

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Allocate cluster for sub-directory
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ _dirAccess. fileAcDrive   ][ bp ]
        call AllocateInitCluster                        ; init/ allocate dir cluster
        stordarg _alloccluster, cx, dx                  ; save cluster
        ifc LFNMakeSubDir_40                            ; if error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init (.) entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea si, offset [ ccbData ][ di ]                ; point to data
        call blankinitDirName                           ; initialize file name 
        mov byte ptr es:[ deName ][ si ], '.'           ; set dot entry

        getdarg cx, dx, _alloccluster                   ; get cluster
        mov word ptr es:[ deStartCluster    ][ si ], dx
        mov word ptr es:[ deStartClusterExt ][ si ], cx

        getdarg dx, ax, _datetime                       ; get date/ time
        mov word ptr es:[ deTime ][ si ], ax            ; time created
        mov word ptr es:[ deDate ][ si ], dx            ; date created
        mov byte ptr es:[ deAttributes ][ si ], ATTR_DIRECTORY

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init (..) entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea si, offset [ ccbData.sizeDIRENTRY ][ di ]
        call blankinitDirName                           ; initialize file name 
        mov word ptr es:[ deName ][ si ], '..'          ; set double dot entry

        mov dx, word ptr [ _dirAccess. fileAcDirCluster. _low  ][ bp ]
        mov cx, word ptr [ _dirAccess. fileAcDirCluster. _high ][ bp ]
        mov word ptr es:[ deStartCluster    ][ si ], dx
        mov word ptr es:[ deStartClusterExt ][ si ], cx

        getdarg dx, ax, _datetime                       ; get date time
        mov word ptr es:[ deTime ][ si ], ax            ; time created
        mov word ptr es:[ deDate ][ si ], dx            ; date created
        mov byte ptr es:[ deAttributes ][ si ], ATTR_DIRECTORY

        mov bx, si
        call SetCCBChanged                              ; say buffer changed

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  in parent directory, build entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getdarg es, si, _ptrParentDirEntry              ; restore si pointer
        getdarg cx, dx, _alloccluster                   ; get cluster
        mov word ptr es:[ deStartCluster    ][ si ], dx
        mov word ptr es:[ deStartClusterExt ][ si ], cx
        mov byte ptr es:[ deAttributes      ][ si ], ATTR_DIRECTORY

        getdarg dx, ax, _datetime                       ; get date time
        mov word ptr es:[ deTime ][ si ], ax            ; time created
        mov word ptr es:[ deDate ][ si ], dx            ; date created

        mov bx, si                                      ; identify dir entry
        call SetCCBChanged                              ; say buffer changed

        xor ax, ax                                      ; clear carry

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  normal exit
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNMakeSubDir_40:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  713Ah Remove Subdirectory                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  ds:dx pointer to subdirectory                                ;
        ;...............................................................;

Int71LFNRemoveSubdirectory:

        Entry
        ddef _cluster
        ddef _pathname, es, dx                          ; arg passed internally as es:dx
        defbytes _dirAccess, sizeDIRACCESS
        defbytes _diskAccess, sizeDISKACCESS
        defbytes _structLongFilename, sizeSTRUCTLONGFILENAME
        
        call CtrlC_Check                                ; look-ahead CtrlC Check

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  parse file name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        mov ax, (FILE_NODEVICENAME + FILEHAS_NOFILENAME)
        lea di, offset [ _dirAccess ][ bp ]             ; dir access
        getdarg es, si, _pathname                       ; arg passed internally as es:dx
        call LFNLocateFile                              ; check file path
        stordarg _cluster, cx, dx                       ; save cluster address
        jnc LFNRemoveSubDir_12                          ; if path valid -->

        call _chgErrorToPathNotFound                    ; if name referenced path
        jmp LFNRemoveSubDir_40                          ; if path invalid -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  trying to remove current directory ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNRemoveSubDir_12:
        call getCurrDirCluster                          ; get cluster of curr directory

        sub dx, word ptr [ _cluster. _low  ][ bp ]
        sbb cx, word ptr [ _cluster. _high ][ bp ]      ; compare 
        or cx, dx                                       ; same directory as current ?
        jnz LFNRemoveSubDir_16                          ; dir is ok -->
        SetError pexterrCurrentDirectory, LFNRemoveSubDir_40

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if directory is empty (contains other than . and .. )
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNRemoveSubDir_16:
        setES ds
        mov ax, word ptr [ _dirAccess. fileAcDrive ][ bp ]
        mov dx, word ptr [ _dirAccess. fileAcCluster. _low  ][ bp ]
        mov cx, word ptr [ _dirAccess. fileAcCluster. _high ][ bp ]
        lea bx, offset _diskAccess [ bp ]               ; pointer to access block
        call initdiskAccess                             ; [ax] is drive, [cx:dx] is cluster

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  make sure entire directory is empty
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov word ptr [ _diskAccess. diskAcOptions        ][ bp ], (ccb_isDIR)
        mov word ptr [ _diskAccess. diskAcPosition. _low ][ bp ], sizeDIRENTRY

LFNRemoveSubDir_22:
        add word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ], sizeDIRENTRY
        adc word ptr [ _diskAccess. diskAcPosition. _high ][ bp ], 0000
        lea bx, offset _diskAccess [ bp ]               ; pointer to access block
        call _FATReadRandom                             ; read into buffer
        jz LFNRemoveSubDir_32                           ; if no more data, ok to delete -->

        cmp byte ptr es:[ bx ], DIRENTRY_NEVERUSED
        jz LFNRemoveSubDir_32                           ; if no more data, ok to delete -->
        cmp byte ptr es:[ bx ], DIRENTRY_DELETED        ; entry deleted ?
        jz LFNRemoveSubDir_22                           ; yes, keep searching -->

        SetError pexterrIllegalName, LFNRemoveSubDir_40

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  ok to remove this entry.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNRemoveSubDir_32:
        mov ax, word ptr [ _dirAccess. fileAcDrive       ][ bp ]
        mov cx, word ptr [ _dirAccess. fileAcCluster. _high ][ bp ]
        mov dx, word ptr [ _dirAccess. fileAcCluster. _low  ][ bp ]
        call ReleaseClusterChain

        les di, dword ptr [ _dirAccess. fileAcBufferPtr ][ bp ]
        mov bx, word ptr [ _dirAccess. fileAcDirOffset  ][ bp ]
        mov byte ptr es:[ di + bx ], DIRENTRY_DELETED
        call SetCCBChanged                              ; say buffer changed

        mov ax, word ptr [ _dirAccess. fileAcDrive ][ bp ]
        call updateDriveBuffers
        clc                                             ; return no error

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  normal exit
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNRemoveSubDir_40:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  713Bh Change Subdirectory                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  ds:dx pointer to subdirectory                                ;
        ;...............................................................;

Int71LFNChangeSubdirectory:

        Entry
        def _drive
        ddef _cluster
        ddef _pathname, es, dx                          ; arg passed internally as es:dx
        defbytes _dirAccess, sizeDIRACCESS
        defbytes _structLongFilename, sizeSTRUCTLONGFILENAME

        call CtrlC_Check                                ; look-ahead CtrlC Check

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  parse file name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        mov ax, (FILE_NODEVICENAME + FILEHAS_NOFILENAME)
        lea di, offset [ _dirAccess ][ bp ]             ; dir access
        getdarg es, si, _pathname                       ; arg passed internally as es:dx
        call LFNLocateFile                              ; check file path
        jnc LFNChangeSubDir_12                          ; if path valid -->

        call _chgErrorToPathNotFound                    ; if name referenced path
        jmp LFNChangeSubDir_40                          ; if path invalid -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy path to CDS table.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNChangeSubDir_12:
        push ds                                         ; save current segment
        push dx                                         ; cluster address
        push cx

        les di, dword ptr [ _RxDOS_pLFNCDS ]            ; actual address of LFNCDS
        mov ax, word ptr [ _structLongFilename. _lfnDrive ][ bp ]
        mov cx, sizeLFNCDS
        mul cx                                          ; ax contains current drive
        add di, ax                                      ; pointer to LFNCDS

        setDS ss                                        ; copy from stack
        lea bx, offset [ _structLongFilename ][ bp ]
        call LFNBuildFullyFormedPATH                    ; copy fully formed path name

        setDS ss                                        ; copy from stack
        les si, dword ptr [ _RxDOS_pCDS ]               ; actual address of CDS
        mov ax, word ptr [ _structLongFilename. _lfnDrive ][ bp ]
        mov cx, sizeCDS
        mul cx                                          ; ax contains current drive
        add si, ax

        pop word ptr es:[ _cdsStartClusterDir. _high ][ si ]
        pop word ptr es:[ _cdsStartClusterDir. _low ][ si ]
        pop ds                                          ; restore ds
        clc                                             ; no error

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  normal exit
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNChangeSubDir_40:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  7141h Delete File                                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  ds:dx pointer to subdirectory                                ;
        ;...............................................................;

Int71LFNDeleteFile:

        Entry
        def _numentries
        ddef _filename, es, dx                          ; arg passed internally as es:dx

        defbytes _diskAccess, sizeDISKACCESS
        defbytes _dirAccess, sizeDIRACCESS
        defbytes _structLongFilename, sizeSTRUCTLONGFILENAME

        call CtrlC_Check                                ; look-ahead CtrlC Check

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  parse file name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        mov si, dx                                      ; name from caller
        lea di, offset [ _dirAccess ][ bp ]             ; dir access
        mov ax, (FILE_NODEVICENAME + FILECANNOT_BEDIRECTORY)
        call LFNLocateFile 
        jc LFNDeleteFile_32                             ; if file/path valid -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Release cluster chain
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ _dirAccess. fileAcDrive      ][ bp ]
        mov cx, word ptr [ _dirAccess. fileAcCluster. _high ][ bp ]
        mov dx, word ptr [ _dirAccess. fileAcCluster. _low  ][ bp ]
        call ReleaseClusterChain

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  mark found entry in directory as deleted
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ _dirAccess. fileAcDrive ][ bp ]
        mov dx, word ptr [ _dirAccess. fileAcDirCluster. _low  ][ bp ]
        mov cx, word ptr [ _dirAccess. fileAcDirCluster. _high ][ bp ]
        lea bx, offset _diskAccess [ bp ]               ; pointer to access block
        call initdiskAccess                             ; [ax] is drive, [cx:dx] is cluster

        mov ax, word ptr [ _structLongFilename. _lfnOffsetShortEntry. _low  ][ bp ]
        mov dx, word ptr [ _structLongFilename. _lfnOffsetShortEntry. _high ][ bp ]
        sub ax, word ptr [ _structLongFilename. _lfnOffsetLongEntry. _low  ][ bp ]
        sbb dx, word ptr [ _structLongFilename. _lfnOffsetLongEntry. _high ][ bp ]
        mov cx, sizeDIRENTRY
        div cx                                          ; number of entries - 1
        inc ax
        storarg _numentries, ax

        mov ax, word ptr [ _structLongFilename. _lfnOffsetLongEntry. _low  ][ bp ]
        mov dx, word ptr [ _structLongFilename. _lfnOffsetLongEntry. _high ][ bp ]
        sub ax, sizeDIRENTRY
        sbb dx, 0000

        lea di, offset [ _diskAccess ][ bp ]            ; dir access
        mov word ptr [ diskAcPosition. _low  ][ di ], ax
        mov word ptr [ diskAcPosition. _high ][ di ], dx

LFNDeleteFile_22:
        call _ReadNextDirEntry                          ; read entry
        jc LFNDeleteFile_28                             ; if error -->

        mov byte ptr es:[ bx ], DIRENTRY_DELETED
        call SetCCBChanged                              ; say buffer changed

        dec word ptr [ _numentries ][ bp ]              ; more to go ?
        jnz LFNDeleteFile_22                            ; yes -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  make persitent
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNDeleteFile_28:
        mov ax, word ptr [ _dirAccess. fileAcDrive      ][ bp ]
        call updateDriveBuffers
        or ax, ax                                       ; return no error

LFNDeleteFile_32:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  7143h Change File mode                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  ds:dx pointer to subdirectory                                ;
        ;                                                               ;
        ;  bl    action                                                 ;
        ;        00  return file attributes in CX                       ;
        ;        01  set file attributes                                ;
        ;        02  get physical size of compressed file               ;
        ;        03  set last read/write time                           ;
        ;              di   last write date                             ;
        ;              cx   last write time                             ;
        ;        04  get last read/write time                           ;
        ;              di   last write date                             ;
        ;              cx   last write time                             ;
        ;        05  set last access date                               ;
        ;              di   last access date                            ;
        ;              cx   last access time                            ;
        ;        06  get last access date                               ;
        ;              di   last access date                            ;
        ;              cx   last access time                            ;
        ;        07  set creation date/ time                            ;
        ;              di   last access date                            ;
        ;              cx   last access time                            ;
        ;        08  get creation date/ time                            ;
        ;              di   last access date                            ;
        ;              cx   last access time                            ;
        ;              si   hundredths (10-millisecond units past       ;
        ;                     time in cx, 0-199)                        ;
        ;                                                               ;
        ;...............................................................;

Int71LFNChangeFileMode:

        Entry
        def  _action, bx                                ; action to take
        def  _filedate, di                              ; file date (if to set)
        def  _filetime, cx                              ; file time (if to set)
        ddef _pathname, es, dx                          ; arg passed internally as es:dx
        ddef _cluster
        defbytes _dirAccess, sizeDIRACCESS
        defbytes _structLongFilename, sizeSTRUCTLONGFILENAME
        
        call CtrlC_Check                                ; look-ahead CtrlC Check

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  parse file name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        mov ax, (FILE_NODEVICENAME)
        lea di, offset [ _dirAccess ][ bp ]             ; dir access
        getdarg es, si, _pathname                       ; arg passed internally as es:dx
        call LFNLocateFile                              ; check file path
        stordarg _cluster, cx, dx                       ; save cluster address
        jc LFNChangeFileMode_18                         ; if file/path not valid -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if found, either get or set the attribute 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        les di, dword ptr [ _dirAccess. fileAcBufferPtr ][ bp ]
        mov bx, word ptr [ _dirAccess. fileAcDirOffset ][ bp ]

        getarg ax, _action                              ; action to take
        Goto 00, LFNChangeFileMode_GetAttrib            ; get
        Goto 01, LFNChangeFileMode_SetAttrib            ; set

        SetError pexterrInvalidFunction, LFNChangeFileMode_18 ; else -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if get file attributes
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNChangeFileMode_GetAttrib:
        RetCallersStackFrame ds, si
        mov cx, ATTR_DIRECTORY
        mov ax, es
        or ax, ax                                       ; if no dir entry, 
        jz LFNChangeFileMode_GetAttrib_08               ; then it must be root dir ->
        mov cl, byte ptr es:[ deAttributes ][ di + bx ]

LFNChangeFileMode_GetAttrib_08:
        mov word ptr [ _CX ][ si ], cx
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if set file attributes
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNChangeFileMode_SetAttrib:
        mov ax, es
        or ax, ax                                       ; root dir ?
        jnz LFNChangeFileMode_SetAttrib_08              ; anything but root -->
        SetError pexterrPathNotFound, LFNChangeFileMode_18

LFNChangeFileMode_SetAttrib_08:
        mov cx, word ptr [ _attributes ][ bp ]
        test cx, not ATTR_SETTABLE
        jz LFNChangeFileMode_12
        SetError pexterrAccessDenied, LFNChangeFileMode_18

LFNChangeFileMode_12:
        mov al, byte ptr es:[ deAttributes ][ di + bx ]
        and al, ATTR_SETTABLE
        cmp al, cl
        jz LFNChangeFileMode_16                           ; if no change -->

        and byte ptr es:[ deAttributes ][ di + bx ], not ATTR_SETTABLE
        or byte ptr es:[ deAttributes ][ di + bx ], cl
        call CCBChanged                                 ; update changed buffer

LFNChangeFileMode_16:
        clc

LFNChangeFileMode_18:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  7147h Get Current Directory                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  dl    drive                                                  ;
        ;  ds:si pointer to path name                                   ;
        ;                                                               ;
        ;...............................................................;

Int71LFNGetCurrentDirectory:

        Entry
        def _drive, dx
        def _actualdrive
         
        push dx                                         ; argument to drive
        call GetActualDrive                             ; actual drive ( in ax )
        storarg _actualdrive, ax                        ; actual drive
        jc LFNGetCurrDir_36                             ; if error -->

        call ptrLFNCurrentDirectory                     ; es:si ptr to curr directory
        push es                                         ; source is at es:

        RetCallersStackFrame ds, bx
        mov es, word ptr [ _DataSegment ][ bx ]
        mov di, word ptr [ _SI ][ bx ]
        pop ds                                          ; copy source segment to ds:

LFNGetCurrDir_22:
        lodsb                                           ; copy buffer
        stosb
        or al, al
        jnz LFNGetCurrDir_22

LFNGetCurrDir_36:
        Return                                          ; ds:si will be returned.

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  714Bh Execute Program                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  ds:dx pointer to path name                                   ;
        ;  es:bx address of LOADEXEC structure                          ;
        ;  al    mode (subfunction)                                     ;
        ;...............................................................;

Int71LFNExecuteProgram:

        Entry
        ddef _filename, es, dx                          ; arg passed internally as es:dx

        defbytes _dirAccess, sizeDIRACCESS
        defbytes _structLongFilename, sizeSTRUCTLONGFILENAME

        call CtrlC_Check                                ; look-ahead CtrlC Check

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
;  lookup
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        mov si, dx                                      ; name from caller
        lea di, offset [ _dirAccess ][ bp ]             ; dir access
        mov ax, (FILE_NODEVICENAME + FILECANNOT_BEDIRECTORY)
        call LFNLocateFile                              ; locate file
        jc LFNExecuteProgram_18                         ; if file/path invalid -->

        setDS ss
        setES ss
        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        lea di, offset [ _dirAccess. fileAcExpandedName ][ bp ]
        call LFNBuildFullyFormedPATH                    ; copy fully formed path name
        mov word ptr [ _dirAccess. fileAcNameOffset ][ bp ], dx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
;  pass dir access pointer
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        push ds
        RetCallersStackFrame ds, si
        mov bx, word ptr [ _BX           ][ si ]        ; get es: bx from caller
        mov es, word ptr [ _ExtraSegment ][ si ]
        pop ds

        xor ax, ax
        lea di, offset [ _dirAccess ][ bp ]             ; work dir access block
        call LoadProgram

LFNExecuteProgram_18:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  714Eh Find First File                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ; Usage:                                                        ;
        ;  cl    allowable attributes                                   ;
        ;  ch    required attributes                                    ;
        ;  si    date/time format                                       ;
        ;         0000   use 64-bit extended date time format           ;
        ;         0001   use MSDOS date time format                     ;
        ;  ds:dx search spec (* and *.* are the same)                   ;
        ;  es:di finddata record                                        ;
        ;                                                               ;
        ; Returns:                                                      ;
        ;  ax    search file handle                                     ;
        ;  cx    0000                                                   ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ; The first locate file call is designed to  validate drive and ;
        ; path. It returns a pointer to the correct directory.          ;
        ;                                                               ;
        ; This information  is  used to  build a  pseudo SFT entry. The ;
        ; entry contains the last search term, and drive/directory/ dir ;
        ; offset entries.                                               ;
        ;                                                               ;
        ; The pseudo SFT entry is passed to LFNLocateFile and LFNLocate-;
        ; FileNext to filter entries by the same  text  search  pattern ;
        ; and attributes.                                               ;
        ;                                                               ;
        ; Last name in search must not exceed MAX_PATH entries.         ;
        ; A pool of possible names is available at PathsPool.           ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ; The application MUST close the file handle with function      ;
        ; 71A1h as soon as it has completed its search                  ;
        ;                                                               ;
        ;...............................................................;

Int71LFNFindFirstFile:

        Entry
        def  _mustMatchAttributes, 0000
        def  _searchAttributes, 0000
        def  _datetimeformat, si
        def  _LFNAllocHandle, 0000
        def  _LFNTempStorePtr, 0000

        ddef _pathname, es, dx                          ; arg passed internally as es:dx
        ddef _finddata, es, di

        call CtrlC_Check                                ; look-ahead CtrlC Check

        and cx, 1f1eh
        mov byte ptr [ _mustMatchAttributes ][ bp ], ch
        mov byte ptr [ _searchAttributes ][ bp ], cl

        RetCallersStackFrame ds, bx
        mov dx, word ptr [ _DataSegment  ][ bx ]        ; ds:dx ptr to search name
        mov ax, word ptr [ _DX           ][ bx ]        ; ds:dx ptr to search name
        stordarg _pathname, dx, ax

        mov es, word ptr [ _ExtraSegment ][ bx ]        ; es:di find data structure
        mov di, word ptr [ _DI           ][ bx ]        ; es:di find data structure
        stordarg _finddata, es, di

        clearMemory sizeFINDDATA_MIN

        getdarg es, di, _pathname
        mov cx, -1
        xor ax, ax
        repnz scasb

        not cx
        dec cx
        cmp cx, size _lfnfindstoreFindText              ; is name too long ?
        ifnc LFNFindFirst_NoMoreFiles                   ; yes, abort now -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  allocate sft
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setES ss
        setDS ss

        call LFNAllocateFindDataStore                   ; allocate a store LFN
        ifc LFNFindFirst_NoHandlesAvailable             ; if none available -->
        storarg _LFNAllocHandle, bx
        storarg _LFNTempStorePtr, di

        getdarg ds, si, _pathname                       ; get source text pointer
        lea di, offset [ _lfnfindstoreFindText ][ di ]
        call CopyString                                 ; copy string

        setDS ss
        getarg di, _LFNTempStorePtr
        lea bx, offset [ _lfnfindstoreLFNStruc ][ di ]  ; long filename, canonical name struct
        call initStructLongFilename

        mov ax, FILEHAS_WILDCHARS + FILTER_ONATTRIBUTES ; must filter on attributes
        mov cx, word ptr [ _searchAttributes ][ bp ]
        mov byte ptr [ _lfnSearchAttribs ][ bx ], cl
        mov cx, word ptr [ _mustMatchAttributes ][ bp ]
        mov byte ptr [ _lfnMustMatchAttribs ][ bx ], cl

        getarg di, _LFNTempStorePtr
        lea si, offset [ _lfnfindstoreFindText ][ di ]  ; file name
        lea di, offset [ _lfnfindstoreDirAccess ][ di ]
        call LFNLocateFile                              ; check file path
        ifc LFNFindFirst_NoMoreFiles                    ; if item not found -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return values in findata argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setDS ss
        getdarg es, di, _finddata
        getarg bx, _LFNTempStorePtr                     ; ptr to find store data
        lea di, offset [ findDataShortFilename ][ di ]
        lea si, offset [ _lfnfindstoreDirAccess. fileAcShortName ][ bx ]
        call CopyString                                 ; copy short filename string

        mov di, word ptr [ _finddata. _pointer ][ bp ]
        lea di, offset [ findDataLongFilename ][ di ]
        lea si, offset [ _lfnfindstoreDirAccess. fileAcExpandedName ][ bx ]
        cmp byte ptr [ si ], 00                         ; long name available ?
        jnz LFNFindFirst_12                             ; no -->
        lea si, offset [ _lfnfindstoreDirAccess. fileAcShortName ][ bx ]

LFNFindFirst_12:
        call CopyStringLowercase                        ; copy long filename string

        getdarg es, di, _finddata
        and word ptr [ _lfnfindstoreDirAccess. fileAcDrive ][ bx ], 7FFFh
        mov si, word ptr [ _lfnfindstoreDirAccess. fileAcBufferPtr. _pointer ][ bx ]
        add si, word ptr [ _lfnfindstoreDirAccess. fileAcDirOffset ][ bx ]
        mov ds, word ptr [ _lfnfindstoreDirAccess. fileAcBufferPtr. _segment ][ bx ]

        mov cx, word ptr [ deAttributes ][ si ]
        mov word ptr es:[ findDataAttributes ][ di ], cx

        mov dx, word ptr [ deTime       ][ si ]
        mov cx, word ptr [ deDate       ][ si ]
        mov word ptr es:[ findDataCreateTime. finddataTime ][ di ], dx
        mov word ptr es:[ findDataCreateTime. finddataDate ][ di ], cx

        mov cx, word ptr [ deFileSize. _high ][ si ]
        mov dx, word ptr [ deFileSize. _low  ][ si ]
        mov word ptr es:[ findDataFileSizeLow. _high ][ di ], cx
        mov word ptr es:[ findDataFileSizeLow. _low  ][ di ], dx

        clc
        getarg ax, _LFNAllocHandle                      ; find handle

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNFindFirst_Return:
        RetCallersStackFrame ds, bx
        mov word ptr [ _AX  ][ bx ], ax                 ; file handle
        mov word ptr [ _CX  ][ bx ], 0000               ; no Unicode support issues

        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Errors
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNFindFirst_NoHandlesAvailable:
        SetError pexterrNoHandlesAvailable, LFNFindFirst_ErrorCleanUp

LFNFindFirst_NoMoreFiles:
        SetError pexterrNoMoreFiles, LFNFindFirst_ErrorCleanUp

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Errors: Clean up allocations
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNFindFirst_ErrorCleanUp:
        getarg bx, _LFNAllocHandle
        or bx, bx
        jz LFNFindFirst_Error08
        mov word ptr [ bx ], 0000                       ; item is released

LFNFindFirst_Error08:
        stc
        jmp LFNFindFirst_Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  714Fh Find Next File                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ; Usage:                                                        ;
        ;  bx    search file handle                                     ;
        ;  si    date/time format                                       ;
        ;         0000   use 64-bit extended date time format           ;
        ;         0001   use MSDOS date time format                     ;
        ;  es:di finddata record                                        ;
        ;                                                               ;
        ;...............................................................;

Int71LFNFindNextFile:

        Entry
        def  _LFNAllocHandle, bx
        def  _LFNTempStorePtr
        def  _datetimeformat, si
        ddef _finddata, es, di

        call CtrlC_Check                                ; look-ahead CtrlC Check

        call LFNValidateFindDataHandle                  ; validate handle in bx
        ifc LFNFindNext_NoMoreFiles                     ; not valid -->

        mov bx, word ptr [ bx ]                         ; ptr to find store data
        storarg _LFNTempStorePtr, bx

        push ds
        RetCallersStackFrame ds, bx
        mov es, word ptr [ _ExtraSegment ][ bx ]        ; es:di find data structure
        mov di, word ptr [ _DI           ][ bx ]        ; es:di find data structure
        stordarg _finddata, es, di

        clearMemory sizeFINDDATA_MIN
        pop ds

        getarg di, _LFNTempStorePtr                     ; ptr to find store data
        lea bx, offset [ _lfnfindstoreLFNStruc ][ di ]  ; LFN 
        mov dx, word ptr [ _lfnOffsetShortEntry. _low ][ bx ]
        mov cx, word ptr [ _lfnOffsetShortEntry. _high ][ bx ]
        add dx, sizeDIRENTRY
        adc cx, 0000

        lea si, offset [ _lfnfindstoreFindText ][ di ]  ; file name
        lea di, offset [ _lfnfindstoreDirAccess ][ di ] ; dir access 
        call LFNLocateFileNext                          ; go search
        ifc LFNFindNext_NoMoreFiles                     ; if item not found -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return values in findata argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setDS ss
        getdarg es, di, _finddata
        getarg bx, _LFNTempStorePtr                     ; ptr to find store data
        lea di, offset [ findDataShortFilename ][ di ]
        lea si, offset [ _lfnfindstoreDirAccess. fileAcShortName ][ bx ]
        call CopyString                                 ; copy short filename string

        mov di, word ptr [ _finddata. _pointer ][ bp ]
        lea di, offset [ findDataLongFilename ][ di ]
        lea si, offset [ _lfnfindstoreDirAccess. fileAcExpandedName ][ bx ]
        cmp byte ptr [ si ], 00                         ; long name available ?
        jnz LFNFindNext_12                              ; no -->
        lea si, offset [ _lfnfindstoreDirAccess. fileAcShortName ][ bx ]

LFNFindNext_12:
        call CopyStringLowercase                        ; copy long filename string

        getdarg es, di, _finddata
        and word ptr [ _lfnfindstoreDirAccess. fileAcDrive ][ bx ], 7FFFh
        mov si, word ptr [ _lfnfindstoreDirAccess. fileAcBufferPtr. _pointer ][ bx ]
        add si, word ptr [ _lfnfindstoreDirAccess. fileAcDirOffset ][ bx ]
        mov ds, word ptr [ _lfnfindstoreDirAccess. fileAcBufferPtr. _segment ][ bx ]

        mov cx, word ptr [ deAttributes ][ si ]
        mov word ptr es:[ findDataAttributes ][ di ], cx

        mov dx, word ptr [ deTime       ][ si ]
        mov cx, word ptr [ deDate       ][ si ]
        mov word ptr es:[ findDataCreateTime. finddataTime ][ di ], dx
        mov word ptr es:[ findDataCreateTime. finddataDate ][ di ], cx

        mov cx, word ptr [ deFileSize. _high ][ si ]
        mov dx, word ptr [ deFileSize. _low  ][ si ]
        mov word ptr es:[ findDataFileSizeLow. _high ][ di ], cx
        mov word ptr es:[ findDataFileSizeLow. _low  ][ di ], dx

        clc

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNFindNext_Return:
        RetCallersStackFrame ds, bx
        mov word ptr [ _CX  ][ bx ], 0000               ; no Unicode support issues

        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Errors
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNFindNext_NoMoreFiles:
        SetError pexterrNoMoreFiles, LFNFindNext_Return

LFNFindNext_InvalidHandle:
        SetError pexterrInvalidHandle, LFNFindNext_Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  7156h Rename File                                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  ds:dx ASCIZ old file or directory name                       ;
        ;  es:di ASCIZ new file or directory name                       ;
        ;...............................................................;

Int71LFNRenameFile:

        Entry
        ddef _oldfilename, es, dx                       ; old filename
        ddef _oldnameDirOffset
        def  _deleteentries

        defbytes _diskAccess, sizeDISKACCESS
        defbytes _existfileAccess, sizeDIRACCESS
        defbytes _renfileAccess, sizeDIRACCESS
        defbytes _OldDirEntry, sizeDIRENTRY
        defbytes _structLongFilename, sizeSTRUCTLONGFILENAME

        call CtrlC_Check                                ; look-ahead CtrlC Check

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  does file exist ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        mov ax, (FILE_NODEVICENAME + FILECANNOT_BEDIRECTORY)
        lea di, offset [ _existfileAccess ][ bp ]       ; dir access
        getdarg es, si, _oldfilename                    ; arg passed internally as es:dx
        call LFNLocateFile                              ; check file path
        ifc LFNRenameFile_40                            ; if file does not exist -->

        lea di, offset [ _structLongFilename ][ bp ]
        mov dx, word ptr [ _lfnOffsetLongEntry. _low  ][ di ]
        mov cx, word ptr [ _lfnOffsetLongEntry. _high ][ di ]
        stordarg _oldnameDirOffset, cx, dx

        mov cx, word ptr [ _lfnDirEntriesReqd ][ di ]
        storarg _deleteentries, cx

        push ds
        setES ss
        lds si, dword ptr [ _existfileAccess. fileAcBufferPtr ][ bp ]
        add si, word ptr [ _existfileAccess. fileAcDirOffset ][ bp ]

        lea di, offset [ _OldDirEntry ][ bp ]
        mov cx, (sizeDIRENTRY / 2)
        rep movsw

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  does rename file exist ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        RetCallersStackFrame ds, bx
        mov es, word ptr [ _ExtraSegment ][ bx ]
        mov si, word ptr [ _DI           ][ bx ]        ; get user's parameter
        pop ds

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        mov ax, (FILE_NODEVICENAME + FILECANNOT_BEDEFINED + FILECANNOT_BEDIRECTORY)
        lea di, offset [ _renfileAccess ][ bp ]         ; dir access
        call LFNLocateFile                              ; check file path
        ifc LFNRenameFile_40                            ; if file does not exist -->

        mov al, byte ptr [ _existfileAccess. fileAcDrive ][ bp ]
        cmp al, byte ptr [ _renfileAccess. fileAcDrive ][ bp ]
        mov ax, offset pexterrPathNotFound
        stc
        ifnz LFNRenameFile_40                           ; cannot move across drives -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  both files seem ok. Create new entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ _existfileAccess. fileAcDrive ][ bp ]
        mov dx, word ptr [ _existfileAccess. fileAcDirCluster. _low  ][ bp ]
        mov cx, word ptr [ _existfileAccess. fileAcDirCluster. _high ][ bp ]
        lea di, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call LFNBuildDirectoryEntry                     ; valid empty entry ?
        jc LFNRenameFile_40                             ; if can't find free slot -->

        mov di, bx
        add di, sizeFilename
        lea si, offset [ _OldDirEntry. sizeFilename ][ bp ]
        mov cx, sizeDIRENTRY - sizeFilename
        rep movsb                                       ; copy allocation/ date details

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  place delete marks on old name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ _existfileAccess. fileAcDrive ][ bp ]
        mov dx, word ptr [ _existfileAccess. fileAcDirCluster. _low  ][ bp ]
        mov cx, word ptr [ _existfileAccess. fileAcDirCluster. _high ][ bp ]
        lea bx, offset _diskAccess [ bp ]               ; pointer to access block
        call initdiskAccess                             ; [ax] is drive, [cx:dx] is cluster

        setES ss
        mov dx, -sizeDIRENTRY
        mov cx, -1
        add dx, word ptr [ _oldnameDirOffset. _low  ][ bp ]
        adc cx, word ptr [ _oldnameDirOffset. _high ][ bp ]
        mov word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ], dx
        mov word ptr [ _diskAccess. diskAcPosition. _high ][ bp ], cx 

        inc word ptr [ _deleteentries ][ bp ]

LFNRenameFile_22:
        lea di, offset [ _diskAccess ][ bp ]
        call _ReadNextDirEntry                          ; read directory sector
        jc LFNRenameFile_38                             ; if error -->

        mov byte ptr es:[ bx ], DIRENTRY_DELETED
        call SetCCBChanged                              ; say buffer changed

        dec word ptr [ _deleteentries ][ bp ]           ; more to go ?
        jnz LFNRenameFile_22                            ; yes -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNRenameFile_38:
        mov ax, word ptr [ _existfileAccess. fileAcDrive ][ bp ]
        call updateDriveBuffers
        or ax, ax                                       ; no carry

LFNRenameFile_40:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  7160h Get Actual Filename                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   cl     minor code                                           ;
        ;          0   Get Full Path Name                               ;
        ;          1   Get Short Path Name                              ;
        ;          2   Get Long Path Name                               ;
        ;                                                               ;
        ;   ch     substexpand                                          ;
        ;          not supported in RxDOS                               ;
        ;                                                               ;
        ;   ds:si  source path (null terminated)                        ;
        ;   es:di  dest path (null terminated)                          ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:di  long, short, or full path in dest field              ;
        ;   cy     if error detected                                    ;
        ;                                                               ;
        ;...............................................................;

Int71LFNGetActualFileName:

        Entry
        ddef _filename, es, si
        ddef _pOutputString

        defbytes _dirAccess, sizeDIRACCESS
        defbytes _structLongFilename, sizeSTRUCTLONGFILENAME

        RetCallersStackFrame es, bx
        mov dx, word ptr es:[ _ExtraSegment ][ bx ]     ; expanded filename
        mov ax, word ptr es:[ _DI ][ bx ]
        stordarg _pOutputString, dx, ax

        mov si, word ptr es:[ _SI ][ bx ]
        mov es, word ptr es:[ _DataSegment ][ bx ]      ; es:si ptr to unexpanded name

        setDS ss
        mov al, cl
        Goto LFNEXPAND_GETFULLPATHNAME,    LFN_GetFullPathName
        Goto LFNEXPAND_GETSHORTPATHNAME,   LFN_GetShortPathName
        Goto LFNEXPAND_GETLONGPATHNAME,    LFN_GetLongPathName

        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Expand Name and Path                                         ;
        ;...............................................................;

LFN_GetFullPathName:
LFN_GetLongPathName:

        mov ax, (FILEHAS_WILDCHARS + FILEMAY_EXIST)
        lea di, word ptr [ _structLongFilename ][ bp ]
        lea di, word ptr [ _lfnCanonicalList ][ di ]
        call BuildCanonicalPointerList                  ; build canonical pointer list
        jc LFN_GetFullPathName_38                       ; if path invalid -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  set drive letter
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getdarg es, di, _pOutputString
        add al, 'A'                                     ; drive letter in Ascii
        stosb                                           ; store drive letter

        mov ax, '\:'
        stosw                                           ; append ':\'

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  set to copy directories (preserve es:di)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea bx, word ptr [ _structLongFilename ][ bp ]
        mov dx, word ptr ss:[ _lfnCanonicalList. _canpNumEntries ][ bx ]

        lea bx, offset [ _lfnCanonicalList. _canpArray ][ bx ]

LFN_GetFullPathName_22:
        or dx, dx                                       ; more entries to go ?
        jz LFN_GetFullPathName_26                       ; no -->
        
        mov ds, word ptr ss:[ _canpStringPointer. _segment ][ bx ]
        mov si, word ptr ss:[ _canpStringPointer. _pointer ][ bx ]
        mov cx, word ptr ss:[ _canpStringLength ][ bx ] ; string length
        rep movsb                                       ; copy to es: di

        mov al, '\'
        stosb                                           ; if more arguments follow

        add bx, offset sizeCANONICALPOINTER
        dec dx
        jnz LFN_GetFullPathName_22

        dec di                                          ; backup over last \

LFN_GetFullPathName_26:
        xor ax, ax                                      ; clear carry
        stosb                                           ; store null terminator

LFN_GetFullPathName_38:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Short Path Name                                          ;
        ;...............................................................;

LFN_GetShortPathName:

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        xor ax, ax
        lea di, offset [ _dirAccess ][ bp ]             ; dir access
        call LFNLocateFile                              ; check file path
        jc LFN_GetShortPathName_38                      ; if not found, exit -->

        getdarg es, di, _pOutputString
        lea si, offset [ _structLongFilename. _lfnShortFCBName ][ bp ]

LFN_GetShortPathName_12:
        lodsb
        stosb
        or al, al                                       ; copy string 
        jnz LFN_GetShortPathName_12

LFN_GetShortPathName_38:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  716Ch Extended Open Create                                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   bx     open access mode flags                               ;
        ;   cx     attributes                                           ;
        ;   dx     action                                               ;
        ;   di     hints                                                ;
        ;   ds:si  asciz name of file                                   ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     handle (or error code)                               ;
        ;   cx     action taken                                         ;
        ;   cy     if error detected                                    ;
        ;                                                               ;
        ;...............................................................;

Int71LFNExtendedOpenCreate:

        Entry
        def  _mode, bx
        def  _attributes, cx
        def  _action, dx
        def  _found, FALSE

        ddef _sft
        ddef _filename, es, si
        defbytes _dirAccess, sizeDIRACCESS
        defbytes _structLongFilename, sizeSTRUCTLONGFILENAME

        call CtrlC_Check                                ; look-ahead CtrlC Check
        call VerifyAvailableHandle                      ; see if handle available
        ifc _LFNExtendedOpenCreate_42                   ; if error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  determine if file exists
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea bx, offset [ _structLongFilename ][ bp ]    ; long filename, canonical name struct
        call initStructLongFilename

        mov ax, (FILECANNOT_BEDIRECTORY)
        lea di, offset [ _dirAccess ][ bp ]             ; dir access
        getdarg es, si, _filename
        call LFNLocateFile                              ; check file path
        jnc _LFNExtendedOpenCreate_Open                 ; if file found, open -->

        storarg _found, FALSE
        test word ptr [ _action ][ bp ], EXTENDEDACTION_CREATE
        jnz _LFNExtendedOpenCreate_Create
        stc                                             ; else set error
        jmp _LFNExtendedOpenCreate_42                   ; if not create, call fails -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  open file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_LFNExtendedOpenCreate_Open:
        storarg _found, TRUE

        test word ptr [ _action ][ bp ], EXTENDEDACTION_TRUNCATE
        jz _LFNExtendedOpenCreate_30                    ; ok to create/ truncate -->

        mov ax, word ptr [ _dirAccess. fileAcDrive ][ bp ]
        mov dx, word ptr [ _dirAccess. fileAcCluster. _low ][ bp ]
        mov cx, word ptr [ _dirAccess. fileAcCluster. _high ][ bp ]
        call ReleaseClusterChain

        mov es, word ptr [ _dirAccess. fileAcBufferPtr. _segment ][ bp ]
        mov bx, word ptr [ _dirAccess. fileAcBufferPtr. _pointer ][ bp ]
        add bx, word ptr [ _dirAccess. fileAcDirOffset ][ bp ]

        mov word ptr es:[ deStartCluster    ][ bx ], 0000
        mov word ptr es:[ deStartClusterExt ][ bx ], 0000
        mov word ptr es:[ deFileSize. _low  ][ bx ], 0000
        mov word ptr es:[ deFileSize. _high ][ bx ], 0000

        mov di, word ptr [ _dirAccess. fileAcBufferPtr. _pointer ][ bp ]
        call CCBChanged                                 ; mark changes made

_LFNExtendedOpenCreate_30:
        mov ax, word ptr [ _mode ][ bp ]
        lea di, offset [ _dirAccess ][ bp ]             ; work dir access block
        call _SFTOpenFile                               ; build an SFT
        jc _LFNExtendedOpenCreate_42                    ; if error -->

        call MapSFTtoAppHandle                          ; record SFT handle into JHT
        mov cx, ACTION_OPENED                           ; say openned
        jmp short _LFNExtendedOpenCreate_42

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  create file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_LFNExtendedOpenCreate_Create:
        getarg cx, _attributes
        lea di, offset [ _dirAccess ][ bp ]             ; work dir access block
        lea si, offset [ _structLongFilename ][ bp ]
        call LFNCreateFile
        jc _LFNExtendedOpenCreate_42                    ; if error -->

        call MapSFTtoAppHandle                          ; record SFT handle into JHT

        mov cx, ACTION_REPLACED_OPENED                  ; say replaced
        cmp word ptr [ _found ][ bp ], TRUE
        jz _LFNExtendedOpenCreate_42                    ; if found, then was truncated ->
        mov cx, ACTION_CREATED_OPENED
        clc                                             ; no carry

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_LFNExtendedOpenCreate_42:
        RetCallersStackFrame es, bx
        mov word ptr es:[ _AX ][ bx ], AX
        jc _LFNExtendedOpenCreate_Return

        mov word ptr es:[ _CX ][ bx ], CX

_LFNExtendedOpenCreate_Return:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  71A0h Get Volume Information                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;  	ds:dx  asciz root name (e.g. "c:\")                         ;
        ;  	es:di  buffer for asciz name of file system                 ;
        ;  	cx     size of es:di buffer                                 ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   bx     file system flags                                    ;
        ;   cx     maximum length of file name [usually 255]            ;
        ;   dx     maximum length of path [usually 260]                 ;
        ;   es:di  asciz name of file system (e.g. "FAT","NTFS","CDFS") ;
        ;                                                               ;
        ;...............................................................;

Int71LFNGetVolumeInformation:

        RetCallersStackFrame es, bx
        mov word ptr es:[ _BX ][ bx ], 4000h            ; supports long filename
        mov word ptr es:[ _CX ][ bx ], 255              ; max name 255
        mov word ptr es:[ _DX ][ bx ], 260              ; supports long filename

        mov ds, word ptr es:[ _ExtraSegment ][ bx ]
        mov di, word ptr es:[ _DI ][ bx ]
        mov word ptr [ di ], 'AF'
        mov word ptr [ di + 2 ], 'T'                    ; FAT
        clc
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  71A1h Find Close                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   bx     handle                                               ;
        ;                                                               ;
        ;...............................................................;

Int71LFNFindClose:

        call LFNValidateFindDataHandle                  ; validate handle in bx
        jc Int71LFNFindClose_12                         ; not valid -->

        mov si, word ptr [ bx ]
        mov word ptr [ _lfnfindstoreOwner ][ si ], 0000
        mov word ptr [ bx ], 0000                       ; within range, so close

Int71LFNFindClose_12:
        clc
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  71A6h Get File Info By Handle                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;...............................................................;

Int71LFNGetFileInfoByHandle:

        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  71A7h File Time                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ; Function 71A7h Minor Code 0h File Time To DOS Time            ;
        ; Function 71A7h Minor Code 1h DOS Time To File Time            ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   bl     Code selector                                        ;
        ;   si     LFN File Time buffer                                 ;
        ;   cx     Dos Directory Time                                   ;
        ;   dx     Dos Directory Date                                   ;
        ;                                                               ;
        ;...............................................................;

Int71LFNFileTime:

        Entry
        def  _DOSMilliseconds
        def  _DOSDate, dx
        def  _DOSTime, cx
        ddef _pFileTime, es, si

        defbytes _tFileTime, 8

        mov al, bl                                      ; minor code
        xchg bh, bl
        xor bh, bh
        storarg _DOSMilliseconds, bx                    ; save milliseconds

        Goto 00,               _LFNFileTimeToDOSTime
        Goto 01,               _LFNDOSTimeToFileTime

        RetCallersStackFrame es, bx
        mov word ptr es:[ _AX ][ bx ], 7100h
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  File Time to DOS Time
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_LFNFileTimeToDOSTime:

        setES ss
        getdarg ds, si, _pFileTime
        lea di, offset [ _tFileTime ][ bp ]
        mov cx, 4
        rep movsw

        mov cx, 10000
        lea di, offset [ _tFileTime ][ bp ]
        call Divide64Bits                               ; convert FILETIME from nanoseconds to microseconds

        mov cx, 1000
        call Divide64Bits                               ; convert FILETIME from microseconds to milliseconds

        mov dx, word ptr [ _tFileTime. _low ][ bp ]
        mov cx, word ptr [ _tFileTime. _high ][ bp ]

IF 0
;   BH       Number of 10 millisecond intervals in 2 seconds to add to the MS-DOS time. 
;            It can be a value in the range 0 to 199. 
; 
;   CX       MS-DOS time. The time is a packed 16-bit value with the following form: 
;
;            Bits     Contents 
;             0-4     Second divided by 2 
;             5-10    Minute (0-59) 
;            11-15    Hour (0-23 on a 24-hour clock) 
; 
;   DX       MS-DOS date. The date is a packed 16-bit value with the following form: 
; 
;            Bits     Contents 
;             0-4     Day of the month (1-31) 
;             5-8     Month (1 = January, 2 = February, and so on) 
;             9-15    Year offset from 1980 (that is, add 1980 to get the actual year) 
;
ENDIF

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  DOS Time to File Time
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_LFNDOSTimeToFileTime:

        mov dx, word ptr [ _tFileTime. _low ][ bp ]
        mov cx, word ptr [ _tFileTime. _high ][ bp ]
      ; add constant

        mov cx, 10000
        lea di, offset [ _tFileTime ][ bp ]
      ; call Multiply64Bits                             ; convert FILETIME from nanoseconds to microseconds

        mov cx, 1000
      ; call Multiply64Bits                             ; convert FILETIME from microseconds to milliseconds

        getdarg es, di, _pFileTime
        lea si, offset [ _tFileTime ][ bp ]
        mov cx, 4
        rep movsw

        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Divide 64 Bits                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   di     64 bit value                                         ;
        ;   cx     Divisor                                              ;
        ;                                                               ;
        ;...............................................................;

Divide64Bits:

        or cx, cx                                       ; protect from zero divisor
        stc                                             ; in case of error
        jz _div64Return                                 ; if so, just return with carry

        xor dx, dx
        mov ax, [ di + 6 ]
        div cx
        mov [ di + 6 ], ax

        mov ax, [ di + 4 ]
        div cx
        mov [ di + 4 ], ax

        mov ax, [ di + 2 ]
        div cx
        mov [ di + 2 ], ax

        mov ax, [ di ]
        div cx
        mov [ di ], ax

_div64Return:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  71A8h Generate ShortName                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;...............................................................;

Int71LFNGenerateShortName:

        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Locate/Validate File                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This function takes an ASCIZ path name, which could contain  ;
        ;  mix of short and long filenames. It determines if the path   ;
        ;  and filename are valid. If the file exists, it completes the ;
        ;  dir access work area.                                        ;
        ;                                                               ;
        ;  The full expanded path is returned as a canonical pointer    ;
        ;  list. The last term of the list is the expanded filename.    ;
        ;  The short name to the file is available in the directory     ;
        ;  buffer.                                                      ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:bx  pointer to Longfilename structure                    ;
        ;   ss:di  pointer to Dir Access area                           ;
        ;   es:si  pointer to input filename                            ;
        ;   ax     options, as follows:                                 ;
        ;                                                               ;
        ;   FILE_NODEVICENAME        no device name allowed             ;
        ;   FILEHAS_WILDCHARS        allowed in name                    ;
        ;   FILEHAS_NOFILENAME       no filename expected               ;
        ;   FILECANNOT_BEDEFINED     filename must not exist            ;
        ;   FILECANNOT_BEDIRECTORY   filename cannot be directory       ;
        ;   FILEMAY_EXIST            file may exist (cluster not -1 )   ;
        ;   FILE_ORDEVICE            file or device must exist          ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster address of file or directory                 ;
        ;   ss:di  pointer to Dir Access area.                          ;
        ;   cy     means path/filename is not valid.                    ;
        ;                                                               ;
        ;  Assumes ss == ds                                             ;
        ;...............................................................;

LFNLocateFile:

        Entry
        def  _drive, 0000
        ddef  _cluster, 0000, 0000
        def  _optSearchOffset, 0000
        def  _options, ax

        def  _ptrstructLongfilename, bx
        def  _ptrDirAccess, di
        ddef _ptrfilename, es, si

        SaveSegments di, si, bx
        CurrSegment ds
        CurrSegment es

        getarg di, _ptrDirAccess
        clearMemory sizeDIRACCESS

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  build canonical path tree
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getdarg es, si, _ptrfilename
        getarg bx, _ptrstructLongfilename
        lea di, word ptr [ _lfnCanonicalList ][ bx ]
        call BuildCanonicalPointerList                  ; build canonical pointer list
        ifc LFNLocateFile_PathNotFound                  ; if path invalid -->

        storarg _drive, ax                              ; drive
        stordarg _cluster, cx, dx                       ; cluster
        storarg _optSearchOffset, bx                    ; opt search entry

        call getDPB                                     ; make sure drive is initialized
        ifc LFNLocateFile_PathNotFound                  ; if illegal drive -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get last entry, compute # lfn entries required for name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, sizeCANONICALPOINTER
        getarg di, _ptrstructLongfilename
        mul word ptr [ _lfnCanonicalList. _canpNumEntries ][ di ]

        mov bx, ax
        lea bx, offset [ _lfnCanonicalList. _canpArray - sizeCANONICALPOINTER ][ di ][ bx ]

        les si, dword ptr [ _canpStringPointer ][ bx ]
        mov word ptr [ _lfnSearchLongNameText. _segment ][ di ], es
        mov word ptr [ _lfnSearchLongNameText. _pointer ][ di ], si

        mov ax, word ptr [ _canpStringLength ][ bx ]
        mov word ptr [ _lfnSearchStringLength ][ di ], ax
        inc ax                                          ; make sure we count null term char as a copy char

        xor dx, dx
        mov cx, LFN_NAMECHARPERENTRY
        div cx                                          ; # entries in ax

        or dx, dx
        jz LFNLocateFile_10
        inc ax                                          ; incr if roundoff

LFNLocateFile_10:
        inc ax                                          ; incr for short entry
        mov word ptr [ _lfnDirEntriesReqd ][ di ], ax   ; required dir entries
        mov word ptr [ _lfnNameRemainder  ][ di ], dx   ; name remainder

        getarg ax, _drive                               ; get drive
        mov word ptr [ _lfnDrive          ][ di ], ax

        getdarg cx, dx, _cluster                        ; cluster to search next
        mov word ptr [ _lfnCluster. _high ][ di ], cx
        mov word ptr [ _lfnCluster. _low  ][ di ], dx

        getarg ax, _options
        mov word ptr [ _lfnOptions        ][ di ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  root directory ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, sizeCANONICALPOINTER
        mul byte ptr [ _optSearchOffset ][ bp ]          ; optimized entry

        mov di, word ptr [ _ptrstructLongfilename ][ bp ]
        lea di, word ptr [ _lfnCanonicalList. _canpArray ][ di ]
        add di, ax                                      ; offset to next entry
        storarg _pCanonicalNextEntry, di                ; pointer to next entry

        cmp word ptr [ _canpStringLength ][ di ], 0000  ; no entry ( empty, ., or .. )
        jnz LFNLocateFile_20                            ; no, go test -->

        test word ptr [ _options ][ bp ], FILEHAS_NOFILENAME
        jnz LFNLocateFile_28                            ; if can't do * name -->

        mov word ptr [ _canpStringLength ][ di ], 0001  ; patch default entry
        mov word ptr [ _canpStringPointer. _segment ][ di ], cs
        mov word ptr [ _canpStringPointer. _pointer ][ di ], offset _DefaultAll
        
        xor cx, cx
        mov word ptr [ sizeCANONICALPOINTER. _canpFlags        ][ di ], cx
        mov word ptr [ sizeCANONICALPOINTER. _canpStringLength ][ di ], cx
        mov word ptr [ sizeCANONICALPOINTER. _canpStringPointer. _segment ][ di ], cx
        mov word ptr [ sizeCANONICALPOINTER. _canpStringPointer. _pointer ][ di ], cx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  search
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFile_20:
        xor cx, cx
        xor dx, dx                                     ; dir offset will be zero
        getarg di, _ptrDirAccess
        getarg bx, _ptrstructLongfilename
        getdarg es, si, _ptrfilename
        call LFNLocateFileNext
        jc LFNLocateFile_38

    ; for next entry, update optimized dir search entry

LFNLocateFile_28:
        pushf
        SaveRegisters ax, bx, cx, dx
        getarg bx, _ptrstructLongfilename
        mov dx, word ptr [ _lfnDirCluster. _low    ][ bx ]
        mov cx, word ptr [ _lfnDirCluster. _high   ][ bx ]
        mov word ptr [ _lfnCanonicalList. _canpCluster. _low    ][ bx ], dx
        mov word ptr [ _lfnCanonicalList. _canpCluster. _high   ][ bx ], cx

    ;; do more: optimized entry and all that !!

        mov cx, word ptr [ _lfnCanonicalList. _canpNumEntries ][ bx ]
        or cx, cx
        jz LFNLocateFile_34
        dec cx

LFNLocateFile_34:
        mov word ptr [ _lfnCanonicalList. _canpOptSearchEntry ][ bx ], cx
        RestoreRegisters dx, cx, bx, ax
        popf

LFNLocateFile_38:
        RestoreSegments bx, si, di
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Errors
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFile_FileNotFound:
        SetError pexterrFileNotFound, LFNLocateFile_38

LFNLocateFile_PathNotFound:
        SetError pexterrPathNotFound, LFNLocateFile_38

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Locate/Validate File                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This function takes an ASCIZ path name, which could contain  ;
        ;  mix of short and long filenames. It determines if the path   ;
        ;  and filename are valid. If the file exists, it completes the ;
        ;  dir access work area.                                        ;
        ;                                                               ;
        ;  The full expanded path is returned as a canonical pointer    ;
        ;  list. The last term of the list is the expanded filename.    ;
        ;  The short name to the file is available in the directory     ;
        ;  buffer.                                                      ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:bx  pointer to Longfilename structure                    ;
        ;   ss:di  pointer to Dir Access area                           ;
        ;   es:si  pointer to input filename                            ;
        ;   cx:dx  position in directory where to start search          ;
        ;   ax     options, as follows:                                 ;
        ;                                                               ;
        ;   FILE_NODEVICENAME        no device name allowed             ;
        ;   FILEHAS_WILDCHARS        allowed in name                    ;
        ;   FILEHAS_NOFILENAME       no filename expected               ;
        ;   FILECANNOT_BEDEFINED     filename must not exist            ;
        ;   FILECANNOT_BEDIRECTORY   filename cannot be directory       ;
        ;   FILEMAY_EXIST            file may exist (cluster not -1 )   ;
        ;   FILE_ORDEVICE            file or device must exist          ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster address of file or directory                 ;
        ;   ss:di  pointer to Dir Access area.                          ;
        ;   cy     means path/filename is not valid.                    ;
        ;                                                               ;
        ;  Assumes ss == ds                                             ;
        ;...............................................................;

LFNLocateFileNext:

        Entry
        def  _drive
        ddef _cluster
        def  _ptrstructLongfilename, bx
        def  _ptrDirAccess, di
        ddef _ptrfilename, es, si
        ddef _dirposition, cx, dx

        def  _options
        def  _subdirectory_term, FALSE
        def  _pCanonicalNextEntry

        ddef _ptrDirEntry
        ddef _dirCluster, 0000, 0000

        defbytes _scandir, sizeSCANDIR


        SaveSegments di, si, bx
        CurrSegment ds
        CurrSegment es

        mov ax, word ptr [ _lfnCanonicalList. _canpDrive ][ bx ]
        storarg _drive, ax

        mov dx, word ptr [ _lfnCanonicalList. _canpCluster. _low  ][ bx ]
        mov cx, word ptr [ _lfnCanonicalList. _canpCluster. _high ][ bx ]
        stordarg _cluster, cx, dx                       ; start cluster of directory

        mov ax, word ptr [ _lfnOptions ][ bx ]
        storarg _options, ax

        mov ax, sizeCANONICALPOINTER
        mul byte ptr [ _lfnCanonicalList. _canpOptSearchEntry ][ bx ]

        lea di, word ptr [ _lfnCanonicalList. _canpArray ][ bx ]
        add di, ax                                      ; offset to next entry
        storarg _pCanonicalNextEntry, di                ; pointer to next entry

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Init Scan Directory
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea di, offset [ _scandir ][ bp ]
        clearMemory sizeSCANDIR                         ; clear scan dir structure

        getarg di, _ptrDirAccess
        lea cx, offset [ fileAcExpandedName ][ di ]
        mov word ptr [ _scandir. _scandirLongFilenameStr ][ bp ], cx

        lea cx, offset [ fileAcShortName ][ di ]
        mov word ptr [ _scandir. _scandirShortFilenameStr ][ bp ], cx

        getdarg cx, dx, _dirposition
        mov word ptr [ _scandir. _scandirFileOffset. _high ][ bp ], cx
        mov word ptr [ _scandir. _scandirFileOffset. _low  ][ bp ], dx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get next name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_20:
        getarg di, _pCanonicalNextEntry                 ; get pointer to next entry
        test word ptr ss:[ _canpFlags ][ di ], CANONICALENTRY_WILDCHARS
        jz LFNLocateFileNext_26                         ; if no wild chars -->

        test word ptr [ _options ][ bp ], FILEHAS_WILDCHARS
        ifz LFNLocateFileNext_FileNotFound              ; if wild chars NOT allowed -->

        cmp word ptr ss:[ sizeCANONICALPOINTER ][ _canpStringLength ][ di ], 0000
        ifnz LFNLocateFileNext_PathNotFound             ; inside path def, wild chars error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if entry is a device name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_26:
        les si, dword ptr ss:[ _canpStringPointer][ di ]    ; string pointer
        mov cx, word ptr ss:[ _canpStringLength ][ di ]     ; string length
        call seeIfDeviceName                                ; determine if it's a device name
        jc LFNLocateFileNext_32                             ; if not a device name -->

     ;  here if device name 

        cmp word ptr ss:[ sizeCANONICALPOINTER ][ _canpStringLength ][ di ], 0000
        ifnz LFNLocateFileNext_PathNotFound                 ; inside path def, error -->
        test word ptr [ _options ][ bp ], FILE_NODEVICENAME ; else if in filename, it is allowed ?
        ifnz LFNLocateFileNext_FileNotFound                 ; error if no device names allowed -->

        clc                                                 ; clear error
        getarg si, _ptrDirAccess
        mov word ptr ss:[ fileAcDevicePtr. _segment ][ si ], es
        mov word ptr ss:[ fileAcDevicePtr. _pointer ][ si ], bx
        mov byte ptr ss:[ fileAcExpandedName ][ si ], 00

        lea bx, offset [ fileAcShortName ][ si ]
        mov cx, word ptr ss:[ _canpStringLength ][ di ]     ; string length
        les si, dword ptr ss:[ _canpStringPointer][ di ]    ; string pointer
        mov di, bx
        call CopyString

        mov ax, 8000h
        jmp LFNLocateFileNext_120

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  scan for path in directory
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_32:
        getarg ax, _drive                               ; drive
        getdarg cx, dx, _cluster                        ; cluster
        stordarg _dirCluster, cx, dx                    ; start cluster of directory

        getarg di, _ptrDirAccess
        mov word ptr [ fileAcDirCluster. _low  ][ di ], dx
        mov word ptr [ fileAcDirCluster. _high ][ di ], cx

        getarg bx, _ptrstructLongfilename
        mov word ptr [ _lfnDirCluster. _low  ][ bx ], dx
        mov word ptr [ _lfnDirCluster. _high ][ bx ], cx        

        getarg si, _pCanonicalNextEntry                 ; get pointer to next entry
        lea di, offset [ _scandir ][ bp ]
        call LFNScanDirectory                           ; scan for name in directory
        jnc LFNLocateFileNext_36                        ; if an item is found -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  entry not found or matched.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, _pCanonicalNextEntry                 ; get pointer to next entry
        cmp word ptr ss:[ sizeCANONICALPOINTER ][ _canpStringLength ][ di ], 0000
        ifnz LFNLocateFileNext_PathNotFound             ; inside path def, error -->

        or word ptr [ _drive ][ bp ], 8000h             ; say not found
        test word ptr [ _options ][ bp ], (FILECANNOT_BEDEFINED + FILEMAY_EXIST)
        ifnz LFNLocateFileNext_112                      ; if not defined is ok -->
        jmp LFNLocateFileNext_FileNotFound              ; not found -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  is FOUND entry a filename ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_36:
        setDS ss
        stordarg _ptrDirEntry, es, si                   ; item address

        test bx, ATTR_VOLUME
        jnz LFNLocateFileNext_52                        ; if volume label -->
        test bx, ATTR_DIRECTORY
        jnz LFNLocateFileNext_42                        ; if directory -->

        getarg di, _pCanonicalNextEntry                 ; get pointer to next entry
        cmp word ptr [ sizeCANONICALPOINTER ][ _canpStringLength ][ di ], 0000
        ifnz LFNLocateFileNext_PathNotFound              ; if path expected, error -->

        test word ptr [ _options ][ bp ], (FILECANNOT_BEDEFINED + FILEHAS_NOFILENAME)
        ifnz LFNLocateFileNext_AccessDenied              ; if access denied -->
        jmp LFNLocateFileNext_100

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  is FOUND entry a directory ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_42:
        getarg di, _pCanonicalNextEntry                 ; get pointer to next entry
        cmp word ptr [ sizeCANONICALPOINTER ][ _canpStringLength ][ di ], 0000
        jz LFNLocateFileNext_48                         ; if path not expected, determine errors -->

        getdarg es, si, _ptrDirEntry
        mov dx, word ptr es:[ deStartCluster    ][ si ] ; cluster
        mov cx, word ptr es:[ deStartClusterExt ][ si ]
        stordarg _cluster, cx, dx                       ; cluster to search next

        xor cx, cx
        mov word ptr [ _scandir. _scandirFileOffset. _low  ][ bp ], cx
        mov word ptr [ _scandir. _scandirFileOffset. _high ][ bp ], cx

        add word ptr [ _pCanonicalNextEntry ][ bp ], sizeCANONICALPOINTER 
        jmp LFNLocateFileNext_20                        ; else, go get next -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Last term. Check Attributes.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_48:
        test word ptr [ _options ][ bp ], FILECANNOT_BEDIRECTORY
        jnz LFNLocateFileNext_FileNotFound              ; if cannot be a directory -->
        test word ptr [ _options ][ bp ], FILECANNOT_BEDEFINED
        jnz LFNLocateFileNext_FileNotFound              ; if cannot be a directory -->

LFNLocateFileNext_52:
        setDS ss
        getdarg es, bx, _ptrDirEntry
        getarg di, _ptrstructLongfilename
        mov cl, byte ptr es:[ deAttributes ][ bx ]
        cmp cl, ATTR_VOLUME
        jnz LFNLocateFileNext_56                        ; compare attribs if not vol id -->

        cmp cl, byte ptr [ _lfnSearchAttribs ][ di ]
        ifz LFNLocateFileNext_100                       ; we are searching for vol id -->

LFNLocateFileNext_Continue:
        add word ptr [ _scandir. _scandirFileOffset. _low  ][ bp ], sizeDIRENTRY
        adc word ptr [ _scandir. _scandirFileOffset. _high ][ bp ], 0000
        jmp LFNLocateFileNext_32                        ; continue -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; test/ filter on attributes
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_56:
        cmp byte ptr [ _lfnSearchAttribs ][ di ], -1    ; matching on attributes ?
        jz LFNLocateFileNext_100                        ; no -->

        test cl, byte ptr [ _lfnSearchAttribs ][ di ]   ; filter on search bit
        jnz LFNLocateFileNext_58                        ; if at least one bit matches -->
        test cl, ATTR_MASK                              ; test for normal file
        jnz LFNLocateFileNext_Continue                  ; if not, then no match -->

LFNLocateFileNext_58:
        mov ch, byte ptr [ _lfnMustMatchAttribs ][ di ]
        or ch, ch
        jz LFNLocateFileNext_100                        ; if no must match, everything that matches search pattern -->

        and cl, ch
        cmp cl, ch                                      ; remainder must match 
        jz LFNLocateFileNext_100                        ; if matches -->
        jmp LFNLocateFileNext_Continue

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  in case of error
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_FileNotFound:
        SetError pexterrFileNotFound, LFNLocateFileNext_120

LFNLocateFileNext_PathNotFound:
        SetError pexterrPathNotFound, LFNLocateFileNext_120

LFNLocateFileNext_AccessDenied:
        SetError pexterrAccessDenied, LFNLocateFileNext_120

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if dir entry available, copy args
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_100:
        setDS ss
        getdarg es, si, _ptrDirEntry
        mov dx, word ptr es:[ deStartCluster    ][ si ] ; cluster
        mov cx, word ptr es:[ deStartClusterExt ][ si ]
        stordarg _cluster, cx, dx                       ; cluster to search next

        getarg di, _ptrstructLongfilename
        mov cx, word ptr es:[ deFileSize. _high ][ si ]
        mov dx, word ptr es:[ deFileSize. _low  ][ si ]
        mov word ptr [ _lfnFileSize. _high ][ di ], cx
        mov word ptr [ _lfnFileSize. _low  ][ di ], dx

        mov ax, sizeCANONICALPOINTER
        mul word ptr [ _lfnCanonicalList. _canpNumEntries ][ di ]

        mov bx, ax
        lea bx, offset [ _lfnCanonicalList. _canpArray - sizeCANONICALPOINTER ][ di ][ bx ]
        les si, dword ptr [ _canpStringPointer ][ bx ]
        mov word ptr [ _lfnSearchLongNameText. _segment ][ di ], es
        mov word ptr [ _lfnSearchLongNameText. _pointer ][ di ], si

        mov cx, word ptr [ _canpStringLength ][ bx ]
        mov word ptr [ _lfnSearchStringLength ][ di ], cx

        mov dx, word ptr [ _scandir. _scandirFileAcLongEntry. _low ][ bp ]
        mov cx, word ptr [ _scandir. _scandirFileAcLongEntry. _high ][ bp ]
        mov word ptr [ _lfnOffsetLongEntry. _low ][ di ], dx
        mov word ptr [ _lfnOffsetLongEntry. _high ][ di ], cx

        mov dx, word ptr [ _scandir. _scandirFileAcShortEntry. _low ][ bp ]
        mov cx, word ptr [ _scandir. _scandirFileAcShortEntry. _high ][ bp ]
        mov word ptr [ _lfnOffsetShortEntry. _low ][ di ], dx
        mov word ptr [ _lfnOffsetShortEntry. _high ][ di ], cx

        getdarg es, si, _ptrDirEntry
        call locateCCBPHeader                           ; si dir pointer/ di ccb

        getarg bx, _ptrDirAccess
        mov word ptr [ fileAcBufferPtr. _segment ][ bx ], es
        mov word ptr [ fileAcBufferPtr. _pointer ][ bx ], di
        mov word ptr [ fileAcDirOffset ][ bx ], cx      ; offset in dir sector

        mov ax, word ptr es:[ ccbLBN. _low     ][ di ]
        mov dx, word ptr es:[ ccbLBN. _high    ][ di ]
        mov word ptr [ fileAcDirSector. _low   ][ bx ], ax; which dir sector
        mov word ptr [ fileAcDirSector. _high  ][ bx ], dx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if dir locate structure has info, return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_112:
        getarg di, _ptrstructLongfilename

        getarg ax, _drive                               ; drive
        mov word ptr [ _lfnDrive               ][ di ], ax

        getdarg cx, dx, _cluster                        ; cluster to search next
        mov word ptr [ _lfnCluster. _low       ][ di ], dx
        mov word ptr [ _lfnCluster. _high      ][ di ], cx

        clc

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  exit
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNLocateFileNext_120:
        pushf
        push ax
        currSegment ds

        getarg ax, _drive                               ; drive
        getarg bx, _ptrDirAccess                        ; dir access
        getarg di, _ptrstructLongfilename               ; long filename
        call updateDirAccessBlock

        pop ax
        popf
        RestoreSegments bx, si, di
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  LFN Create File                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Name check already performed. It is assumed that the name    ;
        ;  does not exist. This routine is only responsible for         ;
        ;  creating a valid directory entry.                            ;
        ;                                                               ;
        ;  cx    attributes                                             ;
        ;  ss:si struct LongFilename                                    ;
        ;  ss:di dir Access                                             ;
        ;...............................................................;

LFNCreateFile:

        Entry
        def _handle
        def _attributes, cx
        def _ptrDirAccess, di
        def _ptrLongFilename, si

        ddef _sftPointer
        defbytes _diskAccess, sizeDISKACCESS

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  find / init available SFT handle
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        call FindAvailableSFTHandle                     ; find an available file handle
        ifc LFNCreateFile_40                            ; if none available -->

        storarg _handle, ax                             ; handle
        stordarg _sftPointer, es, bx                    ; sft pointer
        call InitSFTEntry                               ; clear entry

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  build long and short directory entries
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ fileAcDrive ][ di ]
        mov dx, word ptr [ fileAcDirCluster. _low  ][ di ]
        mov cx, word ptr [ fileAcDirCluster. _high ][ di ]
        getarg di, _ptrLongFilename                     ; long filename, canonical name struct
        call LFNBuildDirectoryEntry                     ; build entry
        jc LFNCreateFile_40                             ; if can't find free slot -->

        getarg cx, _attributes
        mov byte ptr es:[ deAttributes      ][ bx ], cl ; set updated attributes

        call getSysDateinDirFormat
        mov word ptr es:[ deTime            ][ bx ], ax ; time created
        mov word ptr es:[ deDate            ][ bx ], dx ; date created

        mov word ptr es:[ deStartCluster    ][ bx ], 0000
        mov word ptr es:[ deStartClusterExt ][ bx ], 0000
        mov word ptr es:[ deFileSize. _low  ][ bx ], 0000
        mov word ptr es:[ deFileSize. _high ][ bx ], 0000

        mov si, bx
        call locateCCBPHeader                           ; si buffer ptr --> es:di ccb hdr
        call CCBChanged                                 ; mark changes made

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  populate sft entry from dir entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        push es                                         ; es:bx dir entry
        push bx
        push word ptr [ _sftPointer. _segment ][ bp ]   ; sft entry
        push word ptr [ _sftPointer. _pointer ][ bp ]
        call initSFTfromDirEntry

        getarg ax, _handle                              ; return handle
        clc

LFNCreateFile_40:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Locate Free LFN DirSlot                                      ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Locate free LFN dirslot and build LFN dir entries            ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  dir cluster address                                  ;
        ;   ss:di  ptr to LongFilename struct                           ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   es:bx  pointer to SHORT dir entry in ccb                    ;
        ;   cy     if item not found                                    ;
        ;                                                               ;
        ;  Assumes ss == ds                                             ;
        ;...............................................................;

LFNBuildDirectoryEntry:

        Entry
        def  _drive, ax
        ddef _dircluster, cx, dx
        def  _ptrstructLongFilename, di
        def  _numDirEntries
        def  _numDirEntriesRemainder
        def  _LFNsequence
        def  _checksum

        def  _numEmptySeq
        ddef _posEmptyEntry
        ddef _copyPointer
        defbytes _diskAccess, sizeDISKACCESS

        setDS ss                                        ; set ds == ss
        mov word ptr [ _lfnDrive             ][ di ], ax; drive
        mov word ptr [ _lfnDirCluster. _high ][ di ], cx; cx:dx
        mov word ptr [ _lfnDirCluster. _low  ][ di ], dx;

        lea bx, offset _diskAccess [ bp ]               ; pointer to access block
        call initdiskAccess                             ; [ax] is drive, [cx:dx] is cluster
        call getDPB                                     ; check for media change

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; compute # dir entries needed from length of text
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, _ptrstructLongFilename
        call ComputeDirEntries                          ; ss:di points to lfn structure
        storarg _numDirEntries, ax                      ; save num entries
        storarg _numDirEntriesRemainder, dx             ; save remainder

        call computeShortFileName                       ; ss:di points to lfn structure
        storarg _checksum, ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; scan directory 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setES ss
        mov word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ], -sizeDIRENTRY
        mov word ptr [ _diskAccess. diskAcPosition. _high ][ bp ], -1
        mov word ptr [ _diskAccess. diskAcOptions         ][ bp ], (ccb_isDIR)

LFNBuildDirEntry_14:
        mov word ptr [ _numEmptySeq  ][ bp ], 0000

LFNBuildDirEntry_18:
        setDS ss                                        ; set ds == ss
        lea di, offset _diskAccess [ bp ]               ; pointer to access block
        call _ReadNextDirEntry                          ; read entry
        jz LFNBuildDirEntry_28                          ; if at end, allocate dir space -->
        cmp byte ptr es:[ deName ][ bx ], DIRENTRY_NEVERUSED
        jz LFNBuildDirEntry_32                          ; if at end, space is usable -->

        cmp byte ptr es:[ deName ][ bx ], DIRENTRY_DELETED
        jnz LFNBuildDirEntry_14                         ; entry in use, not valid -->

        mov cx, word ptr [ _numEmptySeq  ][ bp ]
        or cx, cx                                       ; location already saved ?
        jnz LFNBuildDirEntry_22                         ; yes, skip -->
        mov ax, word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ]
        mov dx, word ptr [ _diskAccess. diskAcPosition. _high ][ bp ]
        stordarg _posEmptyEntry, dx, ax

LFNBuildDirEntry_22:
        inc cx
        storarg _numEmptySeq, cx

        cmp cx, word ptr [ _numDirEntries ][ bp ]
        jl LFNBuildDirEntry_18
        jmp short LFNBuildDirEntry_32

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  must allocate dir space
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNBuildDirEntry_28:
        lea bx, offset [ _diskAccess ][ bp ]
        call IsRootExtendable                           ; is root expandable ?
        ifc LFNBuildDirEntry_Return                     ; if not, return error -->

        mov ax, word ptr [ _diskAccess. diskAcDrive ][ bp ]
        call AllocateInitCluster                        ; init a cluster
        ifc LFNBuildDirEntry_Return                     ; if can't append -->

        push cx                                         ; cluster address to update
        push dx
        mov ax, word ptr [ _diskAccess. diskAcDrive      ][ bp ]
        mov dx, word ptr [ _diskAccess. diskAcCurCluster. _low  ][ bp ]
        mov cx, word ptr [ _diskAccess. diskAcCurCluster. _high ][ bp ]
        call updateClusterValue
        lea si, offset ccbData [ di ]                   ; dir address to di
        or si, si                                       ; no carry

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  create directory entry in empty slot found
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNBuildDirEntry_32:
        cmp word ptr [ _numEmptySeq ][ bp ], 0000       ; if NULL count, position valid 
        jz LFNBuildDirEntry_34

        getdarg dx, ax, _posEmptyEntry
        mov word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ], ax
        mov word ptr [ _diskAccess. diskAcPosition. _high ][ bp ], dx

LFNBuildDirEntry_34:
        sub word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ], sizeDIRENTRY
        sbb word ptr [ _diskAccess. diskAcPosition. _high ][ bp ], 0000

        getarg di, _ptrstructLongFilename
        mov ax, word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ]
        mov dx, word ptr [ _diskAccess. diskAcPosition. _high ][ bp ]
        mov word ptr [ _lfnOffsetLongEntry. _high ][ di ], dx
        mov word ptr [ _lfnOffsetLongEntry. _low ][ di ], ax

        getarg cx, _numDirEntriesRemainder
        les si, dword ptr [ _lfnSearchLongNameText ][ di ]
        add si, word ptr [ _lfnSearchStringLength ][ di ]
        sub si, cx
        inc si                                          ; make room for terminating null char

        or cx, cx
        jnz LFNBuildDirEntry_36
        mov cx, LFN_NAMECHARPERENTRY
        sub si, cx

LFNBuildDirEntry_36:
        stordarg _copyPointer, es, si                   ; store copy pointer
        dec word ptr [ _numDirEntries ][ bp ]           ; no need to read short entry

        mov ax, word ptr[ _numDirEntries ][ bp ]        ; compute sequence number
        or ax, LFN_START_LFNENTRY
        storarg _LFNsequence, ax

LFNBuildDirEntry_38:
        push cx
        lea di, offset [ _diskAccess ][ bp ]
        call _ReadNextDirEntry                          ; read entry
        call _ClearDirEntry                             ; clear entry

        pop cx                                          ; length
        getdarg ds, si, _copyPointer                    ; source is at ds:si
        call _copyfromStringtoLFN                       ; copy string to LFN

        getarg ax, _LFNsequence
        mov byte ptr es:[ LFN_SequenceByte ][ bx ], al  ; LFN sequence number

        and ax, LFN_SEQUENCEMASK                        ; isolate sequence number
        dec ax
        mov word ptr [ _LFNsequence ][ bp ], ax

        mov dl, byte ptr [ _checksum ][ bp ]
        mov byte ptr es:[ LFN_Checksum ][ bx ], dl      ; set checksum
        mov byte ptr es:[ LFN_Attributes ][ bx ], ATTR_LONGFILENAME

        currSegment ds                                  ; point to default segment
        mov cx, LFN_NAMECHARPERENTRY
        sub word ptr [ _copyPointer. _pointer ][ bp ], cx
        dec word ptr [ _numDirEntries ][ bp ]
        jnz LFNBuildDirEntry_38

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  write short entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea di, offset [ _diskAccess ][ bp ]
        call _ReadNextDirEntry                          ; read entry
        call _ClearDirEntry                             ; clear entry

        getarg di, _ptrstructLongFilename
        mov ax, word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ]
        mov dx, word ptr [ _diskAccess. diskAcPosition. _high ][ bp ]
        mov word ptr [ _lfnOffsetShortEntry. _high ][ di ], dx
        mov word ptr [ _lfnOffsetShortEntry. _low ][ di ], ax

        mov cx, sizeFILENAME
        lea si, offset [ _lfnShortFCBName ][ di ]       ; ptr to short name
        mov di, bx                                      ; destination
        rep movsb                                       ; write entry
        clc

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

LFNBuildDirEntry_Return:
        getarg ax, _drive
        getdarg cx, dx, _dircluster
        getarg di, _ptrstructLongFilename
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Clear Entry, Mark Changed                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:bx  dir entry                                            ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   All registers preserved                                     ;
        ;                                                               ;
        ;  Assumes ss == ds                                             ;
        ;...............................................................;

_ClearDirEntry:

        SaveAllregisters

        xor ax, ax
        mov cx, ( sizeDIRENTRY / 2 )
        mov di, bx
        rep stosw                                       ; clear

        call SetCCBChanged                              ; buffer will be changed
        restoreAllRegisters
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Build Fully Formed PATH String From Canonical String         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:bx  ptr to LongFilename struct                           ;
        ;   es:di  ptr to canonical string buffer                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   dx     offset to last name                                  ;
        ;                                                               ;
        ;  Assumes ss == ds                                             ;
        ;...............................................................;

LFNBuildFullyFormedPATH:

        Entry
        def _numEntries
        def _offsetLastName
        def  _ptrstructLongFilename, bx
        ddef _ptrFullyFormedPath, es, di

        setDS ss
        SaveRegisters di, si, bx

        mov ax, word ptr [ _lfnDrive ][ bx ]
        add ax, ':A'
        stosw

        mov al, '\'
        stosb
        storarg _offsetLastName, di

        lea bx, offset [ _lfnCanonicalList ][ bx ]
        mov ax, word ptr [ _canpNumEntries ][ bx ]
        storarg _numEntries, ax
        or ax, ax
        jz LFNBuildPath_20

        lea bx, offset [ _canpArray ][ bx ]
        dec di

LFNBuildPath_12:
        mov al, '\'
        stosb
        storarg _offsetLastName, di

        mov cx, word ptr ss:[ _canpStringLength ][ bx ]
        lds si, dword ptr ss:[ _canpStringPointer ][ bx ]
        rep movsb

        add bx, sizeCANONICALPOINTER
        dec word ptr [ _numEntries ][ bp ]
        jnz LFNBuildPath_12

LFNBuildPath_20:
        xor ax, ax
        stosb

        setDS ss
        getarg dx, _offsetLastName
        RestoreRegisters bx, si, di
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compute Unique Short Name                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:di  ptr to LongFilename struct                           ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   al     short name checksum                                  ;
        ;                                                               ;
        ;  Assumes ss == ds                                             ;
        ;...............................................................;

computeShortFileName:

        Entry
        def  _highestHit, 0000
        def  _ptrstructLongFilename, di

        defbytes _namehitArray, 32
        defbytes _diskAccess, sizeDISKACCESS

        setDS ss                                        ; set ds == ss
        mov ax, word ptr [ _lfnDrive             ][ di ]; drive
        mov cx, word ptr [ _lfnDirCluster. _high ][ di ]; cx:dx
        mov dx, word ptr [ _lfnDirCluster. _low  ][ di ];

        lea bx, offset _diskAccess [ bp ]               ; pointer to access block
        call initdiskAccess                             ; [ax] is drive, [cx:dx] is cluster

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; create a trial short name from long name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea bx, offset [ _lfnShortFCBName ][ di ]
        les si, dword ptr [ _lfnSearchLongNameText ][ di ]
        mov cx, word ptr [ _lfnSearchStringLength ][ di ]
        call createFCBNAMEfromLFN
        ifz computeSFN_56

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; scan directory 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

computeSFN_12:
        setES ss
        lea di, offset [ _namehitArray ][ bp ]
        clearMemory sizeof_namehitArray                 ; name hit array

        mov word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ], -sizeDIRENTRY
        mov word ptr [ _diskAccess. diskAcPosition. _high ][ bp ], -1
        mov word ptr [ _diskAccess. diskAcOptions         ][ bp ], (ccb_isDIR)

computeSFN_14:
        setDS ss                                        ; set ds == ss
        lea di, offset _diskAccess [ bp ]               ; pointer to access block
        call _ReadNextDirEntry                          ; read entry
        jz computeSFN_32                                ; if at end, allocate dir space -->
        cmp byte ptr es:[ deName ][ bx ], DIRENTRY_NEVERUSED
        jz computeSFN_32                                ; if at end, space is usable -->

        cmp byte ptr es:[ deName ][ bx ], DIRENTRY_DELETED
        jz computeSFN_14                                ; entry in use, not valid -->
        test byte ptr es:[ deAttributes  ][ bx ], ATTR_VOLUME
        jnz computeSFN_14                               ; skip volume and LFN entries -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; see if entry is a match
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov al, '~'
        mov cx, sizefnName
        mov di, bx                                      ; entry is at
        repnz scasb                                     ; see if name contains '~' mark
        jnz computeSFN_14                               ; no, not a contender -->

        sub cx, sizefnName
        not cx                                          ; actual length that may match

        getarg di, _ptrstructLongFilename
        lea si, offset [ _lfnShortFCBName ][ di ]
        mov di, bx                                      ; entry is at
        repz cmpsb                                      ; continue until not equal
        jnz computeSFN_14                               ; not equal -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; get value past '~' character
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov cx, sizefnName
        inc di
        sub cx, di
        add cx, bx                                      ; length of number to get
        xor dx, dx
        xor ax, ax

computeSFN_18:
        mov dl, byte ptr es:[ di ]                      ; grab number past '~' character
        cmp dl, '9' + 1
        jnc computeSFN_14                               ; if not digit, ignore -->
        sub dl, '0'
        jc computeSFN_14                                ; if not digit, ignore -->

        mov bx, ax
        add ax, ax
        add ax, ax
        add ax, bx
        add ax, ax
        add ax, dx
        inc di
        loop computeSFN_18

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; set reserve bits
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        dec ax                                          ; number count is zero based
        cmp ax, word ptr [ _highestHit ][ bp ]          ; does number exceed max 
        jle computeSFN_28                               ; no -->
        storarg _highestHit, ax                         ; else save highest value

computeSFN_28:
        cmp ax, 8 * sizeof_namehitArray                 ; is it within name hit array range ?
        jg computeSFN_14                                ; if not, continue scan -->

        mov bx, ax
        mov di, ax
        shr di, 1                                       ; / 2
        shr di, 1                                       ; / 4
        shr di, 1                                       ; / 8
        and bx, 7
        mov al, byte ptr cs:[ _bitShiftTable ][ bx ]    ; get correct bit
        or byte ptr [ _namehitArray ][ bp ][ di ], al
        jmp computeSFN_14

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  determine conflicts, fixup name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

computeSFN_32:
        mov cx, sizeof_namehitArray
        lea di, offset [ _namehitArray ][ bp ]          ; scan for an empty bit
        mov ax, di

computeSFN_34:
        cmp byte ptr [ di ], 255                        ; all bits assigned ?
        jnz computeSFN_38                               ; no, go determine empty slot -->

        inc di
        loop computeSFN_34

        getarg ax, _highestHit                          ; else, get highest hit
        inc ax                                          ; bump by one
        jmp short computeSFN_44                         ; assign number -->

computeSFN_38:
        sub ax, di
        neg ax
        add ax, ax                                      ; *2
        add ax, ax                                      ; *4
        add ax, ax                                      ; *8
        mov cl, byte ptr [ di ]                         ; one of these is unassigned

computeSFN_42:
        inc ax                                          ; free bit is in ax
        ror cl, 1
        jc computeSFN_42

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compute value to store in short name
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

computeSFN_44:
        getarg di, _ptrstructLongFilename
        lea si, offset [ _lfnShortFCBName + sizefnName - 1 ][ di ]

computeSFN_46:
        xor dx, dx
        mov cx, 10
        div cx                                          ; extract digit
        or dl, '0'
        mov byte ptr [ si ], dl
        dec si

        or ax, ax                                       ; more to go ?
        jnz computeSFN_46                               ; yes -->

        mov byte ptr [ si ], '~'                        ; add the tilde

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compute checksum
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

computeSFN_56:
        setES ss
        getarg di, _ptrstructLongFilename
        lea bx, offset [ _lfnShortFCBName ][ di ]
        call ComputeLFNChecksum                         ; compute checksum

        getarg di, _ptrstructLongFilename
        mov byte ptr [ _lfnChecksum ][ di ], al
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Create SHORT name from LFN                                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:si  ptr to long filename                                 ;
        ;   ss:bx  ptr to short filename                                ;
        ;   cx     length of name                                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ss:bx  short name file filled                               ;
        ;   nz     if name or extension exceeds 8.3 format              ;
        ;                                                               ;
        ;  All registers preserved                                      ;
        ;  Assumes ss == ds                                             ;
        ;...............................................................;

createFCBNAMEfromLFN:

        Entry
        def  _count, cx
        def  _exceedsShortFormat, False
        ddef _ptrLongfilename, es, si
        ddef _ptrShortfilename, ss, bx

        SaveAllRegisters

        setES ss
        mov al, ' '
        mov di, bx
        mov cx, sizeFILENAME
        rep stosb                                       ; clear short name to blanks
        
        std
        getarg cx, _count
        getdarg es, di, _ptrLongfilename
        add di, cx
        mov al, '.'                                     ; essentially strrchr
        repnz scasb                                     ; scan for last '.' extension
        cld                                             ; restore direction
        jnz _createFCBNAME_16                           ; skip extension -->

        inc di
        inc di
        mov cx, sizefnExtension
        mov si, word ptr [ _ptrShortfilename. _pointer ][ bp ]
        add si, sizefnName

_createFCBNAME_08:
        mov al, byte ptr es:[ di ]
        inc di
        or al, al
        jz _createFCBNAME_16
        cmp al, '.'
        jz _createFCBNAME_08
        cmp al, ' '
        jz _createFCBNAME_08

        _uppercase al
        mov byte ptr [ si ], al
        inc si
        dec cx
        jnz _createFCBNAME_08

        cmp byte ptr es:[ di ], 00
        jz _createFCBNAME_16
        storarg _exceedsShortFormat, True

_createFCBNAME_16:
        mov cx, sizefnName
        getdarg es, di, _ptrLongfilename
        mov si, word ptr [ _ptrShortfilename. _pointer ][ bp ]

_createFCBNAME_18:
        mov al, byte ptr es:[ di ]
        inc di
        or al, al
        jz _createFCBNAME_26
        cmp al, '.'
        jz _createFCBNAME_26
        cmp al, ' '
        jz _createFCBNAME_18

        _uppercase al
        mov byte ptr [ si ], al
        inc si
        dec cx
        jnz _createFCBNAME_18

        cmp byte ptr es:[ di ], 00
        jz _createFCBNAME_26
        cmp byte ptr es:[ di ], '.'
        jz _createFCBNAME_26
        storarg _exceedsShortFormat, True

_createFCBNAME_26:
        RestoreAllRegisters

        cmp word ptr [ _exceedsShortFormat ][ bp ], False
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Return Where in Directory                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:di  pointer to dir access block                          ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   dx:ax  directory sector                                     ;
        ;   cl     offset in items from start of sector                 ;
        ;                                                               ;
        ;  Assumes ss == ds                                             ;
        ;...............................................................;

getWhereInDir:

        mov ax, word ptr [ fileAcDirOffset ][ di ]
        sub ax, ccbData                                 ; adjust for header offset
        mov cl, sizeDIRENTRY                            ; bytes per entry
        div cl                                          ; remainder should be zero
        mov cx, ax                                      ; return in cl

        mov ax, word ptr [ fileAcDirSector. _low  ][ di ]
        mov dx, word ptr [ fileAcDirSector. _high ][ di ]
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Scan Directory (Exact Search)                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Note:                                                        ;
        ;   Subdirectories are allocated in the FAT, just like files.   ;
        ;   Use _FATReadRandom to position and read correct cluster.    ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster of dir to search                             ;
        ;   ss:si  ptr to canonical name entry                          ;
        ;   ss:di  ptr scan struct                                      ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   if item found:                                              ;
        ;   ax     drive                                                ;
        ;   bx     attributes                                           ;
        ;   cx:dx  cluster                                              ;
        ;   es:si  pointer to located (SHORT) directory entry           ;
        ;   ss:di  ptr scan struct                                      ;
        ;                                                               ;
        ;   if item NOT found:                                          ;
        ;   cy     if item not found                                    ;
        ;...............................................................;

LFNScanDirectory:

        Entry
        def  _longnameSet, FALSE
        def  _ptrScanStruct, di
        def  _length

        def  _drive, ax
        ddef _cluster, cx, dx
        ddef _filenamePtr
        ddef _ptrShortDirEntry
        ddef _offsetDirLongEntry

        defbytes _diskAccess, sizeDISKACCESS
        defbytes _compareListStruct, sizeCompareListStruct

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init access 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setDS ss                                        ; set ds == ss
        mov cx, word ptr [ _canpStringLength ][ si ]    ; string length
        mov word ptr [ _scandirStringLength ][ di ], cx
        storarg _length, cx

        les bx, dword ptr [ _canpStringPointer ][ si ]  ; string pointer
        mov word ptr [ _scandirStringPointer. _segment ][ di ], es
        mov word ptr [ _scandirStringPointer. _pointer ][ di ], bx
        stordarg _filenamePtr, es, bx

        getarg ax, _drive                               ; get drive
        getdarg cx, dx, _cluster                        ; cluster to search next
        mov word ptr [ _scandirDrive   ][ di ], ax
        mov word ptr [ _scandirCluster. _low  ][ di ], dx
        mov word ptr [ _scandirCluster. _high ][ di ], cx

        lea bx, offset _diskAccess [ bp ]               ; pointer to access block
        call initdiskAccess                             ; [ax] is drive, [cx:dx] is cluster
        call getDPB                                     ; check for media change

        lea di, word ptr [ _compareListStruct ][ bp ]
        getdarg es, si, _filenamePtr
        call BuildLFNCompareList                        ; designed to handle * wild characters

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  lookup entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setES ss
        getarg di, _ptrScanStruct
        mov cx, word ptr [ _scandirFileOffset. _high ][ di ]
        mov dx, word ptr [ _scandirFileOffset. _low  ][ di ]
        sub dx, sizeDIRENTRY
        sbb cx, 0000

        mov word ptr [ _diskAccess. diskAcPosition. _high ][ bp ], cx
        mov word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ], dx
        mov word ptr [ _diskAccess. diskAcOptions         ][ bp ], (ccb_isDIR)

LFN_scanDir_12:
        setDS ss                                        ; set ds == ss
        lea di, offset _diskAccess [ bp ]               ; pointer to access block
        call _ReadNextDirEntry
        stc                                             ; just in case error,
        ifz  LFN_scanDir_78                             ; if no more data -->

        cmp byte ptr es:[ deName ][ bx ], DIRENTRY_DELETED
        jz LFN_scanDir_12                               ; skip deleted entries -->
        cmp byte ptr es:[ deName ][ bx ], DIRENTRY_NEVERUSED
        stc                                             ; just in case error,
        ifz LFN_scanDir_78                              ; if no more data -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; compare long filename
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov dx, word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ]
        mov cx, word ptr [ _diskAccess. diskAcPosition. _high ][ bp ]
        stordarg _offsetDirLongEntry, cx, dx

        getarg di, _ptrScanStruct
        push word ptr [ _scandirLongFilenameStr ][ di ]
        push word ptr [ _scandirShortFilenameStr ][ di ]

        push es   
        push bx                                         ; pointer to current dir entry
        lea si, offset [ _compareListStruct ][ bp ]
        lea di, offset [ _diskAccess ][ bp ]            ; access block in ss: di
        call _compareDirEntries_LongNames               ; compare long names
        jnz LFN_scanDir_12                              ; if item not found -->

        storarg _longnameSet, ax                        ; if LFN true

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  item found
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setDS ss
        getarg ax, _drive
        getarg di, _ptrScanStruct                       ; ptr to scan structure
        mov dx, word ptr [ _diskAccess. diskAcPosition. _low  ][ bp ]
        mov cx, word ptr [ _diskAccess. diskAcPosition. _high ][ bp ]
        mov word ptr [ _scandirFileAcShortEntry. _low  ][ di ], dx
        mov word ptr [ _scandirFileAcShortEntry. _high ][ di ], cx
        mov word ptr [ _scandirFileAcLongEntry. _low   ][ di ], dx
        mov word ptr [ _scandirFileAcLongEntry. _high  ][ di ], cx

        cmp word ptr [ _longnameSet ][ bp ], FALSE
        je LFN_scanDir_76
        getdarg cx, dx, _offsetDirLongEntry
        mov word ptr [ _scandirFileAcLongEntry. _low  ][ di ], dx
        mov word ptr [ _scandirFileAcLongEntry. _high ][ di ], cx

LFN_scanDir_76:
        mov si, bx                                      ; dir entry to si
        mov dx, word ptr es:[ deStartCluster    ][ si ] ; cluster
        mov cx, word ptr es:[ deStartClusterExt ][ si ]
        mov bl, byte ptr es:[ deAttributes      ][ si ] ; attributes
        xor bh, bh                                      ; zero, nc

LFN_scanDir_78:
        setDS ss                                        ; set ds == ss
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compare Dir Long Filename Entries                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   darg   ptr long (asciz) filename store                      ;
        ;   darg   ptr short (asciz) filename store                     ;
        ;   darg   ptr directory entry                                  ;
        ;   ss:di  disk access block                                    ;
        ;   ss:si  compare list struct                                  ;
        ;   cl     search attribute                                     ;
        ;   ch     must match attribute                                 ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:bx  matching short dir entry                             ;
        ;   ax     True if lfn name                                     ;
        ;   zr     entry located                                        ;
        ;...............................................................;

_compareDirEntries_LongNames:

        Entry 4
        arg _ptrLongFilenameAsciz                       ; assumes ss:
        arg _ptrShortFilenameAsciz                      ; assumes ss:
        darg _ptrDirEntry

        def  _LFNFlag, False
        def  _checksum, 0000
        def  _nLenLongEntry, 0000
        def  _nLenShortEntry, 0000
        ddef _diskAccess, ss, di
        ddef _ptrCompareListStruct, ss, si

        saveRegisters ds, di, si

        getarg di, _ptrLongFilenameAsciz
        mov byte ptr [ di ], 0                          ; clear long name, in case none
        getarg di, _ptrShortFilenameAsciz
        mov byte ptr [ di ], 0                          ; clear long name, in case none

_compareDir_LFN_08:
        getdarg es, di, _ptrDirEntry                    ; get lfn entry
        cmp byte ptr es:[ deAttributes ][ di ], ATTR_LONGFILENAME
        jnz _compareDir_LFN_24                          ; if on short entry -->

        storarg _LFNFlag, True
        mov cl, byte ptr es:[ LFN_SequenceByte ][ di ]  ; get sequence byte
        and cx, LFN_SEQUENCEMASK                        ; isolate sequence number
        dec cx                                          ; n - 1

        mov ax, LFN_NAMECHARPERENTRY
        mul cl                                          ; offset to store start in ax

        mov cx, ax
        getarg si, _ptrLongFilenameAsciz
        add si, ax                                      ; offset where to store filename
        call _copyfromLFNtoString                       ; copy string for compare purposes

        getdarg es, di, _ptrDirEntry                    ; get lfn entry
        test byte ptr es:[ LFN_SequenceByte ][ di ], LFN_START_LFNENTRY 
        jz _compareDir_LFN_12                           ; if not end segment --> 

        getarg si, _ptrLongFilenameAsciz
        add si, cx                                      ; offset where to store filename
        mov byte ptr [ si ], 0                          ; insure a nul exists at end        
        storarg _nLenLongEntry, cx                      ; store long name length

        mov al, byte ptr es:[ LFN_Checksum ][ di ]
        mov byte ptr [ _checksum ][ bp ], al            ; grab checksum

_compareDir_LFN_12:
        mov di, word ptr [ _diskAccess ][ bp ]
        call _ReadNextDirEntry                          ; go fetch next entry
        ifz _compareDir_LFN_Return                      ; end, obviously not this entry -->

        stordarg _ptrDirEntry, es, bx
        jmp _compareDir_LFN_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compare full names.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_compareDir_LFN_24:
        setES ss
        getdarg ds, si, _ptrDirEntry                    ; ptr to short dir entry
        getarg di, _ptrShortFilenameAsciz               ; make asciz entry
        call convDirEntrytoASCIZ                        ; have to make short entry in either case
        storarg _nLenShortEntry, cx                     ; store short name length 

        setDS ss
        getarg cx, _nLenLongEntry                       ; long name available ?
        or cx, cx                                       ; long filename available ?
        jz _compareDir_LFN_32                           ; none, skip match on lfn entry -->

        getarg si, _ptrLongFilenameAsciz
        mov di, word ptr [ _ptrCompareListStruct. _pointer ][ bp ]
        test word ptr [ _csFlags ][ di ], CSMATCHES_ALL ; *.* ?
        jnz _compareDir_LFN_Matches                     ; if instant match -->
        call matchOnCompareList                         ; zero if matches
        jz _compareDir_LFN_Matches                      ; if equal -->

_compareDir_LFN_32:
        getarg cx, _nLenShortEntry                      ; short name length 
        getarg si, _ptrShortFilenameAsciz               ; make asciz entry
        mov di, word ptr [ _ptrCompareListStruct. _pointer ][ bp ]
        test word ptr [ _csFlags ][ di ], CSMATCHES_ALL
        jnz _compareDir_LFN_Matches                     ; if instant match -->
        call matchOnCompareList                         ; zero if matches
        jnz _compareDir_LFN_Return                      ; if not equal -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Item Matches
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_compareDir_LFN_Matches:
        cmp word ptr [ _LFNFlag ][ bp ], False          ; lfn ?
        je _compareDir_LFN_Return                       ; if LFN -->

        getdarg es, bx, _ptrDirEntry                    ; ptr to short dir entry
        call ComputeLFNChecksum
        cmp al, byte ptr [ _checksum ][ bp ]            ; checksum valid ?
        jz _compareDir_LFN_Return                       ; yes -->

        xor ax, ax
        storarg _LFNFlag, ax                            ; flag is FALSE, lfn entry invalid

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_compareDir_LFN_Return:
        restoreRegisters si, di, ds

        getdarg es, bx, _ptrDirEntry                    ; ptr to short dir entry
        getarg ax, _LFNFlag
        clc
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Device from String                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:si  input string                                         ;
        ;   cx     input string length                                  ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:bx  pointer to device header                             ;
        ;   cy     device not found                                     ;
        ;...............................................................;

seeIfDeviceName:

        push di
        mov di, si
        call checkforDeviceName                         ; see if char device name
        mov ax, offset pexterrIllegalName               ; (in case its an error)
        jc getDevice_Return                             ; if invalid -->

        mov ax, es
        or ax, ax                                       ; nc character device 

getDevice_Return:
        pop di
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Build Compare List for Unix Style Long Filename              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:si  ptr to filename (ends with null or \)                ;
        ;   ss:di  ptr to compare list structure                        ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   cx     entries in compare table                             ;
        ;   zr     if wild characters NOT used                          ;
        ;                                                               ;
        ;...............................................................;

BuildLFNCompareList:

        Entry
        ddef _pSearchTemplateString, es, si
        ddef _pCompareListStruct, ss, di                ; compare list structure
        ddef _pCompareList, ss, di                      ; compare list store items
        def  _bWildChars, FALSE                         ; if wild chars used
        def  _CountItems, 0000

        SaveRegisters di, si
        call InitCompareListStruct
        mov word ptr ss:[ _cspStartStringSegment ][ di ], es

        call BuildLFN_CheckAllSpecialCase
        jnz BuildLFNCompareList_08                      ; if not *.* case, go parse ->

        storarg _bWildChars, True                       ; else say wild chars used
        or word ptr ss:[ _csFlags ][ di ], CSMATCHES_ALL
        jmp BuildLFNCompareList_36

BuildLFNCompareList_08:
        lea bx, [ _csCompareList ][ di ]
        mov word ptr ss:[ _pCompareList. _pointer ][ bp ], bx  ; compare list store items ptr

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if string starts with wild character
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        call ScanToNextSeparator                         ; scan over first item
        jz BuildLFNCompareList_12                        ; if end of string -->
        cmp al, "*"                                      ; wild character at start byte ?
        jz BuildLFNCompareList_24                        ; if true -->

BuildLFNCompareList_12:
        mov word ptr ss:[ _cspStartString. _pointer ][ di ], dx  ; must start with address
        mov word ptr ss:[ _cspStartString. _csStringLength ][ di ], cx
        jz BuildLFNCompareList_32                        ; go set must end with -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  skip wild character, store entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

BuildLFNCompareList_24:
        storarg _bWildChars, TRUE                       ; set wild chars are used
        call ScanToNextSeparator                        ; skip over wild characters
        jz BuildLFNCompareList_32                       ; if end string, go set must end with -->

        mov bx, word ptr ss:[ _pCompareList. _pointer ][ bp ]  ; compare list store items ptr
        mov word ptr ss:[ _pointer        ][ bx ], dx   ; store pointer
        mov word ptr ss:[ _csStringLength ][ bx ], cx   ; store count
        add word ptr ss:[ _pCompareList. _pointer ][ bp ], sizeCompareListItem

        inc word ptr ss:[ _CountItems ][ bp ]           ; number of entries updated
        cmp word ptr ss:[ _CountItems ][ bp ], maxEntriesCOMPARELIST
        jc BuildLFNCompareList_24                       ; if room for more -->
        jmp short BuildLFNCompareList_36                ; list is incomplete, but exit anyway -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if must end with entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

BuildLFNCompareList_32:
        or cx, cx                                       ; if no end pointer
        jz BuildLFNCompareList_36                       ; skip -->
        
        cmp dx, word ptr ss:[ _cspStartString. _pointer  ][ di ] ; end ptr same as start ptr ?
        jz BuildLFNCompareList_36                       ; skip -->

        sub si, cx                                      ; adjust for end
        mov word ptr ss:[ _cspEndString. _pointer        ][ di ], si  ; must end with
        mov word ptr ss:[ _cspEndString. _csStringLength ][ di ], cx  ; string length

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  exit 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

BuildLFNCompareList_36:
        getarg cx, _CountItems
        mov di, word ptr ss:[ _pCompareListStruct. _pointer ][ bp ]
        mov word ptr ss:[ _csCountEntries ][ di ], cx
        or word ptr ss:[ _csFlags ][ di ], CSNOWILD_CHARS

        getarg ax, _bWildChars
        cmp ax, FALSE                                    ; zr if wild characters NOT used
        jz BuildLFNCompareList_38                        ; if no wild chars
        and word ptr ss:[ _csFlags ][ di ], not CSNOWILD_CHARS

BuildLFNCompareList_38:
        RestoreRegisters si, di
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Check For *.* All Special Case                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:si  ptr to filename (ends with null or \)                ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     True (1)                                             ;
        ;   zr     if *.* case                                          ;
        ;                                                               ;
        ; (assumes ds == ss)                                            ;
        ;...............................................................;

BuildLFN_CheckAllSpecialCase:

        mov cx, es
        or cx, si
        jz _CheckAllSpecialCase_08                      ; if null string

        cmp word ptr es:[ si ], ".*"
        jnz _CheckAllSpecialCase_08
        cmp word ptr es:[ si + 2 ], "*"

_CheckAllSpecialCase_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Match Using Compare List                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  For now, compares are case independent.                      ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:si  ptr to name to compare (ends with null or \)         ;
        ;   ss:di  ptr to compare list structure                        ;
        ;   cx     string length                                        ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   zr     if names compare equal                               ;
        ;                                                               ;
        ; (assumes ds == ss)                                            ;
        ;...............................................................;

matchOnCompareList:

        Entry
        ddef _pCompareList, ss, di
        ddef _pFilename, ss, si
        ddef _pFilenameOffset, ss, si

        def  _wStringLength, cx
        def  _wIfOtherStrings
        def  _wCount

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  first, match on start string (null if *abc string)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setDS ss
        SaveRegisters es
        mov es, word ptr [ _cspStartStringSegment ][ di ]

        mov ax, word ptr [ _cspEndString. _csStringLength ][ di ]
        or ax, word ptr [ _csCountEntries ][ di ]       ; other strings to match on
        storarg _wIfOtherStrings, ax                    ; save if other strings

        mov cx, word ptr [ _cspStartString. _csStringLength ][ di ]
        or cx, cx
        jz matchOnCompareList_18                        ; if no start string -->

        test word ptr [ _csFlags ][ di ], CSNOWILD_CHARS
        jz matchOnCompareList_12                        ; if wild chars -->
        cmp cx, word ptr [ _wStringLength ][ bp ]
        ifnz matchOnCompareList_84                      ; if length's not equal, why compare ?

matchOnCompareList_12:
        mov si, word ptr [ _pFilename. _pointer ][ bp ]
        mov bx, word ptr [ _cspStartString. _pointer ][ di ]
        call _CompareNoCaseCheck                        ; compare strings
        ifnz matchOnCompareList_84                      ; if not equal -->

        mov word ptr [ _pFilenameOffset. _pointer ][ bp ], si
        cmp word ptr [ _wIfOtherStrings ][ bp ], 0000   ; other strings to match on ?
        jz matchOnCompareList_42                        ; no, so end must also match -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  then match on end string (null if 'abc' or 'abc*' string)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

matchOnCompareList_18:
        mov cx, word ptr [ _cspEndString. _csStringLength ][ di ]
        or cx, cx
        jz matchOnCompareList_42                        ; if no end string -->

        mov si, word ptr [ _pFilename. _pointer ][ bp ]
        mov bx, word ptr [ _cspEndString. _pointer ][ di ]
        cmp byte ptr [ bx ], '.'                        ; *. special case ?
        jnz matchOnCompareList_38                       ; no -->
        cmp cx, 1                                       ; *. special case ?
        jnz matchOnCompareList_38                       ; no -->

        mov al, '.'
        mov cx, word ptr [ _wStringLength ][ bp ]

matchOnCompareList_22:
        cmp byte ptr [ si ], '.'                        ; any extension specs ?
        jz matchOnCompareList_NoMatch                   ; if no match -->
        inc si
        loop matchOnCompareList_22                      ; scan entire field -->
        jmp short matchOnCompareList_42                 ; see if more patterns to match

matchOnCompareList_38:
        add si, word ptr [ _wStringLength ][ bp ]
        sub si, cx
        call _CompareNoCaseCheck                        ; compare strings 
        jnz matchOnCompareList_84                       ; if not equal -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  then loop and match on segments strings (null if no segments)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

matchOnCompareList_42:
        mov cx, word ptr [ _csCountEntries ][ di ]
        or cx, cx
        jz matchOnCompareList_84                        ; if no segment strings -->

        storarg _wCount, cx
        lea di, offset [ _csCompareList ][ di ]

matchOnCompareList_46:
        cmp word ptr [ _csStringLength ][ di ], 0000
        jz matchOnCompareList_84                        ; if no more segment strings -->

        mov bx, word ptr [ _pointer ][ di ]             ; get pointer to compare string
        mov al, byte ptr es:[ bx ]                      ; compare char
        _upperCase al                                   ; upper case

        mov si, word ptr [ _pFilenameOffset. _pointer ][ bp ]
        dec si

matchOnCompareList_52:
        inc si
        cmp byte ptr [ si ], 00                         ; if null, no match -->
        jz matchOnCompareList_NoMatch                   ; no match -->
        cmp al, byte ptr [ si ]                         ; compare against filename char
        jnz matchOnCompareList_52                       ; if no match yet -->

    ; if here, match may be possible

        push ax
        mov cx, word ptr [ _csStringLength ][ di ]
        mov word ptr [ _pFilenameOffset. _pointer ][ bp ], si
        call _CompareNoCaseCheck                        ; compare strings 

        pop ax
        jz matchOnCompareList_58                        ; if equal -->

    ; no match, continue looking down stream

        mov si, word ptr [ _pFilenameOffset. _pointer ][ bp ]
        jmp matchOnCompareList_52                       ; continue -->

    ; item matches, see if more segments to compare

matchOnCompareList_58:
        mov word ptr [ _pFilenameOffset. _pointer ][ bp ], si

        add di, sizeCompareListItem
        dec word ptr [ _wCount ][ bp ]
        jnz matchOnCompareList_46                       ; if more segments -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

matchOnCompareList_84:
        RestoreRegisters es
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  no match
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

matchOnCompareList_NoMatch:
        mov ax, es
        or ax, ax                                       ; insure non-zero return
        jmp matchOnCompareList_84                       ; exit -->

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compute LFN Checksum                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:bx  ptr to short name in directory or FCB format         ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   al     checksum                                             ;
        ;                                                               ;
        ;...............................................................;

ComputeLFNChecksum:

        push bx
        xor al, al                                      ; checksum
        mov cx, sizeFILENAME                            ; length of space to checksum
  
ComputeLFNChecksum_08:
        ror al, 1                                       ; rotate and add
        add al, byte ptr es:[ bx ]                      ; rotate and add
        inc bx
        loop ComputeLFNChecksum_08

        pop bx
        ret
          
        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compare NoCase Check                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:bx  ptr to match pattern (provided by user)              ;
        ;   ss:si  ptr to name extracted from directory                 ;
        ;   cx     compare length                                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   zr     if items are equal                                   ;
        ;                                                               ;
        ;...............................................................;

_CompareNoCaseCheck:

        mov al, byte ptr es:[ bx ]                      ; match pattern
        or al, al                                       ; at end of match string provided ?
        jnz _CompareNoCaseCheck_08                      ; if not -->
        cmp al, byte ptr [ si ]                         ; name extracted from directory
        jmp short _CompareNoCaseCheck_24                ; must equal null at filename string ->

_CompareNoCaseCheck_08:
        cmp byte ptr [ si ], 00                         ; end of entry from directory ?
        jnz _CompareNoCaseCheck_12                      ; no, go compare types -->
        cmp al, byte ptr [ si ]                         ; name extracted from directory
        jmp short _CompareNoCaseCheck_24                ; must equal null at filename string ->

_CompareNoCaseCheck_12:
        inc bx
        cmp al, '"'                                     ; ignore double quotes
        jz _CompareNoCaseCheck_12                       ; go fetch next -->
        cmp al, '?'                                     ; question mark matches anything except null
        jz _CompareNoCaseCheck_16                       ; consider equal -->
        cmp al, '*'                                     ; question mark matches anything except null
        jz _CompareNoCaseCheck_16                       ; consider equal -->

        cmp al, byte ptr [ si ]                         ; name extracted from directory
        jz _CompareNoCaseCheck_16                       ; if equal -->

        _upperCase al                                   ; upper case
        cmp al, byte ptr [ si ]                         ; name extracted from directory
        jnz _CompareNoCaseCheck_24                      ; if not equal -->

_CompareNoCaseCheck_16:
        inc si
        loop _CompareNoCaseCheck

        xor ax, ax                                      ; insure zero (equal )

_CompareNoCaseCheck_24:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Scan To Next Separator                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:si  ptr to string                                        ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:si  ptr to char after separator ('*')                    ;
        ;   dx     si value at start                                    ;
        ;   zr     if at end of string                                  ;
        ;                                                               ;
        ;...............................................................;

ScanToNextSeparator:

        xor cx, cx
        mov dx, si
        mov al, byte ptr es:[ si ]

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; scan within field
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

ScanToNextSeparator_08:
        cmp byte ptr es:[ si ], 00                      ; end byte ?
        jz ScanToNextSeparator_32                       ; yes -->
        cmp byte ptr es:[ si ], '\'                     ; end byte ?
        jz ScanToNextSeparator_32                       ; yes -->
        cmp byte ptr es:[ si ], '*'                     ; wild character ?
        jz ScanToNextSeparator_12                       ; if yes -->

        inc cx                                          ; inc count
        inc si                                          ; advance pointer
        jmp ScanToNextSeparator_08                      ; continue scanning -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; scan to next field
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

ScanToNextSeparator_12:
        cmp byte ptr es:[ si ], '*'
        jz ScanToNextSeparator_14                        
        cmp byte ptr es:[ si ], '?'                     ; if *?, treat as part of *
        jnz ScanToNextSeparator_32                      ; if NOT followed by a wild character -->

ScanToNextSeparator_14:
        inc si                                          ; advance ptr
        jmp ScanToNextSeparator_12

ScanToNextSeparator_32:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Init Compare List Struct                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:di  compare list structure                               ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   none.  all registers except ax and cx preserved             ;
        ;                                                               ;
        ;...............................................................;

InitCompareListStruct:

        push es
        setES ss                                        ; set segment
        clearMemory sizeCompareListStruct

        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Copy From LFN To String                                      ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:di  ptr to directory entry                               ;
        ;   ss:si  ptr to String                                        ;
        ;   cx     offset into string buffer                            ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:di  ptr to directory entry                               ;
        ;   ds:si  ptr to Search String                                 ;
        ;   cx     is actual length                                     ;
        ;...............................................................;

_copyfromLFNtoString:

        xor dx, dx

_copyfromLFNString_08:
        mov bx, dx
        mov bl, byte ptr cs:[ LFN_DirEntry_MatchTable ][ bx ]
        cmp bl, -1                                      ; end of table ?
        jz _copyfromLFNString_End                       ; all one -->

        inc dx
        mov ax, word ptr es:[ di ][ bx ]                ; get UNICODE char
        _upperCase al                                   ; upper case character

        mov byte ptr ss:[ si ], al
        or al, al                                       ; end of string ?
        jz _copyfromLFNString_End                       ; yes -->

        inc si
        inc cx
        jmp _copyfromLFNString_08

_copyfromLFNString_End:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Copy from String to LFN                                      ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ds:si  ptr to String                                        ;
        ;   es:bx  ptr to directory entry                               ;
        ;   cx     chars to copy                                        ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   all registers reserved                                      ;
        ;...............................................................;

_copyfromStringtoLFN:

        SaveRegisters si, cx, bx, ax

        mov di, bx
        push di
        xor dx, dx
        xor ax, ax
        clearMemory sizeDIRENTRY                        ; init to zeroes

        pop di
        mov byte ptr es:[ LFN_Attributes ][ di ], ATTR_LONGFILENAME

_copyfromStringtoLFN_08:
        lodsb                                           ; get characters

        mov bx, dx
        mov bl, byte ptr cs:[ LFN_DirEntry_MatchTable ][ bx ]
        mov word ptr es:[ di ][ bx ], ax                ; set UNICODE char

        inc dx
        loop _copyfromStringtoLFN_08

_copyfromStringtoLFN_12:
        mov bx, dx
        mov bl, byte ptr cs:[ LFN_DirEntry_MatchTable ][ bx ]
        cmp bl, -1
        jz _copyfromStringtoLFN_24

        inc dx
        mov word ptr es:[ di ][ bx ], -1                ; pad fill with -1 entries
        jmp _copyfromStringtoLFN_12

_copyfromStringtoLFN_24:
        RestoreRegisters ax, bx, cx, si
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compare String                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   cx     is max length to compare                             ;
        ;   ss:si  ptr to Search String                                 ;
        ;   es:di  ptr to UNICODE name in directory                     ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   zr     entries compare equal                                ;
        ;...............................................................;

_compareLFN_String:

        lodsb                                           ; get character from string
        _upperCase al                                   ; upper case
        or al, al                                       ; null terminator 
        jz _compareLFN_String_End                       ; exit -->
        cmp al, '\'                                     ; string terminator
        jz _compareLFN_String_End                       ; exit -->

        cmp al, '"'
        jz _compareLFN_String                           ; skip quotes -->

        cmp al, '?'
        jz _compareLFN_String_Match                     ; ? always matches -->

        mov ah, byte ptr es:[ di ]                      ; get UNICODE char
        _upperCase ah                                   ; upper case character
        cmp al, ah                                      ; match ?
        jnz _compareLFN_String_End                      ; else fail -->

_compareLFN_String_Match:
        inc di
        inc di
        loop _compareLFN_String

_compareLFN_String_End:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compare LFN String                                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   cx     is max length to compare                             ;
        ;   ds:si  ptr to Search String                                 ;
        ;   es:di  ptr to UNICODE name in directory                     ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   zr     entries compare equal                                ;
        ;...............................................................;

_compareString:

        lodsb                                           ; get character from string
        _upperCase al                                   ; upper case
        or al, al                                       ; null terminator 
        jz _compareString_End                           ; exit -->
        cmp al, '\'                                     ; string terminator
        jz _compareString_End                           ; exit -->
        cmp al, '.'                                     ; string terminator
        jz _compareString_End                           ; exit -->

        cmp al, '"'
        jz _compareString                               ; skip quotes -->

        cmp al, '?'
        jz _compareString_Match                         ; ? always matches -->

        mov ah, byte ptr es:[ di ]                      ; get char
        _upperCase ah                                   ; upper case character
        cmp al, ah                                      ; match ?
        jnz _compareString_End                          ; else fail -->

_compareString_Match:
        inc di
        loop _compareString

        xor cx, cx                                      ; set zero flag if all matched

_compareString_End:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Read Next Entry                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:di  ptr to Disk Access Block                             ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:bx  pointer in block buffer to data                      ;
        ;   cx     remaining bytes in block                             ;
        ;   zr     entry located                                        ;
        ;...............................................................;

_ReadNextDirEntry:

        mov bx, di
        add word ptr ss:[ diskAcPosition. _low  ][ bx ], sizeDIRENTRY
        adc word ptr ss:[ diskAcPosition. _high ][ bx ], 0000
        call _FATReadRandom                             ; read into buffer
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Init structLongFilename                                      ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:bx  ptr to structLongFilename                            ;
        ;                                                               ;
        ;...............................................................;

initStructLongFilename:

        SaveRegisters es, di

        setES ss
        mov di, bx
        clearMemory sizeSTRUCTLONGFILENAME

        mov word ptr [ _lfnSearchAttribs ][ bx ], -1
        RestoreRegisters di, es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compute Direntries for LFN (inc SFN)                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:di  ptr to structLongFilename                            ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     num directory entries required                       ;
        ;   dx     remainder bytes                                      ;
        ;   es:si  ptr to long name                                     ;
        ;                                                               ;
        ;...............................................................;

ComputeDirEntries:

        push bx
        mov ax, sizeCANONICALPOINTER
        mul word ptr [ _lfnCanonicalList. _canpNumEntries ][ di ]

        mov bx, ax
        lea bx, offset [ _lfnCanonicalList. _canpArray - sizeCANONICALPOINTER ][ di ][ bx ]

        xor dx, dx
        mov cx, LFN_NAMECHARPERENTRY
        mov ax, word ptr [ _canpStringLength ][ bx ]
        mov word ptr [ _lfnSearchStringLength ][ di ], ax
        inc ax                                          ; make sure we count null term char as a copy char
        div cx                                          ; # entries in ax

        or dx, dx
        jz ComputeDirEntries_08
        inc ax                                          ; incr if roundoff

ComputeDirEntries_08:
        inc ax                                          ; incr for short entry

        les si, dword ptr [ _canpStringPointer ][ bx ]
        mov word ptr [ _lfnSearchLongNameText. _segment ][ di ], es
        mov word ptr [ _lfnSearchLongNameText. _pointer ][ di ], si
        mov cx, word ptr [ _canpStringLength ][ bx ]

        pop bx
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Build Canonical Pointer List                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Expands filenames into their canonical form, replacing and   ;
        ;  removing any . and .. paths.                                 ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     options: FILECANNOT_BEDIRECTORY                      ;
        ;   es:si  ptr to input filename passed                         ;
        ;   ss:di  ptr to canonical pointer structure                   ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ss:di  ptr to pointer structure                             ;
        ;   cx:dx  current cluster, if search to begin here             ;
        ;   bx     offset to optimized search entry                     ;
        ;   ax     drive, or error                                      ;
        ;   cy     if error detected                                    ;
        ;...............................................................;

BuildCanonicalPointerList:

        Entry
        ddef _UnexpandedFileName, es, si
        ddef _pCanonicalList, ss, di
        def  _options, ax

        xor ax, ax
        def _drive, ax                                  ; initialize to 0's
        def _entries, ax                                ; entries in list
        def _optSearchEntry, ax                         ; optimize search entry
        ddef _currCluster, ax, ax                       ; cluster 0000's

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  skip through any leading spaces
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        SaveSegments
        currSegment ds                                  ; point to default segment
        les di, dword ptr [ _pCanonicalList ][ bp ]
        clearMemory sizeCANONICALPOINTERLIST            ; clear list

        les si, dword ptr [ _UnexpandedFileName ][ bp ]
        call getDrive                                   ; see if argument has drive 
        storarg _drive, ax                              ; save drive value (either spec'd or default)
        mov word ptr [ _UnexpandedFileName. _pointer ][ bp ], si ; save pointer past drive

        mov ax, offset pexterrPathNotFound              ; error message
        jc buildCanonicalPtrs_Return                    ; invalid drive -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get selected drive current directory
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg ax, _drive
        mov cl, sizeCDS
        mul cl                                          ; ax contains offset to current drive

        les si, dword ptr ss:[ _RxDOS_pCDS ]            ; actual address in CDS
        add si, ax                                      ; from
        add si, _cdsActualDirectory                     ; proper offset

        mov cx, word ptr es:[ _cdsStartClusterDir. _high ][ si ]
        mov dx, word ptr es:[ _cdsStartClusterDir. _low ][ si ]
        mov word ptr [ _currCluster. _high ][ bp ], cx
        mov word ptr [ _currCluster. _low ][ bp ], dx   ; current starting cluster

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  build canonical list
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg ax, _drive
        call ptrLFNCurrentDirectory                     ; es:si ptr to curr directory

        xor dx, dx                                      ; no entries set (yet)
        call _parseCanonicalItems                       ; build canonical list from CDS
        jc buildCanonicalPtrs_Return                    ; if error -->
        storarg _optSearchEntry, dx                     ; optimize search entry

        getdarg es, si, _UnexpandedFileName
        call _parseCanonicalItems                       ; build canonical list from input string
        jc buildCanonicalPtrs_Return                    ; if error -->

        or cx, cx                                       ; can optiomize directory search ?
        jnz buildCanonicalPtrs_18                       ; yes -->
        stordarg _currCluster, 0000, 0000               ; can't optimize for current cluster YET
        storarg _optSearchEntry, 0000                   ; optimize search entry

buildCanonicalPtrs_18:
        mov di, word ptr [ _pCanonicalList. _pointer ][ bp ]
        _CANPtrToEntry di, dl

        xor cx, cx
        getarg ax, _drive                               ; return drive

        mov word ptr ss:[ _canpStringPointer. _segment ][ di ], cx
        mov word ptr ss:[ _canpStringPointer. _pointer  ][ di ], cx
        mov word ptr ss:[ _canpStringLength ][ di ], cx
        mov word ptr ss:[ _canpFlags        ][ di ], cx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

buildCanonicalPtrs_Return:
        mov di, word ptr [ _pCanonicalList. _pointer ][ bp ]

        getarg bx, _optSearchEntry                      ; optimize search entry
        mov word ptr [ _canpOptSearchEntry ][ di ], bx

        getdarg cx, dx, _currCluster                    ; where to start search
        mov word ptr [ _canpCluster. _low  ][ di ], dx
        mov word ptr [ _canpCluster. _high ][ di ], cx
        mov word ptr [ _canpDrive ][ di ], ax

        RestoreSegments
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Parse Canonical List                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Scans ASCIZ string passed with path and builds a list of     ;
        ;  pointers to each subdirectory element. Drive is skipped.     ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:di  base pointer to canonical table                      ;
        ;   es:si  pointer to input filename ASCIZ                      ;
        ;   dx     entries already in table                             ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ss:di  pointer to canonical table                           ;
        ;   dx     entries in table                                     ;
        ;   cx     FALSE if can optimize directory search               ;
        ;                                                               ;
        ;...............................................................;

_parseCanonicalItems:

        Entry

        xor ax, ax
        def  _drive, ax                                 ; drive
        def  _entries, dx
        def  _lowestEntry, dx                           ; if falls below lowest entry, turn opt off
        def  _recoverEntry, 0000

        ddef _pCanonicalTable, ss, di
        ddef _pFilename, es, si
        ddef _EntryTextPtr
        ddef _EntryLength

        def  _pCanonicalNextEntry                       ; assumes table is on stack (ss:)
        def  _CanOptimize, TRUE

        SaveSegments di, si

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  test first argument 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        call _skipLeadingSpaces                         ; skip leading spaces
        ifz _parseCanonicalItems_56                     ; if end of string --> 
        cmp al, '\'                                     ; initial path designator
        jnz _parseCanonicalItems_12                     ; no -->

        inc si
        xor dx, dx
        storarg _entries, dx                            ; cancel all entries      
        storarg _CanOptimize, False

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get arguments
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_parseCanonicalItems_12:
        _CANPtrToEntry di, dl
        storarg _pCanonicalNextEntry, di

_parseCanonicalItems_14:
        call _skipLeadingSpaces                         ; skip leading spaces
        ifz _parseCanonicalItems_56                     ; if end of string --> 

        call _GetLeadingBytes                           ; ax contains first two bytes
        ifz _parseCanonicalItems_56                     ; if end of string --> 

        cmp ax, '..'                                    ; is .. entry ?
        jz _parseCanonicalItems_38                      ; yes, backup -->
        cmp al, '.'                                     ; is . entry ?
        ifz _parseCanonicalItems_44                     ; yes, skip entry -->
        cmp ah, ':'                                     ; is drive (:) entry ?
        ifz _parseCanonicalItems_44                     ; yes, skip entry -->

        stordarg _EntryTextPtr, es, si
        call _scanForward                               ; scan forward. cx returns length.

        push es
        push si                                         ; save current pointer

        cmp word ptr [ _CanOptimize ][ bp ], FALSE      ; if already false
        je _parseCanonicalItems_32                      ; no need to optimize search further -->

        getdarg es, si, _EntryTextPtr
        getarg di, _pCanonicalNextEntry
        call _ParseOptimizeCompare                      ; maybe we can optimize entry
        jnz _parseCanonicalItems_32                     ; no, save entry -->

        inc word ptr [ _recoverEntry ][ bp ]
        jmp short _parseCanonicalItems_34

_parseCanonicalItems_32:
        getdarg es, si, _EntryTextPtr
        getarg di, _pCanonicalNextEntry
        mov word ptr ss:[ _canpStringPointer. _segment ][ di ], es
        mov word ptr ss:[ _canpStringPointer. _pointer  ][ di ], si
        mov word ptr ss:[ _canpStringLength ][ di ], cx ; string length
        mov word ptr ss:[ _canpFlags        ][ di ], dx ; wild character flags

        cmp word ptr [ _CanOptimize ][ bp ], FALSE      ; if already false
        je _parseCanonicalItems_34                      ; skip test -->

        mov ax, word ptr [ _entries ][ bp ]
        cmp ax, word ptr [ _lowestEntry ][ bp ]         ; current entry falls below lowest ?
        jge _parseCanonicalItems_34                     ; not yet -->
        storarg _CanOptimize, FALSE                     ; cannot optimize search

_parseCanonicalItems_34:
        inc word ptr [ _entries ][ bp ]

        add di, sizeCANONICALPOINTER
        storarg _pCanonicalNextEntry, di

        pop si
        pop es                                          ; restore scan pointer
        jmp _parseCanonicalItems_14

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  process .. entry
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_parseCanonicalItems_38:
        getarg cx, _entries
        or cx, cx                                       ; can we go back ?
        jnz _parseCanonicalItems_40                     ; yes -->
        SetError pexterrPathNotFound, _parseCanonicalItems_64

_parseCanonicalItems_40:
        dec word ptr [ _entries ][ bp ]
        sub di, sizeCANONICALPOINTER
        storarg _pCanonicalNextEntry, di

        mov word ptr ss:[ _canpStringLength ][ di ], 0000; string length
        mov word ptr ss:[ _canpStringPointer. _segment ][ di ], 0000
        mov word ptr ss:[ _canpStringPointer. _pointer  ][ di ], 0000

        cmp word ptr [ _CanOptimize ][ bp ], FALSE      ; if already false
        jz _parseCanonicalItems_44                      ; skip test -->

        mov ax, word ptr [ _entries ][ bp ]
        cmp ax, word ptr [ _lowestEntry ][ bp ]         ; current entry falls below lowest ?
        jge _parseCanonicalItems_44                     ; not yet -->
        storarg _CanOptimize, FALSE                     ; cannot optimize search

_parseCanonicalItems_44:
        call _scanForward                               ; scan forward. cx returns length.
        jmp _parseCanonicalItems_14                     ; get next -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  done. can we fully optimize ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_parseCanonicalItems_56:
        getarg ax, _recoverEntry
        or ax, ax
        jz _parseCanonicalItems_64

        cmp ax, word ptr [ _lowestEntry ][ bp ]
        je _parseCanonicalItems_64
        storarg _CanOptimize, FALSE                     ; cannot optimize search
        clc

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_parseCanonicalItems_64:
        getarg dx, _entries
        getarg cx, _CanOptimize

        getdarg ds, di, _pCanonicalTable
        mov word ptr [ _canpNumEntries ][ di ], dx      ; entries

        RestoreSegments si, di
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compare. See If Directory Search can be Optimized.           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:di  canonical table entry                                ;
        ;   es:si  pointer to item in input filename                    ;
        ;   cx     length of item                                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   zr     entries are equal                                    ;
        ;                                                               ;
        ;...............................................................;

_ParseOptimizeCompare:

        saveAllRegisters
        cmp cx, word ptr [ _canpStringLength ][ di ]
        jnz _ParseOptimizeCompare_NotEqual              ; not be equal -->

        cmp word ptr [ _canpStringPointer. _segment ][ di ], 0000
        jz _ParseOptimizeCompare_NotEqual               ; not be equal -->

        setDS es
        mov es, word ptr ss:[ _canpStringPointer. _segment ][ di ]
        mov di, word ptr ss:[ _canpStringPointer. _pointer ][ di ]

_ParseOptimizeCompare_08:
        lodsb                                           ; get character
        _upperCase al
        mov ah, al

        mov al, byte ptr es:[ di ]
        _upperCase al

        cmp ah, al                                      ; compare
        jnz _ParseOptimizeCompare_NotEqual              ; if not equal -->

        inc di
        loop _ParseOptimizeCompare_08                   ; else compare to end -->

        xor cx, cx
        jmp short _ParseOptimizeCompare_Return

_ParseOptimizeCompare_NotEqual:
        mov ax, es
        or ax, ax                                       ; not zero

_ParseOptimizeCompare_Return:
        restoreAllRegisters
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Skip Leading Spaces                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:si  pointer to input filename                            ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   al     character at es:si                                   ;
        ;   zr     null character                                       ;
        ;                                                               ;
        ;...............................................................;

_skipLeadingSpaces:

        mov al, byte ptr es:[ si ]                      ; get next byte
        or al, al                                       ; null terminator ?
        jz _skipLeadingSpaces_12                        ; yes, end of string -->
        cmp al, ' '                                     ; space ?
        jnz _skipLeadingSpaces_12                       ; no, return -->
        inc si
        jmp _skipLeadingSpaces

_skipLeadingSpaces_12:
        or al, al                                       ; no carry, zr/nz set
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Leading Bytes                                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:si  pointer to input filename                            ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:si  adjusted pointer to input filename                   ;
        ;   ax     characters at es:si                                  ;
        ;   zr     null character                                       ;
        ;                                                               ;
        ;...............................................................;

_GetLeadingBytes:

        push si
        call _getNextNonQuotedCharacter                 ; get character
        mov cl, al                                      ; store character

        call _getNextNonQuotedCharacter                 ; get character
        mov ah, al                                      ; create al/ah pair
        mov al, cl
        or al, al
        pop si
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Scan Forward                                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:si  pointer to input filename                            ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:si  points to just after next \ or end of line           ;
        ;   al     terminating character                                ;
        ;   cx     length of string without quotes                      ;
        ;   dx     set to CANONICALENTRY_WILDCHARS if wild chars found  ;
        ;   zr     if string terminator found                           ;
        ;                                                               ;
        ;...............................................................;

_scanForward:

        xor dx, dx
        call _getNextNonQuotedCharacter                 ; get next character
        mov cx, si                                      ; save base pointer
        dec cx                                          ; account for current character

_scanForward_12:
        cmp al, '?'
        jz _scanForward_18
        cmp al, '*'
        jnz _scanForward_22

_scanForward_18:
        mov dx, CANONICALENTRY_WILDCHARS

_scanForward_22:
        call _getNextNonQuotedCharacter                 ; get next character
        jz _scanForward_28                              ; if end of string -->

        cmp al, '\'                                     ; scan until \
        jnz _scanForward_12                             ; keep looking -->
        inc cx                                          ; adjusts sub below to not incl \ char

_scanForward_28:
        sub cx, si                                      ; compute address difference
        neg cx                                          ; actual length
        or al, al                                       ; end of line status
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Next Non-Quoted Character                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:si  pointer to input filename                            ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:si  points to just after next \ or end of line           ;
        ;   al     character                                            ;
        ;   zr     if string terminator found                           ;
        ;                                                               ;
        ;...............................................................;

_getNextNonQuotedCharacter:

        mov al, byte ptr es:[ si ]                      ; get byte
        or al, al                                       ; null terminator ?
        jz _getNextNonQuoted_12                         ; yes, end of string -->

        inc si                                          ; point to next byte
        cmp al, '"'                                     ; quote char ?
        jnz _getNextNonQuoted_12
        jmp _getNextNonQuotedCharacter                  ; yes, get next -->

_getNextNonQuoted_12:
        or al, al
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Ptr Current Directory                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   es:si  current directory (CDS or LFNCDS)                    ;
        ;   NZ     if LFNCDS                                            ;
        ;                                                               ;
        ;...............................................................;

ptrLFNCurrentDirectory:

        Entry
        def  _actualdrive, ax

        push ax
        mov cx, sizeLFNCDS
        mul cx

        les si, dword ptr [ _RxDOS_pLFNCDS ]            ; LFNCDS base address
        add si, ax                                      ; drive offset
        add si, 3
        cmp byte ptr es:[ _lfncdsLongDirectory - 3 ][ si ], 0
        jnz ptrLFNCurrDir_16                            ; if name valid -->

        mov ax, sizeCDS
        mul word ptr [ _actualdrive ][ bp ]             ; ax contains offset to current drive

        les si, dword ptr [ _RxDOS_pCDS ]               ; CDS base address
        add si, ax                                      ; drive info
        mov ax, _cdsActualDirectory                     ; proper offset
        add al, byte ptr es:[ _cdsNonSubstOffset ][ si ]
        add si, ax
        xor ax, ax

ptrLFNCurrDir_16:
        pop ax
        clc
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Update Dir Access Block                                      ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   bx     ptr dirAccess block                                  ;
        ;   di     ptr longfilename block                               ;
        ;                                                               ;
        ;...............................................................;

updateDirAccessBlock:

        Entry
        def _drive, ax
        ddef _cluster, cx, dx
        def _ptrDirAccess, bx
        def _ptrstructLongfilename, di

        setDS ss
        setES ss
        getarg di, _ptrstructLongfilename
        mov cx, word ptr [ _lfnSearchStringLength ][ di ]
        les si, dword ptr [ _lfnSearchLongNameText ][ di ]
        lea bx, offset [ _lfnShortFCBName ][ di ]
        call createFCBNAMEfromLFN

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init other values
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setES ss
        getarg bx, _ptrDirAccess
        getarg di, _ptrstructLongfilename
        mov dx, word ptr [ _lfnDirCluster. _low  ][ di ]
        mov cx, word ptr [ _lfnDirCluster. _high ][ di ]
        mov word ptr [ fileAcDirCluster. _low    ][ bx ], dx
        mov word ptr [ fileAcDirCluster. _high   ][ bx ], cx

        mov dx, word ptr [ _lfnCluster. _low     ][ di ]
        mov cx, word ptr [ _lfnCluster. _high    ][ di ]
        mov word ptr [ fileAcCluster. _low       ][ bx ], dx
        mov word ptr [ fileAcCluster. _high      ][ bx ], cx

        getarg ax, _drive
        mov word ptr [ fileAcDrive ][ bx ], ax
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  LFN Allocate FindData Store                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   cy     could not allocate                                   ;
        ;   ss:di  ptr to Allocate area (if not cy)                     ;
        ;...............................................................;

LFNAllocateFindDataStore:

        setDS ss
        mov bx, offset RxDOS_LFNTempStorage
        mov cx, word ptr [ RxDOS_LFNTempStoreCount ]

LFNAllocFindDataStore_08:
        cmp word ptr [ bx ], 0000
        jz LFNAllocFindDataStore_14

        add bx, 2                                       ; size dd pointer
        loop LFNAllocFindDataStore_08                   ; see if more -->
        stc
        ret

LFNAllocFindDataStore_14:
        push bx                                         ; handle
        sub bx, offset RxDOS_LFNTempStorage             ; compute offset
        shr bx, 1                                       ; 
        mov ax, sizeLFNTEMPSTORE
        mul bl
        add ax, offset RxDOS_Heap
        mov di, ax
        clearMemory sizeLFNTEMPSTORE

        pop bx
        mov word ptr [ bx ], di                         ; allocate item

        clc
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  LFN Release FindData                                         ;
        ;...............................................................;

LFNReleaseFindData:

        setDS ss
        mov ax, word ptr [ _RxDOS_CurrentPSP ]
        mov bx, offset RxDOS_LFNTempStorage
        mov cx, word ptr [ RxDOS_LFNTempStoreCount ]

LFNReleaseFindData_08:
        cmp word ptr [ bx ], 0000                       ; allocated ?
        jz LFNReleaseFindData_12                        ; no, skip -->

        mov si, word ptr [ bx ]                         ; pointer to area
        cmp ax, word ptr [ _lfnfindstoreOwner ][ si ]   ; same as terminating owner ?
        jnz LFNReleaseFindData_12                       ; no, skip to next -->

        mov word ptr [ bx ], 0000
        mov word ptr [ _lfnfindstoreOwner ][ si ], 0000
        jmp short LFNReleaseFindData_14

LFNReleaseFindData_12:
        add bx, 2                                       ; size dw pointer
        loop LFNReleaseFindData_08                      ; see if more -->

LFNReleaseFindData_14:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  LFN Release FindData                                         ;
        ;...............................................................;

LFNValidateFindDataHandle:

        cmp bx, offset RxDOS_LFNTempStorage             ; see if find close table range
        jc LFNValidateFindData_12                       ; not valid -->

        mov cx, word ptr [ RxDOS_LFNTempStoreCount ]
        add cx, cx
        add cx, offset RxDOS_LFNTempStorage             ; see if find close table range
        cmp bx, cx                                      ; is handle out of range ?
        jnc LFNValidateFindData_12                      ; not valid -->

        cmp word ptr [ bx ], 0000                       ; already closed ?
        jz LFNValidateFindData_12                       ; yes, skip -->

        clc
        ret

LFNValidateFindData_12:
        stc
        ret

RxDOS   ENDS
        END
