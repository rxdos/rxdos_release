        TITLE   'Ren - RxDOS Command Shell Rename'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell Rename                                   ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Programmer's Notes:                                          ;
        ;                                                               ;
        ;  Command Shell consists of  two parts bound  together into a  ;
        ;  single executable load.  There  exists  a  single  resident  ;
        ;  command shell which is accessible by an Int 2Eh.             ;
        ;                                                               ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc
        include rxdoscin.inc

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;...............................................................;

RxDOSCMD SEGMENT PUBLIC 'CODE'
         assume cs:RxDOSCMD, ds:RxDOSCMD, es:RxDOSCMD, ss:RxDOSCMD

        public _Rename

        extrn CheckFileClose                            : near
        extrn CmndError_FileAlreadyExists               : near
        extrn DisplayErrorMessage                       : near
        extrn DisplayLine                               : near
        extrn PreProcessCmndLine                        : near
        extrn BuildParsedFilenameArray                  : near
        extrn CopyString                                : near

        extrn BuildFullPathFirstFile                    : near
        extrn BuildFullPathNextFile                     : near
        extrn BuildFullPathClose                        : near
        extrn CmndError_NoFilesFound                    : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Rename filenameA filenameB                                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_Rename:

        Entry
        def _ifAnyRenamed, 0000
        def _findhandle, -1
        def _ptrname
        def __argarray, di                              ; args array

        defbytes _srcfile, sizeParsedFilename
        defbytes _destfile, sizeParsedFilename
        defbytes _finddata, sizeFINDDATA
        defbytes __pathArg, sizeLFNPATH

        mov cx, 2                                       ; must have two arguments
        mov dx, cx                                      ; must have two arguments
        xor bx, bx
        call PreProcessCmndLine                         ; make sure args are ok
        ifc _renameError                                ; if error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  make sure second arg contains no drive or path info
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, __argarray
        mov cx, word ptr [ sizeArgCmd. arglength ][ di ]
        mov si, word ptr [ sizeArgCmd. argpointer ][ di ]; second argument
        lea di, offset [ _destfile ][ bp ]              ; dest parse info
        call BuildParsedFilenameArray                   ; make sure second arg contains no drive, path

        cmp word ptr [ _destfile. parsepathlength ][ bp ], 0000
        ifnz _renameError                               ; second argument contains drive:path info -->
        cmp word ptr [ _destfile. parseptrfilename ][ bp ], 0000
        ifz _renameError                                ; if no name, then we have an error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  parse first argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, __argarray
        mov cx, word ptr [ arglength ][ di ]
        mov si, word ptr [ argpointer ][ di ]           ; first argument
        lea di, offset [ _srcfile ][ bp ]               ; src parse info
        call BuildParsedFilenameArray                   ; make sure second arg contains no drive, path

        cmp word ptr [ _srcfile. parseptrfilename ][ bp ], 0000
        ifz _renameError                                ; if no name, then we have an error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  simple case (no wild card case)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ _srcfile. parsewildcharflag ][ bp ]
        add ax, word ptr [ _destfile. parsewildcharflag ][ bp ]
        jnz _rename_14                                  ; wild chars used -->

        getarg di, __argarray
        mov dx, word ptr [ argpointer ][ di ]           ; first arg
        mov di, word ptr [ argpointer. sizeArgCmd ][ di ]; second argument
        Int21 LFNRenameFile                             ; rename file
        ifc _renameError                                ; if error -->

        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  try to locate file(s)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_rename_14:
        getarg di, __argarray
        mov si, word ptr [ argpointer ][ di ]           ; point to source filename arg

        lea di, offset [ _finddata ][ bp ]              ; finddata workspace
        lea dx, offset [ __pathArg ][ bp ]              ; expanded path (sizeLFNPATH)
        fileAttrib ATTR_NORMAL
        call BuildFullPathFirstFile                     ; filename found for arg ?
        storarg _findHandle, bx                         ; save find handle
        storarg _ptrName, dx                            ; present name to next
        jc _rename_54                                   ; no more -->

_rename_18:
        mov si, word ptr [ _destfile. parseptrfilename ][ bp ]
        lea di, offset [ _finddata. findDataLongFilename ][ bp ]
        cmp byte ptr [ di ], 00
        jnz _rename_22

        push si
        push di
        lea si, offset [ _finddata. findDataShortFilename ][ bp ]
        call CopyString                                 ; overwrite long name if missing
        pop di
        pop si

_rename_22:
        mov al, byte ptr [ si ]
        or al, al                                       ; end of name ?
        jz _rename_36                                   ; not yet -->
        cmp al, '*'                                     ; asterisk ?
        jz _rename_36                                   ; cause rest of compare to look like ?'s

        inc si
        cmp al, '?'                                     ; this char a '?' ?
        jz _rename_24                                   ; yes, skip -->
        mov byte ptr [ di ], al                         ; else store template char

_rename_24:
        inc di
        jmp _rename_22
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  execute rename
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_rename_36:
        storarg _ifAnyRenamed, True

        lea dx, offset [ __pathArg ][ bp ]              ; expanded path (sizeLFNPATH)
        lea di, offset [ _finddata. findDataLongFilename ][ bp ]
        Int21 LFNRenameFile                             ; rename file
        jc _renameError                                 ; if error -->

        getarg bx, _findHandle
        getarg si, _ptrName                             ; expanded path (sizeLFNPATH)
        lea di, offset [ _finddata ][ bp ]              ; name area
        call BuildFullPathNextFile                      ; filename found for arg ?
        jnc _rename_18                                  ; continue deleting -->

        getarg bx, _findHandle
        call BuildFullPathClose

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  no more files
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_rename_54:
        cmp word ptr [ _ifAnyRenamed ][ bp ], 0000      ; any files renamed ?
        jnz _rename_56                                  ; yes -->

        mov dx, offset CmndError_NoFilesFound
        call DisplayErrorMessage

_rename_56:
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  error in rename command
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_renameError:
        mov dx, offset CmndError_FileAlreadyExists
        call DisplayErrorMessage
        Return

RxDOSCMD                        ENDS
                                END
