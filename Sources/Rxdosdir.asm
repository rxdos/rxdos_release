        TITLE   'Dir - RxDOS Command Shell Dir Function'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell Dir                                      ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Programmer's Notes:                                          ;
        ;                                                               ;
        ;  Command Shell consists of  two parts bound  together into a  ;
        ;  single executable load.  There  exists  a  single  resident  ;
        ;  command shell which is accessible by an Int 2Eh.             ;
        ;                                                               ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc
        include rxdoscin.inc

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;...............................................................;

RxDOSCMD SEGMENT PARA PUBLIC 'CODE'
         assume cs:RxDOSCMD, ds:RxDOSCMD, es:RxDOSCMD, ss:RxDOSCMD

        public _Dir
        public _makePath
        public BuildParsedFilenameArray

        extrn CheckOptOneArg                            : near
        extrn CmndError_InvalidDrive                    : near
        extrn CmndError_NoFilesFound                    : near
        extrn CRLF                                      : near
        extrn DisplayLine                               : near
        extrn DisplayErrorMessage                       : near
        extrn PreProcessCmndLine                        : near
        extrn RxDOS_AllFiles                            : near
        extrn RxDOS_AllExtensions                       : near
        extrn setPagingMode                             : near
        extrn _AppendPathName                           : near
        extrn CopyString                                : near
        extrn CheckFileClose                            : near
        extrn CheckFile                                 : near

        extrn _mul32                                    : near
        extrn _div32                                    : near

        extrn _DirAttribSwitch                          : near
        extrn _DirBareSwitch                            : near
        extrn _DirLowerCaseSwitch                       : near
        extrn _DirOrderSwitch                           : near
        extrn _DirPauseSwitch                           : near
        extrn _DirSubDirSwitch                          : near
        extrn _DirSwitches                              : near
        extrn _DirWideSwitch                            : near
        extrn _Dir_DirectoryOf                          : near
        extrn _Dir_DirEntry                             : near
        extrn _Dir_FileEntry                            : near
        extrn _Dir_Files_Summary                        : near
        extrn _Dir_Files_MBSummary                      : near
        extrn _Dir_NoVolumeLabel                        : near
        extrn _Dir_VolumeLabel                          : near
        extrn _Dir_VolumeSerialNumber                   : near

        extrn _lowerCaseString                          : near
        extrn _endofString                              : near
        extrn _sprintf                                  : near
        extrn returnVolumeName                          : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Dir Time To Ascii (USA Format)                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     time value                                           ;
        ;   di     address to display time of day                       ;
        ;                                                               ;
        ;...............................................................;

_dirTimeToAscii:

        push di
        
        push ax
        mov ch, 'a'
        mov cl, 11                              ; ch = 0
        shr ax, cl                              ; hours
        cmp ax, 13                              ; 1 pm or greater ?
        jc _dirTimeToAscii_08                   ; no -->

        sub ax, 12                              ; 1 pm or greater
        mov ch, 'p'                             ; set pm flag
        
_dirTimeToAscii_08:
        mov cl, 10
        div cl
        or ax, '00'                             ; conv to ascii
        stosw

        mov al,':'
        stosb

        pop ax
        mov cl, 5
        shr ax, cl                              ; minutes
        and ax, 00111111b                       ; mask off hours
        
        mov cl, 10
        div cl
        or ax, '00'                             ; conv to ascii
        stosw

        xor ah, ah
        mov al, ch
        stosw

        pop di
        cmp byte ptr [ di ], '0'
        jnz _dirTimeToAscii_12
        mov byte ptr [ di ], ' '

_dirTimeToAscii_12:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Dir Date To Ascii (USA Format)                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     date value                                           ;
        ;   di     address to display date of day                       ;
        ;                                                               ;
        ;...............................................................;

_dirDateToAscii:

        push di
        push ax
        mov cl,  5
        shr ax, cl                              ; Month
        and ax, 00001111b                       ; mask off year 

        mov cl, 10
        div cl
        or ax, '00'                             ; conv to ascii
        stosw

        mov al,'-'
        stosb

        pop ax
        push ax
        and ax, 00011111b                       ; mask off everything but day
        
        mov cl, 10
        div cl
        or ax, '00'                             ; conv to ascii
        stosw

        mov al,'-'
        stosb

        pop ax
        mov cl,  9
        shr ax, cl                              ; Year
        and ax, 01111111b                       ; mask
        add ax, 80                              ; 80 plus
        cmp ax, 100                             ; past year 2000 ?
        jc _dirDateToAscii_08                   ; no need to adjust -->

        sub ax, 100                             ; 2000 plus

_dirDateToAscii_08:
        mov cl, 10
        div cl
        or ax, '00'                             ; conv to ascii
        stosw

        xor al, al
        stosb

        pop di
        cmp byte ptr [ di ], '0'
        jnz _dirDateToAscii_12
        mov byte ptr [ di ], ' '

_dirDateToAscii_12:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Make Path                                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   si     points to parsed filename                            ;
        ;   di     points to filename to build                          ;
        ;...............................................................;

_makePath:

        push si
        push di
        mov bx, si
        mov byte ptr [ di ], 00

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  insert drive  
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ parsedrive ][ bx ]
        add ax, ':A'
        mov word ptr [ di ], ax                         ; set drive 

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  insert path
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_makePath_08:
    ;    mov si, word ptr [ parseptrpath ][ bx ]
        or si, si                                       ; path available ?
        jz _makePath_16                                 ; no -->

        call CopyString                                 ; copy path to output

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  insert filename
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_makePath_16:
        mov si, word ptr [ parseptrfilename ][ bx ]
        or si, si                                       ; filename available ?
        jz _makePath_32                                 ; no -->

        mov al, '\'
        stosb 
        call CopyString                                 ; copy filename to output

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  insert extension
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_makePath_24:
        mov si, word ptr [ parseptrextension ][ bx ]
        or si, si                                       ; extension available ?
        jz _makePath_32                                 ; no -->

        mov al, '.'
        stosb 
        call CopyString                                 ; copy filename to output

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_makePath_32:
        pop di
        pop si
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Directory                                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_Dir:

        Entry
        def  _currdisk
        def  _maxdisk
        def  _filesread
        def  _dirhandle, -1
        def  _extensionFlag
        def  __argarray, di                             ; arg array
        def  _summaryline
        def  _freespaceFraction
        ddef _freespace
        ddef _totalfilespace

        defbytes _asciiFileTime, 20
        defbytes _asciiFileDate, 20
        defbytes _pathname, sizeLFNPATH                 ; search pathname
        defbytes _filename, sizeLFNPATH                 ; search filename
        defbytes _parsedfilename, sizeParsedFilename
        defbytes _finddata, sizeFINDDATA
        defbytes _printbuffer, 1400
        defbytes _extendedfreespace, sizeEXTENDEDFREESPACE

        xor ax, ax
        mov word ptr [ _filesread ][ bp ], ax
        mov word ptr [ _totalfilespace. _low  ][ bp ], ax
        mov word ptr [ _totalfilespace. _high ][ bp ], ax

        mov cx, 0000                                    ; min args
        mov dx, 0001                                    ; max args
        mov bx, offset _DirSwitches                     ; dir switches
        call PreProcessCmndLine                         ; process switches and args
        ifc _dir_Exit                                   ; if error -->

        mov ax, word ptr [ _DirPauseSwitch. swFlags ]
        call setPagingMode

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get current, max disks
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Int21 CurrentDisk                               ; get current disk
        mov dl, al
        inc al                                          ; a=1, ...
        storarg _currdisk, ax                           ; save disk letter

        Int21 SelectDisk                                ; use select disk to get max
        xor ah, ah                                      ; top part is 00's
        storarg _maxdisk, ax                            ; save max disk letter

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if no args, create a *.* arg
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, __argarray
        mov si, word ptr [ argpointer ][ di ]           ; locate dir argument
        or si, si                                       ; no name provided ?
        jnz _dir_06                                     ; name provided -->
        mov si, offset RxDOS_AllFiles                   ; dummy path 

_dir_06:
        lea di, offset [ _pathname ][ bp ]
        call CopyString                                 ; copy whatever was entered

_dir_08:
        dec di                                          ; backup over null
        cmp byte ptr [ di - 1 ], ':'                    ; only entered drive and colon ?
        jnz _dir_10                                     ; no -->

        mov si, offset RxDOS_AllFiles                   ; dummy path 
        call CopyString                                 ; append all files

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get specific info from command: drive, extension
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_dir_10:
        xor cx, cx
        lea si, offset [ _pathname ][ bp ]
        lea di, offset [ _parsedfilename ][ bp ]
        call BuildParsedFilenameArray                   ; pointers/ length of arguments

        cmp word ptr [ _parsedfilename. parsedrive ][ bp ], -1
        jz _dir_12                                      ; no drive specified -->
        mov ax, word ptr [ _maxdisk ][ bp ]             ; get max disk letter
        cmp ax, word ptr [ _parsedfilename. parsedrive ][ bp ]
        jg _dir_12                                      ; drive is acceptable
        jmp _dir_DriveError                             ; else invalid drive selected -->

_dir_12:
        mov ax, word ptr [ _parsedfilename. parseptrextension ][ bp ]
        mov word ptr [ _extensionFlag ][ bp ], ax       ; save whether extension was ever present

        mov cl, LFNEXPAND_GETFULLPATHNAME
        lea si, offset [ _pathname ][ bp ]
        lea di, offset [ _filename ][ bp ]
        mov word ptr [ di ], '\'                        ; init area
        Int21 LFNGetActualFileName                      ; expand name
        jnc _dir_16                                     ; if name is ok -->

        mov cl, LFNEXPAND_GETFULLPATHNAME
        mov si, offset RxDOS_AllFiles                   ; dummy path 
        lea di, offset [ _filename ][ bp ]
        mov word ptr [ di ], '\'                        ; init area
        Int21 LFNGetActualFileName                      ; expand name

_dir_16:
        xor cx, cx
        lea si, offset [ _filename ][ bp ]
        lea di, offset [ _parsedfilename ][ bp ]

        xor ah, ah
        mov al, byte ptr [ si ]                         ; filename always starts with drive 
        storarg _currdisk, ax                           ; save current disk
        sub al, 'A'                                     ; subtract drive
        cmp al, byte ptr [ _maxdisk ][ bp ]             ; is it greater than max drive ?
        ifnc _dir_DriveError                            ; yes, bad drive -->

        call BuildParsedFilenameArray
        jnz _dir_28                                     ; if wild characters found -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  test directory, handle  . and .. special cases
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        fileAttrib ATTR_DIRECTORY
        mov si, FINDDATA_MSDOSDATEFORMAT
        lea di, offset [ _finddata ][ bp ]
        lea dx, offset [ _filename ][ bp ]
        Int21 LFNFindFirstFile                          ; locate file
        jc _dir_28

        storarg _dirhandle, ax                          ; save handle

_dir_20:
        test byte ptr [ _finddata. findDataAttributes ][ bp ], ATTR_DIRECTORY
        jz _dir_28                                      ; if not a directory -->

        cmp byte ptr [ _finddata. findDataShortFilename ][ bp ], '.'
        jnz _dir_22  

        getarg bx, _dirhandle                           ; get handle
        mov si, FINDDATA_MSDOSDATEFORMAT                ; format
        lea di, offset [ _finddata ][ bp ]              ; find data
        Int21 LFNFindNextFile                           ; locate next file
        jnc _dir_20                                     ; see if also a dir -->
        jmp short _dir_28

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if a directory name, change to include *.* for all files
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_dir_22:
        lea si, offset [ _filename ][ bp ]
        lea di, offset [ _pathname ][ bp ]
        Call CopyString

        mov si, offset RxDOS_AllFiles                   ; *.*
        lea di, offset [ _pathname ][ bp ]              ; append here
        call _AppendPathName                            ; append all files to dir

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  is file name present ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_dir_28:
        getarg bx, _dirhandle                           ; file handle
        call CheckFileClose

        lea si, offset [ _pathname ][ bp ]
        lea di, offset [ _filename ][ bp ]
        mov word ptr [ di ], '\'                        ; init area

        mov cl, LFNEXPAND_GETFULLPATHNAME
        Int21 LFNGetActualFileName                      ; expand name to get real display name
        jnc _dir_42                                     ; if ok -->

        lea si, offset [ _pathname ][ bp ]
        lea di, offset [ _filename ][ bp ]
        call CopyString

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  display header
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_dir_42:
        test word ptr [ _DirBareSwitch. swFlags ], SW_SWITCHSET
        jnz _dir_52                                     ; skip header  -->

        mov al, byte ptr [ _currdisk ][ bp ]            ; get current disk value
        mov bx, offset _Dir_NoVolumeLabel               ; assume no volume label
        lea di, offset [ _finddata ][ bp ]              ; get volume label
        call returnVolumeName                           ; get volume for drive
        ifc _dir_DriveError                             ; cannot open drive -->
        jnz _dir_44                                     ; if no volume name -->

        push di                                         ; save vol label pointer
        mov bx, offset _Dir_VolumeLabel                 ; print statement format

_dir_44:
        lea di, offset [ _currdisk ][ bp ]              ; pointer to current disk
        push di                                         ; current disk
        push bx                                         ; format
        lea di, offset [ _printbuffer ][ bp ]
        push di
        call _sprintf
        add sp, ax                                      ; # args passed
        call DisplayLine

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  display drive: path
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea si, offset [ _filename ][ bp ]
        lea di, offset [ _pathname ][ bp ]
        call CopyString                                 ; copy to pathname

_dir_48:
        dec di                  
        cmp byte ptr [ di ], '\'                        ; isolate path
        jnz _dir_48

        mov byte ptr [ di ], 0

        lea di, offset [ _pathname ][ bp ]
        push di                                         ; first arg encountered

        mov di, offset _Dir_DirectoryOf
        push di
        lea di, offset [ _printbuffer ][ bp ]
        push di
        call _sprintf
        add sp, ax                                      ; # args passed
        call DisplayLine

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  scan through files
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_dir_52:
        fileAttrib 0000, ATTR_DIRECTORY
        mov si, FINDDATA_MSDOSDATEFORMAT
        lea di, offset [ _finddata ][ bp ]
        lea dx, offset [ _filename ][ bp ]              ; pointer to search filename
        Int21 LFNFindFirstFile                          ; locate file
        ifc _dir_72                                     ; if none located -->
        storarg _dirhandle, ax                          ; save handle

_dir_54:
        inc word ptr [ _filesread ][ bp ]               ; files read

        lea di, offset [ _finddata. findDataLongFilename ][ bp ]
        push di

        mov ax, word ptr [ _finddata. findDataCreateTime. finddataTime ][ bp ]
        lea di, offset [ _asciiFileTime ][ bp ]
        call _dirTimeToAscii
        push di

        mov ax, word ptr [ _finddata. findDataCreateTime. finddataDate ][ bp ]
        lea di, offset [ _asciiFileDate ][ bp ]
        call _dirDateToAscii
        push di

        cmp byte ptr [ _finddata. findDataShortFilename ][ bp ], '.'
        test byte ptr [ _finddata. findDataAttributes ][ bp ], ATTR_DIRECTORY
        jnz _dir_60                                     ; if a directory -->

        lea di, offset [ _finddata. findDataFileSizeLow ][ bp ]
        mov ax, word ptr [ _low  ][ di ]
        mov dx, word ptr [ _high ][ di ]
        add word ptr [ _totalfilespace. _low  ][ bp ], ax
        adc word ptr [ _totalfilespace. _high ][ bp ], dx
        push di                                         ; third arg: file size

_dir_60:
        lea di, offset [ _finddata. findDataShortFilename ][ bp ]
        mov al, byte ptr [ di ]                         ; start of filename
        call _endofString                               ; point to null terminator

        push di                                         ; terminator, in case no extension
        cmp al, '.'                                     ; filename either . or .. ?
        jz _dir_64                                      ; yes, no need to find extension -->

        mov al, '.'
        lea cx, offset [ _finddata. findDataShortFilename ][ bp ]
        xchg di, cx                                     ; determine search length
        sub cx, di                                      ; search length
        repnz scasb                                     ; else scan for extension
        jnz _dir_64                                     ; if found, di points to extension
        mov byte ptr [ di-1 ], 0                        ; else, di will point to a null (no extension)
        pop ax
        push di                                         ; extension address, if found

_dir_64:
        lea di, offset [ _finddata. findDataShortFilename ][ bp ]
        push di                                         ; pointer to filename

        mov di, offset _Dir_FileEntry
        test byte ptr [ _finddata. findDataAttributes ][ bp ], ATTR_DIRECTORY
        ifz _dir_66                                     ; if not a directory -->

        mov di, offset _Dir_DirEntry

_dir_66:
        push di
        lea di, offset [ _printbuffer ][ bp ]
        push di
        call _sprintf
        add sp, ax                                      ; # args passed

        test word ptr [ _DirLowerCaseSwitch. swFlags ], SW_SWITCHSET
        jz _dir_68                                      ; not lower case option ->
        push ss
        push si                                         ; where string
        call _lowerCaseString                           ; lowercase string

_dir_68:
        call DisplayLine                                ; print line
        ifc _dir_Exit                                   ; control C abort -->

        getarg bx, _dirhandle                           ; dir handle
        mov si, FINDDATA_MSDOSDATEFORMAT                ; format
        lea di, offset [ _finddata ][ bp ]              ; finddata
        Int21 LFNFindNextFile                           ; locate next file
        ifnc _dir_54                                    ; if none located -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  print number of files 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_dir_72:
        cmp word ptr [ _filesread ][ bp ], 0000         ; any files read ?
        jnz _dir_76                                     ; yes -->

        mov dx, offset CmndError_NoFilesFound
        call DisplayLine                                ; file not found
        jmp short _dir_Exit

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  display files, space used, space free
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_dir_76:
        mov cx, sizeEXTENDEDFREESPACE
        lea dx, offset [ _filename ][ bp ]
        lea di, offset [ _extendedfreespace ][ bp ]
        Int21 LFNGetExtFreeSpace                        ; get free space

        mov word ptr [ _summaryline ][ bp ], offset _Dir_Files_Summary
        mov ax, word ptr es:[ _extFreeAvailablePhysSectors. _low   ][ di ]
        mov dx, word ptr es:[ _extFreeAvailablePhysSectors. _high  ][ di ]
        mov cx, word ptr es:[ _extFreeBytesPerSector. _low  ][ di ]
        call _mul32                                     ; x bx = total space

        test word ptr es:[ _extFreeAvailablePhysSectors. _high  ][ di ], 0FFF0h
        jz _dir_82                                      ; if it doesn't exceed 4GB result -->

        mov ax, word ptr es:[ _extFreeAvailablePhysSectors. _low   ][ di ]
        mov dx, word ptr es:[ _extFreeAvailablePhysSectors. _high  ][ di ]
        add ax, 0ffffh
        adc dx, 7h                                      ; handle possible rounding 
        mov cx, 2 * 1024                                ; 1024 * 1024 / 512
        call _div32
        xor dx, dx

        mov word ptr [ _summaryline ][ bp ], offset _Dir_Files_MBSummary
        lea di, offset [ _freespaceFraction ][ bp ]
        mov word ptr [ di ], 0000
        push di

_dir_82:
        stordarg _freespace, dx, ax

        lea di, offset [ _freespace ][ bp ]
        push di
        lea di, offset [ _totalfilespace ][ bp ]
        push di
        lea di, offset [ _filesread ][ bp ]
        push di

        push word ptr [ _summaryline ][ bp ]
        lea di, offset [ _printbuffer ][ bp ]
        push di
        call _sprintf
        add sp, ax                                      ; # args passed
        call DisplayLine                                ; print line

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_dir_Exit:
        getarg bx, _dirhandle                           ; file handle
        call CheckFileClose

_dir_Return:
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if error
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_dir_DriveError:
        mov dx, offset CmndError_InvalidDrive
        call DisplayErrorMessage                        ; display message
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Build Parsed Filename Array                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Scans a filename and builds a pointer list to drive, path,   ;
        ;  filename and extension terms.                                ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   cx     length of argument                                   ;
        ;   es:si  ptr to input filename passed                         ;
        ;   ss:di  ptr to parse list structure                          ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ss:di  ptr to pointer structure                             ;
        ;   zr     if name contains NO wild characters                  ;
        ;...............................................................;

BuildParsedFilenameArray:

        Entry
        
        def _length, cx
        def _source, si
        def _parseliststruct, di

        clearMemory sizeParsedFilename                  ; clear parsed filename area

        or cx, cx                                       ; length passed ?
        jnz _filenameparsearray_06                      ; yes -->

        push si
        push di
        mov di, si
        xor ax, ax
        mov cx, -1
        repnz scasb                                     ; scan arg length
        not cx
        dec cx
        pop di
        pop si

_filenameparsearray_06:
        mov word ptr [ parseptrstartstring. _pointer ][ di ], si
        mov word ptr [ parseptrstartstring. _segment ][ di ], es
        mov word ptr [ parsestringlength ][ di ], cx
        mov word ptr [ parsedrive ][ di ], -1           ; set drive invalid
        storarg _length, cx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if drive is passed
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr es:[ si ]                      ; get first term 
        cmp ah, ':'                                     ; drive separator ?
        jnz _filenameparsearray_08                      ; no -->

        and ax, 1Fh
        dec ax
        mov word ptr [ parsedrive ][ di ], ax           ; drive

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  scan string through entire path until extension, if any.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_filenameparsearray_08:
        getarg si, _source                              ; source ptr
        add si, cx                                      ; points to end

_filenameparsearray_12:
        dec si
        mov al, byte ptr es:[ si ]

        Goto '?', _filenameparsearray_WildChar
        Goto '*', _filenameparsearray_WildChar
        Goto '.', _filenameparsearray_Extension
        Goto ':', _filenameparsearray_Path
        Goto '\', _filenameparsearray_Path

_filenameparsearray_18:
        loop _filenameparsearray_12

        getarg ax, _source
        mov word ptr [ parseptrfilename ][ di ], si
        jmp short _filenameparsearray_32

_filenameparsearray_WildChar:
        mov word ptr [ parsewildcharflag ][ di ], True
        jmp _filenameparsearray_18

_filenameparsearray_Extension:
        mov dx, si
        inc dx
        mov word ptr [ parseptrextension ][ di ], dx

        getarg ax, _length
        sub ax, cx
        mov word ptr [ parseextensionlength ][ di ], ax
        jmp _filenameparsearray_18

_filenameparsearray_Path:
        mov dx, si
        inc dx
        mov word ptr [ parseptrfilename ][ di ], dx

_filenameparsearray_32:
        mov ax, word ptr [ parseptrfilename ][ di ]
        sub ax, word ptr [ _source ][ bp ]
        mov word ptr [ parsepathlength ][ di ], ax

        mov ax, word ptr [ parseptrextension ][ di ]
        sub ax, word ptr [ parseptrfilename ][ di ]
        dec ax
        mov word ptr [ parsefilenamelength ][ di ], ax

        mov ax, word ptr [ parsewildcharflag ][ di ]
        or ax, ax
        Return

RxDOSCMD                        ENDS
                                END
