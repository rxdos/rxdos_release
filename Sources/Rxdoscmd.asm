        TITLE   'Cmd - RxDOS Command Shell'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Programmer's Notes:                                          ;
        ;                                                               ;
        ;  Command Shell consists of  two parts bound  together into a  ;
        ;  single executable load.  There  exists  a  single  resident  ;
        ;  command shell which is accessible by an Int 2Eh.             ;
        ;                                                               ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc
        include rxdoscin.inc

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Execution Control                                            ;
        ;...............................................................;

        EXECCONTROL struc

exCtrlArgArray                  dd ?
exCtrlStdInHandle               dw ?
exCtrlStdOutHandle              dw ?

exCtrlFlags                     dw ?
exCtrlStdInFileName             db sizeEXPANDNAME dup(?)
exCtrlStdOutFileName            db sizeEXPANDNAME dup(?)
exCtrlExecBlock                 db sizeEXEC dup(?)

        EXECCONTROL ends

exCtrlPiped                     equ 8000h
exCtrlAppend                    equ 4000h               ; append to stdout
exTempInputFile                 equ 2000h               ; temp stdin created
sizeEXECCONTROL                 equ size EXECCONTROL

_BAT                            equ 0
_EXE                            equ 1
_COM                            equ 2
_UNKNOWN                        equ -1

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  If Special Values                                            ;
        ;...............................................................;

IF_NOT                          equ 01h
IF_ERRORLEVEL                   equ 02h
IF_EXIST                        equ 03h

_ARGTEXT                        equ _high
_ARGTYPE                        equ _low

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Shell                                          ;
        ;...............................................................;

RxDOSCMD SEGMENT PARA PUBLIC 'CODE'
         assume cs:RxDOSCMD, ds:RxDOSCMD, es:RxDOSCMD, ss:RxDOSCMD

        org 100h

        public CommandBegin

        public CheckOptOneArg
        public CmndError_BadSwitch
        public CmndError_CannotCopyOverSelf
        public CmndError_CannotCreateFile
        public CmndError_ContentsLostBeforeCopy
        public CmndError_FileAlreadyExists
        public CmndError_FileNotFound
        public CmndError_InvalidDate
        public CmndError_InvalidDrive
        public CmndError_InvalidTime
        public CmndError_NoFilesFound
        public CmndError_SyntaxError
        public CmndError_InvalidNumberArguments
        public CmndError_OutOfEnvironmentSpace
        public CmndError_ReadErrorAbort

        public CRLF
        public CmndLookup
        public _Copy_FilesCopied
        public CountArgs

        public InsertArg
        public deleteArg
        public deleteEnvVariable
        public DisplayErrorCode
        public DisplayErrorMessage
        public DisplayOutEnvSpace
        public DisplayLine
        public insertEnvVariable
        public PreProcessCmndLine
        public ReplaceForVariables
        public returnVolumeName
        public searchEnvVariable
        public setPagingMode
        public FixUpArguments

        public CheckFile
        public CheckFileClose
        public BuildFullPathFirstFile
        public BuildFullPathNextFile
        public BuildFullPathClose
        public LFNReturnPreferedFilenamePtr

        public RxDOSIntl_DateTemplate
        public RxDOSIntl_DayOfWeek
        public RxDOSIntl_TimeTemplate
        public RxDOSIntl_DateTimeTable

        public _div32
        public _mul32

        public RxDOS_AllFiles
        public RxDOS_AllExtensions
        public RxDOS_DefaultPrompt
        public RxDOS_ForArgs
        public RxDOS_Prompt
        public RxDOS_PromptSpec
        public RxDOS_Version

        public CopyString
        public _AppendPathName
        public _CmndParse_SeparatorCheck
        public _DirAttribSwitch
        public _DirBareSwitch
        public _DirLowerCaseSwitch
        public _DirOrderSwitch
        public _DirPauseSwitch
        public _DirSubDirSwitch
        public _DirSwitches
        public _DirWideSwitch
        public removeLooseSwitches
        public _Dir_DirectoryOf
        public _Dir_DirEntry
        public _Dir_FileEntry
        public _Dir_Files_Summary
        public _Dir_Files_MBSummary
        public _Dir_NoVolumeLabel
        public _Dir_VolumeLabel
        public _Dir_VolumeSerialNumber
        public _endofString
        public _EnvSegment
        public _GetNumber
        public _getStdinLine
        public _CommandParser
        public _lowerCaseString
        public _PleaseEnterDate
        public _PleaseEnterTime
        public _ShowCurrentDate
        public _ShowCurrentTime
        public _sprintf
        public _SwitchChar
        public _computeLength
        public _Seeif_WildCharacter
        public RxDOS_TruenameCurrDir

    ; in rxdoscpy.asm

        extrn _Copy                                     : near

    ; in rxdosdir.asm

        extrn _Dir                                      : near

    ; in rxdosfor.asm

        extrn _For                                      : near

    ; in rxdosprm.asm

        extrn _Prompt                                   : near
        extrn _Date                                     : near
        extrn _Time                                     : near

        extrn DisplayPrompt                             : near
        extrn formatCurrentDate                         : near
        extrn formatCurrentTime                         : near

    ; in rxdosren.asm

        extrn _Rename                                   : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Parser/ Execute                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ds:si  command line beginning with a count                  ;
        ;          (this fct does not rely on CR at end of buffer)      ;
        ;                                                               ;
        ;...............................................................;

_Int2E_CommandParser  PROC FAR

        push ds
        push si
        mov si, ss
        mov ds, si
        mov si, sp                                      ; old stack to ds: si

        cld
        cli
        setSS cs
        mov sp, offset RxDOS_AltStack                   ; switch stacks
        sti

        saveRegisters ds, si                            ; save pointer to other stack
        lds si, dword ptr [ si ]                        ; restore old ds:si values.
        call _CommandParser                             ; call command parser

        cli
        pop si
        pop ss    
        mov sp, si                                      ; back to old stack.
        sti

        pop si
        pop ds                                          ; restore ds: si values

        iret
_Int2E_CommandParser  ENDP

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Command Parser/ Execute                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ds:si  command line beginning with a count                  ;
        ;          (this fct does not rely on CR at end of buffer)      ;
        ;                                                               ;
        ;   The command is executed, which may require loading another  ;
        ;   program.                                                    ;
        ;                                                               ;
        ;...............................................................;

_CommandParser:

        Entry
        ddef _OriginalCmdLine, ds, si                   ; argument is copied
        defbytes _commandLine, 256                      ; argument is copied
        defbytes __argarray, sizemaxArgArray            ; argument array (last arg is null)

        SaveAllRegisters                                ; save all registers
        cld                                             ; and direction

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get switch character
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Int21 GetSetSwitchChar, 00                      ; get switch char
        mov byte ptr cs:[ _SwitchChar ], dl             ; save switch character

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy command line 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getdarg ds, si, _OriginalCmdLine

        lodsb 
        mov cl, al                                      ; length
        xor ch, ch
        or cx, cx                                       ; any arguments ?
        jz _commandParser_36                            ; if no arguments -->

        setES cs
        lea di, offset [ _commandLine ][ bp ]
        rep movsb                                       ; copy command line
        
        xor ax, ax
        stosb                                           ; add a null terminator

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  parse
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setDS cs        
        mov cx, maxArgArray - 2                         ; # argument array entries
        lea si, offset [ _commandLine ][ bp ]
        lea di, offset [ __argarray   ][ bp ]
        call _BuildArgArray                             ; break up into arg list
        jz _CommandParser_36                            ; if no arguments -->

        mov si, word ptr [ __argarray. argpointer ][ bp ]; get lead argument
        cmp byte ptr [ si ], ':'                        ; label line ?
        jz _CommandParser_36                            ; ignore -->

        lea di, offset [ __argarray   ][ bp ]
        call _executeCommandArray
        
        cmp word ptr [ RxDOS_BatchFile. batchEchoStatus ], No
        jz _CommandParser_36                            ; if no echo -->
        call CRLF

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  command completed
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_CommandParser_36:
        restoreAllRegisters
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Control C Interrupt Service Routine                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ds:si  command line beginning with a count                  ;
        ;          (this fct does not rely on CR at end of buffer)      ;
        ;                                                               ;
        ;   The command is executed, which may require loading another  ;
        ;   program.                                                    ;
        ;                                                               ;
        ;...............................................................;

_ControlC_ISR   PROC FAR

        Int21 GetPSPAddress
        cmp bx, word ptr cs:[ _PSPSegment ]             ; current cmd processor copy ?
        jz _ControlC_ISR_12                             ; yes, accept ctlC -->

        stc
        ret 2                                           ; cause abort

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  abort current command, batch files
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ControlC_ISR_12:
        cli
        mov ax, cs
        mov ds, ax
        mov es, ax
        mov ss, ax
        mov sp, word ptr [ RxDOS_PrevStackFrame ]       ; current stack frame
        sti

_ControlC_ISR_16:
        cmp word ptr [ RxDOS_BatchFile. batchFileHandle ], 0000
        jz _ControlC_ISR_24                             ; if not running a batch file -->

        call _EndCall                                   ; terminate batch file
        jmp _ControlC_ISR_16                            ; undo all nested batch calls

_ControlC_ISR_24:
        jmp CommandInput   

_ControlC_ISR   ENDP

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Execute Command Stored in Arg Array                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  argument array                                       ;
        ;...............................................................;

_executeCommandArray:

        Entry
        def __argarray, di                              ; argument array
        def __startCmdLine
        def __leadAtcommand, false
        def __stdin                                     ; stdin on entry
        def __stdout                                    ; stdout on entry
        
        defbytes __execCtrlBlock, sizeEXECCONTROL
        defbytes __pathArg, sizeLFNPATH

        mov cx, STDIN
        call _assignGetCurrHandle                       ; get current handle
        storarg __stdin, ax                             ; stdin on entry

        mov cx, STDOUT
        call _assignGetCurrHandle                       ; get current handle
        storarg __stdout, ax                            ; stdout on entry

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  scan remainder of command line for pipe, stdin and stdout args
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor ax, ax
        lea di, offset [ __execCtrlBlock ][ bp ]
        clearMemory sizeEXECCONTROL                     ; clear exec control block

        mov dx, -1
        lea bx, offset [ __execCtrlBlock ][ bp ]
        mov word ptr [ exCtrlStdInHandle ][ bx ], dx    ; no stdin file
        getarg di, __argarray                           ; argument array

_executeArray_04:
        mov dx, -1
        lea bx, offset [ __execCtrlBlock ][ bp ]
        mov word ptr [ exCtrlStdOutHandle ][ bx ], dx   ; no stdout file (stdin may have been piped)

        lea bx, offset [ __execCtrlBlock ][ bp ]
        call _assignRedirectedDevices                   ; arg array in [di]

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  split leading @ sign
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, __argarray                           ; restore pointer to arg array
        mov si, word ptr [ argpointer ][ di ]           ; get argument
        mov cx, word ptr [ arglength ][ di ]            ; arg length

        cmp byte ptr [ si ], "@"                        ; starts with leading @ sign ?
        jnz _executeArray_08                            ; no -->

        storarg __leadAtcommand, true
        inc word ptr [ argpointer ][ di ]               ; get argument
        dec word ptr [ arglength ][ di ]                ; arg length
        jnz _executeArray_08                            ; yes -->
        call DeleteArg                                  ; delete @arg

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if command is valid
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_executeArray_08:
        mov si, word ptr [ argpointer ][ di ]           ; get argument
        mov cx, word ptr [ arglength ][ di ]            ; arg length
        call checkInstallableCommandInterface           ; command executed ?
        jnz _executeArray_36                            ; yes -->

        getarg di, __argarray                           ; restore pointer to arg array
        mov si, word ptr [ argpointer ][ di ]           ; get lead argument
        mov cx, word ptr [ arglength ][ di ]            ; get arg length
        mov di, offset RxDOS_InternalCommands
        call CmndLookup                                 ; lookup command
        jnc _executeArray_26                            ; if command found -->

        call checkUnixStyleCommands                     ; Unix style commands ?
        jnz _executeArray_10                            ; if no unix cmds -->

        getarg di, __argarray                           ; restore pointer to arg array
        mov si, word ptr [ argpointer ][ di ]           ; get lead argument
        mov cx, word ptr [ arglength ][ di ]            ; get arg length
        mov di, offset RxDOS_UNIXStyleCommands
        call CmndLookup                                 ; lookup command
        jnc _executeArray_26                            ; if command found -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if valid drive letter
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_executeArray_10:
        getarg di, __argarray
        mov si, word ptr [ argpointer ][ di ]           ; get lead argument
        cmp word ptr ss:[ si + 1 ], ':'                 ; disk select ?
        jnz _executeArray_12                            ; if unknown -->

        call CountArgs                                  ; count how many arguments
        call _DiskSelect                                ; process disk select
        jmp short _executeArray_36

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if .exe, .com, or .bat (filename in ss:si )
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_executeArray_12:
        mov ax, _UNKNOWN
        lea dx, offset [ __PathArg ][ bp ]
        call _executeProgram                            ; try to execute program
        jc _executeArray_36                             ; if error -->

        call _saveBatchArguments                        ; if batch file, save arguments
        jmp short _executeArray_36
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  go process internal command
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_executeArray_26:
        getarg di, __argarray                           ; argument pointer
        call SplitArgs                                  ; for cd.. cases

        add di, sizeArgCmd                              ; skip past lead arg
        mov si, word ptr [ argpointer ][ di ]           ; get lead argument
        call CountArgs                                  ; count how many arguments
        call bx                                         ; go execute commands

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  done. temp input file created ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_executeArray_36:
        mov byte ptr [ PageLines ], 00                  ; cancel page lines

        mov bx, STDIN
        mov ax, word ptr [ __execCtrlBlock. exCtrlStdInHandle ][ bp ]
        call _CloseRedirectedDevice

        getarg di, __argarray
        test word ptr [ __execCtrlBlock. exCtrlFlags ][ bp ], exTempInputFile
        jz _executeArray_42                             ; not piped in -->

        lea dx, offset [ __execCtrlBlock. exCtrlStdInFileName ][ bp ]
        Int21 LFNDeleteFile                             ; delete temp stdin

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  is arg piped ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_executeArray_42:
        getarg di, __argarray
        test word ptr [ __execCtrlBlock. exCtrlFlags ][ bp ], exCtrlPiped
        jnz _executeArray_46

        mov bx, STDIN
        getarg ax, __stdin                              ; stdin on entry
        call _CloseRedirectedDevice                     ; close device

        mov bx, STDOUT
        getarg ax, __stdout                             ; stdout on entry
        call _CloseRedirectedDevice                     ; close device
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  arg is piped
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_executeArray_46:
        mov ax, word ptr [ di ]
        add di, sizeArgCmd                              ; skip past lead arg
        or ax, ax                                       ; at end of search ?
        jnz _executeArray_46                            ; keep loopking -->

        and word ptr [ __execCtrlBlock. exCtrlFlags ][ bp ], NOT exCtrlPiped
        or word ptr [ __execCtrlBlock. exCtrlFlags ][ bp ], exTempInputFile
        storarg __argarray, di                          ; next in arg array

    ; must reopen file given by temp...
    ; maybe should not have closed it.

        mov cx, STDOUT
        call _assignGetCurrHandle                       ; get current handle
        mov word ptr [ __execCtrlBlock. exCtrlStdInHandle ][ bp ], ax

        mov bx, STDOUT
        Int21 CommitFile                                ; close stdout

        xor cx, cx
        xor dx, dx
        mov bx, STDOUT
        Int21 MoveFilePointer, SEEK_BEG                 ; point to beg of file

        lea si, offset [ __execCtrlBlock. exCtrlStdOutFileName ][ bp ]
        lea di, offset [ __execCtrlBlock. exCtrlStdInFileName ][ bp ]
        call CopyString

        lea di, offset [ __execCtrlBlock. exCtrlStdOutFileName ][ bp ]
        mov byte ptr [ di ], 0

        mov bx, STDOUT
        mov cx, STDIN
        Int21 ForceFileHandle                           ; redirect stdout -> stdin

        mov bx, STDOUT
        mov ax, word ptr [ __execCtrlBlock. exCtrlStdOutHandle ][ bp ]
        call _CloseRedirectedDevice

        getarg di, __argarray                           ; next in arg array
        jmp _executeArray_04

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Build Argument Array                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ds:si  command line beginning with a count                  ;
        ;          (this fct does not rely on CR at end of buffer)      ;
        ;   ss:di  argument array                                       ;
        ;          (returns a pointer into the command line at the      ;
        ;           start of each argument.  Multiple switches are      ;
        ;           detected by testing the get command switch char).   ;
        ;   cx     max number of arguments allowed                      ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   zr     if no arguments passed                               ;
        ;...............................................................;

_BuildArgArray:

        Entry
        ddef _commandLine, ds, si
        ddef __argarray, ss, di                         ; (maxArgArray) arguments
        def _args, cx

        def _argsStored, 0000
        def _argTableStart, di
        def _insideQuotes, FALSE

        setES ss

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  skip whitespace
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_buildArgArray_06:
        lodsb                                           ; get character
        or al, al                                       ; end of line ?
        jz _buildArgArray_36                            ; yes, end of arg -->
        cmp al, ' '                                     ; leading white space ?
        jz _buildArgArray_06                            ; yes, skip -->

        dec si

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  record argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_buildArgArray_14:
        mov word ptr [ argpointer ][ di ], si           ; store argument pointer
        mov word ptr [ arglength ][ di ], 0000          ; store argument length
        add di, sizeArgCmd

        inc word ptr [ _argsStored ][ bp ]
        dec word ptr [ _args ][ bp ]                    ; more args allowed ?
        jle _buildArgArray_36                           ; no -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  test lead character
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lodsb                                           ; get next character
        or al, al                                       ; end of line ?
        jz _buildArgArray_36                            ; yes, end of arg -->

        mov ah, byte ptr [ si ]                         ; look ahead
        cmp ax, '>>'                                    ; redirect ?
        jnz _buildArgArray_18                           ; yes, go process -->

        inc si
        mov word ptr [ arglength - sizeArgCmd ][ di ], 0002
        jmp _buildArgArray_06

_buildArgArray_18:
        mov word ptr [ arglength - sizeArgCmd ][ di ], 0001
        call _CmndParse_SeparatorCheck                  ; parse break ?
        jz _buildArgArray_06                            ; yes, record argument -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  accept all characters until next separator
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_buildArgArray_22:
        cmp al, doubleQuote                             ; quote character ?
        jnz _buildArgArray_24                           ; no ->
        not word ptr [ _insideQuotes ][ bp ]            ; set inside quotes flag

_buildArgArray_24:
        lodsb                                           ; get next character
        or al, al                                       ; end of line ?
        jz _buildArgArray_30                            ; yes, end of arg -->

        cmp word ptr [ _insideQuotes ][ bp ], 0000      ; inside quotes ?
        jnz _buildArgArray_22                           ; yes, no break characters -->

        cmp al, ' '                                     ; space break ?
        jz _buildArgArray_30                            ; yes, skip whitespace -->
        cmp al, byte ptr [ _SwitchChar ]                ; switch character ?
        jz _buildArgArray_30                            ; yes, record argument -->
        call _CmndParse_SeparatorCheck                  ; parse break ?
        jnz _buildArgArray_22                           ; no -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  accept all characters until next separator
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_buildArgArray_30:
        push ax
        dec si                                          ; don't include separator char
        mov ax, si                                      ; compute length of argument
        sub ax, word ptr [ argpointer - sizeArgCmd ][ di ]
        mov word ptr [ arglength - sizeArgCmd ][ di ], ax

        pop ax
        cmp al, ' '                                     ; space break ?
        jz _buildArgArray_06                            ; yes, skip whitespace -->

        or al, al
        jnz _buildArgArray_14

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_buildArgArray_36:
        xor ax, ax
        stosw                                           ; store argument pointer
        stosw                                           ; store argument length
        stosw                                           ; store argument pointer
        stosw                                           ; store argument length

        getarg ax, _argsStored
        getarg di, _argTableStart
        cmp word ptr [ argpointer ][ di ], 0000         ; args passed ?
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Split Args                                                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This routine detects and corrects concatenated args.         ;
        ;                                                               ;
        ;  Some args are passed in the arg array as a single arg but    ;
        ;  in fact they need to be split up, as in 'cd..'               ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   di     pointer to args array                                ;
        ;...............................................................;

SplitArgs:

        Entry
        def __argarray, di                              ; pointer to args
        def __cmdstarts, di

        mov si, word ptr [ argpointer ][ di ]           ; get argument
        mov cx, word ptr [ arglength ][ di ]            ; arg length
        or si, si                                       ; args passed ?
        jz _SplitArgs_36                                ; none -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  parse command
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_SplitArgs_12:
        lodsb
        or al, al                                       ; null ?
        jz _SplitArgs_36                                ; end of search -->
        cmp al, ' '+1                                   ; space or other break ?
        jc _SplitArgs_36                                ; end of search -->
        cmp al, '\'                                     ; dir break ?
        jz _SplitArgs_16                                ; yes, split arg -->
        cmp al, '.'                                     ; period break ?
        jz _SplitArgs_16                                ; yes, split arg -->
        cmp al, doubleQuote                             ; double quotes ?
        jz _SplitArgs_16                                ; yes, split arg -->
        cmp al, singleQuote                             ; single quotes ?
        jz _SplitArgs_16                                ; yes, split arg -->

        loop _SplitArgs_12                              ; continue if none of the above -->
        jmp short _SplitArgs_36

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  insert arg
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_SplitArgs_16:
        mov ax, word ptr [ arglength ][ di ]
        sub ax, cx                                      ; length of arg
        mov word ptr [ arglength ][ di ], ax            ; save length

        dec si                                          ; arg pointer
        add di, sizeArgCmd                              ; insert after current arg
        call InsertArg                                  ; insert arg

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_SplitArgs_36:
        getarg di, __cmdstarts                          ; pointer to args
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Command Parse Separator Check                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   al     character from parser                                ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   zr     is a separator character                             ;
        ;...............................................................;

_CmndParse_SeparatorCheck:

        push es
        push di
        push cx

        setES cs
        mov di, offset _CmndParse_Separators
        mov cx, offset _CmndParse_SeparatorsLength
        repnz scasb

        pop cx
        pop di
        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Scan and Assign Redirection                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  argument array                                       ;
        ;   ss:bx  pointer to Execution Control Block                   ;
        ;...............................................................;

_assignRedirectedDevices:

        Entry
        def __argarray, di                              ; argument array
        def __contargarray, di                          ; continue argument array
        def __execCtrlBlock, bx                         ; execution control block

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  scan remainder of command line for pipe, stdin and stdout args
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_assignRedirect_06:
        getarg di, __contargarray                       ; continue argument array

_assignRedirect_08:
        mov si, word ptr [ argpointer ][ di ]
        or si, si                                       ; end of args ?
        ifz _assignRedirect_36                          ; yes -->

        mov ax, word ptr [ si ]                         ; get arg assignment
        cmp ax, '>>'                                    ; append to stdout ?
        ifz _assignRedirect_AppendStdOut                ; yes -->
        cmp al, '>'                                     ; stdout ?
        jz _assignRedirect_StdOut                       ; yes -->
        cmp al, '<'                                     ; stdin ?
        jz _assignRedirect_StdIn                        ; yes -->
        cmp al, '|'                                     ; pipe ?
        ifz _assignRedirect_Pipe                        ; yes -->

        add di, sizeArgCmd                              ; advance over command index
        jmp _assignRedirect_08                          ; go to next -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  stdout
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_assignRedirect_StdOut:
        mov byte ptr [ si ], 00                         ; set a null at terminator
        storarg __contargarray, di                      ; continue argument array
        call deleteArg                                  ; kill '>' arg

        mov bx, word ptr [ __execCtrlBlock ][ bp ]      ; execution control block
        lea dx, offset [ exCtrlStdOutFileName ][ bx ]   ; offset to filename
        call _asgnGetFileName                           ; arg to [dx]

        ExCreateFile dx, ATTR_NORMAL
        ifc _assignRedirect_Error                       ; just display error -->

        push ax
        mov bx, ax
        mov cx, STDOUT
        call _assignGetCurrHandle

        getarg di, __execCtrlBlock
        mov word ptr [ exCtrlStdOutHandle ][ di ], ax   ; save old handle
        Int21 ForceFileHandle                           ; redirect stdout

        pop bx                                          ; this close frees file handle after 
        Int21 CloseFile                                 ; force replicate handle
        jmp _assignRedirect_06

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  stdin
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_assignRedirect_StdIn:
        mov byte ptr [ si ], 00                         ; set a null at terminator
        storarg __contargarray, di                      ; continue argument array
        call deleteArg                                  ; kill '<' arg

        mov bx, word ptr [ __execCtrlBlock ][ bp ]      ; execution control block
        lea dx, offset [ exCtrlStdInFileName ][ bx ]    ; offset to filename
        call _asgnGetFileName                           ; arg to [dx]

        ExOpenFile dx, OPEN_ACCESS_READONLY             ; open read only
        ifc _assignRedirect_Error                       ; just display error -->

        mov bx, ax
        mov cx, STDIN
        call _assignGetCurrHandle
        getarg di, __execCtrlBlock
        mov word ptr [ exCtrlStdInHandle ][ di ], ax    ; save old handle
        Int21 ForceFileHandle                           ; redirect stdout

        jmp _assignRedirect_06

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  pipe
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_assignRedirect_Pipe:
        mov byte ptr [ si ], 00                         ; set a null at terminator
        storarg __contargarray, di                      ; continue argument array
        mov word ptr [ argpointer ][ di ], 0000         ; place an end marker in arg list
        mov word ptr [ arglength  ][ di ], 0000         ; place an end marker in arg list

        mov bx, word ptr [ __execCtrlBlock ][ bp ]      ; execution control block
        lea bx, offset [ exCtrlStdOutFileName ][ bx ]   ; offset to filename
        mov byte ptr [ bx ], 0

        mov dx, bx
        mov cx, OPEN_ACCESS_READWRITE                   ; create read/write
        Int21 CreateUniqueFile                          ; if not found, create 
        ifc _assignRedirect_Error                       ; just display error -->

        push ax
        mov bx, ax
        mov cx, STDOUT
        call _assignGetCurrHandle

        getarg di, __execCtrlBlock
        mov word ptr [ exCtrlStdOutHandle ][ di ], ax   ; save old handle
        Int21 ForceFileHandle                           ; redirect stdout

        pop bx                                          ; this close frees file handle after 
        Int21 CloseFile                                 ; force replicate handle

        getarg di, __contargarray                       ; continue argument array
        getarg bx, __execCtrlBlock                      ; execution control block
        mov word ptr [ exCtrlArgArray. _pointer ][ bx ], di
        or word ptr [ exCtrlFlags ][ bx ], exCtrlPiped  ; say arg is piped.
        jmp _assignRedirect_36                          ; exit -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  append stdout
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_assignRedirect_AppendStdOut:
        mov byte ptr [ si ], 00                         ; set a null at terminator
        storarg __contargarray, di                      ; continue argument array
        call deleteArg                                  ; kill '>' arg
        call deleteArg                                  ; kill '>' arg

        mov bx, word ptr [ __execCtrlBlock ][ bp ]      ; execution control block
        lea dx, offset [ exCtrlStdOutFileName ][ bx ]   ; offset to filename
        call _asgnGetFileName                           ; arg to [dx]

        ExCreateFile dx                                 ; create NORMAL
        jc _assignRedirect_Error                        ; just display error -->

        push ax
        xor cx, cx
        xor dx, dx
        mov bx, ax
        Int21 MoveFilePointer, SEEK_END                 ; point to end of file

        mov cx, STDOUT
        call _assignGetCurrHandle
        getarg bx, __execCtrlBlock
        mov word ptr [ exCtrlStdOutHandle ][ bx ], ax   ; save old handle

        pop bx
        push bx
        Int21 ForceFileHandle                           ; redirect stdout

        pop bx
        Int21 CloseFile

        jmp _assignRedirect_06

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  redirection error
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_assignRedirect_Error:
        call DisplayErrorCode
        stc

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_assignRedirect_36:
        getarg di, __argarray                           ; argument array
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Kill Arg From List                                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   cx     handle whose value we want                           ;
        ;   ax     value of handle returned                             ;
        ;...............................................................;

_assignGetCurrHandle:

        push es
        push si
        push bx
        push cx
        Int21 GetPSPAddress

        pop si                                          ; restore handle offset
        mov es, bx                                      ; set PSP address
        les bx, dword ptr es:[ pspFileHandlePtr ]       ; point to file handles
        mov al, byte ptr es:[ bx + si ]                 ; recover existing handle
        xor ah, ah

        pop bx
        pop si
        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Filename From A Piped Or Redirected Argument             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   di     points to arg list                                   ;
        ;   dx     destination where to copy arg                        ;
        ;                                                               ;
        ;  arg is removed from arg list (it is deleted) once copied     ;
        ;...............................................................;

_asgnGetFileName:

        push ax
        push dx
        push di
        call fixDoubleQuotes                            ; remove excess double quotes
        mov cx, word ptr [ arglength ][ di ]            ; get length
        mov si, word ptr [ argpointer ][ di ]           ; get current arg

        mov di, dx
        rep movsb                                       ; move string

        xor ax, ax
        stosb                                           ; null terminate

        pop di
        call deleteArg                                  ; kill arg

        pop dx
        pop ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Close Redirected Std Device                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   al     restore value for handle (or -1)                     ;
        ;   bx     stdout or stdin                                      ;
        ;...............................................................;

_CloseRedirectedDevice:

        Entry
        def _RestoreValue, ax
        def _Handle, bx

        push es
        cmp al, -1                                      ; std device redirected ?
        jz _closeRedirect_42                            ; no -->

        Int21 GetPSPAddress
        mov es, bx                                      ; set PSP address

        les si, dword ptr es:[ pspFileHandlePtr ]       ; point to file handles
        add si, word ptr [ _Handle ][ bp ]
        cmp al, byte ptr es:[ si ]                      ; same as existing handle ?
        jz _closeRedirect_42                            ; yes, no need to close -->

        push es
        push si
        mov bx, word ptr [ _Handle ][ bp ]
        Int21 CloseFile                                 ; close std device

        pop si
        pop es                                          ; point to file handles
        mov ax, word ptr _RestoreValue[ bp ]            ; restore handle to 
        mov byte ptr es:[ si ], al                      ; restore handle

_closeRedirect_42:
        pop es
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Lookup Argument                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   cs:di  pointer to supported commands                        ;
        ;   ds:si  pointer to argument (term by a null or switch char)  ;
        ;   cx     length of argument                                   ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   bx     pointer to command execution address                 ;
        ;   cy     argument not supported internally                    ;
        ;...............................................................;

CmndLookup:

        push di
        push si
        push cx
        add di, 4                                       ; point to arg text

        mov al, byte ptr [ si ]                         ; from command
        cmp al, ' '+1                                   ; out of range ?
        jc cmndLookup_28                                ; yes -->
        cmp al, 128                                     ; out of range ?
        jnc cmndLookup_28                               ; yes -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compare argument against table
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

cmndLookup_12:
        mov al, byte ptr [ si ]                         ; from command
        _lowerCase al

        cmp al, ' '+1
        jc cmndLookup_22
        cmp al, '\'
        jz cmndLookup_22
        cmp al, '.'
        jz cmndLookup_22
        cmp al, doubleQuote
        jz cmndLookup_22
        cmp al, singleQuote
        jz cmndLookup_22
        call _CmndParse_SeparatorCheck
        jz cmndLookup_22

        cmp al, byte ptr cs:[ di ]                      ; compare
        jnz cmndLookup_28                               ; if not this command -->

        inc si
        inc di                                          ; else keep looking
        loop cmndLookup_12

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  argument found
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

cmndLookup_22:
        cmp byte ptr cs:[ di ], 00                      ; end of command ?
        jnz cmndLookup_28                               ; no, so not a match -->

        pop cx
        pop ax                                          ; leave si pointing at end 
        pop di
        mov bx, word ptr cs:[ di ]                      ; where to execute
        clc                                             ; nocarry
        ret

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  find next entry in table
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

cmndLookup_28:
        pop cx
        pop si
        pop di
        add di, word ptr cs:[ di + 2 ]                  ; point to next command
        cmp word ptr cs:[ di ], -1                      ; end of command table ?
        jnz cmndLookup                                  ; no, lookup next -->

        stc
        mov bx, offset _NotValidCommand                 ; default not valid command
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Installable Command Interface                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  pointer to argument array                            ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   nz     if commadn executed                                  ;
        ;...............................................................;

checkInstallableCommandInterface:

        xor ax, ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Count Number of Arguments                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  pointer to argument array                            ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   ax     number of arguments in arg array                     ;
        ;...............................................................;

CountArgs:
        push es
        push di
        push cx

        mov cx, maxArgArray                             ; max number of entries

CountArgs_06:
        cmp word ptr [ argpointer ][ di ], 0000         ; terminating arg ?
        jz CountArgs_08                                 ; yes, done -->
        add di, sizeArgCmd                              ; next arg
        loop CountArgs_06                               ; else, keep looking -->

CountArgs_08:
        mov ax, maxArgArray
        sub ax, cx                                      ; # arguments

        pop cx
        pop di
        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Check Number of Arguments                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   bx/ dx are set with the number of expected arguments.       ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cy     too many arguments message output                    ;
        ;...............................................................;

CheckNoArgs:
        xor dx, dx                                      ; no args
        xor bx, bx                                      ; no switches
        jmp short PreProcessCmndLine                    ; check

CheckOneArg:
        mov cx, 0001                                    ; must have one arg
        mov dx, 0001                                    ; must have one arg
        xor bx, bx                                      ; no switches
        jmp short PreProcessCmndLine                    ; check

CheckOptOneArg:
        mov cx, 0000                                    ; must have none, 
        mov dx, 0001                                    ;   or one arg
        xor bx, bx                                      ; no switches
      ; jmp short PreProcessCmndLine                    ; check

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Pre Process Command Line                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  pointer to argument array                            ;
        ;   cx     acceptable min # args                                ;
        ;   dx     acceptable max # args                                ;
        ;   bx     pointer to switches                                  ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cy     too many arguments message output                    ;
        ;...............................................................;

PreProcessCmndLine:

        push di
        push cx
        push dx

        call _GetSwitches                               ; switches ok ?
        jc _preProcessCmndLine_12                       ; no -->

        pop dx
        pop cx
        push dx
        push cx
        call ConsolidateArgs                            ; just in case quotes were missed
        call _TooManyArguments                          ; too many still left ?

        call FixUpArguments                             ; null terminate args
        or ax, ax                                       ; how many args passed

_preProcessCmndLine_12:
        pop dx
        pop cx
        pop di
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Tests for Too Many Arguments                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  pointer to argument array                            ;
        ;   ax     actual argument count                                ;
        ;   cx     acceptable min # args                                ;
        ;   dx     acceptable max # args                                ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cy     too many arguments message output                    ;
        ;...............................................................;

_TooManyArguments:

        push di
        push ax
        push cx

        cmp ax, dx                                      ; 
        jz _TooManyArguments_16                         ; if expected # args -->
        jg _TooManyArguments_06                         ; if greater than expected -->
        cmp ax, cx
        jge _TooManyArguments_16                        ; if within min/max
        mov dx, offset CmndError_ParametersMissing
        jmp short _TooManyArguments_08

_TooManyArguments_06:
        mov dx, offset CmndError_TooManyParameters
        call DisplayLine

        pop cx
        push cx
        add cx, cx
        add di, cx                                      ; argument ptr that is extra
        mov dx, word ptr ss:[ di ]                      ; get arg address

_TooManyArguments_08:
        call DisplayLine                                ; show arg
        stc

_TooManyArguments_16:
        pop cx
        pop ax
        pop di
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Consolidate Command Line Arguments                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  pointer to argument array                            ;
        ;   cx     min arguments expected                               ;
        ;   dx     max arguments expected                               ;
        ;   ax     arguments counted                                    ;
        ;                                                               ;
        ;  All registers preserved                                      ;
        ;                                                               ;
        ;  When a command that usually accepts a single argument        ;
        ;  detects multiple arguments, it will attempt to bundle these  ;
        ;  arguments together under certain "common-sense" rules:       ;
        ;                                                               ;
        ;  - The previous arg DOES NOT terminate in a double quote      ;
        ;                                                               ;
        ;  - The current argument DOES NOT start with \, \\ or x:       ;
        ;                                                               ;
        ;  - The current argument DOES NOT start with ", \, \\ or x:    ;
        ;                                                               ;
        ;                                                               ;
        ;  Examples where args are merged:                              ;
        ;                                                               ;
        ;  >cmd c:\aaa\bbbb ccc\dddd eeee\foo.bat                       ;
        ;       ---------------------------------                       ;
        ;  >dir aaaa bbb ccc                                            ;
        ;       ------------                                            ;
        ;                                                               ;
        ;  Examples where args are NOT merged:                          ;
        ;                                                               ;
        ;  >cmd c:\aaa\bbbb ccc\dddd d:\eeee\foo.bat                    ;
        ;       -------------------- ---------------                    ;
        ;  >cmd c:\aaa\bbbb ccc\dddd \eeee\foo.bat                      ;
        ;       -------------------- -------------                      ;
        ;  >cmd "c:\aaa\bbbb ccc\dddd" eeee\foo.bat                     ;
        ;       ---------------------- ------------                     ;
        ;                                                               ;
        ;...............................................................;

ConsolidateArgs:

        SaveRegisters di, si, dx, cx, bx

        call CountArgs

        cmp dx, 0001                                    ; max one arg ?
        jne _consolidateArgs_24                         ; no, skip consolidate args -->
        cmp ax, 0001                                    ; more than one arg passed ?
        jle _consolidateArgs_24                         ; no, nothing to consolidate -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
; loop through arguments
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

_consolidateArgs_08:
        add di, sizeArgCmd                              ; start with next arg

_consolidateArgs_12:
        mov si, word ptr [ argpointer ][ di ]
        or si, si                                       ; at end ?
        jz _consolidateArgs_24                          ; return -->

        mov bx, word ptr [ argpointer. -sizeArgCmd ][ di ]
        add bx, word ptr [ arglength. -sizeArgCmd ][ di ]
        cmp byte ptr [ bx - 1 ], doubleQuote            ; previous command ended in dbl Quotes ?
        jz _consolidateArgs_08                          ; yes -->

        cmp byte ptr [ si ], '\'                        ; starts with \ ?
        jz _consolidateArgs_08                          ; yes -->
        cmp byte ptr [ si ], '.'                        ; starts with dot ?
        jz _consolidateArgs_08                          ; yes -->
        cmp byte ptr [ si ], doubleQuote                ; starts with dbl quotes ?
        jz _consolidateArgs_08                          ; yes -->
        cmp byte ptr [ si + 1 ], ':'                    ; drive specification ?
        jz _consolidateArgs_08                          ; yes -->

        add si, word ptr [ arglength ][ di ]
        sub si, word ptr [ argpointer. -sizeArgCmd ][ di ] ; length of merged arguments
        mov word ptr [ arglength. -sizeArgCmd ][ di ], si
        call deleteArg
        jmp _consolidateArgs_12

_consolidateArgs_24:
        RestoreRegisters bx, cx, dx, si, di

        call CountArgs
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Test/ Process Switches                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  pointer to argument array                            ;
        ;   bx     pointer to switches to process (or null)             ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cy     if invalid use of switches                           ;
        ;                                                               ;
        ;          Otherwise, switch array passed is filled with info.  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Switch array consists of a switch letter followed by:        ;
        ;                                                               ;
        ;    'x'        switch letter                                   ;
        ;    flags      type of data value expected                     ;
        ;    min        min value expected                              ;
        ;    max        max value expected                              ;
        ;    actual     actual value passed in command                  ;
        ;                                                               ;
        ;    a flag bit is set if the flag was encountered in cmnd.     ;
        ;...............................................................;

_GetSwitches:
        
        Entry
        def _switches, bx
        
        push di
        push si
        push cx

        or bx, bx                                       ; switches expected ?
        jz _getSwitches_06                              ; no -->
        call _initSwitchTable                           ; init switches expected
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  walk through all args
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_getSwitches_06:
        mov si, word ptr [ argpointer ][ di ]           ; get arg 
        or si, si                                       ; is it a null arg ?
        jz _getSwitches_36                              ; yes, exit -->

        mov al, byte ptr [ si ]                         ; get value at arg
        cmp al, byte ptr [ _SwitchChar ]                ; switch arg ?
        jnz _getSwitches_12                             ; no -->

        getarg bx, _switches
        or bx, bx                                       ; any switches allowed ?
        jz _getSwitches_24                              ; if not allowed, drop from list

        mov ax, word ptr [ si + 1 ]                     ; get switch characters
        call _matchSwitch                               ; is this switch valid ?
        jc _getSwitches_24                              ; not allowed -->

        or word ptr [ swFlags ][ bx ], SW_SWITCHSET     ; switch is set
        call deleteArg                                  ; remove this argument
        jz _getSwitches_36                              ; if no more args -->
        jmp _getSwitches_06

_getSwitches_12:
        add di, sizeArgCmd
        jmp _getSwitches_06

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  switch is illegal
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_getSwitches_24:
        push si                                         ; save argument ptr
        mov dx, offset CmndError_BadSwitch
        call DisplayLine                                ; error message

        pop dx
        call DisplayLine                                ; show arg
        stc

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_getSwitches_36:
        pop cx
        pop si
        pop di
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Match Switch                                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   al     character                                            ;
        ;   bx     switch table                                         ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   bx     pointer to matchin switch entry                      ;
        ;   cy     match not found                                      ;
        ;...............................................................;

_matchSwitch:

        _lowerCase al

_matchSwitch_04:
        cmp al, byte ptr cs:[ bx ]
        jz _matchSwitch_08

        add bx, sizeSWITCHENTRY                         ; next entry
        cmp byte ptr cs:[ bx ], -1                      ; end of table ?
        jnz _matchSwitch_04                             ; not yet -->
        stc

_matchSwitch_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Init Switch Table                                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   bx     switch table                                         ;
        ;...............................................................;

_initSwitchTable:

        push bx

_initSwitchTable_04:
        cmp byte ptr cs:[ bx ], -1                      ; end of table ?
        jz _initSwitchTable_08                          ; yes, all done -->
        and word ptr cs:[ swFlags ][ bx ], not SW_SWITCHSET
        add bx, sizeSWITCHENTRY                         ; next entry
        jmp _initSwitchTable_04

_initSwitchTable_08:
        pop bx
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Remove Loose Switches                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   di     command line aguments array                          ;
        ;                                                               ;
        ;  Return:                                                      ;
        ;   di     command line aguments array                          ;
        ;   ax     updated number of args                               ;
        ;                                                               ;
        ;...............................................................;

removeLooseSwitches:

        Entry
        def _argarray, di

removeLooseSwitches_08:
        mov si, word ptr [ argpointer ][ di ]
        mov cx, word ptr [ arglength ][ di ]
        or si, si                                       ; at end of commands ?
        jz removeLooseSwitches_24                       ; yes -->

        add di, sizeArgCmd
        mov al, byte ptr [ si ]                         ; get value at arg
        cmp al, byte ptr [ _SwitchChar ]                ; switch arg ?
        jnz removeLooseSwitches_08                      ; no -->

        sub di, sizeArgCmd                              ; restore pointer to switch
        call deleteArg                                  ; delete it
        jmp removeLooseSwitches_08

removeLooseSwitches_24:
        mov ax, di
        sub ax, word ptr [ _argarray ][ bp ]            ; determine # args
        shr ax, 1
        shr ax, 1

        getarg di, _argarray                            ; restore argarray
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Delete Argument                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  argument list                                        ;
        ;...............................................................;

deleteArg:

        push di
        cmp word ptr [ argpointer ][ di ], 0000         ; at end of list ?
        jz deleteArg_12                                 ; yes -->

deleteArg_08:
        mov ax, word ptr [ argpointer. sizeArgCmd ][ di ]
        mov dx, word ptr [ arglength. sizeArgCmd ][ di ]
        mov word ptr [ argpointer ][ di ], ax           ; move pointer
        mov word ptr [ arglength ][ di ], dx            ; and length
        add di, sizeArgCmd                              ; next arg
        or ax, ax                                       ; any more ?
        jnz deleteArg_08                                ; yes -->

        cmp word ptr [ argpointer ][ di ], 0000         ; two null terminators ?
        jnz deleteArg_08                                ; not yet -->

        mov word ptr [ argpointer ][ di ], 0000
        mov word ptr [ arglength ][ di ], 0000

deleteArg_12:
        pop di
        cmp word ptr [ argpointer ][ di ], 0000         ; at end of list ?
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Insert Argument                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  argument list (insert at this address)               ;
        ;   si     address value to insert                              ;
        ;   cx     length to insert                                     ;
        ;                                                               ;
        ;...............................................................;

insertArg:

        push di
        push si
        push cx

_insertArg_12:
        xchg si, word ptr [ argpointer ][ di ]
        xchg cx, word ptr [ arglength ][ di ]
        add di, sizeArgCmd

        mov ax, cx
        or ax, word ptr [ sizeArgCmd ][ di ]            ; 2nd null follows ?
        jnz _insertArg_12                               ; no, keep inserting -->

        mov word ptr [ argpointer ][ di ], ax           ; make sure we put two NULLS
        mov word ptr [ arglength ][ di ], ax
        mov word ptr [ argpointer ][ di ], ax           ; make sure we put two NULLS
        mov word ptr [ arglength ][ di ], ax

        pop cx
        pop si
        pop di
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Fix Up Arguments                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  argument list                                        ;
        ;                                                               ;
        ;  Will place a null terminator at the end of each argument     ;
        ;  and will remove any embedded double quotes.                  ;
        ;                                                               ;
        ;...............................................................;

FixUpArguments:

        Entry
        def  __argarray, di

        push di
        push ax
        push bx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  walk through each arg
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

fixUpArguments_04:
        getarg di, __argarray
        mov si, word ptr [ argpointer ][ di ]
        or si, si                                       ; more args ?
        jz fixUpArguments_56                            ; no -->

        call fixDoubleQuotes                            ; scan and remove double quotes in arg 

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  place a terminating null (even if must make room for it)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

fixUpArguments_16:
        xor ax, ax
        getarg di, __argarray
        mov si, word ptr [ argpointer ][ di ]
        add si, word ptr [ arglength ][ di ]
        cmp byte ptr [ si ], 0                          ; already a term null ?
        jz fixUpArguments_42                            ; yes -->

        cmp byte ptr [ si ], ' '                        ; can overwrite space ?
        jz fixUpArguments_40                            ; yes -->
        cmp byte ptr [ si ], ','                        ; can overwrite comma ?
        jz fixUpArguments_40                            ; yes -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  must insert character
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

fixUpArguments_22:
        xchg al, byte ptr [ si ]
        inc si
        or al, al
        jnz fixUpArguments_22

        getarg di, __argarray
        mov byte ptr [ si ], 0                          ; store term null

fixUpArguments_24:
        add di, sizeArgCmd
        cmp word ptr [ argpointer ][ di ], 0000
        jz fixUpArguments_42

        inc word ptr [ argpointer ][ di ]               ; fix address (inserted value)
        jmp fixUpArguments_24

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  ok to overwrite character
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

fixUpArguments_40:
        mov byte ptr [ si ], 0                          ; set null terminator

fixUpArguments_42:
        add word ptr [ __argarray ][ bp ], sizeArgCmd
        jmp fixUpArguments_04

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

fixUpArguments_56:
        pop bx
        pop ax
        pop di

        or ax, ax
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  fix Arg: Double Quotes                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  ptr to argument                                      ;
        ;                                                               ;
        ;...............................................................;

fixDoubleQuotes:

        Entry
        def  __argarray, di
        
        mov si, word ptr [ argpointer ][ di ]           ; pointer
        mov cx, word ptr [ arglength ][ di ]            ; get length
        or cx, cx                                       ; zero length string ?
        jz fixDoubleQuote_16                            ; yes, no more to scan -->

        mov di, si

fixDoubleQuote_08:
        mov al, doubleQuote
        repnz scasb                                     ; double quote in argument ?
        jnz fixDoubleQuote_16                           ; if no double quotes left -->

        push cx                                         ; length
        push di                                         ; scan pointer

        mov si, di                                      ; copy si --> di
        dec di
        rep movsb

        mov byte ptr [ di ], 0

        getarg di, __argarray
        dec word ptr [ arglength ][ di ]

        pop di
        pop cx
        dec cx
        jg fixDoubleQuote_08

fixDoubleQuote_16:
        getarg di, __argarray
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  check Unix Style Commands                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   Returns ZR if environment string contains RXDOS=UNIX        ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   NZ     no Unix argumnets                                    ;
        ;...............................................................;

checkUnixStyleCommands:

        push es
        mov si, offset RxDOS_RXDOSSpec                  ; locate RXDOS=
        call searchEnvVariable                          ; env variable located ?
        jnz _checkUnixEnv_16                            ; if no unix cmds -->

        mov es, word ptr [ _EnvSegment ]                ; point to segment
        add di, dx                                      ; point to value
        mov si, offset RxDOS_UNIXSpec

_checkUnixEnv_08:
        mov al, byte ptr es:[ di ]                      ; from env string
        _upperCase al

        cmp al, byte ptr [ si ]                         ; compare against string
        jnz _checkUnixEnv_16                            ; if no unix cmds -->

        inc si
        inc di
        cmp byte ptr [ si ], 00                         ; end of string ?
        jnz _checkUnixEnv_08                            ; not yet -->

_checkUnixEnv_16:
        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Save Batch File Arguments                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:si  points to null terminated string                     ;
        ;   ss:di  pointer to ArgArray                                  ;
        ;   ss:dx  pointer to fully expanded pathname                   ;
        ;   ax     type, which must be set to _BAT                      ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cy     if error                                             ;
        ;...............................................................;

_saveBatchArguments:

        Entry
        def __argarray, di
        def __pathArg, dx
        def __type, ax
        def __tempargptr, di

        cmp ax, _BAT                                    ; is type BATCH
        jnz _saveBatchArguments_Error                   ; no -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if file accessible ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_saveBatchArguments_08:
        cmp word ptr [ RxDOS_StackFrameNumEntries ], 0000
        jg _saveBatchArguments_10                       ; don't close if nested call -->

        mov bx, word ptr [ RxDOS_BatchFile. batchFileHandle ]
        or bx, bx                                       ; batch file in progress ?
        jz _saveBatchArguments_10                       ; no -->
        Int21 CloseFile                                 ; close current batch file
        mov word ptr [ RxDOS_BatchFile. batchFileHandle ], 0000

_saveBatchArguments_10:
        getarg si, __pathArg
        ExOpenFile si, OPEN_ACCESS_NOINHERIT+OPEN_ACCESS_READONLY
        jc _saveBatchArguments_Error                    ; if can't open, quit batch file -->
        mov word ptr [ RxDOS_BatchFile. batchFileHandle ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if file accessible ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor bx, bx
        mov word ptr [ RxDOS_BatchFile. batchNumArgs ], bx
        mov di, offset [ RxDOS_BatchFile. batchArgStore ]

_saveBatchArguments_32:
        mov word ptr [ RxDOS_BatchFile. batchArgPtrs ][ bx ], 0000

        getarg si, __tempargptr
        add word ptr [ __tempargptr ][ bp ], sizeArgCmd
        mov cx, word ptr [ arglength ][ si ]            ; length
        mov si, word ptr [ argpointer ][ si ]           ; arg pointer
        or si, si                                       ; null terminator ?
        jz _saveBatchArguments_Exit                     ; then all done -->

        inc word ptr [ RxDOS_BatchFile. batchNumArgs ]
        mov word ptr [ RxDOS_BatchFile. batchArgPtrs ][ bx ], di
        inc bx
        inc bx

        rep movsb                                       ; copy string
        xor ax, ax
        stosb                                           ; terminate with a null
        jmp _saveBatchArguments_32

_saveBatchArguments_Error:
        stc

_saveBatchArguments_Exit:
        getarg di, __argarray
        getarg dx, __pathArg
        getarg ax, __type
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Replace Temporary Variables                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:si  points to null terminated string                     ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cx     length of new string                                 ;
        ;...............................................................;

ReplaceTempVariables:

        Entry
        def  _stringPointer, si
        def  _deleteFrom
        defbytes _tempVar, 128

        push di
        push si
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  scan for % argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_replaceTempVar_08:
        lodsb                                           ; scan for % symbol
        or al, al                                       ; null terminator ?
        ifz _replaceTempVar_36                          ; yes -->
        cmp al, '%'                                     ; percent character ?
        jnz _replaceTempVar_08

        mov al, byte ptr [ si ]                         ; get character immed after
        or al, al                                       ; null terminator ?
        ifz _replaceTempVar_36                          ; yes -->

        cmp al, '%'                                     ; %% case ?
        jz _replaceTempVar_PercentPercent               ; yes -->

        dec si                                          ; backup over %
        cmp al, '9'+1                                   ; outside arg values ?
        jnc _replaceTempVar_EnvVariable                 ; yes -->
        sub al, '0'                                     ; outside argument values ?
        jc _replaceTempVar_EnvVariable                  ; yes -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  numeric argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov cx, 2                                       ; bytes to delete
        call deleteString                               ; delete cx bytes at si

        xor ah, ah
        cmp ax, word ptr [ RxDOS_BatchFile. batchNumArgs ]
        jge _replaceTempVar_16                          ; just delete arg -->

        add ax, ax                                      ; index pointer
        mov di, ax
        mov di, word ptr [ RxDOS_BatchFile. batchArgPtrs ][ di ]
        or di, di
        jz _replaceTempVar_16                           ; if no arg value -->
        call insertString                               ; insert arg at [ di ]

_replaceTempVar_16:
        jmp _replaceTempVar_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  %% case
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_replaceTempVar_PercentPercent:
        mov cx, 1
        call deleteString                               ; delete cx bytes at si
        jmp _replaceTempVar_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  environment variable
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_replaceTempVar_EnvVariable:
        lea di, offset [ _tempVar ][ bp ]
        storarg _deleteFrom, si
        inc si                                          ; skip init %

_replaceEnvVariable08:
        lodsb                                           ; scan for next % symbol
        stosb                                           ; save at _tempvar
        or al, al                                       ; null terminator ?
        jz _replaceTempVar_36                           ; yes -->
        cmp al, '%'                                     ; percent character ?
        jnz _replaceEnvVariable08                       ; keep searching -->

        mov word ptr [ di-1 ], '='                      ; cancel trailing %

        mov cx, si
        sub cx, word ptr [ _deleteFrom ][ bp ]          ; delete from
        push si
        push cx                                         ; delete length

        lea si, offset [ _tempVar ][ bp ]
        call searchEnvVariable                          ; environment string found ?
        jnz _replaceEnvVariable36                       ; not found -->

        pop cx
        push cx                                         ; length to delete 
        mov si, word ptr [ _deleteFrom ][ bp ]          ; delete from
        call deleteString                               ; delete cx bytes at si

        push ds
        mov si, di
        add si, dx                                      ; contents of variable
        mov ds, word ptr [ _EnvSegment ]                ; point to segment
        lea di, offset [ _tempVar ][ bp ]               ; where to copy
        call CopyString                                 ; copy null term string

        pop ds
        mov si, word ptr [ _deleteFrom ][ bp ]          ; delete from
        lea di, offset [ _tempVar ][ bp ]               ; where to copy
        call insertString                               ; insert

_replaceEnvVariable36:
        pop cx
        pop si
        jmp _replaceTempVar_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_replaceTempVar_36:
        xor ax, ax
        mov cx, -1
        getarg di, _stringPointer
        repnz scasb
        neg cx
        sub cx, 2

        pop si
        pop di
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Replace 'For' Variable                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:si  points to null terminated string                     ;
        ;   ss:bx  points to null terminated replacement string         ;
        ;   al     replacement letter                                   ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cx     length of new string                                 ;
        ;...............................................................;

ReplaceForVariables:

        Entry
        def  _stringPointer, si
        def  _replaceText, bx
        def  _replaceVar

        push di
        push si

        _lowerCase al
        mov word ptr [ _replaceVar ][ bp ], ax          ; replace variable to ah
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  scan for % argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_replaceForVar_08:
        lodsb                                           ; scan for % symbol
        or al, al                                       ; null terminator ?
        ifz _replaceForVar_36                           ; yes -->
        cmp al, '%'                                     ; percent character ?
        jnz _replaceForVar_08

        mov al, byte ptr [ si ]                         ; get character immed after
        _lowerCase al
        cmp al, byte ptr [ _replaceVar ][ bp ]          ; same as var requested ?
        jnz _replaceForVar_08                           ; not yet -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  numeric argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        dec si                                          ; backup over %
        mov cx, 2                                       ; bytes to delete
        call deleteString                               ; delete cx bytes at si

        mov di, word ptr [ _replaceText ][ bp ]         ; insert text
        call insertString                               ; insert [ di ] arg
        jmp _replaceForVar_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_replaceForVar_36:
        xor ax, ax
        mov cx, -1
        getarg di, _stringPointer
        repnz scasb
        neg cx
        sub cx, 2

        pop si
        pop di
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Delete String                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:si  points inside string                                 ;
        ;   cx     bytes to delete                                      ;
        ;...............................................................;

deleteString:

        push di
        push ax
        push si
        push cx
        or cx, cx                                       ; if null call
        jz deleteString_36                              ; nothing to do -->

deleteString_08:
        lodsb                                           ; test for null term
        or al, al
        jz deleteString_32
        loop deleteString_08

        pop cx
        pop si
        push si
        push cx
        mov di, si
        add si, cx                                      ; skip pointer

        call CopyString                                 ; copy string
        dec si                                          ; point to null byte

deleteString_32:
        mov byte ptr [ si ], 0

deleteString_36:
        pop cx
        pop si
        pop ax
        pop di
        ret        

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Insert String                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:si  points inside string                                 ;
        ;   ds:di  points to insert string (null term)                  ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   ss:si  points to char past insert string                    ;
        ;...............................................................;

insertString:

        push cx
        push ax
        push di

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compute length of insert string
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor ax, ax
        mov cx, -1
        repnz scasb                                     ; search length of insert string

        neg cx                                          ; make len-1 positive
        sub cx, 2                                       ; kill null string arg
        sub di, 2                                       ; point to last valid byte before null
        push di                                         ; end of arg pointer
        push cx                                         ; length of insert string

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compute length of master string
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor ax, ax
        mov cx, -1
        mov di, si
        repnz scasb                                     ; search length of insert string
        neg cx                                          ; make len-1 positive
        dec cx                                          ; kill null string arg
        dec di

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  open string
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        std                                             ; copy reverse direction
        pop ax
        push ax                                         ; length of insert string
        mov si, di                                      ; 
        add di, ax                                      ; where to copy to
        rep movsb                                       ; copy

        pop cx
        pop si
        push di                                         ; save return pointer
        rep movsb                                       ; copy
        cld                                             ; restore direction

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        pop si                                          ; return pointer - 1
        inc si

        pop di
        pop ax
        pop cx
        ret        

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Length of String                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   stack  address of string                                    ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cx     length of string                                     ;
        ;...............................................................;

_stringLength:

        Entry 2
        darg  __string

        push es
        push di
        getdarg es, di, __string
        xor ax, ax
        mov cx, -1
        repnz scasb                                     ; search length of insert string

        not cx                                          ; make len positive
        dec cx                                          ; dec for term null
        pop di
        pop es
        Return 

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Number at String                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:si  pointer to text                                      ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   ax     contains value                                       ;
        ;   cy     if value contained text                              ;
        ;...............................................................;

_GetNumber:
        
        xor dx, dx

_GetNumber_04:
        lodsb
        cmp al, ' '+1                                   ; end of string ?
        jc _GetNumber_16                                ; yes, exit ok -->

        cmp al, '9'+1                                   ; valid number ?
        jnc _GetNumber_14                               ; no, error exit -->
        sub al, '0'                                     ; valid number ?
        jc _GetNumber_14                                ; no, error exit -->

        and ax, 15
        push ax
        mov ax, dx
        add dx, dx                                      ; 2
        add dx, dx                                      ; 4
        add dx, ax                                      ; 5
        add dx, dx                                      ; 10
        pop ax
        add dx, ax                                      ; add digit
        jmp _GetNumber_04                               ; continue -->

_GetNumber_14:
        xor ax, ax
        stc
        ret

_GetNumber_16:
        mov ax, dx
        or ax, ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Lower Case String                                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   stack  string pointer                                       ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   di     points to end (null) byte                            ;
        ;   cx     characters in string                                 ;
        ;...............................................................;

_lowerCaseString:
        
        Entry 2
        darg _string

        push es
        xor cx, cx
        getdarg es, di, _string

_lowerCaseString_08:
        mov al, byte ptr es:[ di ]
        or al, al
        jz _lowerCaseString_16

        cmp al, 'A'
        jc _lowerCaseString_10
        cmp al, 'Z'+1
        jnc _lowerCaseString_10
        or al, 20h                                      ; lower case

_lowerCaseString_10:
        mov byte ptr es:[ di ], al
        inc di
        inc cx
        jmp _lowerCaseString_08

_lowerCaseString_16:
        pop es
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compare Sub Strings                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   si     points to a null terminated string                   ;
        ;   di     points to longer string                              ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   zr     strings match regardless of case                     ;
        ;...............................................................;

_compareSubString:

        mov bl, byte ptr [ si ]
        or bl, bl
        jz _compareSubString_08

        inc si
        _lowerCase bl

        mov al, byte ptr [ di ]
        inc di
        _lowerCase al

        cmp al, bl
        jz _compareSubString

_compareSubString_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Copy NULL Terminated String                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   si     points to a null terminated string                   ;
        ;   di     points to destination string                         ;
        ;...............................................................;

CopyString:
        lodsb                                           ; copy character
        stosb                                           ; copy character
        or al, al                                       ; until zero byte
        jnz copyString

        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Locate End Of String                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   di     points to a null terminated string                   ;
        ;...............................................................;

_endofString:

        push ax
        push cx

        xor ax, ax
        mov cx, -1
        repnz scasb                                     ; locate null terminator
        dec di                                          ; backup over null byte

        pop cx
        pop ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Append Path Name                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   si     points to a null term string to Add                  ;
        ;   di     points to a null term string where to Append         ;
        ;...............................................................;

_AppendPathName:

        cmp byte ptr [ di ], 00
        jz _AppendPathName_12                           ; if no path given, don't search -->

        xor ax, ax
        mov cx, -1
        repnz scasb                                     ; search for terminating null

        dec di                                          ; backup to null
        mov al, '\'
        cmp byte ptr [ di - 1 ], al                     ; does path have terminating \ ?
        jz _AppendPathName_12                           ; yes, no need to add another -->
        stosb                                           ; add terminating \

_AppendPathName_12:
        call CopyString
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Search Env String                                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ds:si  points to input command string                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ds:si  points to end of input string (imm after =)          ;
        ;   di     points to keyword value in env string                ;
        ;   dx     offset in env variable to value past = sign          ;
        ;   nz     env variable not found                               ;
        ;...............................................................;

searchEnvVariable:

        Entry
        def _search, si                                 ; search text
        def _envbegpointer, 0000                        ; located env variable

        push es
        mov es, word ptr [ _EnvSegment ]                ; point to segment
        xor di, di

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  search through each item
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

searchEnvVariable_08:
        storarg _envbegpointer, di                      ; located env variable
        getarg si, _search                              ; get search text pointer
        cmp byte ptr es:[ di ], 00                      ; end of env area ?
        jz searchEnvNotFound                            ; not found -->
        
searchEnvVariable_12:
        mov ah, byte ptr [ si ]
        or ah, ah                                       ; if end string -->
        jz searchEnvVariable_Next                       ; not equal, skip to next -->

        _upperCase ah

        mov al, byte ptr es:[ di ]                      ; compare
        _upperCase al

        cmp ah, al                                      ; compare
        jnz searchEnvVariable_Next                      ; not equal, skip to next -->

        inc si
        inc di
        cmp al, '='                                     ; at equal sign ?
        jz searchEnvVariableFound                       ; we have search item -->
        cmp byte ptr es:[ di-1 ], 00                    ; end of env string ?
        jnz searchEnvVariable_12                        ; continue -->
        jmp searchEnvVariable_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  skip to next env variable
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

searchEnvVariable_Next:
        inc di
        cmp byte ptr es:[ di-1 ], 00                    ; end of env string ?
        jnz searchEnvVariable_Next                      ; continue -->
        jmp searchEnvVariable_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  item not found
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

searchEnvNotFound:
        mov ax, es
        or ax, ax                                       ; not zero means not found
        jmp short searchEnvReturn

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  item found
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

searchEnvVariableFound:
        mov dx, word ptr [ _envbegpointer ][ bp ]       ; return start pointer
        sub di, dx                                      ; distance from beg pointer
        xchg di, dx                                     ; return offset in dx
        xor ax, ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

searchEnvReturn:
        pop es
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Delete Env Variable                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   di     pointer to env entry to delete                       ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   di     pointer to end byte of environment                   ;
        ;...............................................................;

deleteEnvVariable:

        push es
        mov es, word ptr [ _EnvSegment ]                ; point to segment
        cmp byte ptr es:[ di ], 00                      ; at end of env block ?
        jz deleteEnvVariable_36                         ; yes, don't delete -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  locate start of next string
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov si, di                                      ; save di pointer

deleteEnvVariable_08:
        inc si
        cmp byte ptr es:[ si-1 ], 00                    ; locate end of this string 
        jnz deleteEnvVariable_08                        ; continue -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  is this end of env block ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

deleteEnvVariable_12:
        mov byte ptr es:[ di ], 00                      ; put end mark.
        cmp byte ptr es:[ si ], 00                      ; at end of env block ?
        jz deleteEnvVariable_36                         ; yes, end of delete -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy a string
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

deleteEnvVariable_16:
        mov al, byte ptr es:[ si ]
        stosb                                           ; copy byte
        inc si
        or al, al                                       ; did we copy a null terminator ?
        jnz deleteEnvVariable_16                        ; not yet -->
        jmp deleteEnvVariable_12                        ; 

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

deleteEnvVariable_36:
        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Insert Env Variable                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ds:si  pointer to text to insert into environment           ;
        ;   di     must point to end null byte of environment block     ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   ax     bytes remaining in Environment                       ;
        ;   cy     if could not add to environment block                ;
        ;...............................................................;

insertEnvVariable:

        Entry
        def endPointer, di

        push es
        mov es, word ptr ss:[ _EnvSegment ]             ; point to env segment
        mov cx, word ptr ss:[ _EnvSize ]
        sub cx, di                                      ; bytes available
        jle insertEnvVariable_36                        ; if none -->

insertEnvVariable_08:
        lodsb
        stosb                                           ; copy byte
        or al, al                                       ; all done ?
        jz insertEnvVariable_36                         ; yes -->

        loop insertEnvVariable_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  can't insert (insufficient space)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        stc                                             ; error, if not all copied
        mov ax, 0000                                    ; remaining space
        getarg di, endPointer                           ; restore pointer to old end
        mov byte ptr es:[ di ], 00                      ; restore end pointer

        pop es
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

insertEnvVariable_36:
        mov ax, word ptr ss:[ _EnvSize ]
        sub ax, di                                      ; space left
        mov byte ptr es:[ di ], 00                      ; put double end pointer
        dec di                                          ; restore pointer

        pop es
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Find Program or Bat File                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:dx  where to store path                                  ;
        ;   ss:si  pointer to name (not null terminated)                ;
        ;   cx     length of path field                                 ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cy     not found anywhere in path                           ;
        ;   ax     contains type of program found                       ;
        ;           0000 - program is a batch file                      ;
        ;           0001 - program is a com or exe file                 ;
        ;   bx     contains handle to file                              ;
        ;...............................................................;

_findProgram:

        Entry
        def _length, cx
        def _argument, si
        def _pathArg, dx                                ; where to store path
        def _extensionsearchorder                       ; pointer within extension search order
        def _envpathpointer, 0000                       ; PATH=, if any
        def _extensionFlag, False                       ; if extension in name
        def _execnamelength
        def _endofname

        defbytes _finddata, sizeFINDDATA
        defbytes _execname, sizeLFN

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  isolate name from rest of command line
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, _pathArg
        rep movsb                                       ; copy program name
        storarg _endofname, di
        mov byte ptr [ di ], 0                          ; null terminate

        mov al, '*'
        getarg di, _argument
        getarg cx, _length
        repnz scasb                                     ; scan name 
        ifz _findProgram_Error                          ; if it contains wild characters -->

        mov al, '?'
        getarg di, _argument
        getarg cx, _length
        repnz scasb                                     ; scan name 
        ifz _findProgram_Error                          ; if it contains wild characters -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get path= pointer
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov si, offset RxDOS_PathSpec
        call searchEnvVariable                          ; locate PATH=
        jnz _findProgram_10                             ; if not found -->

        add di, dx                                      ; address after =
        storarg _envpathpointer, di                     ; location in path statement

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  extract path, name, and extension components
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_findProgram_10:
        getarg cx, _length
        getarg di, _endofname

_findProgram_12:
        dec di
        cmp byte ptr [ di ], '\'                        ; path character ?
        jz _findProgram_16                              ; yes, path found -->
        dec cx                                          ; more to scan ?
        jz _findProgram_18                              ; if no extension -->

        cmp byte ptr [ di ], '.'                        ; possible extension ?
        jnz _findProgram_12                             ; not yet -->
        storarg _extensionFlag, True                    ; extension found
        jmp _findProgram_12                             ; continue until \ found -->

_findProgram_16:
        inc di
        storarg _envpathpointer, 0000                   ; prevent search by nulling ptr to path arg

_findProgram_18:
        mov si, di 
        lea di, offset [ _execname ][ bp ]
        getarg cx, _endofname
        sub cx, si                                      ; isolate file name + any extension
        storarg _execnamelength, cx                     ; save length
        rep movsb                                       ; save name in _execname
        mov byte ptr [ di ], 0                          ; null terminate

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  search through current directory first
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_findProgram_24:
        getarg dx, _pathArg                             ; pointer to path arg
        lea di, offset [ _finddata ][ bp ]
        FileAttrib ATTR_NORMAL
        call CheckFile                                  ; find file
        jnc _findProgram_50                             ; if found -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  search within directory by extension type
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_findProgram_30:
        cmp word ptr [ _extensionFlag ][ bp ], True     ; extension already in name ?
        jz _findProgram_40                              ; disable extension search -->

        mov si, offset RxDOS_ExecOrder                  ; look for names in order, current dir
        storarg _extensionsearchorder, si

_findProgram_32:
        mov si, word ptr [ si ]                         ; get extension text entry
        getarg di, _endofname                           ; point to end of name 
        call CopyString                                 ; copy extension

        mov dx, word ptr [ _pathArg ][ bp ]             ; pointer to path arg
        lea di, offset [ _finddata ][ bp ]              ; finddata workspace
        FileAttrib ATTR_NORMAL
        call CheckFile                                  ; find file
        jnc _findProgram_50                             ; if located -->

        add word ptr [ _extensionsearchorder ][ bp ], 2 ; pt to next entry
        getarg si, _extensionsearchorder
        cmp word ptr [ si ], 0000                       ; end of search order list ?
        jnz _findProgram_32                             ; not yet -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  search through all paths
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_findProgram_40:
        getarg si, _envpathpointer                      ; path=
        or si, si                                       ; if no path, 
        ifz _findProgram_CantFind                       ; then can't find -->

        cmp byte ptr [ si ], 0
        ifz _findProgram_CantFind                       ; then can't find -->

        push ds
        mov ds, word ptr [ _EnvSegment ]
        mov di, word ptr [ _pathArg ][ bp ]             ; pointer to destination path arg

_findProgram_42:
        lodsb
        or al, al                                       ; end of string ?
        jz _findProgram_44                              ; yes -->
        cmp al, ';'                                     ; end of term ?
        jz _findProgram_44                              ; yes -->

        stosb
        jmp _findProgram_42

_findProgram_44:
        pop ds                                          ; restore ds
        storarg _envpathpointer, si                     ; save end of path search

        cmp di, word ptr [ _pathArg ][ bp ]             ; pointer to path arg
        jz _findProgram_40                              ; if emptry string -->

        mov al, '\'
        cmp al, byte ptr [ di - 1 ]                     ; pathname terminated by \ ?
        jz _findProgram_46                              ; yes -->
        stosb                                           ; add \ to pathname

_findProgram_46:
        lea si, offset [ _execname ][ bp ]
        getarg cx, _execnamelength                      ; get name length
        rep movsb                                       ; append program name

        storarg _endofname, di
        mov byte ptr [ di ], 0                          ; null terminate
        jmp _findProgram_24

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  file found, determine if .exe or .com
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_findProgram_50:
        lea di, offset [ _finddata ][ bp ]              ; name area
        call LFNReturnPreferedFilenamePtr               ; point to real name

        mov di, dx
        xor ax, ax
        mov cx, -1
        repnz scasb                                     ; length of string
        not cx                                          ; make len positive
        dec cx                                          ; dec for term null

_findProgram_54:
        cmp byte ptr [ di - 1], '.'                     ; backup over extension, if any
        jz _findProgram_56                              ; until found -->

        dec di
        loop _findProgram_54

        mov ax, _UNKNOWN
        jmp short _findProgram_62

_findProgram_56:
        mov dx, word ptr [ di ]                         ; now we have extension
        or dx, 2020h

        mov ax, _BAT                                    ; _bat
        cmp dx, 'ab'
        jz _findProgram_62

        mov ax, _COM                                    ; _com
        cmp dx, 'oc'
        jz _findProgram_62

        mov ax, _EXE                                    ; _exe
        cmp dx, 'xe'
        jz _findProgram_62

        mov ax, _UNKNOWN

_findProgram_62:
        or ax, ax                                       ; no carry (no error)
        getarg si, _pathArg                             ; expanded argument name
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  error
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_findProgram_CantFind:
_findProgram_Error:
        stc
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Load Program                                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:si  program to load                                      ;
        ;   ss:di  argument array                                       ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;  Note:                                                        ;
        ;   Must return error message.                                  ;
        ;                                                               ;
        ;...............................................................;

_loadProgram:

        Entry
        ddef     _args, ss, di
        ddef     _progname, ss, si
        defbytes _ExecBlock, sizeEXEC
        defbytes _CmdLine, 128
        defbytes _FCB1, sizeFCB
        defbytes _FCB2, sizeFCB

        push es
        push di
        setES ss
        lea di, offset _ExecBlock [ bp ]
        clearMemory sizeEXEC

        lea di, offset _FCB1 [ bp ]
        call _initFCB
        mov word ptr [ _ExecBlock. lexecFCB_1. _pointer  ][ bp ], di
        mov word ptr [ _ExecBlock. lexecFCB_1. _segment  ][ bp ], ss

        lea di, offset _FCB2 [ bp ]
        call _initFCB
        mov word ptr [ _ExecBlock. lexecFCB_2. _pointer  ][ bp ], di
        mov word ptr [ _ExecBlock. lexecFCB_2. _segment  ][ bp ], ss

        lea bx, offset _ExecBlock [ bp ]
        mov ax, word ptr [ _EnvSegment ]
        mov word ptr [ _ExecBlock. lexecEnvironment  ][ bp ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  copy command tail.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea di, offset [ _CmdLine ][ bp ]
        mov word ptr ss:[ di ], 0D00h                   ; pre-init command line

        mov word ptr [ _ExecBlock. lexecCommandTail. _pointer  ][ bp ], di
        mov word ptr [ _ExecBlock. lexecCommandTail. _segment  ][ bp ], ss

        getdarg  es, si, _args
        add si, sizeArgCmd                              ; point to next entry
        cmp word ptr es:[ si ], 0000                    ; any more arguments ?
        jz _loadProgram_20                              ; if no args -->

        xor ax, ax
        stosb                                           ; start with length term

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; capture leading spaces
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov si, word ptr es:[ argpointer ][ si ]

_loadProgram_08:
        cmp byte ptr es:[ si - 1 ], ' '                 ; preceeding a space ?
        jnz _loadProgram_12                             ; no -->
        dec si
        jmp _loadProgram_08

_loadProgram_12:
        call CopyString                                 ; copy command tail

        dec di
        mov byte ptr es:[ di ], cr                      ; end with cr

        dec di
        mov cx, di
        lea di, offset [ _CmdLine ][ bp ]
        sub cx, di                                      ; actual length
        mov byte ptr es:[ di ], cl                      ; save starter length

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get FCB arguments
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, 2901h                                   ; ok to skip leading sep
        lea di, offset _FCB1 [ bp ]

        getdarg  es, si, _args
        mov si, word ptr es:[ argpointer ][ sizeArgCmd ][ si ]
        int 21h

        mov ax, 2901h                                   ; ok to skip leading sep
        lea di, offset _FCB2 [ bp ]
        int 21h

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  go run program
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_loadProgram_20:
        setES ss
        lea bx, offset _ExecBlock [ bp ]
        getdarg ds, dx, _progname
        Int21 LFNExecuteProgram, 00

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get return status when done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        mov ax, cs
        mov ds, ax
        mov es, ax                                      ; restore registers

        Int21 GetReturnCode                             ; get return value
        and ax, 255                                     ; get previous return value
        mov word ptr [ RxDOS_ExecReturnCode ], ax       ; save return code

        pop di
        pop es
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  initialize an FCB data structure                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   es:di  pointer to FCB                                       ;
        ;...............................................................;

_initFCB:
        push ax
        push di
        clearMemory sizeFCB

        lea di, offset fcbName [ di ]
        clearMemory sizeFILENAME, 2020h                 ; double spaces

        pop di
        pop ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  return volume name                                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   al     is disk to read volume                               ;
        ;   di     pointer to find data struc                           ;
        ;   cy     drive has no volume name                             ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   di     points to short volum name                           ;
        ;   cy     drive has no volume name                             ;
        ;                                                               ;
        ;...............................................................;

returnVolumeName:

        push bx
        push di
        cmp al, 'Z'-40h
        jnc returnVolumeName_08

        or al, al                                       ; default disk ?
        jnz returnVolumeName_06                         ; no -->

        Int21 CurrentDisk
        inc al                                          ; a=1, ...

returnVolumeName_06:
        add al, 'a'-1

returnVolumeName_08:
        mov bx, offset RxDOS_RootDirectory - 2
        mov byte ptr [ bx ], al
        mov dx, bx

        pop di
        lea bx, offset [ findDataLongFilename ][ di ]
        mov byte ptr es:[ bx ], 0

        lea bx, offset [ findDataShortFilename ][ di ]
        mov byte ptr es:[ bx ], 0

        FileAttrib ATTR_VOLUME
        call CheckFile                                  ; grab volume label, if any
        mov dx, 0000
        jnc short returnVolumeName_18
        
        mov dx, 0001                                    ; else assume not found error
        cmp al, errInvalidDrive                         ; if drive not found,
        jz returnVolumeName_16                          ; then return error -->
        cmp al, errPathNotFound                         ; if path not found,
        jz returnVolumeName_16                          ; then return error -->
        or dx, dx
        jmp short returnVolumeName_18

returnVolumeName_16:
        stc

returnVolumeName_18:
        lea di, offset [ findDataShortFilename ][ di ]
        pop bx
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Error Code                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ax     error code                                           ;
        ;...............................................................;

DisplayErrorCode:

        push bx
        cmp ax, CmndError_RefTableEntries
        jnc DisplayErrorCode_08

        mov bx, ax
        add bx, bx
        add bx, offset CmndError_TextReferenceTable
        mov dx, word ptr cs:[ bx ]                      ; get address of message
        cmp dx, 0000                                    ; error not defined ?
        jz DisplayErrorCode_08
        call DisplayErrorMessage                        ; display message

DisplayErrorCode_08:
        pop bx
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Display Out of Environment Space Message                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Only displays message if CY set                              ;
        ;                                                               ;
        ;...............................................................;

DisplayOutEnvSpace:

        jnc DisplayOutEnvSpace_08
        
        pushf
        push di
        mov dx, offset CmndError_OutOfEnvironmentSpace
        call DisplayErrorMessage
        pop di
        popf

DisplayOutEnvSpace_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Display Error Message                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   cs:dx  points to error message                              ;
        ;...............................................................;

DisplayErrorMessage:

        call DisplayLine                                ; print line
        call CRLF                                       ; new line
        stc
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Display Cr/Lf                                                ;
        ;...............................................................;

CRLF:
        push es
        push dx

        setES ds
        mov dx, offset RxDOS_NewLine
        call DisplayLine                                ; cr/lf

        pop dx
        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Display Line                                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   es:si  points to message                                    ;
        ;...............................................................;

DisplayLine:

        SaveRegisters di, cx, ax
        mov di, dx                                      ; scan actual length of string

        xor ax, ax                                      ; null terminator
        cmp al, byte ptr es:[ di ]                      ; is string null ?
        jz DisplayLine_22                               ; yes, no need to display -->

        mov cx, -1
        repnz scasb                                     ; scan total length
        not cx                                          ; fixup
        dec cx                                          ;  ...length
        call DisplayLine_ByCount

DisplayLine_22:
        RestoreRegisters di, cx, ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Display Line Count                                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   es:dx  points to message                                    ;
        ;   cx     characters                                           ;
        ;...............................................................;

DisplayLine_ByCount:

        saveAllRegisters

        setds es
        cmp byte ptr ss:[ PageLines ], 00               ; page lines ?
        jnz _displayPaginate_12                         ; go paginate -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if not paginate, can output entire stream
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_displayPaginate_10:
        setds es
        mov bx, STDOUT
        Int21 WriteFile                                 ; output
        jmp short _displayPaginate_22

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if paginate, must really test after each line
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_displayPaginate_12:
        push cx                                         ; starting length
        mov di, dx                                      ; string address
        mov al, ControlJ                                ; search for control J
        repnz scasb                                     ; scan total length
        mov ax, cx                                      ; remaining length after ctl-j
        pop cx                                          ; restore original length
        jnz _displayPaginate_10                         ; output line as is -->

        sub cx, ax                                      ; real length of current string
        push cx
        push ax                                         ; save remaining length
        mov bx, STDOUT
        Int21 WriteFile                                 ; output line
        call _Paginate                                  ; see if pagination requested
        pop cx                                          ; remaining length
        pop ax                                          ; (exchanged registers)
        add dx, ax                                      ; next 

        or cx, cx                                       ; more ?
        jnz _displayPaginate_12                         ; yes -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  exit
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_displayPaginate_22:
        restoreAllRegisters
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Paginate                                                     ;
        ;...............................................................;

_Paginate:
        SaveAllRegisters

        setDS ss
        inc word ptr [ LinesDisplayed ]                 ; incr lines displayed
        cmp byte ptr [ PageLines ], 00                  ; page lines ?
        jz _Paginate_14                                 ; don't worry about lines

        call GetScreenLines
        dec ax                                          ; # lines on screen - 1
        cmp ax, word ptr [ LinesDisplayed ]             ; beyond # screen lines ?
        jg _Paginate_14                                 ; not yet -->

        mov word ptr [ LinesDisplayed ], 0000           ; reset screen lines

        call isStdOutAFile
        jz _Paginate_14                                 ; yes, no pause needed -->
        call _Pause

_Paginate_14:
        RestoreAllRegisters
        ret 

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Set Paging Mode                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  If value is 8000 (switch is set), then set Paging Mode.      ;
        ;                                                               ;
        ;...............................................................;

setPagingMode:

        mov byte ptr [ PageLines ], 00                  ; paging mode
        and ax, SW_SWITCHSET
        jz setPagingMode_08

        mov byte ptr [ PageLines ], -1                  ; paging mode
        mov word ptr [ LinesDisplayed ], 0000           ; # lines displayed, curr page

setPagingMode_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Is StdOut A File ?                                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   zr     stdout is a file                                     ;
        ;   nz     stdout is NOT a file                                 ;
        ;...............................................................;

isStdOutAFile:
        
        mov bx, STDOUT
        Int21 IoControl, 00h                            ; is device piped from a file ?
        mov ax, 1                                       ; non-zero value
        jc _isStdOutAFile_No                            ; we'll assume its not -->

        test dx, sftIsDevice                            ; if bit 7 = 0, handle is a file
        jnz _isStdOutAFile_No                           ; we'll assume its not -->
        
_isStdOutAFile_Yes:
        xor ax, ax                                      ; zero value if bat file

_isStdOutAFile_No:
        or ax, ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Scan Print Buffer                                            ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This routine accepts a variable number of arguments and for- ;
        ;  mats an output buffer.  It works very similar to C's sprintf ;
        ;  function.                                                    ;
        ;                                                               ;
        ;  The input buffer may contain imbedded formatting codes:      ;
        ;                                                               ;
        ;  %c      insert character (pointer to character on stack)     ;
        ;  %s      insert string    (pointer to string on stack)        ;
        ;  %d      insert decimal   (pointer to decimal on stack)       ;
        ;  %ld     insert long      (pointer to long on stack)          ;
        ;                                                               ;
        ;  If a format command contains numbers, the number is inter-   ;
        ;  preted as a field width.  The output value will be right     ;
        ;  aligned within the field width if the width is preceeded     ;
        ;  with a negative sign.  Any characters that exceeds the       ;
        ;  field width is ignored.                                      ;
        ;                                                               ;
        ;  If a format command contains a comma, the number will be     ;
        ;  decimal edited.                                              ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   stack  argument                                             ;
        ;     .    argument                                             ;
        ;     .    argument                                             ;
        ;   stack  argument                                             ;
        ;   stack  format buffer                                        ;
        ;   stack  output buffer                                        ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   dx     pointer to output buffer                             ;
        ;   ax     number of arguments passed on stack                  ;
        ;                                                               ;
        ;  Note:                                                        ;
        ;   The number of arguments returned by _sprintf depends on the ;
        ;   number of arguments encountered.  It will never include the ;
        ;   two  standard  arguments,  the  format  and  output  buffer ;
        ;   addresses. These arguments are  poped off  the  stack  when ;
        ;   this routine performs an exit.                              ;
        ;                                                               ;
        ;...............................................................;

_sprintf:

        Entry 2
        arg _format
        arg _output
        def _args, 0000
        def _varg
        def _fieldwidth
        def _fieldflags

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  scan/copy buffer
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea di, word ptr [ _format ][ bp ]
        add di, 2                                       ; point to prev arg
        storarg _varg, di                               ; save var arg pointer

        mov si, word ptr [ _format ][ bp ]
        mov di, word ptr [ _output ][ bp ]

_sprintf_06:
        mov word ptr [ _fieldwidth ][ bp ], 0000        ; no width
        mov word ptr [ _fieldflags ][ bp ], 0000        ; no flags

_sprintf_08:
        lodsb                                           ; get character
        stosb                                           ; copy to output
        or al, al                                       ; all done ?
        ifz _sprintf_86                                 ; yes -->

        cmp al, '%'                                     ; format code ?
        jnz _sprintf_08                                 ; not yet -->
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  format code
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov word ptr [ _fieldwidth ][ bp ], 0000        ; no width
        mov word ptr [ _fieldflags ][ bp ], 0000        ; no flags

        dec di                                          ; kill format % in output
        mov byte ptr [ di ], 0                          ; stick a null code there

_sprintf_12:
        lodsb                                           ; get character that follows
        _lowerCase al
        or al, al                                       ; all done ?
        ifz _sprintf_86                                 ; yes -->

        mov cx, SPRINTF_LONGFLAG
        cmp al, 'l'                                     ; long flag ?
        jz _sprintf_16                                  ; yes -->

        mov cx, SPRINTF_LEFTALIGN
        cmp al, '-'                                     ; left justify
        jz _sprintf_16                                  ; yes -->

        mov cx, SPRINTF_ZEROFIELDFILL
        cmp al, 'z'                                     ; zero fill ?
        jz _sprintf_16                                  ; yes -->

        mov cx, SPRINTF_COMMADELIM
        cmp al, ','                                     ; decimal delimeter ?
        jnz _sprintf_18                                 ; no -->

_sprintf_16:
        or word ptr [ _fieldflags ][ bp ], cx
        jmp _sprintf_12

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  is it a field width ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_sprintf_18:
        cmp al, '9'+1                                   ; number ?
        jnc _sprintf_22                                 ; no -->
        cmp al, '0'                                     ; number ?
        jc _sprintf_22                                  ; no -->

        and ax, 15                                      ; get number
        mov dx, word ptr [ _fieldwidth ][ bp ]          ; get width
        add dx, dx                                      ; x2
        add dx, dx                                      ; x4
        add dx, word ptr [ _fieldwidth ][ bp ]          ; x5
        add dx, dx                                      ; x10
        add dx, ax
        mov word ptr [ _fieldwidth ][ bp ], dx
        jmp _sprintf_12

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if its a valid formatting code
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_sprintf_22:
        cmp al, 'd'                                     ; decimal output ?
        jz _sprintf_26
        cmp al, 'x'                                     ; hex output ?
        jz _sprintf_26
        cmp al, 'c'                                     ; character ?
        jz _sprintf_32
        cmp al, 's'                                     ; string ?
        jz _sprintf_36
        cmp al, '%'                                     ; percent percent ?
        jz _sprintf_46

        jmp _sprintf_06

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  decimal
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_sprintf_26:
        push si
        getarg si, _varg                                ; get variable arg ptr
        mov si, word ptr [ si ]                         ; get argument pointer        
        add word ptr [ _varg ][ bp ], 2
        inc word ptr [ _args ][ bp ]

        xor dx, dx
        mov ax, word ptr [ si ]                         ; get number
        test word ptr [ _fieldflags ][ bp ], SPRINTF_LONGFLAG
        jz _sprintf_28
        mov dx, word ptr [ si+2 ]                       ; get long

_sprintf_28:
        mov bx, word ptr [ _fieldflags ][ bp ]          ; flags
        mov cx, word ptr [ _fieldwidth ][ bp ]          ; width
        call _sprintfNum                                ; convert numeric to ascii

        pop si
        jmp _sprintf_06

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  character
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_sprintf_32:
        push si
        getarg si, _varg                                ; get variable arg ptr
        mov si, word ptr [ si ]                         ; get argument pointer        
        add word ptr [ _varg ][ bp ], 2
        inc word ptr [ _args ][ bp ]

        mov cx, word ptr [ _fieldwidth ][ bp ]          ; width
        call _sprintfInitField

        mov al, byte ptr [ si ]
        stosb                                           ; store character

        pop si

        mov cx, word ptr [ _fieldwidth ][ bp ]          ; width
        or cx, cx
        ifz _sprintf_06

        dec cx
        call _sprintfPadField
        jmp _sprintf_06

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  string
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_sprintf_36:
        push si
        getarg si, _varg                                ; get variable arg ptr
        mov si, word ptr [ si ]                         ; get argument pointer        
        add word ptr [ _varg ][ bp ], 2
        inc word ptr [ _args ][ bp ]

        mov cx, word ptr [ _fieldwidth ][ bp ]          ; width
        call _sprintfInitField

_sprintf_38:
        lodsb
        or al, al                                       ; null terminator ?
        jz _sprintf_40                                  ; yes -->
        stosb
        or cx, cx                                       ; fixed count ?
        jz _sprintf_38
        loop _sprintf_38

_sprintf_40:
        or cx, cx                                       ; still more length to go ?
        jz _sprintf_42                                  ; no -->
        add di, cx                                      ; advance field length

_sprintf_42:
        pop si
        jmp _sprintf_06

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  %
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_sprintf_46:
        stosb
        jmp _sprintf_06

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  all done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_sprintf_86:
        mov ax, word ptr [ _args   ][ bp ]
        add ax, ax                                      ; # words left on stack
        mov si, word ptr [ _output ][ bp ]
        mov dx, si
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Convert Long (dx:ax) to Ascii                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   dx:ax  long value                                           ;
        ;   cx     size of field to right justify                       ;
        ;   bx     display option flags:                                ;
        ;          8000  insert decimal commas                          ;
        ;                                                               ;
        ;...............................................................;

_sprintfNum:

        Entry
        defbytes _decDisplay, 16
        def _fill
        def _decimalflag, bx
        def _fieldwidth, cx
        def _spacing, 0003                              ; set spacing

        push si
        push di
        lea di, offset [ _decDisplay ][ bp ]
        call _sprintfInitField                          ; init field to spaces
        push di

_sprintfNum_08:
        mov cx, 10
        call _div32                                     ; divide by 10
        or cl, '0'
        mov byte ptr [ di ], cl                         ; store character
        inc di
        
        mov cx, dx
        or cx, ax                                       ; more to go ?
        jz _sprintfNum_10                               ; no -->

        test word ptr [ _decimalflag ][ bp ], SPRINTF_COMMADELIM
        jz _sprintfNum_08                               ; no -->
        dec byte ptr [ _spacing ][ bp ]                 ; spacing break ?
        jnz _sprintfNum_08                              ; not yet -->

        mov byte ptr [ _spacing ][ bp ], 03             ; set spacing
        mov byte ptr [ di ], ','                        ; store comma
        inc di
        jmp _sprintfNum_08
        
_sprintfNum_10:
        pop cx
        sub di, cx                                      ; total # chars output
        mov cx, di                                      ; length to cx

        mov dx, word ptr [ _fieldwidth ][ bp ]
        sub dx, cx                                      ; bytes to fill
        jg _sprintfNum_12
        xor dx, dx

_sprintfNum_12:
        storarg _fill, dx                               ; bytes to fill

        pop di
        push di                                         ; where to begin right justify
        lea si, offset [ _decDisplay ][ bp ]            ; where data stored
        cmp word ptr [ _fieldwidth ][ bp ], 0000        ; left justified output ?
        jz _sprintfNum_20                               ; yes -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  right justified
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        add di, word ptr [ _fieldwidth ][ bp ]          ; blank fill field first

_sprintfNum_16:
        lodsb                                           ; get character
        dec di
        mov byte ptr [ di ], al                         
        loop _sprintfNum_16

        mov al, ' '
        getarg cx, _fill                                ; bytes to fill
        or cx, cx
        jz _sprintfNum_32

        test word ptr [ _decimalflag ][ bp ], SPRINTF_ZEROFIELDFILL
        jz _sprintfNum_18                               ; if no zero fill -->
        mov al, '0'
        
_sprintfNum_18:
        dec di
        mov byte ptr [ di ], al                         
        loop _sprintfNum_18
        jmp short _sprintfNum_32

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  left justified
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_sprintfNum_20:
        lodsb                                           ; get character
        stosb
        loop _sprintfNum_20

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  done
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_sprintfNum_32:
        pop di
        add di, word ptr [ _fieldwidth ][ bp ]          ; point past field

        pop si
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Init Field to Spaces                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   di     pointer to buffer                                    ;
        ;   cx     width of field                                       ;
        ;   bx     8000 if right justified                              ;
        ;...............................................................;

_sprintfInitField:
        
        or cx, cx                                       ; fixed count ?
        jz _sprintfInitField_08                         ; no -->

        push di
        push cx
        push ax
        mov al, ' '
        rep stosb

        pop ax
        pop cx
        pop di

_sprintfInitField_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Pad Field                                                    ;
        ;...............................................................;

_sprintfPadField:
        or cx, cx                                       ; still more length to go ?
        jz _sprintfPadField_08                          ; no -->
        add di, cx                                      ; advance field length

_sprintfPadField_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  32 Bit Divide                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   dx:ax  numerator (low order in ax)                          ;
        ;   cx     divisor                                              ;
        ;                                                               ;
        ;...............................................................;

_div32: or cx, cx                                       ; protect from zero divisor
        stc                                             ; in case of error
        jz _div32_return                                ; if so, just return with carry

        push bx
        mov bx, dx
        xchg ax, bx
        xor dx, dx
        div cx
      
        xchg ax, bx
        div cx                                          ; remainder will be in dx
        mov cx, dx
        mov dx, bx
        pop bx

_div32_return:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  32 Bit Multiply                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   dx:ax  numerator                                            ;
        ;   cx     multiplier                                           ;
        ;                                                               ;
        ;...............................................................;

_mul32: or dx, dx                                       ; simple multiply ?
        jnz _mul32_12                                   ; not really -->

        mul cx
        ret

_mul32_12:
        push bx
        mov bx, ax
        mov ax, dx
        mul cx
        xchg ax, bx

        mul cx
        add dx, bx
        pop bx                                          ; restore bx
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Screen Lines                                             ;
        ;...............................................................;

GetScreenLines:
        
        push ds
        push bx
        xor bx, bx
        mov ds, bx
        mov bx, offset 484h
        mov al, byte ptr [ bx ]                         ; Video Rows
        cbw                                             ; extend to full word
        inc al                                          ; correct number
        pop bx
        pop ds
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Physical Screen Clear                                        ;
        ;...............................................................;

PhysClearScreen:

        call SetPageZero                                ; set page 0
        call GetScreenLines
        mov dh, al
        mov dl, 79

        mov ax, 0600h                                   ; entire screen
        mov bx, 0700h                                   ; blue/white        
     ;; mov bx, 7100h                                   ; blue/white        
        mov cx, 0000h                                   ; from home row
        int 10h

        mov ax, 0F00h                                   ; read current mode
        int 10h                                         ; get page into BH

        mov ax, 0200h
        mov dx, 0100h
        int 10h                                         ; position cursor

        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Set Page Zero                                                ;
        ;...............................................................;

SetPageZero:

        mov ax, 0500h                                   ; set Page zero
        int 10h
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Commands (Alphabetical)                                      ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Break [ ON | OFF ]                                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Sets, clears or reports on RxDOS break option.               ;
        ;                                                               ;
        ;...............................................................;

_Break:

        Entry
        def __argarray, di
        def _args, ax

        call CheckOptOneArg                             ; see if 1 arg 
        jc _BreakExit                                   ; if error -->
        
        or ax, ax                                       ; any arguments ?
        jz _BreakPrintcurrent                           ; none, print current status -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  test for On/ Off
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov si, word ptr [ argpointer ][ di ]           ; get arg pointer
        mov cx, word ptr [ arglength ][ di ]            ; get arg length
        mov di, offset RxDOS_OnOff
        call CmndLookup                                 ; lookup command
        jc _BreakError                                  ; if not on/off, print line -->

        mov dl, bl
        Int21 CtrlBreakCheck, setControlC               ; set/clear Ctrl Break check
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  must specify on or off
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_BreakError:
        mov dx, offset CmndError_MustSpecifyOnOff
        call DisplayErrorMessage
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  print verify on/off value
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_BreakPrintcurrent:
        Int21 CtrlBreakCheck, getControlC               ; get/clear Ctrl Break check

        mov bl, dl
        xor bh, bh
        add bx, bx
        mov dx, word ptr [ _BreakOptions ][ bx ]
        call DisplayLine

_BreakExit:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Call batchfile arguments                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_Call:
_Loadhigh:

        Entry
        def __argarray, di
        def __tempargptr
        defbytes _pathArg, 128

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  find and execute program (unless its a batch file)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, _BAT                                    ; force type to be bat
        lea dx, offset [ _pathArg ][ bp ]
        call _executeProgram                            ; load and execute
        jc _call_22                                     ; if some kind of error -->
        cmp ax, _BAT                                    ; is it a bat program ?
        jz _call_12                                     ; yes -->

        clc                                             ; clear error
        jmp short _call_22

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  execute (call) batch
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_call_12:
        xor cx, cx
        xor dx, dx
        mov bx, word ptr [ RxDOS_BatchFile. batchFileHandle ]
        Int21 MoveFilePointer, SEEK_CUR                 ; get current position

        mov word ptr [ RxDOS_BatchFile. batchFilePosition. _low ], ax
        mov word ptr [ RxDOS_BatchFile. batchFilePosition. _high ], dx

        mov si, offset RxDOS_BatchFile
        mov di, word ptr [ RxDOS_PrevStackFrame ]
        mov cx, sizeBATCH_ARGS
        rep movsb                                       ; copy current args to save area

        sub word ptr [ RxDOS_PrevStackFrame ], sizeBATCH_ARGS
        inc word ptr [ RxDOS_StackFrameNumEntries ]

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  setup batch file arguments
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, _BAT                                    ; exit must contain _BAT type
        getarg di, __argarray
        lea dx, offset [ _pathArg ][ bp ]               ; pointer to file name
        call _saveBatchArguments                        ; if batch file, save arguments
        jnc _call_22                                    ; all done -->
         
        add word ptr [ RxDOS_PrevStackFrame ], sizeBATCH_ARGS
        dec word ptr [ RxDOS_StackFrameNumEntries ]     ; if error
        stc
             
_call_22:
        getarg di, __argarray
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Change Directory                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Changes the default directory for a drive.                   ;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_ChangeDir:
        
        Entry
        def  _disk
        def  __argarray, di
        defbytes _tempstring, sizeLFNPATH

        call CheckOptOneArg                             ; see if 1 arg 
        ifc _changeDir_32                               ; if switch not expected -->
        jnz _changeDir_18                               ; if argument passed -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  display current
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Int21 CurrentDisk                               ; get current disk

        mov dl, al                                      ; save drive letter
        or al, 'a'                                      ; drive
        lea di, offset [ _tempstring + 3][ bp ]
        mov byte ptr [ di - 3 ], al
        mov byte ptr [ di - 2 ], ':'
        mov byte ptr [ di - 1 ], '\'

        inc dl
        mov si, di
        Int21 LFNGetCurrentDirectory                    ; get current directory

        push ss
        push si                                         ; where string
        call _lowerCaseString

        lea dx, offset _tempstring [ bp ]
        call DisplayLine
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  change drive: directory
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_changeDir_18:
        getarg di, __argarray
        mov si, word ptr ss:[ di ]                      ; pointer to lead argument
        lea di, offset [ _tempstring ][ bp ]
        mov word ptr [ di ], '\'                        ; init area

        mov cl, LFNEXPAND_GETFULLPATHNAME
        Int21 LFNGetActualFileName                      ; expand name

        getarg di, __argarray
        mov si, word ptr ss:[ di ]                      ; pointer to lead argument
        cmp byte ptr [ si+1 ], ':'                      ; drive ?
        jnz _changeDir_22                               ; no -->
        
        mov dl, byte ptr [ si ]                         ; get drive letter
        _lowerCase dl
        sub dl, 'a'                                     ; drive maps to 0, ...
        mov byte ptr [ _disk ][ bp ], dl                ; save disk

        Int21 SelectDisk                                ; can we select a drive ?
        Int21 CurrentDisk                               ; get current disk

        inc si
        inc si                                          ; point [si] past drive
        cmp al, byte ptr [ _disk ][ bp ]                ; disk changed ?
        jz _changeDir_22                                ; yes -->

        mov dx, offset CmndError_InvalidDrive
        call DisplayErrorMessage                        ; display message
        jmp short _changeDir_32

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  change directory
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_changeDir_22:
        mov dx, si                                      ; points to change string
        Int21 LFNChangeSubdirectory                     ; try changing dir
        jnc _changeDir_32                               ; if invalid
        call DisplayErrorCode                           ; if error

_changeDir_32:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Clear Screen                                                 ;
        ;...............................................................;

_Cls:
        call isStdOutAFile
        jnz _clsScreen                                  ; its a physical device -->

        mov dl, 'L'-40h                                 ; to a file we'll pipe a top of forms
        Int21 DisplayOutput
        jmp short _cls_36

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see how many lines
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_clsScreen:
        call PhysClearScreen

_cls_36:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Delete filename                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_Delete:

        Entry
        def __argarray, di
        def _nfiles, 0000
        def _allfiles, True
        def _filenameArg
        def _ptrName
        def _findHandle, -1
        defbytes _finddata, sizeFINDDATA
        defbytes __pathArg, sizeLFNPATH

        call CheckOneArg
        jnc _Delete_06                                  ; arguments wrong -->

        mov dx, offset CmndError_FileNameMissing
        call DisplayErrorMessage
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  delete all files ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Delete_06:
        mov dx, word ptr [ argpointer ][ di ]           ; point to filename arg
        storarg _filenameArg, dx                        ; save pointer
        lea di, offset [ _finddata ][ bp ]              ; finddata workspace
        FileAttrib ATTR_NORMAL
        call CheckFile                                  ; does file exist ?
        ifc _Delete_36                                  ; no -->

; check for * or *.* cases

        xor ax, ax
        mov cx, -1
        getarg di, _filenameArg                         ; get pointer to filename
        repnz scasb                                     ; scan to end 

        not cx                                          ; make len positive
        dec cx                                          ; dec for term null

_Delete_12:
        dec di
        cmp byte ptr [ di ], '\'
        jz _Delete_18                                   ; ok to stop searching if \ -->
        cmp byte ptr [ di ], ':'
        jz _Delete_18                                   ; ok to stop searching if : -->
        cmp byte ptr [ di ], 00
        jz _Delete_16                                   ; skip null char -->
        cmp byte ptr [ di ], '.'
        jz _Delete_16                                   ; skip . -->
        cmp byte ptr [ di ], '*'
        jz _Delete_16                                   ; skip wild char -->
        cmp byte ptr [ di ], '?'
        jnz _Delete_22                                  ; if not wild char, then not *.* case

_Delete_16:
        loop _Delete_12

_Delete_18:
        mov dx, offset _DeleteAllFiles
        call DisplayLine                                ; display line

        Int21 ConsoleInputNoEcho                        ; read keyboard
        _upperCase al

        push ax
        call CRLF
        pop ax
        cmp al, "Y"                                     ; if Yes -->
        jz _Delete_22
        cmp al, "N"                                     ; repeat if not right -->
        jnz _Delete_18
        jmp short _Delete_42                            ; else, its No -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  try to locate file(s)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Delete_22:
        fileAttrib ATTR_NORMAL
        getarg si, _filenameArg                         ; get filename pointer
        lea di, offset [ _finddata ][ bp ]              ; name area
        lea dx, offset [ __pathArg ][ bp ]              ; expanded path (sizeLFNPATH)
        call BuildFullPathFirstFile                     ; filename found for arg ?
        storarg _findHandle, bx                         ; save find handle
        storarg _ptrName, dx                            ; present name to next
        jc _Delete_36                                   ; no more -->

_Delete_26:
        lea dx, offset [ __pathArg ][ bp ]              ; expanded path (sizeLFNPATH)
        Int21 LFNDeleteFile                             ; delete file

        inc word ptr [ _nfiles ][ bp ]

        getarg dx, _ptrName
        getarg bx, _findHandle
        lea di, offset [ _finddata ][ bp ]              ; name area
        call BuildFullPathNextFile                      ; filename found for arg ?
        jnc _Delete_26                                  ; continue deleting -->

        getarg bx, _findHandle
        call BuildFullPathClose

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  no more files
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Delete_36:
        cmp word ptr [ _nfiles ][ bp ], 0000            ; any files deleted ?
        jnz _Delete_42                                  ; yes -->

        mov dx, offset CmndError_NoFilesFound
        call DisplayErrorMessage

_Delete_42:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Disk Select Command                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_DiskSelect:

        push di
        add di, sizeArgCmd                              ; check for no arguments 
        jc _diskSelect_32                               ; if error -->

        pop di
        push di                                         ; restore pointer to arg 0
        mov si, word ptr [ argpointer ][ di ]           ; point to lead argument
        cmp byte ptr [ si+1 ], ':'                      ; drive ?
        jnz _diskSelect_32                              ; no -->
        
        mov dl, byte ptr [ si ]                         ; get drive letter
        _lowerCase dl
        sub dl, 'a'                                     ; drive maps to 0, ...
        mov byte ptr [ _disk ][ bp ], dl                ; save disk

        Int21 SelectDisk                                ; can we select a drive ?
        Int21 CurrentDisk                               ; get current disk

        inc si
        inc si                                          ; point [si] past drive
        cmp al, byte ptr [ _disk ][ bp ]                ; disk changed ?
        jz _diskSelect_32                               ; yes -->

        mov dx, offset CmndError_InvalidDrive
        call DisplayErrorMessage                        ; display message

_diskSelect_32:
        pop di
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Echo                                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_Echo:

        Entry
        def __argarray, di
        def _args, ax

        or ax, ax                                       ; no arguments ?
        jz _echo_printcurrent                           ; none, print current status -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  test special echo case (symbol char immediately following echo keyword)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ argpointer ][ di ]
        sub ax, word ptr [ argpointer. -sizeArgCmd ][ di ]
        cmp ax, 4                                       ; first arg immediately next to echo (echo. case)
        jnz _echo_OnOffTest                             ; no, go test for On/Off special case -->

        mov si, word ptr [ argpointer ][ di ]
        mov al, byte ptr [ si ]                         ; get first character

        setES ss
        mov di, offset _CmndParse_FileSysReserved
        mov cx, offset _CmndParse_FileSysReservedLen
        repnz scasb                                     ; char not special 
        jnz _echo_printline                             ; just print line -->

        getarg di, __argarray
        inc word ptr [ argpointer ][ di ]
        dec word ptr [ arglength ][ di ]
        jnz _echo_printline                             ; print remainder of line -->

        call DeleteArg                                  ; delete @arg
        jnz _echo_printline                             ; if addtl args follow -->
        jmp short _echo_printCRLF                       ; if blank line

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  test for On/ Off
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_echo_OnOffTest:
        mov si, word ptr [ argpointer ][ di ]           ; get arg pointer
        mov cx, word ptr [ arglength ][ di ]            ; get arg length
        mov di, offset RxDOS_OnOff
        call CmndLookup                                 ; lookup command
        jc _echo_printline                              ; if not on/off, print line -->

        mov byte ptr [ RxDOS_BatchFile. batchEchoStatus ], bl ; set echo status 
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  print current line
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_echo_printline:
        getarg di, __argarray
        mov dx, word ptr [ argpointer ][ di ]           ; get start of string
        call DisplayLine

_echo_printCRLF:
        call CRLF
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  print on/off value
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_echo_printcurrent:
        mov bx, word ptr [ RxDOS_BatchFile. batchEchoStatus ]
        add bx, bx
        mov dx, word ptr [ _EchoOptions ][ bx ]
        call DisplayLine
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  End Call                                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_EndCall:

        mov bx, word ptr [ RxDOS_BatchFile. batchFileHandle ]
        Int21 CloseFile                                 ; close file
        mov word ptr [ RxDOS_BatchFile. batchFileHandle ], 0000
        mov word ptr [ RxDOS_BatchFile. batchEchoStatus ], Yes

        cmp word ptr [ RxDOS_StackFrameNumEntries ], 0000
        jz _endcall_18                                  ; if no more files to backup to -->

        dec word ptr [ RxDOS_StackFrameNumEntries ]
        add word ptr [ RxDOS_PrevStackFrame ], sizeBATCH_ARGS
        mov si, word ptr [ RxDOS_PrevStackFrame ]
        mov di, offset RxDOS_BatchFile
        mov cx, sizeBATCH_ARGS
        rep movsb                                       ; copy

        mov si, word ptr [ RxDOS_PrevStackFrame ]
        cmp word ptr [ RxDOS_StackFrameNumEntries ], 0000

_endcall_18:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Execute Program or Batch File                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   si     points to filename                                   ;
        ;   ax     require type                                         ;
        ;...............................................................;

_executeProgram:

        Entry
        def __argarray, di
        def __progname, si
        def _pathArg, dx                                ; where to return fully qualified path name
        def _requireType, ax                            ; require type (_BAT or _UNKNOWN)
        def __type

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  find program
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg dx, _pathArg                             ; where to return fully qualified path name
        mov si, word ptr [ argpointer ][ di ]           ; argument name
        mov cx, word ptr [ arglength ][ di ]            ; argument length
        call _findProgram                               ; locate program or bat file
        storarg __type, ax                              ; arg type
        jc _executeprogram_Error                        ; if program not found -->
        cmp ax, _BAT                                    ; is it a bat program ?
        jz _executeprogram_12                           ; yes -->

        cmp word ptr [ _requireType ][ bp ], _BAT       ; BAT required ?
        jz _executeprogram_Error                        ; can't run -->

        getarg dx, _pathArg                             ; where to return fully qualified path name
        getarg di, __argarray
        call _loadProgram                               ; else load and execute program

_executeprogram_12:
        clc

_executeprogram_Exit:
        getarg di, __argarray
        getarg si, __progname
        getarg dx, _pathArg
        getarg ax, __type
        Return                                          ; return

_executeprogram_Error:
        getarg di, __argarray                           ; arg array
        call _NotValidCommand                           ; not valid (not found )
        stc                                             ; set error exit
        jmp _executeprogram_Exit

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Goto Label                                                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  if batch file, locate label                                  ;
        ;...............................................................;

_Goto:

        Entry
        def  _labelPtr, si
        ddef _filePosition
        defbytes _buffer, sizeCmdLineStruct

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  prep argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        call CheckOneArg
        jc _Goto_36                                     ; error -->

        getarg si, _labelPtr

_goto_06:
        cmp byte ptr [ si ], ':'
        jnz _goto_08
        inc si
        jmp _goto_06

_goto_08:
        storarg _labelPtr, si

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  running batch file ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        cmp word ptr [ RxDOS_BatchFile. batchFileHandle ], 0000
        jz _Goto_36                                     ; if not running a batch file -->

        call getBatchPosition                           ; save current batch file position
        mov word ptr [ _filePosition. _low  ][ bp ], ax
        mov word ptr [ _filePosition. _high ][ bp ], dx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  read lines 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor cx, cx
        xor dx, dx
        mov bx, word ptr [ RxDOS_BatchFile. batchFileHandle ]
        Int21 MoveFilePointer, SEEK_BEG                 ; start at beg of file        

_goto_16:
        xor ax, ax                                      ; no echo searching lines
        mov bx, word ptr [ RxDOS_BatchFile. batchFileHandle ]
        lea si, offset [ _buffer ][ bp ]
        call _ReadBatch                                 ; read batch line
        jz _goto_32                                     ; set position -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compare against search argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor bh, bh
        mov bl, byte ptr [ _buffer. bufActualLength ][ bp ]
        lea di, offset [ _buffer. bufData ][ bp ]
        mov byte ptr [ di + bx ], 00
        mov si, word ptr [ _labelPtr ][ bp ]

_goto_18:
        cmp byte ptr [ di ], ' '
        jz _goto_22
        cmp byte ptr [ di ], ':'
        jnz _goto_26

_goto_22:
        inc di
        jmp _goto_18

_goto_26:
        call _compareSubString                          ; label located ?
        jnz _goto_16                                    ; no -->
        jmp short _goto_36                              ; line located !

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  can't find, reset line location
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_goto_32:
        mov dx, word ptr [ _filePosition. _low  ][ bp ]
        mov cx, word ptr [ _filePosition. _high ][ bp ]
        mov bx, word ptr [ RxDOS_BatchFile. batchFileHandle ]
        Int21 MoveFilePointer, SEEK_BEG                 ; restore pointer

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_goto_36:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  If [not] ERRORLEVEL number command                           ;
        ;  If [not] EXIST filename command                              ;
        ;  If [not] string1==string2 command                            ;
        ;...............................................................;

_If:

        Entry 
        def __argarray, di
        def _notflag, 0000
        def _usedportion, 0000

        defbytes _finddata, sizeFINDDATA
        defwords _argtypeArray, 20                      ; 5 args (type and text pointers)

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  type first few arguments
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lea di, offset [ _argtypeArray ][ bp ]          ; pointer to arg store value
        push di

        xor ax, ax
        mov cx, 5 * 4                                   ; words to clear
        rep stosw        

        pop bx
        mov cx, 5                                       ; # args to scan
        getarg di, __argarray

_If_08: cmp word ptr [ argpointer ][ di ], 0000         ; any more args ?
        jz _If_16                                       ; quit typing args -->

        push di
        push bx
        push cx

        mov si, word ptr [ argpointer ][ di ]           ; get lead argument
        mov cx, word ptr [ arglength ][ di ]            ; get arg length
        mov word ptr [ bx. _argtext ], si               ; pointer to text

        mov di, offset RxDOS_IfOptions
        call CmndLookup                                 ; lookup command
        mov ax, bx                                      ; save type

        pop cx
        pop bx
        pop di
        jc _If_12
        mov word ptr [ bx. _argtype ], ax               ; value returned from lookup
        
_If_12: add bx, 4                                       ; lookup first three args
        add di, sizeArgCmd                              ; lookup first three args
        loop _If_08             

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if == special case
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_If_16: lea bx, offset [ _argtypeArray ][ bp ]          ; pointer to arg 
        cmp word ptr [ bx. _argtype ], IF_NOT           ; not argument ?
        jnz _If_20                                      ; no -->
        add bx, 4                                       ; then we'll use next as base
        storarg _notflag, -1                            ; if not

_If_20: cmp word ptr [ bx. _argtype ], IF_ERRORLEVEL
        ifz _ifErrorLevel
        cmp word ptr [ bx. _argtype ], IF_EXIST
        ifz _ifExist

        mov si, word ptr [ bx + 4 ][ _argtext ]         ; next arg
        or si, si                                       ; no text ?
        jz _IfSyntaxError                               ; syntax error -->

        cmp word ptr [ si ], "=="                       ; equals case ?
        jz _ifEquals

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  syntax error
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_IfSyntaxError:
        mov dx, offset CmndError_SyntaxError
        call DisplayErrorMessage                        ; display message
        Return
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if equals
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ifEquals:
        mov si, word ptr [ bx     ][ _argtext ]
        or si, si                                       ; arg before =='s 
        jz _IfSyntaxError                               ; if syntax error -->

        mov di, word ptr [ bx + 12][ _argtext ]
        storarg _usedportion, di
        or di, di                                       ; arg after =='s 
        jz _IfSyntaxError                               ; if syntax error -->

        push si
        call _ifStringlength                            ; get length of source string
        mov cx, ax

        push di
        call _ifStringlength                            ; get length of dest string
        cmp cx, ax                                      ; compare lengths
        jnz _ifNotEquals                                ; if syntax error -->

        rep cmpsb                                       ; strings compare equal ?
        lahf                                            ; zer/not zero to ah
        xor ah, byte ptr [ _notflag ][ bp ]             ; toggle Not Equal Bit
        and ah, 01000000b                               ; not zero if logical continue
        jnz _ifTrue

_ifNotEquals:
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if errorlevel
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ifErrorLevel:
        mov si, word ptr [ bx + 4 ][ _argtext ]         ; get text pointer to next arg
        storarg _usedportion, si
        or si, si
        jz _IfSyntaxError                               ; if syntax error -->

        call _GetNumber                                 ; get expected number
        jc _IfSyntaxError                               ; if syntax error -->
        
        dec ax                                          ; need to force carry
        cmp ax, word ptr [ RxDOS_ExecReturnCode ]       ; current >= return value ?
        lahf                                            ; zer/not zero to ah
        xor ah, byte ptr [ _notflag ][ bp ]             ; toggle Not Equal Bit
        and ah, 00000001b                               ; not zero if logical continue
        jnz _ifTrue                                     ; do rest of line -->
        
        Return
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if exist
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ifExist:
        FileAttrib ATTR_DIRECTORY
        mov dx, word ptr [ bx + 4 ][ _argtext ]         ; get text pointer to next arg
        storarg _usedportion, dx
        lea di, offset [ _finddata ][ bp ]
        call CheckFile                                  ; does this file exist ?

        lahf                                            ; zero/not zero flag to ah
        xor ah, byte ptr [ _notflag ][ bp ]             ; toggle Not Equal Bit
        and ah, 00000001b                               ; not zero means not True
        jz _ifTrue                                      ; do rest of line -->

        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if cond is true. execute command that follows
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ifTrue:
        getarg di, __argarray
        getarg dx, _usedportion                         ; used args
        sub di, sizeArgCmd

_ifTrue_08:
        add di, sizeArgCmd
        cmp word ptr [ argpointer ][ di ], 0000
        ifz _IfSyntaxError
        cmp word ptr [ argpointer ][ di ], dx           ; located used part of If ?
        jc _ifTrue_08                                   ; not yet -->

        add di, sizeArgCmd
        call _executeCommandArray
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compute string lengths
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ifStringlength:
        Entry 1
        arg   _str

        push si
        push di
        getarg si, _str

_ifStringlength_08:
        cmp byte ptr [ si ], ' '
        jz _ifStringlength_12
        cmp byte ptr [ si ], '='
        jz _ifStringlength_12
        cmp byte ptr [ si ], 0
        jz _ifStringlength_12

        inc si
        jmp _ifStringlength_08

_ifStringlength_12:
        mov ax, si
        sub ax, word ptr _str [ bp ]

        pop di
        pop si
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Make Directory                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_makeDir:
        
        call CheckOneArg                                ; see if 1 arg 
        jc _makeDir_32                                  ; if error -->

_makeDir_22:
        mov dx, word ptr [ argpointer ][ di ]           ; point to filename arg
        Int21 LFNCreateSubdirectory                     ; try changing dir
        jnc _makeDir_32                                 ; if valid -->

        cmp ax, errAccessDenied                         ; access denined ?
        jnz _makeDir_26                                 ; show other errors -->

        mov dx, offset CmndError_SubDirAlreadyExists
        call DisplayErrorMessage
        jmp short _makeDir_32

_makeDir_26:
        call DisplayErrorCode                           ; if error

_makeDir_32:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Path                                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_Path:

        Entry
        def __argarray, di
        def _pathArgBeg
        def _pathEndPtr, 0000
        def _foundflag, TRUE

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  locate current path arg, if any.
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov si, offset RxDOS_PathSpec                   ; locate PATH=
        call searchEnvVariable                          ; env variable located ?
        jz _Path_08                                     ; if arg located -->
        storarg _foundflag, FALSE                       ; if not found
        storarg _pathEndPtr, di                         ; else this points to end
        xor di, di                                      ; say not found

_Path_08:
        storarg _pathArgBeg, di

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if no command given, type out current path value
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg di, __argarray
        mov si, word ptr [ argpointer ][ di ]           ; point to arg array
        or si, si                                       ; any args passed ?
        jnz _Path_16                                    ; yes, go update path -->

        cmp word ptr [ _foundflag ][ bp ], TRUE         ; path found ?
        jnz _Path_14                                    ; no arg found -->

        push es
        getarg dx, _pathArgBeg                          ; restore arg to path
        mov es, word ptr [ _EnvSegment ]
        call DisplayLine                                ; show current path
        call CRLF                                       ; cr/lf
        pop es
        Return

_Path_14:
        mov dx, offset CmndError_NoPath
        call DisplayLine                                ; show current path
        Return
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  else, set path
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Path_16:
        cmp byte ptr [ si ], '='                        ; are we on = sign ?
        jnz _Path_20                                    ; no -->
        mov si, word ptr [ sizeArgCmd. argpointer ][ di ]; get next arg

_Path_20:
        push si                                         ; env string to add
        getarg di, _pathEndPtr                          ; get pointer to end
        getarg dx, _pathArgBeg                          ; restore arg to path
        cmp word ptr [ _foundflag ][ bp ], TRUE         ; path arg found ?
        jnz _Path_24                                    ; no, no need to delete -->

        mov di, dx
        call deleteEnvVariable

_Path_24:
        mov si, offset RxDOS_PathSpec                   ; locate PATH=
        call insertEnvVariable
        call DisplayOutEnvSpace                         ; if error, display message
        pop si                                          ; path string to insert
        jc _Path_Return

        call insertEnvVariable
        call DisplayOutEnvSpace                         ; if error, display message

_Path_Return:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Pause                                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_Pause:
        mov dx, offset _PressAnyKeyToContinue
        Int21 DisplayString
        Int21 ClearBufferedKeyboardInput, KeyboardInput

        call CRLF
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Remark (Rem )                                                ;
        ;...............................................................;

_Rem:
        ret                                             ; do nothing

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Remove Directory                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:di  Arg Array                                            ;
        ;   ax     Number of arguments in array                         ;
        ;...............................................................;

_RemDir:
        
        call CheckOneArg                                ; see if 1 arg 
        jc _removeDir_32                                ; if error -->

        mov dx, word ptr [ argpointer ][ di ]           ; point to filename arg
        Int21 LFNRemoveSubdirectory                     ; try command
        jnc _removeDir_32                               ; if valid -->

        call DisplayErrorCode                           ; if error

_removeDir_32:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  SET                                                          ;
        ;  SET variable=                                                ;
        ;  SET variable=string                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Display, set or remove environment variable.                 ;
        ;...............................................................;

_Set:

        Entry
        def __argarray, di
        def __deleteFlag

        call CountArgs
        or ax, ax                                       ; if no args 
        jnz _SetEnvVariable                             ; set/ clear env variable -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  display environment variables
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_SetDisplay:
        xor si, si
        mov es, word ptr [ _EnvSegment ]

_SetDisplay_08:
        cmp byte ptr es:[ si ], 00
        jz _SetDisplayReturn
        cmp byte ptr es:[ si ], ';'                     ; if comment,
        jz _SetDisplay_12                               ; skip -->

        mov dx, si
        call DisplayLine
        call CRLF

_SetDisplay_12:
        inc si                                          ; scan to end of env string
        cmp byte ptr es:[ si - 1 ], 00
        jnz _SetDisplay_12
        jmp _SetDisplay_08 

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  upper case up to '='
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_SetEnvVariable:
        getarg di, __argarray
        mov si, word ptr [ argpointer ][ di ]           ; point to arg array

_SetEnvVariable_08:
        mov al, byte ptr [ si ]                         ; get character
        _upperCase al
        mov byte ptr [ si ], al                         ; save it 

        inc si
        cmp al, '='                                     ; line contains a break ?
        jz _SetEnvVariable_12                           ; yes -->
        or al, al                                       ; end of string ?
        jnz _SetEnvVariable_08                          ; not yet -->

        mov dx, offset CmndError_SyntaxError
        call DisplayErrorMessage                        ; display message
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  search/ delete
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_SetEnvVariable_12:
        xor ax, ax
        mov al, byte ptr [ si ]                         ; if char that follows is not
        storarg __deleteFlag, ax                        ; non-zero means insert

        mov si, word ptr [ argpointer ][ di ]           ; point to arg array
        call searchEnvVariable                          ; env variable located ?
        jnz _SetEnvVariable_18                          ; not found -->

        call deleteEnvVariable                          ; delete env variable
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  insert
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_SetEnvVariable_18:
        getarg si, __argarray
        mov si, word ptr [ argpointer ][ si ]           ; get lead arg
        cmp word ptr [ __deleteFlag ][ bp ], 0000       ; non-zero means insert
        jz _SetDisplayReturn                            ; delete only -->

        call insertEnvVariable                          ; insert at end
        call DisplayOutEnvSpace                         ; if error, display message

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_SetDisplayReturn:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Shift                                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  No parameters expected                                       ;
        ;...............................................................;

_Shift:

        mov si, offset ( RxDOS_BatchFile. batchArgPtrs ); copy args down
        mov cx, word ptr [ RxDOS_BatchFile. batchNumArgs ]
        or cx, cx
        jz _shift_32                                    ; if no arguments -->

_shift_08:
        mov ax, word ptr [ argpointer. sizeArgCmd ][ si ]
        mov dx, word ptr [ arglength. sizeArgCmd ][ si ]
        mov word ptr [ argpointer ][ si ], ax
        mov word ptr [ arglength ][ si ], dx            ; shift arg down
        add si, sizeArgCmd                              ; point to next arg
        loop _shift_08                                  ; loop through all args

        dec word ptr [ RxDOS_BatchFile. batchNumArgs ]  ; decr # args

_shift_32:
        mov word ptr [ argpointer ][ si ], 0000         ; null out last arg
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Truename [ anypath ]                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Displays the DOS expanded filename.                          ;
        ;                                                               ;
        ;...............................................................;

_Truename:

        Entry
        defbytes _buffer, sizeLFNPATH

        call CheckOptOneArg                             ; should have an arg
        jc _truename_36                                 ; error -->

        mov si, word ptr [ argpointer ][ di ]           ; get argument passed
        or si, si                                       ; if not actually null
        jnz _truename_24                                ; then fine -->
        mov si, offset RxDOS_TruenameCurrDir            ; get argument passed

_truename_24:
        lea di, offset [ _buffer ][ bp ]
        mov word ptr [ di ], 0000                       ; init area

        mov cl, LFNEXPAND_GETFULLPATHNAME
        Int21 LFNGetActualFileName

        lea dx, offset [ _buffer ][ bp ]
        call DisplayLine                                ; print line

_truename_36:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Type filename                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Unlike MSDOS, TYPE will permit both the use of the pause /p  ;
        ;  switch and wild card characters in a name.                   ;
        ;                                                               ;
        ;...............................................................;

_Type:

        Entry
        def __argarray, di
        def __wildchars, FALSE
        def _abortFlag, FALSE
        def _handle

        def _ptrName
        def _findHandle, -1

        defbytes _finddata, sizeFindData
        defbytes __pathArg, sizeLFNPATH
        defbytes _buffer, SAFE_ALLOCATION

        mov cx, 0001                                    ; at least one arg
        mov dx, 0999                                    ; unlimitd number of args
        mov bx, offset _TypeSwitches
        call PreProcessCmndLine                         ; process switches
        ifc _TypeReturn                                 ; if error -->

        mov ax, word ptr [ _TypePauseSwitch. swFlags ]
        call setPagingMode

        getarg di, __argarray
        call fixUpArguments                             ; null terminate args

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get next argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_TypeNext:
        getarg di, __argarray
        mov dx, word ptr [ argpointer ][ di ]
        or dx, dx                                       ; any more args ?
        ifz _TypeReturn                                 ; if no more -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get path for argument
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        fileAttrib ATTR_NORMAL
        mov di, word ptr [ __argarray ][ bp ]
        mov si, word ptr [ argpointer ][ di ]           ; point to filename arg
        lea di, offset [ _finddata ][ bp ]              ; name area
        lea dx, offset [ __pathArg ][ bp ]              ; expanded path (sizeLFNPATH)
        call BuildFullPathFirstFile                     ; filename found for arg ?
        storarg _findHandle, bx                         ; save find handle
        storarg _ptrName, dx                            ; present name to next
        jnc _TypeOpenFile                               ; yes, open and display -->

        mov dx, offset _TypeCannotFind          
        call DisplayLine

        mov di, word ptr [ __argarray ][ bp ]
        mov dx, word ptr [ argpointer ][ di ]           ; point to filename arg
        call DisplayLine                                ; show arg
        jmp _TypeReturn                                 ; return -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  open and print
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_TypeOpenFile:
        lea dx, offset [ __pathArg ][ bp ]              ; expanded path
        ExOpenFile dx, OPEN_ACCESS_READONLY             ; read only
        storarg _handle, ax                             ; save handle or error
        jc _TypeReturn                                  ; if something went wrong -->

        call CRLF

        lea di, offset [ _finddata ][ bp ]              ; name area
        call LFNReturnPreferedFilenamePtr
        call DisplayLine                                ; show arg
        call CRLF

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  read and list
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_TypeReadFile:
        getarg bx, _handle                              ; get handle
        mov cx, SAFE_ALLOCATION                         ; how much to read
        lea dx, offset [ _buffer ][ bp ]                ; where to read
        Int21 ReadFile                                  ; read buffer
        jc _TypeCloseFile
        
        or ax, ax                                       ; at end of file ?
        jz _TypeCloseFile                               ; yes, go to next -->

        mov cx, ax                                      ; how much read
        lea dx, offset [ _buffer ][ bp ]                ; where to read
        call DisplayLine_ByCount                        ; display buffer
        jnc _TypeReadFile                               ; keep reading file if no carry -->
        
        storarg _abortFlag, TRUE                        ; else abort

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  close file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_TypeCloseFile:
        getarg bx, _handle                              ; get handle
        Int21 CloseFile
        call CRLF

        cmp word ptr [ _abortFlag ][ bp ], TRUE         ; abort ?
        jz _TypeReturn                                  ; exit ->

        getarg dx, _ptrName                             ; name field
        getarg bx, _findHandle                          ; find handle
        lea di, offset [ _finddata ][ bp ]              ; name area
        call BuildFullPathNextFile                      ; filename found for arg ?
        jnc _TypeOpenFile                               ; yes, open file -->

        getarg bx, _findHandle                          ; find handle
        storarg _findHandle, -1                         ; close handle
        call BuildFullPathClose                         ; close find file
        add word ptr [ __argarray ][ bp ], sizeArgCmd
        jmp _TypeNext                                   ; else go to next -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_TypeReturn:
        getarg bx, _findHandle                          ; find handle
        call BuildFullPathClose                         ; close find file
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Verify [ ON | OFF ]                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Sets, clears or reports on RxDOS disk verify parameters.     ;
        ;                                                               ;
        ;...............................................................;

_Verify:

        Entry
        def __argarray, di
        def _args, ax

        call CheckOptOneArg                             ; see if 1 arg 
        jc _VerifyExit                                  ; if error -->
        
        or ax, ax                                       ; any arguments ?
        jz _VerifyPrintcurrent                          ; none, print current status -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  test for On/ Off
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov si, word ptr [ argpointer ][ di ]           ; get arg pointer
        mov cx, word ptr [ arglength ][ di ]            ; get arg length
        mov di, offset RxDOS_OnOff
        call CmndLookup                                 ; lookup command
        jc _VerifyError                                 ; if not on/off, print line -->

        mov al, bl
        Int21 SetVerifySwitch                           ; set or clear verify switch
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  must specify on or off
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_VerifyError:
        mov dx, offset CmndError_MustSpecifyOnOff
        call DisplayErrorMessage
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  print verify on/off value
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_VerifyPrintcurrent:
        Int21 GetVerify

        mov bl, al
        xor bh, bh
        add bx, bx
        mov dx, word ptr [ _VerifyOptions ][ bx ]
        call DisplayLine

_VerifyExit:
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Version                                                      ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  No parameters expected                                       ;
        ;...............................................................;

_Ver:
        call CheckNoArgs                                ; check for no arguments 
        call CRLF

        mov dx, offset RxDOS_Version
        call DisplayLine

        mov dx, offset RxDOS_VersionCopyright
        call DisplayLine
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Volume                                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Displays Volume Information                                  ;
        ;                                                               ;
        ;...............................................................;

_Vol:

        Entry
        def  _currdisk, 0000

        defbytes _finddata, sizeFindData
        defbytes _printbuffer, 128

        Int21 CurrentDisk
        add al, 'A'                                     ; get drive
        mov byte ptr [ _currdisk ][ bp ], al            ; save current disk

        call checkOptOneArg
        jc _Vol_60                                      ; parameter is wrong -->
        jz _Vol_30                                      ; if no parameters -->

        mov si, word ptr [ argpointer ][ di ]           ; get pointer to first arg
        mov ax, word ptr [ si ]
        cmp ah, ':'                                     ; not a drive specification ?
        jnz _Vol_62                                     ; no -->

        _upperCase al                                   ; disk id
        cmp al, 'Z'+1
        jnc _Vol_62
        cmp al, 'A'
        jc _Vol_62

        storarg _currdisk, ax                           ; save disk

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get disk volume
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Vol_30:
        lea di, offset [ _finddata ][ bp ]
        call returnVolumeName
        jc _Vol_62                                      ; cannot open drive -->

        mov bx, offset _Dir_NoVolumeLabel               ; assume no volume label
        jnz _Vol_34                                     ; if no volume name -->

        push di                                         ; save vol label pointer
        mov bx, offset _Dir_VolumeLabel                 ; print statement format

_Vol_34:
        lea di, offset [ _currdisk ][ bp ]              ; pointer to current disk
        push di                                         ; current disk
        push bx                                         ; format
        lea di, offset [ _printbuffer ][ bp ]
        push di
        call _sprintf
        add sp, ax                                      ; # args passed
        call DisplayLine

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Vol_60:
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  drive specification error
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_Vol_62:
        mov dx, offset CmndError_InvalidDrive
        call DisplayErrorMessage
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Commands Not Supported                                       ;
        ;...............................................................;

_Chcp:
_Ctty:
        mov dx, offset CmndError_NotSupportedInRxDOS
        call DisplayErrorMessage
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Other Commands                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;                                                               ;
        ;                                                               ;
        ;...............................................................;

_Help:
_History:
_Move:
        mov dx, offset CmndError_NotSupportedYet
        call DisplayErrorMessage
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Not A Valid Command                                          ;
        ;...............................................................;

_NotValidCommand:

        push di
        mov dx, offset CmndError_BadCommandOrFileName
        call DisplayLine                                ; display error message

        pop di
        mov bx, word ptr [ sizeArgCmd ][ di ]           ; get second argument
        or bx, bx                                       ; second argument ?
        jz _NotValidCommand_08                          ; if no second argument -->
        mov word ptr ss:[ bx ], 0                       ; set at null terminator

_NotValidCommand_08:
        mov dx, word ptr [ argpointer ][ di ]           ; get command name
        call DisplayLine                                ; display error message

        mov dx, offset CmndError_EndOfBadCommand
        call DisplayLine                                ; display error message
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Exit                                                         ;
        ;...............................................................;

_Exit:

        push es
        Int21 GetPSPAddress

        mov es, bx
        cmp word ptr es:[ pspParentId ], 0000           ; no parent ?
        jz _Exit_12                                     ; can't exit -->

        lds bx, dword ptr [ _Int23_Original ]
        Int21 SetIntVector, 23h                         ; restore int 23
        Int21 TerminateProgram, 00h                     ; else ok

_Exit_12:
        pop es
        ret 

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Check File                                                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  cx   attributes                                              ;
        ;  dx   filename                                                ;
        ;  di   finddata structure                                      ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;  cy   file not found or other error                           ;
        ;...............................................................;

CheckFile:

        mov si, FINDDATA_MSDOSDATEFORMAT
        Int21 LFNFindFirstFile                          ; locate file
        jc CheckFile_22                                 ; if not found -->

        mov bx, ax
        Int21 LFNFindClose                              ; close search file
        clc

CheckFile_22:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Check File Close                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  bx   handle, or -1                                           ;
        ;...............................................................;

CheckFileClose:

        cmp bx, -1
        jz CheckFileClose_08
        Int21 LFNFindClose

CheckFileClose_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Return Prefered Filename                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  di   ptr FINDDATA structure                                  ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;  dx   returns ptr to Long name if present                     ;
        ;        else, Short name                                       ;
        ;                                                               ;
        ;...............................................................;

LFNReturnPreferedFilenamePtr:

        lea dx, offset [ findDataLongFilename ][ di ]
        cmp byte ptr es:[ findDataLongFilename ][ di ], 00
        jnz _lfnPref_08

        lea dx, offset [ findDataShortFilename ][ di ]

_lfnPref_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Build Expanded Filename                                      ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  si   ptr source filename arg (may contain partial path info) ;
        ;  di   ptr FINDDATA structure                                  ;
        ;  dx   ptr expanded path arg                                   ;
        ;  cx   search attributes                                       ;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  bx   handle to findfirst or -1                               ;
        ;  dx   offset to name field in expanded path arg               ;
        ;                                                               ;
        ;...............................................................;

BuildFullPathFirstFile:

        Entry
        def _patharg, si
        def _expandedpath, dx
        def _attributes, cx
        def _finddata, di
        def _handle, -1
        def _ptrName

        SaveRegisters di, si

        mov di, dx
        mov byte ptr [ di ], 00                         ; init area
        mov cl, LFNEXPAND_GETFULLPATHNAME
        Int21 LFNGetActualFileName                      ; expanded name (may include *.*)

        xor ax, ax
        mov cx, -1
        repnz scasb                                     ; length of expanded string
        not cx                                          ; make len positive
        dec cx                                          ; dec for term null

BuildFullPathFirstName_08:
        cmp byte ptr es:[ di - 1 ], '\'                 ; path divider ?
        jz BuildFullPathFirstName_12                    ; yes -->
        dec di
        loop BuildFullPathFirstName_08

BuildFullPathFirstName_12:
        storarg _ptrName, di

        getarg dx, _patharg                             ; expanded path
        getarg di, _finddata                            ; find work area
        getarg cx, _attributes                          ; search attributes
        mov si, FINDDATA_MSDOSDATEFORMAT                ; date format
        Int21 LFNFindFirstFile                          ; locate file
        jc BuildFullPathFirstName_24                    ; if it does not exist -->

        storarg _handle, ax                             ; save handle

        getarg di, _finddata
        call LFNReturnPreferedFilenamePtr               ; get whichever name is available

        mov si, dx
        getarg di, _ptrName
        call CopyString                                 ; append filename

BuildFullPathFirstName_24:
        RestoreRegisters si, di

        getarg bx, _handle
        getarg dx, _ptrName
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Build Next Expanded Filename                                 ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  di   ptr FINDDATA structure                                  ;
        ;  dx   ptr expanded path arg                                   ;
        ;  bx   search handle                                           ;
        ;                                                               ;
        ;...............................................................;

BuildFullPathNextFile:

        Entry
        def _handle, bx
        def _expandedpath, dx
        def _finddata, di

        SaveRegisters di, si, dx, cx, bx, ax

        getarg di, _finddata                            ; find work area
        mov si, FINDDATA_MSDOSDATEFORMAT                ; date format
        Int21 LFNFindNextFile                           ; locate next file
        jc BuildFullPathNextName_24                     ; if it does not exist -->

        getarg di, _finddata
        call LFNReturnPreferedFilenamePtr               ; get whichever name is available
        mov si, dx

        getarg di, _expandedpath                        ; where to copy expanded name
        call CopyString                                 ; append filename

BuildFullPathNextName_24:
        RestoreRegisters ax, bx, cx, dx, si, di
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Build Close Expanded Filename                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  bx   search handle                                           ;
        ;                                                               ;
        ;...............................................................;

BuildFullPathClose:

        cmp bx, -1
        jz BuildFullPathClose_08
        Int21 LFNFindClose

BuildFullPathClose_08:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compute Length                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  cx   length of string                                        ;
        ;  si   starting address of string.                             ;
        ;...............................................................;

_computeLength:
        push si

_computeLength_06:
        cmp byte ptr [ si ], ' '+ 1
        jc _computeLength_08
        cmp byte ptr [ si ], ')'
        jz _computeLength_08
        cmp byte ptr [ si ], ','
        jz _computeLength_08
        cmp byte ptr [ si ], ';'
        jz _computeLength_08

        inc si
        jmp _computeLength_06

_computeLength_08:
        pop cx
        xchg cx, si
        sub cx, si
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  delete Lead At Sign                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;  di   starting address of NULL terminated string.             ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;  ZR   if lead character is @ sign                             ;
        ;  all registers preserved except flags.                        ;
        ;...............................................................;

deleteLeadAtSign:
        cmp byte ptr [ di ], '@'                        ; starts with @ sign ?
        jnz deleteLeadAtSign_12                         ; no -->

        pushf
        SaveSegments di, si, ax

        setDS es
        mov si, di                                      ; copy buffer ptr
        inc si                                          ; source
        call CopyString

        RestoreSegments ax, si, di
        popf

deleteLeadAtSign_12:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  See if Wild Characters Are Used.                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  cx   length of string                                        ;
        ;  si   starting address of string.                             ;
        ;                                                               ;
        ;...............................................................;

_Seeif_WildCharacter:

        push si
        push cx

_WildCharacter_08:
        lodsb
        cmp al, '?'
        jz _WildCharacter_12
        cmp al, '*'
        jz _WildCharacter_12

        loop _WildCharacter_08
        stc                                     

_WildCharacter_12:
        pop cx
        pop si
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Create Default Master Environment, if none                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   bx     pointer to an environment                            ;
        ;   es     pointer to PSP                                       ;
        ;...............................................................;

_DefaultCreateEnvironment:

        Entry
        def  _CurrentPSP, es
        ddef _pgmName, 0000, 0000                       ; save ptr to orig pgm name

        or bx, bx                                       ; env block passed ?
        jz _crEnvironment_24                            ; if no name passed, create -->

        mov es, bx                                      ; current segment
        xor di, di                                      ; byte offset
        cmp word ptr es:[ di ], 0000                    ; empty env block ?
        jnz _crEnvironment_36                           ; no, leave alone -->

        add di, 4                                       ; skip over # strings word
        stordarg _pgmName, es, di                       ; save ptr to orig pgm name

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; allocate env segment
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_crEnvironment_24:
        mov bx, word ptr [ _EnvSize ]                   ; from /E or default size
        shr bx, 1
        shr bx, 1
        shr bx, 1
        shr bx, 1                                       ; # paragraphs
        Int21 AllocateMemory                            ; try to allocate
        jc _crEnvironment_36                            ; if couldn't -->
        
        mov es, ax
        mov word ptr [ _EnvSegment ], ax                ; updated env segment address
        
        xor di, di
        mov cx, word ptr [ _EnvSize ]
        clearMemory 

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; create COMSPEC
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        cmp word ptr [ _pgmName. _segment ][ bp ], 0000 ; source name ?
        jz _crEnvironment_36                            ; no -->
        
        mov si, offset RxDOS_CommandSpec
        call insertEnvVariable                          ; insert COMSPEC=
        jc _crEnvironment_36                            ; if no room -->

        push ds
        getdarg ds, si, _pgmName                        ; com spec name
        call insertEnvVariable                          ; insert spec name
        pop ds                                          ; restore stack
     ;  jc _crEnvironment_36                            ; if no room -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; exit
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_crEnvironment_36:
        mov bx, word ptr [ _EnvSegment ]
        mov es, word ptr [ _CurrentPSP ][ bp ]
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Save Batch File Position                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   bx     batch file handle                                    ;
        ;   dx:ax  position in batch file                               ;
        ;...............................................................;

getBatchPosition:

        xor cx, cx
        xor dx, dx
        mov bx, word ptr [ RxDOS_BatchFile. batchFileHandle ]
        Int21 MoveFilePointer, SEEK_CUR
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Manage Stdin Input                                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:dx  buffer (pointer to max length)                       ;
        ;...............................................................;

_getStdinLine:

        Entry
        def _buffer, dx

        push di
        push bx
        push dx

        mov di, dx
        mov bx, STDIN
        Int21 IoControl, 00h                            ; get stdin device data
        test dx, sftIsDevice                            ; file or device ?
        jnz _getStdinLine_32                            ; device, just read directly -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  read by character if file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_getStdinLine_08:
        mov di, word ptr [ _buffer ][ bp ]
        mov bl, byte ptr [ bufActualLength ][ di ]      ; store character into buffer
        xor bh, bh
        lea dx, offset [ bufData ][ di + bx ]           ; data address

        mov cx, 1
        mov bx, STDIN
        Int21 ReadFile                                  ; read character
        jc _getStdinLine_28

        or ax, ax                                       ; read anything ?
        jz _getStdinLine_28                             ; no, see if return -->

        mov di, word ptr [ _buffer ][ bp ]
        mov bl, byte ptr [ bufActualLength ][ di ]      ; store character into buffer
        xor bh, bh

        mov dl, byte ptr [ bufData ][ di + bx ]         ; get character 
        cmp dl, 'J'-40h                                 ; character lf ?
        jz _getStdinLine_08                             ; ignore lf -->

        push dx
        Int21 DisplayOutput                             ; echo character

        pop dx
        inc byte ptr [ bufActualLength ][ di ]          ; store character into buffer
        cmp dl, 'M'-40h                                 ; character cr ?
        jz _getStdinLine_34                             ; yes, end of input -->

        mov al, byte ptr [ bufActualLength ][ di ]      ; actual less than max ?
        cmp al, byte ptr [ bufMaxLength ][ di ]         ; get max
        jc _getStdinLine_08
        jmp short _getStdinLine_34

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  end of file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_getStdinLine_28:
        mov di, word ptr [ _buffer ][ bp ]
        mov dl, byte ptr [ bufActualLength ][ di ]      ; store character into buffer
        or dl, dl                                       ; any characters ?
        jnz _getStdinLine_34                            ; yes, return -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  cancel stdin
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, 1                                       ; this may have to change !
        mov bx, STDIN
        call _CloseRedirectedDevice

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  read directly if device
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_getStdinLine_32:
        pop dx                                          ; buffer address
        Int21 GetLine                                   ; read direct from device

_getStdinLine_34:
        getarg dx, _buffer
        pop bx
        pop di
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Read Line                                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:si  buffer (pointer to max length)                       ;
        ;   bx     file handle (null value means stdin )                ;
        ;   al     echo status                                          ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   ax     actual length not incl cr.                           ;
        ;                                                               ;
        ;...............................................................;

_ReadLine:

        Entry
        def _Handle, bx
        ddef _buffer, ss, si

        or bx, bx                                       ; process from batch file ?
        jz _ReadLine_08                                 ; no, read from console -->

        call _ReadBatch                                 ; else read batch file
        jmp short _ReadLine_26

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  read from keyboard
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ReadLine_08:
        call DisplayPrompt                              ; display prompt

        mov dx, word ptr [ _buffer. _pointer ][ bp ]
        call _getStdinLine                              ; read buffer
       ;; call CRLF

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ReadLine_26:
        mov si, word ptr [ _buffer. _pointer ][ bp ]
        mov al, byte ptr [ bufActualLength ][ si ]
        and ax, 255                                     ; actual length
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Read Batch Line                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;   ss:si  buffer (pointer to max length)                       ;
        ;   bx     file handle (null value means stdin )                ;
        ;   al     echo status                                          ;
        ;...............................................................;

_ReadBatch:

        Entry
        def  _bytesRead
        def  _echoline, ax
        def  _Handle, bx
        ddef _buffer, ss, si

        add si, bufData                                 ; offset to data
        ddef _bufferData, ss, si

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  read line
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov cx, sizeCmdLine
        mov dx, word ptr [ _bufferData. _pointer ][ bp ]; where to read
        mov bx, word ptr [ RxDOS_BatchFile. batchFileHandle ]

        call ReadBatchLine
        mov word ptr [ _bytesRead ][ bp ], ax           ; save count
        jnz _ReadBatch_08                               ; if none -->

        call _EndCall                                   ; end batch file call

        xor ax, ax                                      ; return zero bytes
        mov di, word ptr [ _buffer. _pointer ][ bp ]    ; buffer address
        mov byte ptr [ bufActualLength ][ di ], al
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  trim line to carriage return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ReadBatch_08:
        mov di, word ptr [ _buffer. _pointer ][ bp ]    ; buffer address
        mov byte ptr [ bufActualLength ][ di ], al

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  perform variable replacement
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ReadBatch_20:
        mov bx, word ptr [ _bytesRead ][ bp ]
        mov si, word ptr [ _bufferData. _pointer ][ bp ]; buffer address
        mov byte ptr [ si+bx ], 00
        call ReplaceTempVariables                       ; replace %n variables

        mov si, word ptr [ _buffer. _pointer ][ bp ]    ; buffer address
        mov byte ptr [ bufActualLength ][ si ], cl

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  echo enabled ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        cmp byte ptr [ _echoLine ][ bp ], 00
        jz _ReadBatch_32                                ; echo is off -->

        mov cx, ax                                      ; length of line
        mov di, word ptr [ _bufferData. _pointer ][ bp ]; buffer address
        call deleteLeadAtSign                           ; clean up if lead at sign
        jz _ReadBatch_32                                ; if at sign, no echo -->

_ReadBatch_24:
        cmp byte ptr [ di ], ' '                        ; still in white space ?
        jnz _ReadBatch_26                               ; no, echo line -->
        inc di
        loop  _ReadBatch_24

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  echo this line
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ReadBatch_26:
        call DisplayPrompt                              ; display prompt

        mov dx, di
        call DisplayLine                                ; echo line
        call CRLF

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_ReadBatch_32:
        mov di, word ptr [ _buffer. _pointer ][ bp ]    ; buffer address
        mov ax, word ptr [ _bytesRead ][ bp ]
        or di, di                                       ; data always returned
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Read Batch Line                                              ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   bx     batch file handle                                    ;
        ;   dx     line buffer address                                  ;
        ;...............................................................;


ReadBatchLine:

        Entry
        def  _Handle, bx
        def  _lineBuffer, dx
        def  _bytesRead, 0000
        ddef _filePosition
        defbytes _tempbuffer, 2

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get current position in file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor cx, cx
        xor dx, dx
        Int21 MoveFilePointer, SEEK_CUR
        mov word ptr [ _FilePosition. _low  ][ bp ], ax
        mov word ptr [ _FilePosition. _high ][ bp ], dx ; save current position

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get current position in file
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getarg si, _lineBuffer
        mov byte ptr [ si ], 00                         ; set null terminator at beg

        getarg bx, _Handle
        mov cx, sizeCmdLine
        getarg dx, _lineBuffer                          ; where to read batch file
        Int21 ReadFile                                  ; read
        jc ReadBatchLine_36                             ; if error -->
        mov word ptr [ _bytesRead ][ bp ], ax

        or ax, ax                                       ; end of batch file ?
        jz ReadBatchLine_36                             ; yes -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  trim line to carriage return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov cx, ax                                      ; scan line 
        getarg di, _lineBuffer                          ; where to read batch file

        mov al, ControlM
        repnz scasb                                     ; scan for cr
        jz ReadBatchLine_20                             ; carriage return found -->

ReadBatchLine_12:
        mov cx, 1
        lea dx, offset [ _tempbuffer ][ bp ]            ; scan char by character till cr
        Int21 ReadFile                                  ; read
        jc ReadBatchLine_36                             ; if error -->
        or ax, ax                                       ; end of file ?
        jz ReadBatchLine_36                             ; yes -->

        cmp byte ptr [ _tempbuffer ][ bp ], ControlM    ; carriage return ?
        jnz ReadBatchLine_12                            ; no -->
        jmp short ReadBatchLine_30                      ; process line a bit -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  position line immediately after cr
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

ReadBatchLine_20:
        mov ax, word ptr [ _bytesRead ][ bp ]
        sub ax, cx
        push ax

        cmp byte ptr [ di ], ControlJ
        jnz ReadBatchLine_22                            ; if line feed doesn't follow -->
        inc ax                                          ; account for lf

ReadBatchLine_22:
        mov dx, word ptr [ _FilePosition. _low  ][ bp ]
        mov cx, word ptr [ _FilePosition. _high ][ bp ] ; orig position
        add dx, ax
        adc cx, 0000

        getarg bx, _Handle
        Int21 MoveFilePointer, SEEK_BEG                 ; get current position

        pop ax
        dec ax                                          ; don't include cr
        mov word ptr [ _bytesRead ][ bp ], ax           ; bytes read

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  does line begin with a line feed ?
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

ReadBatchLine_30:
        getarg di, _lineBuffer
        cmp byte ptr [ di ], ControlJ
        jnz ReadBatchLine_32                            ; no -->

        getarg cx, _bytesRead
        mov si, di
        inc si
        rep movsb                                       ; copy

        dec word ptr [ _bytesRead ][ bp ]

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  place null terminator at end
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

ReadBatchLine_32:
        getarg bx, _bytesRead
        getarg si, _lineBuffer
        mov byte ptr [ si+bx ], 00                      ; set null terminator at end 

        mov ax, bx
        or si, si                                       ; nz means valid return
        Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  end of file or error
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

ReadBatchLine_36:
        xor ax, ax
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Command Shell Start                                          ;
        ;...............................................................;

CommandBegin:

        cli
        mov ax, cs
        mov ds, ax
        mov es, ax
        mov ss, ax
        mov sp, offset RxDOS_CmdStack

        sti
        cld

        mov word ptr [ RxDOS_BatchFile. batchEchoStatus ], Yes ; echo is ON
        mov word ptr [ RxDOS_BatchFile. batchNumArgs ], 0000   ; no args

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  process startup command switches
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        push ds
        mov ax, ds
        sub ax, sizePSP / sizeParagraph
        mov ds, ax                                      ; segment 

        xor cx, cx
        mov si, offset 81h                              ; where command line is loc
        mov cl, byte ptr [ si - 1 ]
        call ProcessStartupSwitches                     ; get options
        pop ds

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  initialize stack frame
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Entry
        defbytes _cmdline, sizeCmdLineStruct
        defbytes _SaveBatchInfo, sizeBATCH_ARGS         ;  previous batch file 

        mov word ptr [ RxDOS_PrevStackFrame ], sp       ; current stack frame

        lea si, offset [ _cmdline ][ bp ]
        mov byte ptr ss:[ bufActualLength ][ si ], 0

        mov si, offset RxDOS_BatchFile
        mov di, word ptr [ RxDOS_PrevStackFrame ]       ; copy args to stack frame
        mov cx, sizeBATCH_ARGS
        rep movsb                                       ; copy

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  release unwanted memory (may be removed later)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Int21 GetPSPAddress
        mov word ptr [ _PSPSegment ], bx
        
        mov es, bx                                      ; block to be changed
        mov bx, offset RXDOSLASTADDRESS + sizeParagraph + 100h
        shr bx, 1
        shr bx, 1
        shr bx, 1
        shr bx, 1
        Int21 ModifyAllocatedMemory, 00

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get env, psp addresses
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov es, word ptr [ _PSPSegment ]                ; get PSP address
        mov bx, word ptr es:[ pspEnvironment ]          ; get seg of Environment
        mov word ptr [ _EnvSegment ], bx
        call _DefaultCreateEnvironment

        dec bx                                          ; locate Mem Control Block
        mov es, bx                                      ; point to mem block
        mov bx, word ptr es:[ _memAlloc ]               ; allocation in paras of env block
        shl bx, 1
        shl bx, 1
        shl bx, 1
        shl bx, 1                                       ; conv paras to segs
        mov word ptr [ _EnvSize ], bx                   ; save env size

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  set DTA
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Int21 GetDTA
        mov word ptr [ RxDOS_OrigDTA. _segment ], es
        mov word ptr [ RxDOS_OrigDTA. _pointer ], bx
        currSegment es

        mov dx, offset RxDOS_DTA
        Int21 SetDTA                                    ; set new disk transfer address

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  set interrupt vectors
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Int21 GetIntVector, 23h
        mov word ptr [ _Int23_Original. _pointer ], bx
        mov word ptr [ _Int23_Original. _segment ], es
        
        mov dx, offset _ControlC_ISR
        Int21 SetIntVector, 23h                         ; int 23h

        mov dx, offset CommandInput
        Int21 SetIntVector, 22h                         ; int 22h

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init screen
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setES ds                                        ; restore es
        call PhysClearScreen                            ; start with a clear screen

        mov dx, offset RxDOS_Version
        call DisplayLine                                ; print startup version

        mov dx, offset RxDOS_VersionCopyright
        call DisplayLine                                ; print startup version

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  execute Autoexec.Bat
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        ExOpenFile AUTOEXEC_BAT, OPEN_ACCESS_READONLY
        jc CommandInput                                 ; if not found -->

        mov word ptr [ RxDOS_BatchFile. batchFileHandle ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  basic command loop
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

CommandInput:
        setDS ss                                        ; restore ds
        setES ds                                        ; restore es
        mov sp, word ptr [ RxDOS_PrevStackFrame ]       ; current stack frame
        call SetPageZero                                ; set page 0

        mov dx, offset _Int2E_CommandParser
        Int21 SetIntVector, 2Eh                         ; int 2Eh

        lea si, offset [ _cmdline ][ bp ]
        mov byte ptr [ bufMaxLength ][ si ], sizeCmdLine

        mov bx, word ptr [ RxDOS_BatchFile. batchFileHandle ]
        mov al, byte ptr [ RxDOS_BatchFile. batchEchoStatus ]
        call _ReadLine
        jz CommandInput                                 ; no, redisplay prompt -->

        mov bx, ax
        lea si, offset [ _cmdline. bufActualLength ][ bp ]
        call _CommandParser                             ; go parse / execute
        jmp CommandInput   

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Process Startup Switches                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Inputs:                                                      ;
        ;   ds:si  Pointer to Command Line                              ;
        ;   cx     Command line length                                  ;
        ;                                                               ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ; Warning: DS points to command line buffer                     ;
        ;...............................................................;

ProcessStartupSwitches:

        mov bx, cx
        mov byte ptr [ si + bx ], 0                     ; null terminate line

_startupSwitches_08:
        lodsb

_startupSwitches_12:
        or al, al                                       ; end of line ?
        jz _startupSwitches_Return                      ; yes -->

        cmp al, '/'                                     ; switch ?
        jnz _startupSwitches_08                         ; not yet -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  get character past switch
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        lodsb
        _upperCase al
        cmp al, 'E'                                     ; set environment ?
        jz _startupSwitches_EnvSize                     ; yes -->
        cmp al, 'C'                                     ; one command ?
        jz _startupSwitches_SingleCommand               ; yes -->
        jmp _startupSwitches_12                         ; continue processing -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  One time command (unsupported)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_startupSwitches_SingleCommand:
        jmp _startupSwitches_08                         ; continue processing -->
        
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Get Env Size
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_startupSwitches_EnvSize:
        xor dx, dx                                      ; accum value

        inc si                                          ; skip through ':' or other delim
      ; cmp al, ':'                                     ; expect separator
      ; jz _startupSwitches_20                          ; ok -->

_startupSwitches_20:
        lodsb                                           ; get character
        cmp al, '0'
        jc _startupSwitches_26                          ; if not a digit -->
        cmp al, '9'
        jg _startupSwitches_26                          ; if not a digit -->

        and ax, 15                                      ; strip down value
        mov cx, dx
        add dx, dx                                      ; 2
        add dx, dx                                      ; 4
        add dx, cx                                      ; 5
        add dx, dx                                      ; 10
        add dx, ax                                      ; add new value
        jmp _startupSwitches_20

_startupSwitches_26:
        cmp dx, DEFAULT_MINALLOWEDENVIRONMENT
        jc _startupSwitches_12
        cmp dx, DEFAULT_MAXALLOWEDENVIRONMENT
        jnc _startupSwitches_12

        mov word ptr ss:[ _EnvSize ], dx                ; env size
        jmp _startupSwitches_12                         ; continue processing -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_startupSwitches_Return:
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Internal Commands                                            ;
        ;...............................................................;

        Even

RxDOS_InternalCommands:

        Cmnd _ChangeDir,        "cd"
        Cmnd _ChangeDir,        "chdir"
        Cmnd _MakeDir,          "md"
        Cmnd _MakeDir,          "mkdir"
        Cmnd _RemDir,           "rd"
        Cmnd _RemDir,           "rmdir"

        Cmnd _Break,            "break"
        Cmnd _Call,             "call"
        Cmnd _Chcp,             "chcp"
        Cmnd _Cls,              "cls"
        Cmnd _Copy,             "copy"
        Cmnd _Ctty,             "ctty"
        Cmnd _Date,             "date"
        Cmnd _Delete,           "del"
        Cmnd _Dir,              "dir"
        Cmnd _Echo,             "echo"
        Cmnd _Delete,           "erase"
        Cmnd _Exit,             "exit"
        Cmnd _For,              "for"
        Cmnd _Goto,             "goto"
        Cmnd _If,               "if"
        Cmnd _Loadhigh,         "lh"
        Cmnd _Loadhigh,         "loadhigh"
        Cmnd _Path,             "path"
        Cmnd _Pause,            "pause"
        Cmnd _Prompt,           "prompt"
        Cmnd _Rem,              "rem"
        Cmnd _Rename,           "rename"
        Cmnd _Rename,           "ren"
        Cmnd _Set,              "set"
        Cmnd _Shift,            "shift"
        Cmnd _Time,             "time"
        Cmnd _Truename,         "truename"
        Cmnd _Type,             "type"
        Cmnd _Verify,           "verify"
        Cmnd _Ver,              "ver"
        Cmnd _Vol,              "vol"
        dw -1

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Unix Style Commands                                          ;
        ;...............................................................;

        Even

RxDOS_UNIXStyleCommands:

        Cmnd _Type,             "cat"                   ; 
        Cmnd _Copy,             "cp"                    ;
        Cmnd _Delete,           "rm"                    ; 
        Cmnd _Dir,              "ls"                    ; 
        Cmnd _Move,             "mv"                    ; * future feature
        Cmnd _Set,              "setenv"                ;
        dw -1

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Environment Variables                                        ;
        ;...............................................................;

RxDOS_CommandSpec:              asciz "COMSPEC="
RxDOS_DirSpec:                  asciz "DIRCMD="
RxDOS_PathSpec:                 asciz "PATH="
RxDOS_PromptSpec:               asciz "PROMPT="
RxDOS_RxDOSSwitchSpec:          asciz "SWITCH="
RxDOS_RxDOSSpec:                asciz "RXDOS="
RxDOS_UNIXSpec:                 asciz "UNIX"

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Execution Order                                              ;
        ;...............................................................;

RxDOS_ExecReturnCode:           dw 0
RxDOS_ExecOrder:                dw _asciz_com
                                dw _asciz_exe
                                dw _asciz_bat
                                dw 0

_asciz_com:                     asciz ".com"
_asciz_exe:                     asciz ".exe"    
_asciz_bat:                     asciz ".bat"

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  On/Off Options                                               ;
        ;...............................................................;

RxDOS_OnOff:                    Cmnd 1,                 "on"
                                Cmnd 0,                 "off"
                                dw -1

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  If Options                                                   ;
        ;...............................................................;

RxDOS_IfOptions:                Cmnd IF_ERRORLEVEL,     "errorlevel"
                                Cmnd IF_EXIST,          "exists"
                                Cmnd IF_EXIST,          "exist"
                                Cmnd IF_NOT,            "not"
                                dw -1

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  For Options                                                  ;
        ;...............................................................;

RxDOS_ForArgs:                  Cmnd _IN,               "in"
                                Cmnd _DO,               "do"
                                dw -1

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Version Prompt                                               ;
        ;...............................................................;

RxDOS_Version:                  db "RxDOS Version 7.1.5", 0

RxDOS_VersionCopyright:         db 13, 10
                                db "(c) Copyright 1999 Api Software - All rights reserved", 13, 10 
                                db "    For updates, visit http://rxdos.com", 13, 10, 13, 10, 0

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Error Table                                                  ;
        ;...............................................................;

        Even

CmndError_TextReferenceTable:

        dw 0                                            ; 0000
        dw 0                                            ; 0001
        dw CmndError_FileNotFound                       ; 0002
        dw CmndError_InvalidDirectory                   ; 0003
        dw 0                                            ; 0004
        dw CmndError_AccessDenied                       ; 0005
        dw 0                                            ; 0006
        dw 0                                            ; 0007
        dw 0                                            ; 0008
        dw 0                                            ; 0009
        dw 0                                            ; 0010
        dw 0                                            ; 0011
        dw 0                                            ; 0012
        dw 0                                            ; 0013
        dw 0                                            ; 0014
        dw 0                                            ; 0015
        dw CmndError_CurrentDirectory                   ; 0016

CmndError_RefTableEntries       equ ($ - CmndError_TextReferenceTable)/2

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Error Messages                                               ;
        ;...............................................................;

CmndError_AccessDenied:           asciz "access denied"
CmndError_BadCommandOrFileName:   asciz "bad command: could not find or execute '"
CmndError_EndOfBadCommand:        asciz "'", 13, 10
CmndError_BadSwitch:              asciz "bad switch - "
CmndError_CannotCopyOverSelf:     asciz "file cannot be copied over itself"
CmndError_CannotCreateFile:       asciz "cannot create destination file"
CmndError_ContentsLostBeforeCopy: asciz "contents of file lost before copy"
CmndError_CurrentDirectory:       asciz "cannot remove current directory"
CmndError_FileAlreadyExists:      asciz "file not found or already exists"
CmndError_FileNotFound:           asciz "file not found"  
CmndError_NoFilesFound:           asciz " no files found", 13, 10
CmndError_InvalidDate:            asciz "invalid date"
CmndError_InvalidDirectory:       asciz "invalid directory"
CmndError_InvalidDrive:           asciz "invalid drive"
CmndError_InvalidTime:            asciz "invalid time"
CmndError_MustSpecifyOnOff:       asciz "must specify ON or OFF"
CmndError_NotSupportedYet:        asciz "command not supported yet !"
CmndError_NotSupportedInRxDOS:    asciz "command not supported in RxDOS"
CmndError_FileNameMissing:        asciz "file name missing"
CmndError_SubDirAlreadyExists:    asciz "subdirectory already exists"
CmndError_SyntaxError:            asciz "syntax error"
CmndError_TooManyParameters:      asciz "too many parameters - "
CmndError_ParametersMissing:      asciz "parameter(s) missing"
CmndError_NoPath:                 asciz "no path"
CmndError_InvalidNumberArguments: asciz "invalid number of arguments"
CmndError_OutOfEnvironmentSpace:  asciz "out of environment space"
CmndError_ReadErrorAbort:         asciz "error reading file"

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Other Variables                                              ;
        ;...............................................................;

RxDOS_NewLine:                  db 13, 10, 0

                                db "c:"
RxDOS_RootDirectory             db "\*.*", 0            ; root directory
RxDOS_AllFiles                  db "*"                  ; combines to for *.*
RxDOS_AllExtensions             db ".*", 0

RxDOS_TruenameCurrDir           db ".", 0               ; current directory

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Control C Handling                                           ;
        ;...............................................................;

                               even 
_Int23_Original                 dd 0

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Echo/ Paging                                                 ;
        ;...............................................................;

PageLines                       db 00                   ; paging mode
LinesDisplayed                  dw 00                   ; # lines displayed, curr page

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Prompt                                                       ;
        ;...............................................................;

RxDOS_DefaultPrompt             db "$n$g", 0
RxDOS_Prompt                    db "$n$g", 0, 128 dup(0)

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Special Command Separators                                   ;
        ;...............................................................;

_UnixStyle                      db 0                    ; non-zero if Unix style commands    
_SwitchChar                     db "/"                  ; switch character
_CmndParse_Separators           db " <>|[],+=()%"       ; valid break characters
_CmndParse_SeparatorsLength     equ ( $ - _CmndParse_Separators )

_CmndParse_FileSysReserved      db ":.\[];,+="          ; file system reserved chars
_CmndParse_FileSysReservedLen   equ ( $ - _CmndParse_FileSysReserved )

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Environment Location/ Size                                   ;
        ;...............................................................;

_PSPSegment                     dw 0                    ; PSP Segment Address
_EnvSegment                     dw 0                    ; Env Segment Address
_EnvSize                        dw DEFAULT_MINENVIRONMENT ; Env Size (bytes)

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Batch Arguments                                              ;
        ;...............................................................;

        BATCH_ARGS struc

batchArgPtrs                    dw ?                    ; 0
                                dw ?                    ; 1 arg pointers
                                dw ?                    ; 2       .
                                dw ?                    ; 3       .
                                dw ?                    ; 4       .
                                dw ?                    ; 5       .
                                dw ?                    ; 6       .
                                dw ?                    ; 7       .
                                dw ?                    ; 8       .
                                dw ?                    ; 9       .

batchNumArgs                    dw ?                    ; number of batch arguments
batchFileHandle                 dw ?                    ; batch file handle
batchFilePosition               dd ?                    ; batch file position
batchEchoStatus                 dw ?                    ; echo status

batchArgStore                   db 1024 dup(0)

        BATCH_ARGS ends

sizeBATCH_ARGS                  equ size BATCH_ARGS

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Batch File Arguments                                         ;
        ;...............................................................;

RxDOS_BatchFile                 db sizeBATCH_ARGS dup(0)
RxDOS_PrevStackFrame            dw 0                    ; end call stack frame
RxDOS_StackFrameNumEntries      dw 0                    ; number of entries in stack

 IFNDEF RxDOS_DEBUG
AUTOEXEC_BAT                    db "\autoexec.bat", 0

 ELSE
AUTOEXEC_BAT                    db "\autoexec.tst", 0

 ENDIF

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Restore Original Parameters                                  ;
        ;...............................................................;

RxDOS_OrigDTA                   dd ?                    ; original DTA value

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Type Switches                                                ;
        ;...............................................................;

_TypeSwitches:
_TypePauseSwitch:               Switch 'p', 0
                                db -1

_TypeCannotFind:                asciz 'cannot find - '

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Dir Switches                                                 ;
        ;...............................................................;

_DirSwitches:
_DirPauseSwitch:                Switch 'p', 0
_DirWideSwitch:                 Switch 'w', 0
_DirAttribSwitch:               Switch 'a', SW_LETTERCHOICE, SW_DIR_ATTRIB
_DirOrderSwitch:                Switch 'o', SW_LETTERCHOICE, SW_DIR_ORDER
_DirSubDirSwitch:               Switch 's', 0
_DirLowerCaseSwitch:            Switch 'l', 0
_DirBareSwitch:                 Switch 'b', 0
                                db -1

SW_DIR_ATTRIB:                  db '-dnsra', 0
SW_DIR_ORDER:                   db '-negsd', 0

_Dir_NoVolumeLabel:             asciz " Volume in drive %c has no label", 13, 10
_Dir_VolumeLabel:               asciz " Volume in drive %c is %s", 13, 10
_Dir_VolumeSerialNumber:        asciz " Volume Serial Number is %s", 13, 10
_Dir_DirectoryOf:               asciz " Directory of %s", 13, 10, 13, 10
_Dir_Files_Summary:             db    "    %5d file(s) %13,ld bytes", 13, 10
                                db    "                  %13,ld bytes free", 13, 10, 0
_Dir_Files_MBSummary:           db    "    %5d file(s) %13,ld bytes", 13, 10
                                db    "                  %9,ld.%2zd MB free", 13, 10, 0

_Dir_FileEntry:                 db    "%8s %3s %9,ld %s %s %s", 13, 10, 0
_Dir_DirEntry:                  db    "%8s %3s <DIR>     %s %s %s", 13, 10, 0

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Copy Messages                                                ;
        ;...............................................................;

_Copy_FilesCopied:              db    "    %5d file(s) copied", 13, 10, 0

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Pause/ Continue                                              ;
        ;...............................................................;

_PressAnyKeyToContinue:         asciz "Press any key to continue . . . $"
_Dir_Continuing:                asciz "(continuing %s)", 13, 10

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Break Display Options                                        ;
        ;...............................................................;

_BreakOptions:                  dw _BreakIsOFF, _BreakIsON

_BreakIsOFF:                    asciz "Break is OFF", 13, 10
_BreakIsON:                     asciz "Break is ON", 13, 10

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Echo Display Options                                         ;
        ;...............................................................;

_EchoOptions:                   dw _EchoIsOFF, _EchoIsON

_EchoIsOFF:                     asciz "Echo is OFF", 13, 10
_EchoIsON:                      asciz "Echo is ON", 13, 10

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Verify Display Options                                       ;
        ;...............................................................;

_VerifyOptions:                 dw _VerifyIsOFF, _VerifyIsON

_VerifyIsOFF:                   asciz "Verify is OFF", 13, 10
_VerifyIsON:                    asciz "Verify is ON", 13, 10

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Delete Yes/No                                                ;
        ;...............................................................;

_DeleteAllFiles:                db    "All files in directory will be deleted!", 13, 10
                                asciz "Are you sure (Y/N)?"

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Date Display Options                                         ;
        ;...............................................................;

_ShowCurrentDate:               asciz "Current date is %s"
_PleaseEnterDate:               asciz 13, 10, "Enter new date "

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Time Display Options                                         ;
        ;...............................................................;

_ShowCurrentTime:               asciz "Current time is %s"
_PleaseEnterTime:               asciz 13, 10, "Enter new time: "

RxDOSIntl_DateTimeTable:        intldate 001, "mm/dd/yyyy hh:mm:ss.00" ; USA
                                intldate 002, "yyyy-mm-dd HH:mm:ss,00" ; Canada-French
                                intldate 003, "dd/mm/yyyy hh:mm:ss.00" ; Latin America
                                intldate 031, "dd-mm-yyyy HH:mm:ss,00" ; Netherlands
                                intldate 032, "dd/mm/yyyy HH:mm:ss,00" ; Belgium
                                intldate 033, "dd.mm.yyyy HH:mm:ss,00" ; France
                                intldate 034, "dd/mm/yyyy HH:mm:ss,00" ; Spain
                                intldate 036, "yyyy-mm-dd HH:mm:ss,00" ; Hungary
                                intldate 038, "yyyy-mm-dd HH:mm:ss,00" ; Yugoslavia
                                intldate 039, "dd/mm/yyyy HH.mm.ss,00" ; Italy
                                intldate 041, "dd.mm.yyyy HH,mm,ss.00" ; Switzerland
                                intldate 042, "yyyy-mm-dd HH:mm:ss,00" ; Czechoslovakia
                                intldate 044, "dd/mm/yyyy HH:mm:ss.00" ; United Kingdom
                                intldate 045, "dd-mm-yyyy HH:mm:ss,00" ; Denmark
                                intldate 046, "yyyy-mm-dd HH.mm.ss,00" ; Sweden 
                                intldate 047, "dd.mm.yyyy HH:mm:ss,00" ; Norway
                                intldate 048, "yyyy-mm-dd HH:mm:ss,00" ; Hungary
                                intldate 049, "dd.mm.yyyy HH:mm:ss,00" ; Germany
                                intldate 055, "dd/mm/yyyy HH:mm:ss,00" ; Brazil
                                intldate 061, "dd/mm/yyyy HH:mm:ss.00" ; Intl English
                                intldate 351, "dd-mm-yyyy HH:mm:ss,00" ; Portugal
                                intldate 358, "dd.mm.yyyy HH.mm.ss,00" ; Finland
                                dw -1

RxDOSIntl_TimeTemplate          db "hh:mm:ss.00", 0
RxDOSIntl_DateTemplate          db "www mm/dd/yyyy", 0              ; USA

RxDOSIntl_DayOfWeek             db "SunMonTueWedThuFriSat", 0

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Disk Transfer Address                                        ;
        ;...............................................................;

                              even  
RxDOS_DTA:                      db 128 dup(' ')

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Command Shell Stack                                          ;
        ;...............................................................;

                              even  
                              dw 5E5Eh                        ; bottom signature

                              db ( 4* 1024) dup(?)
RxDOS_AltStack:               dw ?

                              db (12* 1024) dup(?)
RxDOS_CmdStack:               dw ?

RXDOSLASTADDRESS              equ $

RxDOSCMD                      ENDS
                              END  CommandBegin
