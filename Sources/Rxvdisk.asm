        TITLE   'RxVdisk - RxDOS Virtual Disk Driver'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RXDOS VDISK Driver                                           ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;


        include rxdosmac.inc
        include rxdosdef.inc

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RXDOS VDISK Driver                                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage: rxvdisk [ size in kb ]                                ;
        ;                                                               ;
        ;  To use, add device=rxvdisk entry to config.sys               ;
        ;  Size argument is optional and is in kb                       ;
        ;                                                               ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Macros                                                       ;
        ;...............................................................;

ComputeAddress  macro seg, reg, sector

     ifnb <sector>
        mov ax, sector
        endif

        mov dx, cs
        add dx, ( RxVDISK_DATASTORAGE - RxVDISK_Header ) / 16
        add ax, ax                                      ; 2
        add ax, ax                                      ; 4
        add ax, ax                                      ; 8
        add ax, ax                                      ; 10h
        add ax, ax                                      ; 20h
        add dx, ax
        mov seg, dx
        xor reg, reg
        endm

ComputeWords   macro

        mov ax, (sizeSECTOR/ 2)
        mul cx
        mov cx, ax                                      ; words to move
        endm

ComputeBytes   macro

        mov ax, sizeSECTOR
        mul cx
        mov cx, ax                                      ; bytes to move
        endm

RxVDISK SEGMENT PARA PUBLIC 'CODE'
        assume cs:RxVDISK, ds:RxVDISK, es:RxVDISK, ss:RxVDISK

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Device Header                                                ;
        ;...............................................................;

RxVDISK_Header:

__Next          dd -1
__Attribute     dw 2000h
__Strategy      dw RxVDISK_Strategy
__Interrupt     dw RxVDISK_Interrupt
__Units         db 1
                db 7 dup(?)

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RXDOS VDISK Dispatch Table                                   ;
        ;...............................................................;

RxVDISK_Dispatch:
                dw RxVDISK_DriverInit
                dw RxVDISK_MediaCheck
                dw RxVDISK_BuildBPB
                dw RxVDISK_Unsupported                  ; ioctl in
                dw RxVDISK_Read
                dw RxVDISK_Read
                dw RxVDISK_Unsupported                  ; in status
                dw RxVDISK_Unsupported                  ; in flush
                dw RxVDISK_Write
                dw RxVDISK_Write
                dw RxVDISK_Unsupported                  ; out status
                dw RxVDISK_Unsupported                  ; out flush
                dw RxVDISK_Unsupported                  ; ioctl out

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RXDOS VDISK Driver                                           ;
        ;...............................................................;

RxVDISK_BOOTREC:db 3 dup(0)                             ; Jump
                db 'RxVDISK '                           ; OEM Name

RxVDISK_BOOTREC_BPB:
                dw 512                                  ; bytes per sector
                db 1                                    ; sectors per cluster
                dw 1                                    ; reserved sectors
                db 1                                    ; number of FAT copies
                dw 64                                   ; max alloc root directory
                dw 360                                  ; max sectors (360 = 180k)
                db 0fch                                 ; media descriptor
                dw 2                                    ; sectors per fat
RxVDISK_BOOTREC_END   equ $

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RXDOS VDISK BPB                                              ;
        ;...............................................................;

RxVDISK_BPB     db (RxVDISK_BOOTREC_END - RxVDISK_BOOTREC_BPB) dup(?)


        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxVDISK Strategy                                             ;
        ;...............................................................;

RxVDISK_Strategy:

        retf

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxVDISK Dispatch                                             ;
        ;...............................................................;

RxVDISK_Interrupt:

        SaveAllRegisters

        cld
        push es
        push bx
        setDS cs
        xor ah, ah
        mov al, byte ptr es:[ rhFunction ][ bx ]
        add ax, ax
        mov di, ax
        call word ptr [ RxVDISK_Dispatch ][ di ]

        pop bx
        pop es
        or word ptr es:[ rhStatus ][ bx ], OP_DONE

        RestoreAllRegisters
        retf

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxVDISK Media Check                                          ;
        ;...............................................................;

RxVDISK_MediaCheck:

        mov byte ptr es:[ mrReturn ][ bx ], MEDIA_UNCHANGED
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxVDISK Build Parameter Block                                ;
        ;...............................................................;

RxVDISK_BuildBPB:

        push ds
        mov di, offset RxVDISK_BPB
        mov word ptr es:[ bbrBPBAddress. _pointer ][ bx ], di
        mov word ptr es:[ bbrBPBAddress. _segment ][ bx ], ds

        setES ds
        ComputeAddress ds, si, 0000                     ; sector 0

        mov si, (RxVDISK_BOOTREC_BPB - RxVDISK_BOOTREC)
        mov cx, size RxVDISK_BPB
        rep movsb                                       ; copy BPB from boot record

        pop ds
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxVDISK Read                                                 ;
        ;...............................................................;

RxVDISK_Read:

        mov cx, word ptr es:[ rwrBytesReq ][ bx ]       ; sectors
        ComputeWords                                    ; sectors --> words

        mov ax, word ptr es:[ rwrStartSec ][ bx ]
        ComputeAddress ds, si                           ; compute address

        les di, dword ptr es:[ rwrBuffer ][ bx ]
        rep movsw
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxVDISK Write                                                ;
        ;...............................................................;

RxVDISK_Write:

        mov cx, word ptr es:[ rwrBytesReq ][ bx ]       ; sectors
        ComputeWords                                    ; sectors --> words

        lds si, dword ptr es:[ rwrBuffer ][ bx ]
        mov ax, word ptr es:[ rwrStartSec ][ bx ]
        ComputeAddress es, di                           ; compute address

        rep movsw
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Driver Init                                                  ;
        ;...............................................................;

RxVDISK_DriverInit:

        mov dx, ds
        add dx, ( RxVDISK_DATASTORAGE - RxVDISK_Header ) / 16

   ; add cmd line parameter parsing here
   ; irParamAddress dd ?      input: config.sys device= line

        add dx, 2d00h                                   ; size **********
        mov word ptr es:[ irEndAddress. _pointer ][ bx ], 0
        mov word ptr es:[ irEndAddress. _segment ][ bx ], dx
        mov byte ptr es:[ irUnits ][ bx ], 1            ; units

        mov si, offset RxVDISK_BPB
        mov word ptr es:[ irParamAddress. _pointer ][ bx ], si
        mov word ptr es:[ irParamAddress. _segment ][ bx ], cs
        mov word ptr es:[ irMessageFlag ][ bx ], 0

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init BPB
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setES ds
        mov si, offset RxVDISK_BOOTREC_BPB
        mov di, offset RxVDISK_BPB
        mov cx, size RxVDISK_BPB
        rep movsb

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        ComputeAddress es, di, 0000                     ; sector 0

        mov cx, word ptr [ RxVDISK_BPB. _bpbMaxAllocRootDir ]
        shr cx, 1
        shr cx, 1
        shr cx, 1
        shr cx, 1                                       ; alloc root dir x 32 / 512
        inc cx                                          ; inc for boot sector
        add cx, word ptr [ RxVDISK_BPB. _bpbSectorsPerFat ]

   ; sectors per fat is size / #sectors per cluster ==> bytes (FAT16 or FAT12)

        ComputeWords                                    ; sectors --> words

        xor ax, ax
        rep stosw                                       ; init boot + FAT + root sector

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init boot sector
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor di, di
        mov si, offset RxVDISK_BOOTREC
        mov cx, RxVDISK_BOOTREC_END - RxVDISK_BOOTREC
        rep movsb

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  init FAT sectors
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        ComputeAddress es, di, 0001                     ; sector 1

        mov al, 0fch
        stosb                                           ; set media type

        mov al, 0ffh
        stosb                                           ; mark first two entries in use
        stosb

        mov dx, offset rxvdisk_startupMsg
        Int21 DisplayString

RxVDISK_Unsupported:

        clc
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RXDOS VDISK Data Area                                        ;
        ;...............................................................;

rxvdisk_startupMsg:
        db "RxDOS Real-Mode Virtual Disk", 13, 10, '$', 0

        org ($-RxVDISK_Header)+ 16 - (($-RxVDISK_Header) mod 16)

RxVDISK_DATASTORAGE        equ $


RxVDISK ENDS
        END RxVDISK_Header
