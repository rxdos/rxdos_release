        TITLE   'Generic IOControl - DOS IO Control Function 440D Services'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  IOControl - DOS IO Control Function 440D Services            ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc

RxDOS   SEGMENT PARA PUBLIC 'CODE'
        assume cs:RxDOS, ds:RxDOS, es:RxDOS, ss:RxDOS

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  IOControl - DOS IO Control Function 440D Services            ;
        ;...............................................................;

        public _IOControl_440D

        extrn getDrive                          : near
        extrn getDPB                            : near
        extrn _mul32                            : near
        extrn _RetCallersStackFrame             : near
        extrn AmountFreeSpace                   : near

        extrn pexterrInvalidFunction            : near
        extrn pexterrFileNotFound               : near
        extrn pexterrPathNotFound               : near
        extrn pexterrIllegalName                : near
        extrn pexterrNoHandlesAvailable         : near
        extrn pexterrAccessDenied               : near
        extrn pexterrInvalidHandle              : near
        extrn pexterrArenaTrashed               : near
        extrn pexterrNotEnoughMemory            : near
        extrn pexterrInvalidBlock               : near
        extrn pexterrInvalidAccess              : near
        extrn pexterrInvalidDrive               : near
        extrn pexterrCurrentDirectory           : near
        extrn pexterrNoMoreFiles                : near
        extrn pexterrFileExists                 : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Device Parameters Block                                      ;
        ;...............................................................;

DEVICEPARAMETERS     STRUC

devparmSpecialFcts      db ?                            ; special functions
devparmDeviceType       db ?                            ; device type
devparmDeviceAttribs    dw ?                            ; device attributes
devparmNumCylinders     dw ?                            ; # cylinders (size)
devparmMediaType        db ?                            ; media type (00h all except 360k disk)
devparmBPB              db sizeBPB dup(?)               ; bpb

DEVICEPARAMETERS     ENDS

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Device Type Codes
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

DEVICETYPE_360K         equ 00h
DEVICETYPE_720K         equ 02h
DEVICETYPE_FLOPPY       equ 01h                         ; 1.2 M floppy
DEVICETYPE_8_SD         equ 03h                         ; 8 inch single density
DEVICETYPE_8_DD         equ 04h                         ; 8 inch single density
DEVICETYPE_FIXEDDISK    equ 05h                         ; fixed disk
DEVICETYPE_TAPEDRIVE    equ 06h                         ; tape drive
DEVICETYPE_144FLOPPY    equ 07h                         ; 1.44M floppy
DEVICETYPE_RWOPTICAL    equ 08h                         ; read/ write optical disk
DEVICETYPE_288FLOPPY    equ 09h                         ; 2.88M floppy

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Device Parameters Block                                      ;
        ;...............................................................;

MEDIAPARAMETERS      STRUC

mediaparamSpecialFcts   db ?                            ; always 0
mediaparamDiskHeads     dw ?                            ; disk heads
mediaparamDiskCyl       dw ?                            ; disk cylinders
mediaparamFirstSector   dw ?                            ; first sector
mediaparamDiskSectors   dw ?                            ; disk sectors
mediaparamXferAddress   dd ?                            ; xfer address (not used)

MEDIAPARAMETERS      ENDS

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  440Dh Generic IOCTL                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Usage:                                                       ;
        ;  ax    440dh                                                  ;
        ;  bl    drive number ( default= 0, A:= 1, ... )                ;
        ;  ch    category code                                          ;
        ;  cl    minor code (function)                                  ;
        ;  ds:dx parameter block (returns data if function 60h or 61h)  ;
        ;                                                               ;
        ;...............................................................;

_IOControl_440D:

        mov al, cl                                      ; get fct minor code

        Goto 60h,      GetDeviceParameters



      ; Goto 40h,      SetDeviceParameters
      ; Goto 41h,      WriteLogicalDeviceTrack
      ; Goto 42h,      FormatLogicalDeviceTrack
      ; Goto 46h,      SetVolumeSerialNumber
      ; Goto 47h,      SetAccessFlag

      ; Goto 4Ah,      LockLogicalVolume
      ; Goto 4Bh,      LockPhysicalVolume

      ; Goto 61h,      ReadLogicalDeviceTrack
      ; Goto 62h,      VerifyLogicalDeviceTrack
      ; Goto 66h,      GetVolumeSerialNumber
      ; Goto 67h,      GetAccessFlag
      ; Goto 68h,      SenseMediaType
      ; Goto 6Ah,      UnlockLogicalVolume
      ; Goto 6Bh,      UnlockPhysicalVolume
      ; Goto 6Ch,      GetLockFlag
      ; Goto 6Dh,      EnumerateOpenFiles
      ; Goto 6Eh,      FindSwapFile
      ; Goto 6Fh,      GetDriveMapInformation
      ; Goto 70h,      GetCurrentLockState
      ; Goto 71h,      GetFirstCluster

        mov ax, offset pexterrInvalidFunction
        stc
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  440D 60 Get Device Parameters                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Get Device Parameters                                        ;
        ;                                                               ;
        ;  cx    buffer size (should equal sizeDPB + size WORD)         ;
        ;  dl    drive                                                  ;
        ;  es:di pointer to buffer                                      ;
        ;                                                               ;
        ;...............................................................;

GetDeviceParameters:

        Entry
        ddef _ptrtoBuffer
        def  _size, cx

        mov ax, offset pexterrInvalidFunction
        cmp cx, sizeExtDPB
        stc
        jnz Get_ExtDPB_return

        xor dh, dh
        mov ax, dx
        call getDPB                                     ; get DPB
        jc Get_ExtDPB_return                            ; if error -->
        stordarg _ptrtoBuffer, es, bx                   ; save pointer

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; es:bx points to DPB for drive
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        RetCallersStackFrame es, bx
        mov di, word ptr es:[ _DI ][ bx ]
        mov es, word ptr es:[ _ExtraSegment ][ bx ]     ; buffer address

        getdarg ds, si, _ptrtoBuffer                    ; save pointer
        getarg cx, _size

        sub cx, 2
        mov es:[ di ], cx                               ; set length in WORD
        add di, 2                                       ; bump pointer
        rep movsb                                       ; copy DPB to return address

        or ax, ax                                       ; clear carry

Get_ExtDPB_return:
        ret

RxDOS   ENDS
        END

