        TITLE   'fat - walk through FAT Tables for rxdos'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  FAT Walk Through FAT Tables for rxdos                        ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Mike Podanoffsky                   ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This is free software; you can redistribute it and/or modify ;
        ;  it under the terms of the GNU General Public License, see    ;
        ;  the file COPYING.                                            ;
        ;                                                               ;
        ;  mail: mike.podanoffsky@mindspring.com                        ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc

RxDOS   SEGMENT PUBLIC 'CODE'
        assume cs:RxDOS, ds:RxDOS, es:RxDOS, ss:RxDOS

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Walk Through FAT Tables                                      ;
        ;...............................................................;

        public AllocateCluster
        public AllocateInitCluster
        public AmountFreeSpace
        public UpdateFreeSpace
        public AppendCluster
        public computeLogSectorNumber

        public getNextClusterDskAccess
        public ReleaseClusterChain
        public ReleaseClusterChainDskAccess

        public updateClusterValue
        public _FATReadRandom

        extrn CCBChanged                        : near
        extrn getDPB                            : near
        extrn getAddrDPB                        : near
        extrn readBuffer                        : near
        extrn readFATBuffer                     : near
        extrn readSelBuffer                     : near
        extrn SelBuffer                         : near
        extrn CCBChanged                        : near
        extrn updateChangedCCB                  : near
        extrn updateAllChangedCCBBuffers        : near
        extrn _DebugInterruptTrap               : near
        extrn DevRead                           : near
        extrn _cmp32                            : near
        extrn _mul32                            : near
        extrn _div32                            : near

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Definitions                                                  ;
        ;...............................................................;

MINCLUSTER      EQU 2                                   ; min cluster value

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Next Cluster                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   ss:di  ptr diskAccess structure                             ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  next cluster                                         ;
        ;   zr     if end of cluster chain.                             ;
        ;                                                               ;
        ;  Preserves:                                                   ;
        ;   es, ds, di, si, bx, ax                                      ;
        ;...............................................................;

getNextClusterDskAccess:

        mov ax, word ptr ss:[ diskAcDrive ][ di ]
        mov dx, word ptr ss:[ diskAcCurCluster. _low ][ di ]
        mov cx, word ptr ss:[ diskAcCurCluster. _high ][ di ]
        call getNextCluster

      ; mov word ptr ss:[ diskAcCurCluster. _low  ][ di ], dx
      ; mov word ptr ss:[ diskAcCurCluster. _high ][ di ], cx
        ret 

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Next Cluster                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  current cluster                                      ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  next cluster                                         ;
        ;   zr     if end of cluster chain.                             ;
        ;                                                               ;
        ;  Preserves:                                                   ;
        ;   es, ds, di, si, cx, bx, ax                                  ;
        ;...............................................................;

getNextCluster:

        saveRegisters es, di, si, bx, ax

        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        jnc getNextCluster_12                           ; exit if error -->

        mov cx, -1
        mov dx, cx                                      ; on error, set -1 to cx: dx
        cmp dx, cx                                      ; set zr ( == -1)
        stc                                             ; plus carry
        jmp short getNextCluster_Return                 ; exit -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if FAT32
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

getNextCluster_12:
        cmp word ptr es:[ _dpbSectorsPerFat ][ bx ], 0000
        jnz getNextCluster_16

        call _FAT32GetNextCluster
        jmp short getNextCluster_Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if FAT16, FAT12, ...
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

getNextCluster_16:
        call _FAT16GetNextCluster

getNextCluster_Return:
        restoreRegisters ax, bx, si, di, es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Next Cluster                                  (non FAT32);
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   dx     current cluster                                      ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   dx     next cluster                                         ;
        ;   zr     if end of cluster chain.                             ;
        ;                                                               ;
        ;  Preserves:                                                   ;
        ;   es, ds, di, si, cx, bx, ax                                  ;
        ;...............................................................;

_FAT16GetNextCluster:

        Entry
        def _drive, ax
        def _cluster, dx

        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        mov dx, -1                                      ; presume end if error
        jc _FAT16GetNextCluster_Return                  ; exit if error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  determine FAT32, FAT16, FAT12, ...
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ _cluster ][ bp ]             ; get cluster #
        cmp ax, MINCLUSTER                              ; less than min cluster ?
        jc _FAT16GetNextCluster_22                      ; exit with -1 -->

        cmp ax, word ptr es:[ _dpbMaxClusterNumber ][ bx ]
        jc _FAT16GetNextCluster_08                      ; if valid -->
        jnz _FAT16GetNextCluster_22                     ; else exit with -1 -->
        
_FAT16GetNextCluster_08:
        xor dx, dx
        test word ptr es:[ _dpbMaxClusterNumber ][ bx ], 0F000h
        jnz _FAT16GetNextCluster_16Bits                 ; if 16 -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  12 bit FAT entries
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FAT16GetNextCluster_12Bits:
        mov ax, word ptr [ _drive ][ bp ]               ; get drive
        mov dx, word ptr [ _cluster ][ bp ]             ; and cluster
        call _get_12Bit_ClusterValue
        jmp short _FAT16GetNextCluster_Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  16 bit FAT entries
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FAT16GetNextCluster_16Bits:
        mov cx, word ptr es:[ _dpbBytesPerSector ][ bx ]
        shr cx, 1
        div cx                                          ; FAT sector/ Offset

      ; ax will contain FAT sector
      ; dx will contain byte offset into FAT sector

        add dx, dx                                      ; make word offset
        push dx

        xor cx, cx
        mov dx, word ptr es:[ _dpbFirstFAT ][ bx ]      ; where is first FAT table ?
        add dx, ax                                      ; add offset required
        mov ax, word ptr [ _drive ][ bp ]               ; get drive
        call readFATBuffer                              ; read FAT Table
        or byte ptr es:[ ccbStatus ][ di ], ( ccb_isFAT )

        pop bx                                          ; word offset into FAT table
        mov dx, word ptr es:[ ccbData ][ bx + di ]      ; get FAT word

        xor cx, cx
        mov ax, dx
        and ax, 0FFF8h                                  ; FAT value, 12 bit entries.
        cmp ax, 0FFF8h                                  ; end of chain ?
        jnz _FAT16GetNextCluster_22                     ; no -->
        mov dx, -1                                      ; if end, set end value
        mov cx, dx

_FAT16GetNextCluster_22:
        clc

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FAT16GetNextCluster_Return:
        getarg ax, _drive
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Next Cluster                                     (FAT32) ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  current cluster                                      ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  next cluster                                         ;
        ;   zr     if end of cluster chain.                             ;
        ;                                                               ;
        ;  Preserves:                                                   ;
        ;   es, ds, di, si, cx, bx, ax                                  ;
        ;...............................................................;

_FAT32GetNextCluster:

        Entry
        def _drive, ax
        ddef _cluster, cx, dx
        ddef _alloccluster, -1, -1                      ; allocated cluster at -1 (error for now)
        ddef _maxclusters

        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        ifc _FAT32GetNextCluster_Return                  ; exit if error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  determine FAT32, FAT32, FAT12, ...
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        call SeeIfMaxClusters                           ; at max clusters ?
        stordarg _maxclusters, cx, dx                   ; (save max clusters )
        ifc _FAT32GetNextCluster_22                     ; exit with -1 -->

        mov cx, word ptr es:[ _dpbBytesPerSector ][ bx ]
        shr cx, 1
        shr cx, 1                                       ; divide by 4 (4-bytes/ entry)

        getdarg dx, ax, _cluster                        ; restore cluster address
        call _div32                                     ; FAT sector/ offset

      ; dx:ax will contain FAT sector
      ; cx will contain byte offset into FAT sector

        push cx
        add ax, word ptr es:[ _dpbxReserveSectors. _low ][ bx ]
        adc dx, word ptr es:[ _dpbxReserveSectors. _high ][ bx ] ; point to fat sector

        mov cx, dx
        mov dx, ax                                      ; copy sector address to cx:dx
        getarg ax, _drive                               ; set drive
        call readFATBuffer                              ; read FAT Table
        or byte ptr es:[ ccbStatus ][ di ], ( ccb_isFAT )

        pop bx                                          ; offset into FAT table
        add bx, bx
        add bx, bx
        mov dx, word ptr es:[ ccbData. _low ][ bx + di ]; get FAT 
        mov cx, word ptr es:[ ccbData. _high ][ bx + di ]
        stordarg _alloccluster, cx, dx

        call isEndOfFAT32
        jnz _FAT32GetNextCluster_22                     ; no -->
        stordarg _alloccluster, -1, -1                  ; set end value

_FAT32GetNextCluster_22:
        clc

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FAT32GetNextCluster_Return:
        getdarg cx, dx, _alloccluster
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Scan Ahead Cluster Map                                       ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Determines how many sequential clusters can be read.         ;
        ;  It will never return more than 65,536 seq clusters.          ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   stack  max clusters to scan                                 ;
        ;   ax     drive                                                ;
        ;   cx:dx  current cluster                                      ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     # clusters that can be sequentially read             ;
        ;   cx:dx  last cluster value                                   ;
        ;...............................................................;

scanClusterMap:

        Entry 1
        arg   _maxClustersToScan

        def  _drive, ax
        ddef  _cluster, cx, dx
        def  _clusterCount, 0000

scanClusterMap_12:
        inc word ptr [ _clusterCount ][ bp ]            ; cluster count
        getarg ax, _maxClustersToScan
        or ax, ax                                       ; zero clusters to scan ?
        jz scanClusterMap_22                            ; yes -->

        getarg ax, _drive
        call getNextCluster                             ; get next cluster

        cmp cx, -1                                      ; at max cluster ?
        jnz scanClusterMap_14                           ; no -->
        cmp dx, -1                                      ; end ?
        jz scanClusterMap_22                            ; yes -->

scanClusterMap_14:
        mov ax, dx
        sub ax, word ptr [ _cluster. _low  ][ bp ]      ; are clusters sequential (diff = 1)
        cmp ax, 1                                       ;  ... identified by an offset of 1
        jnz scanClusterMap_22                           ; if NOT sequential -->

        cmp cx, word ptr [ _cluster. _high ][ bp ]
        jnz scanClusterMap_22                           ; not sequential -->

        stordarg _cluster, cx, dx                       ; save cluster
        dec word ptr [ _maxClustersToScan ][ bp ]       ; scan more ?
        jnz scanClusterMap_12                           ; yes -->

scanClusterMap_22:
        mov ax, word ptr [ _clusterCount ][ bp ]        ; # sequential clusters
        getdarg cx, dx, _cluster                        ; return end cluster
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Update Cluster Value                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Updates the value given for any cluster.                     ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster                                              ;
        ;   stack  darg cluster value                                   ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cx     original contents in cluster cell                    ;
        ;                                                               ;
        ;...............................................................;

updateClusterValue:

        Entry 2
        darg _updatevalue

        SaveRegisters es, bx, ax

        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        jc updateClusterValue_Return                    ; exit if error -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Update FAT32 cluster
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        cmp word ptr es:[ _dpbSectorsPerFat ][ bx ], 0000
        jnz updateClusterValue_FAT16

        push word ptr [ _updatevalue. _high ][ bp ]     ; stack args
        push word ptr [ _updatevalue. _low ][ bp ]      ; cx:dx is cluster address
        call _FAT32UpdateClusterValue
        jmp short updateClusterValue_Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Update FAT16 cluster
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

updateClusterValue_FAT16:
        mov cx, word ptr [ _updatevalue. _low ][ bp ] 
        call _FAT16UpdateClusterValue

updateClusterValue_Return:
        RestoreRegisters ax, bx, es
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Update Cluster Value                                 (FAT32) ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Updates the value given for any cluster.                     ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster                                              ;
        ;   stack  value                                                ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cx:dx  original contents in cluster cell                    ;
        ;                                                               ;
        ;...............................................................;

_FAT32UpdateClusterValue:

        Entry 2
        darg _updatevalue

        def _drive, ax
        ddef _cluster, cx, dx
        def _sectorsize
        ddef _sector

        saveRegisters es, di, si, bx, ax


;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  fix EOF value 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        cmp word ptr [ _updatevalue. _high ][ bp ], -1
        jnz _FAT32UpdateCluster_08
        cmp word ptr [ _updatevalue. _low  ][ bp ], -1
        jnz _FAT32UpdateCluster_08
        mov word ptr [ _updatevalue. _high ][ bp ], 0FFFh

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  find fat sector
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FAT32UpdateCluster_08:
        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        call SeeIfMaxClusters                           ; at max clusters ?
        ifnc _FAT32UpdateCluster_12                     ; exit with -1 -->

        mov word ptr [ _updatevalue. _low ][ bp ], -1
        mov word ptr [ _updatevalue. _high ][ bp ], -1
        jmp short _FAT32UpdateCluster_26

_FAT32UpdateCluster_12:
        mov cx, word ptr es:[ _dpbBytesPerSector ][ bx ]
        shr cx, 1
        shr cx, 1                                       ; divide by 4 (4-bytes/ entry)

        getdarg dx, ax, _cluster                        ; restore cluster address
        call _div32                                     ; FAT sector/ offset

      ; dx:ax will contain FAT sector
      ; cx will contain byte offset into FAT sector

        push cx
        add ax, word ptr es:[ _dpbxReserveSectors. _low ][ bx ]
        adc dx, word ptr es:[ _dpbxReserveSectors. _high ][ bx ] ; point to fat sector

        mov cx, dx
        mov dx, ax                                      ; copy sector address to cx:dx
        getarg ax, _drive                               ; set drive
        call readFATBuffer                              ; read FAT Table
        or byte ptr es:[ ccbStatus ][ di ], ( ccb_isFAT )

        pop bx                                          ; offset into FAT table
        add bx, bx
        add bx, bx

        getdarg cx, dx, _updatevalue
        xchg dx, word ptr es:[ ccbData. _low ][ bx + di ]
        xchg cx, word ptr es:[ ccbData. _high ][ bx + di ]
        stordarg _updatevalue, cx, dx

        call CCBChanged
        or cx, cx

_FAT32UpdateCluster_26:
        getdarg cx, dx, _updatevalue
        call isEndOfFAT32

        restoreRegisters ax, bx, si, di, es
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Update Cluster Value                             (non FAT32) ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Updates the value given for any cluster.                     ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx     value                                                ;
        ;   dx     cluster                                              ;
        ;                                                               ;
        ;  Returns:                                                     ;
        ;   cx     original contents in cluster cell                    ;
        ;                                                               ;
        ;...............................................................;

_FAT16UpdateClusterValue:

        Entry
        def _drive, ax
        def _value, cx
        def _cluster, dx
        def _sectorsize
        ddef _sector

        saveRegisters es, di, si, dx, bx, ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  determine whether its 12 or 16 bit FAT entries
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        jc _FAT16UpdateClusterValue_04                  ; if error -->

        mov ax, word ptr [ _cluster ][ bp ]             ; get cluster #
        cmp ax, MINCLUSTER                              ; invalid number 
        jc _FAT16UpdateClusterValue_04                  ; exit -->
        cmp ax, word ptr es:[ _dpbMaxClusterNumber ][ bx ]
        jc _FAT16UpdateClusterValue_08                  ; if valid cluster # -->
        jz _FAT16UpdateClusterValue_08                  ; if valid cluster # -->

_FAT16UpdateClusterValue_04:
        mov dx, -1
        cmp dx, -1                                      ; sets zero (end of list)
        stc                                             ; and set carry (error )
        jmp _FAT16UpdateClusterValue_Return

_FAT16UpdateClusterValue_08:
        xor dx, dx
        test word ptr es:[ _dpbMaxClusterNumber ][ bx ], 0F000h
        ifnz _FAT16UpdateClusterValue_16Bits            ; if 16 bit FAT -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  12 bit FAT entries
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FAT16UpdateClusterValue_12Bits:
        mov cx, ax
        add ax, ax
        add ax, cx                                      ; nibble address

        mov cx, word ptr es:[ _dpbBytesPerSector ][ bx ]
        mov word ptr [ _sectorsize ][ bp ], cx
        dec word ptr [ _sectorsize ][ bp ]
        shl cx, 1                                       ; nibbles / sector
        div cx                                          ; sector to read

      ; ax will contain sector
      ; dx will contain nibble offset

        shr dx, 1                                       ; word offset
        push dx                                         ; word offset

        xor cx, cx                                      ; 32 bit address
        mov dx, ax
        add dx, word ptr es:[ _dpbFirstFAT ][ bx ]      ; where is first FAT table ?
        stordarg _sector, cx, dx                        ; read next cluster sector
        mov ax, word ptr [ _drive ][ bp ]               ; get drive
        call readFATBuffer                              ; read FAT Table
        or byte ptr es:[ ccbStatus ][ di ], ( ccb_isFAT )

        getarg cx, _cluster                             ; get cluster value
        getarg dx, _value                               ; get value to update
        and dx, 0FFFh

        pop bx                                          ; word offset into FAT table
        cmp bx, word ptr [ _sectorsize ][ bp ]          ; at sector size -1 boundry ?
        jnz _FAT16UpdateClusterValue_16                 ; no, ok to return as is -->

        call _updateCrossSectorEntry
      ; call updateAllChangedCCBBuffers                 ; optimized when commented out.

        clc                                             ; if no carry
        jmp short _FAT16UpdateClusterValue_Return

      ; see if odd or even cluster

_FAT16UpdateClusterValue_16:
        shr cx, 1                                       ; even or odd cluster
        jnc _FAT16UpdateClusterValue_20                 ; even, take value -->

        shl dx, 1
        shl dx, 1
        shl dx, 1
        shl dx, 1                                       ; shift value

        mov cx, word ptr es:[ ccbData ][ bx + di ]      ; get current value
        and word ptr es:[ ccbData ][ bx + di ], 000Fh   ; clear area.
        
        shr cx, 1
        shr cx, 1
        shr cx, 1
        shr cx, 1                                       ; old value shifted correctly
        jmp short _FAT16UpdateClusterValue_22

_FAT16UpdateClusterValue_20:
        mov cx, word ptr es:[ ccbData ][ bx + di ]      ; get current value
        and word ptr es:[ ccbData ][ bx + di ], 0F000h  ; clear area.

_FAT16UpdateClusterValue_22:
        or word ptr es:[ ccbData ][ bx + di ], dx       ; update FAT word

_FAT16UpdateClusterValue_26:
        and cx, 0FFFh                                   ; mask off unwanted bits
        mov ax, cx
        and ax, 0FF8h                                   ; FAT value, 12 bit entries.
        cmp ax, 0FF8h                                   ; end of chain ?
        jnz _FAT16UpdateClusterValue_UpdatedRet         ; no -->

        mov cx, -1                                      ; if end, set end value
        jmp short _FAT16UpdateClusterValue_UpdatedRet

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  16 bit FAT entries
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FAT16UpdateClusterValue_16Bits:
        mov cx, word ptr es:[ _dpbBytesPerSector ][ bx ]
        shr cx, 1
        div cx                                          ; FAT sector/ Offset

      ; ax will contain FAT sector
      ; dx will contain byte offset into FAT sector

        add dx, dx                                      ; make word offset
        push dx

        xor cx, cx
        mov dx, word ptr es:[ _dpbFirstFAT ][ bx ]      ; where is first FAT table ?
        add dx, ax                                      ; add offset required
        mov ax, word ptr [ _drive ][ bp ]               ; get drive
        call readFATBuffer                              ; read FAT Table
        or byte ptr es:[ ccbStatus ][ di ], ( ccb_isFAT )

        pop bx                                          ; word offset into FAT table
        getarg cx, _value                               ; update value
        xchg cx, word ptr es:[ ccbData ][ bx + di ]     ; update value

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  update
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FAT16UpdateClusterValue_UpdatedRet:
        call CCBChanged
        or cx, cx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FAT16UpdateClusterValue_Return:
        restoreRegisters ax, bx, dx, si, di, es
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Update Cross Sector Entry                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   es:di  points to current ccb                                ;
        ;   bx     offset into buffer                                   ;
        ;   dx     value to update                                      ;
        ;   cx     cluster number                                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   cx     value at cluster (value before update)               ;
        ;...............................................................;

_updateCrossSectorEntry:

        Entry
        def  _sectorflag, cx
        def  _updatevalue, dx
        def  _returnvalue

        mov ax, word ptr es:[ ccbData ][ bx + di ]      ; get value at current sector
        storarg _returnvalue, ax

        shr cx, 1                                       ; even or odd cluster
        jnc _updateCrossSector_20                       ; even, take value -->

        shl dx, 1
        shl dx, 1
        shl dx, 1
        shl dx, 1
        and dx, 0FFF0h
        storarg _updatevalue, dx
        and byte ptr es:[ ccbData ][ bx + di ], 0Fh     ; init area
        or byte ptr es:[ ccbData ][ bx + di ], dl       ; set high value (just the single nibble)
        jmp short _updateCrossSector_22                 ; 

_updateCrossSector_20:
        mov byte ptr es:[ ccbData ][ bx + di ], dl      ; update low order FAT word

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  update next sector
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_updateCrossSector_22:
        call CCBChanged

        xor ah, ah
        mov al, byte ptr es:[ ccbDrive       ][ di ]
        mov cx, word ptr es:[ ccbLBN. _high  ][ di ]
        mov dx, word ptr es:[ ccbLBN. _low   ][ di ]
        inc dx
        adc cx, 0                                       ; 32 bit add
        call readFATBuffer                              ; read FAT Table
        or byte ptr es:[ ccbStatus ][ di ], ( ccb_isFAT )

        getarg dx, _updatevalue
        mov ax, word ptr es:[ ccbData ][ di ]           ; get value at current sector
        test word ptr [ _sectorflag ][ bp ], 1          ; even or odd cluster
        jz _updateCrossSector_30                        ; even, take value -->
        mov byte ptr es:[ ccbData ][ di ], dh           ; set low order value

        xchg ah, al
        mov al, byte ptr [ _returnvalue ][ bp ]
        shr ax, 1
        shr ax, 1
        shr ax, 1
        shr ax, 1                                       ; return value in ax
        jmp short _updateCrossSector_32                 ; 

_updateCrossSector_30:
        and byte ptr es:[ ccbData ][ di ], 0F0h         ; clear high order
        or byte ptr es:[ ccbData ][ di ], dh            ; update high order FAT word

        xchg ah, al        
        mov al, byte ptr [ _returnvalue ][ bp ]

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  rebuild original value
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_updateCrossSector_32:
        push ax
        call CCBChanged

        pop cx
        and cx, 0FFFh
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get 12Bit FAT Table Value                                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   dx     current cluster                                      ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   dx     value at cluster (next cluster)                      ;
        ;   zr     if end of cluster chain.                             ;
        ;...............................................................;

_get_12Bit_ClusterValue:

        Entry
        def  _drive, ax
        def  _cluster, dx
        def  _sectorsize
        ddef _sector
        ddef _dpb, es, bx

        mov ax, dx
        add ax, ax
        add ax, dx                                      ; nibble address

        xor dx, dx
        mov cx, word ptr es:[ _dpbBytesPerSector ][ bx ]
        mov word ptr [ _sectorsize ][ bp ], cx
        dec word ptr [ _sectorsize ][ bp ]
        shl cx, 1                                       ; nibbles / sector
        div cx                                          ; sector to read

      ; ax will contain sector
      ; dx will contain nibble offset

        shr dx, 1                                       ; word offset
        push dx                                         ;

        xor cx, cx                                      ; 32 bit address
        mov dx, ax
        add dx, word ptr es:[ _dpbFirstFAT ][ bx ]      ; where is first FAT table ?
        stordarg _sector, cx, dx                        ; 32 bit sector address

        mov ax, word ptr [ _drive ][ bp ]               ; get drive
        call readFATBuffer                              ; read FAT Table
        or byte ptr es:[ ccbStatus ][ di ], ( ccb_isFAT )

        pop bx                                          ; word offset into FAT table
        mov dx, word ptr es:[ ccbData ][ bx + di ]      ; get FAT word

        cmp bx, word ptr [ _sectorsize ][ bp ]          ; at sector size -1 boundry ?
        jnz _get_12Bit_ClusterValue_12                  ; no, ok to return as is -->

        push dx                                         ; else save what we have
        getdarg cx, dx, _sector                         ; read next cluster sector
        add dx, 0001                                    ; incr by one
        adc cx, 0000

        mov ax, word ptr [ _drive ][ bp ]               ; get drive
        call readFATBuffer                              ; read FAT Table
        or byte ptr es:[ ccbStatus ][ di ], ( ccb_isFAT )

        pop dx
        mov dh, byte ptr es:[ ccbData ][ di ]           ; get FAT word

_get_12Bit_ClusterValue_12:
        test word ptr [ _cluster ][ bp ], 1             ; is cluster Odd ?
        jz _get_12Bit_ClusterValue_14                   ; no, just take value -->

        mov cl, 4
        shr dx, cl

_get_12Bit_ClusterValue_14:
        xor cx, cx                                      ; return 0000 in high order
        and dx, 0FFFh                                   ; 12 bit mask
        mov ax, dx
        and ax, 00FF8h                                  ; FAT value, 12 bit entries.
        cmp ax, 00FF8h                                  ; end of chain ?
        jnz _get_12Bit_ClusterValue_16                  ; no -->
        mov dx, -1                                      ; if end, set end value
        mov cx, dx

_get_12Bit_ClusterValue_16:
        getarg ax, _drive
        clc
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Delete Cluster Chain (Dsk Access Pointer)                    ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Removes ENTIRE cluster chain starting at the address passed  ;
        ;  in the diskAccess block.                                     ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ds:di  dsk access pointer                                   ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   none   if end of cluster chain.                             ;
        ;...............................................................;

ReleaseClusterChainDskAccess:

        push ax

        mov ax, word ptr ss:[ diskAcDrive ][ di ]
        mov cx, word ptr ss:[ diskAcCurCluster. _high ][ di ]
        mov dx, word ptr ss:[ diskAcCurCluster. _low ][ di ]
        call ReleaseClusterChain

        mov word ptr ss:[ diskAcCurCluster. _low  ][ di ], 0000
        mov word ptr ss:[ diskAcCurCluster. _high ][ di ], 0000
        pop ax
        ret 

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Delete Cluster Chain                                         ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Removes ENTIRE cluster chain starting at the address passed  ;
        ;  in cx:dx.                                                    ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  starting cluster value                               ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   none   if end of cluster chain.                             ;
        ;                                                               ;
        ;...............................................................;

ReleaseClusterChain:

        Entry
        ddef _cluster, cx, dx

        saveSegments ax, bx, si, di
        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        jc releaseClusterChain_Return                   ; if device invalid -->

        cmp word ptr es:[ _dpbSectorsPerFat ][ bx ], 0000
        jz releaseClusterChain_32Bits

        mov dx, word ptr [ _cluster. _low ][ bp ]
        call _FAT16ReleaseClusterChain                  ; release 16-bits
        mov cx, 0000                                    ; insure 0000's for top 16 bits
        jmp short releaseClusterChain_Return

releaseClusterChain_32Bits:
        getdarg cx, dx, _cluster
        call _FAT32ReleaseClusterChain                  ; release 32-bits

releaseClusterChain_Return:
        restoreSegments di, si, bx, ax
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Delete Cluster Chain                              (non FAT32);
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Removes cluster addressed in dx from *both* FAT tables and   ;
        ;  returns pointer to next cluster ( or FFFF).                  ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   dx     cluster address                                      ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   zr     if end of cluster chain.                             ;
        ;...............................................................;

_FAT16ReleaseClusterChain:

        saveAllRegisters
        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        jc _FAT16ReleaseClusterChain_16                 ; if device invalid -->

        or dx, dx                                       ; cluster valid ?
        jz _FAT16ReleaseClusterChain_16                 ; no, exit -->
        cmp dx, word ptr es:[ _dpbMaxClusterNumber ][ bx ]
        jc _FAT16ReleaseClusterChain_08
        jnz _FAT16ReleaseClusterChain_16                ; no, exit -->

_FAT16ReleaseClusterChain_08:
        cmp word ptr es:[ _dpbFreeCount. _low ][ bx ], -1
        jz _FAT16ReleaseClusterChain_12                 ; if value not initialized -->
        inc word ptr es:[ _dpbFreeCount. _low ][ bx ]   ; increment allocated unit

_FAT16ReleaseClusterChain_12:
        xor cx, cx                                      ; 16-bit drives, ok to pass args in cx
        call _FAT16UpdateClusterValue                   ; _release cluster at ax:dx
        jz _FAT16ReleaseClusterChain_16                 ; no more -->

        mov dx, cx                                      ; next cluster
        cmp dx, -1                                      ; end of chain ?
        jnz _FAT16ReleaseClusterChain_08                ; no -->

_FAT16ReleaseClusterChain_16:
        restoreAllRegisters
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Delete Cluster Chain                                 (FAT32) ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Removes cluster addressed in dx from *both* FAT tables and   ;
        ;  returns pointer to next cluster ( or FFFF).                  ;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster address                                      ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   zr     if end of cluster chain.                             ;
        ;...............................................................;

_FAT32ReleaseClusterChain:

        Entry
        ddef _cluster, cx, dx

        saveAllRegisters
        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        jc _FAT32ReleaseClusterChain_16                 ; if device invalid -->
        call SeeIfMaxClusters                           ; at max clusters ?
        jc _FAT32ReleaseClusterChain_16                 ; invalid -->

        getdarg cx, dx, _cluster
        call isEndOfFAT32
        jz _FAT32ReleaseClusterChain_16

_FAT32ReleaseClusterChain_08:
        add word ptr es:[ _dpbFreeCount. _low ][ bx ], 0001  ; increment allocated unit
        adc word ptr es:[ _dpbFreeCount. _high ][ bx ], 0000
        or word ptr es:[ _dpbxFlags     ][ bx ], BPB_FREESPACEALTERED

        mov di, -1
        push di
        push di                                         ; update value is -1
        call _FAT32UpdateClusterValue                   ; release block
        jnz _FAT32ReleaseClusterChain_08                ; keep going till end -->

_FAT32ReleaseClusterChain_16:
        restoreAllRegisters
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Find All Free Space                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  # free clusters                                      ;
        ;   es:bx  pointer to DPB                                       ;
        ;...............................................................;

AmountFreeSpace:

        Entry
        def  _currcluster
        def  _maxclusters
        def  _entriesPerSector

        def _drive, ax
        ddef _dpb
        ddef _sector
        ddef _buffer

        ddef  _freeclusters, 0000, 0000

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; if FAT32 drive
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        call getDPB                                     ; (es:bx) Device Parameter Block
        stordarg _dpb, es, bx                           ; save
        ifc amountFreeSpace_Return                      ; exit if invalid -->

        mov cx, word ptr es:[ _dpbFreeCount. _high ][ bx ]
        mov dx, word ptr es:[ _dpbFreeCount. _low  ][ bx ]
        stordarg _freeclusters, cx, dx

        cmp word ptr es:[ _dpbSectorsPerFat ][ bx ], 0000
        ifz amountFreeSpace_Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; if FAT16 drive
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        cmp word ptr es:[ _dpbFreeCount. _low ][ bx ], -1
        ifnz amountFreeSpace_Return

amountFreeSpace_12:
        xor cx, cx
        mov word ptr [ _currcluster  ][ bp ], cx
        mov word ptr [ _freeclusters. _low  ][ bp ], cx
        mov word ptr [ _freeclusters. _high ][ bp ], cx

        mov dx, word ptr es:[ _dpbMaxClusterNumber ][ bx ]
        mov word ptr [ _maxclusters ][ bp ], dx

        test word ptr es:[ _dpbMaxClusterNumber ][ bx ], 0F000h
        jnz amountFree16BitFAT                          ; if 16-bit FAT -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  12 Bit FAT 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov word ptr [ _currcluster  ][ bp ], MINCLUSTER

amountFree12BitFAT:
        getarg ax, _drive                               ; restore drive
        getarg dx, _currcluster                         ; current cluster
        getdarg es, bx, _dpb                            ; restore dpb
        call _get_12Bit_ClusterValue                    ; optimized get value

        or dx, dx                                       ; cluster free ?
        jnz amountFree12BitFAT_18                       ; no -->
        inc word ptr [ _freeclusters. _low ][ bp ]

amountFree12BitFAT_18:
        inc word ptr [ _currcluster ][ bp ]             ; current cluster
        dec word ptr [ _maxclusters ][ bp ]             ; more clusters to go ?
        jnz amountFree12BitFAT                          ; yes -->
        jmp amountFreeSpace_Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  16 Bit FAT 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

amountFree16BitFAT:
        mov cx, word ptr es:[ _dpbBytesPerSector ][ bx ]
        shr cx, 1                                       ; entries per sector
        storarg _entriesPerSector, cx

        xor cx, cx
        mov dx, word ptr es:[ _dpbFirstFAT ][ bx ]      ; where is first FAT table ?
        stordarg _sector, cx, dx
        storarg _currcluster, cx
        add word ptr [ _maxclusters ][ bp ], 0002       ; adjust for fat16 entries

        mov ax, word ptr [ _drive ][ bp ]               ; get drive
        call readFATBuffer                              ; read FAT Table
        stordarg _buffer, es, di                        ; save buffer address
        or byte ptr es:[ ccbStatus ][ di ], ( ccb_isFAT )

amountFree16BitFAT_08:
        xor ax, ax
        getarg cx, _entriesPerSector
        mov di, word ptr [ _buffer. _pointer ][ bp ]
        lea di, offset [ ccbData ][ di ]                ; start of buffer

amountFree16BitFAT_12:
        repnz scasw                                     ; search for null word
        jnz amountFree16BitFAT_16                       ; count is zero -->
        inc word ptr [ _freeclusters. _low ][ bp ]      ; increment free space
        or cx, cx                                       ; more clusters this sector ?
        jnz amountFree16BitFAT_12                       ; yes -->

amountFree16BitFAT_16:
        mov cx, word ptr [ _entriesPerSector ][ bp ]
        add cx, word ptr [ _currcluster ][ bp ]
        mov word ptr [ _currcluster ][ bp ], cx

        sub cx, word ptr [ _maxclusters ][ bp ]         ; entries to go
        jnc amountFreeSpace_Return                      ; if at end -->

        neg cx
        cmp cx, word ptr [ _entriesPerSector ][ bp ]
        jnc amountFree16BitFAT_20
        mov word ptr [ _entriesPerSector ][ bp ], cx    ; max to read last sector

amountFree16BitFAT_20:
        getarg ax, _drive                               ; get drive
        getdarg es, di, _buffer                         ; no longer scan through ccb
        inc word ptr [ _sector ][ bp ]
        getdarg cx, dx, _sector                         ; get next sector
        call readSelBuffer                              ; else read buffer (bypass scan)
        jmp amountFree16BitFAT_08
        
amountFreeSpace_Return:
        getarg ax, _drive
        getdarg es, bx, _dpb                            ; restore dpb
        getdarg cx, dx, _freeclusters                   ; get free clusters
        mov word ptr es:[ _dpbFreeCount. _low ][ bx ], dx
        mov word ptr es:[ _dpbFreeCount. _high ][ bx ], cx

        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Update Free Space on FAT32 Volumes                           ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;                                                               ;
        ;...............................................................;

UpdateFreeSpace:

        SaveAllRegisters
        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        jc UpdateFreeSpace_Return                       ; if drive invalid -->

        cmp word ptr es:[ _dpbSectorsPerFat ][ bx ], 0000
        jnz UpdateFreeSpace_Return                      ; if not FAT32 -->

        test word ptr es:[ _dpbxFlags ][ bx ], BPB_FREESPACEALTERED
        jz UpdateFreeSpace_Return                       ; if free space not updated -->

        and word ptr es:[ _dpbxFlags  ][ bx ], NOT BPB_FREESPACEALTERED
        mov dx, word ptr es:[ _dpbFreeCount. _low ][ bx ]
        mov cx, word ptr es:[ _dpbFreeCount. _high ][ bx ]
        mov word ptr es:[ _dpbxFreeClusterCount. _low ][ bx ], dx
        mov word ptr es:[ _dpbxFreeClusterCount. _high ][ bx ], cx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  update INFO sector
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        push cx                                         ; (high)
        push dx                                         ; (low ) free space value

        xor cx, cx
        mov dx, word ptr es:[ _dpbxFSINFOSector ][ bx ] ; free space (INFO) sector
        call readBuffer                                 ; read sector

        lea bx, offset [ ccbData. sizeSector - (size BIGFATBOOTFSINFO) - 4 ][ di ]
        pop word ptr [ _bfFSInfoFreeClusterCnt. _low ][ si ]
        pop word ptr [ _bfFSInfoFreeClusterCnt. _high ][ si ]

        call updateChangedCCB                           ; update changed buffer

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

UpdateFreeSpace_Return:
        RestoreAllRegisters
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Allocate Next Free Cluster                                   ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  recommended start search address                     ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  allocated cluster number                             ;
        ;   cy     if end of cluster chain.                             ;
        ;...............................................................;

AllocateCluster:

        Entry
        def _drive, ax

        ddef _dpb
        ddef _maxclusters
        ddef _cluster, cx, dx
        ddef _origCluster, cx, dx

        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        stordarg _dpb, es, bx                           ; save _dpb address
        jc allocateCluster_NoneFree                     ; if device invalid -->

        call GetMaxClusters
        stordarg _maxclusters, cx, dx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  loop through entire FAT table search for free cluster
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

allocateCluster_12:
        getarg ax, _drive                               ; get drive
        getdarg cx, dx, _cluster                        ; starting cluster
        call getNextCluster                             ; get value at cluster
        jc allocateCluster_Return                       ; if error -->

        mov ax, dx
        or ax, cx                                       ; is cluster free ?
        jz allocateCluster_18                           ; yes -->

        add word ptr [ _cluster. _low  ][ bp ], 0001    ; increment by one
        adc word ptr [ _cluster. _high ][ bp ], 0000

        getdarg cx, dx, _cluster
        cmp dx, word ptr [ _origCluster. _low  ][ bp ]  ; back at recommended cluster ?
        jnz allocateCluster_14                          ; no, keep searching -->
        cmp cx, word ptr [ _origCluster. _high ][ bp ]  ; back at recommended cluster ?
        jz allocateCluster_NoneFree                     ; yes, none are free -->

allocateCluster_14:
        push word ptr [ _maxclusters. _high ][ bp ]
        push word ptr [ _maxclusters. _low  ][ bp ]
        call _cmp32
        jc allocateCluster_12

        mov word ptr [ _cluster. _low  ][ bp ], MINCLUSTER
        mov word ptr [ _cluster. _high ][ bp ], 0000
        jmp allocateCluster_12

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if no space available on disk
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

allocateCluster_NoneFree:
        stc
        jmp short allocateCluster_Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  update allocated buffer
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

allocateCluster_18:
        mov dx, -1
        push dx
        push dx                                         ; set allocate value 0xFFFFFFFF
        getarg ax, _drive                               ; get drive
        getdarg cx, dx, _cluster                        ; get cluster value
        call updateClusterValue                         ; update value at cluster cx:dx

        getdarg es, bx, _dpb                                ; get _dpb address
        cmp word ptr es:[ _dpbFreeCount. _high ][ bx ], -1  ; value initialized ?
        jz allocateCluster_Return                           ; no -->

        sub word ptr es:[ _dpbFreeCount. _low ][ bx ], 0001 ; subtract allocated unit
        sbb word ptr es:[ _dpbFreeCount. _high ][ bx ], 0000
        or word ptr es:[ _dpbxFlags     ][ bx ], BPB_FREESPACEALTERED

 ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

allocateCluster_Return:
        getarg ax, _drive
        getdarg cx, dx, _cluster                        ; get cluster value
        getdarg es, bx, _dpb
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Allocate/Init A Cluster.                                     ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Note:                                                        ;
        ;   This function will allocate a cluster and completely fill   ;
        ;   it with zeroes.  Ideal for directories which must have      ;
        ;   initialized entries.                                        ;
        ;                                                               ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster                                              ;
        ;   es:di  pointer to a ccb buffer                              ;
        ;...............................................................;

AllocateInitCluster:

        Entry
        def _drive, ax
        def _secPerCluster
        ddef _cluster
        ddef _sector

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  allocate a free cluster / sector
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        saveRegisters si, bx
        call getAddrDPB                                 ; point to dpb

        mov cx, 0001
        add cl, byte ptr es:[ _dpbClusterSizeMask ][ bx ]
        mov word ptr [ _secPerCluster ][ bp ], cx

        xor dx, dx
        xor cx, cx                                      ; recommended cluster 
        call AllocateCluster                            ; allocate a cluster
        stordarg _cluster, cx, dx
        jc AllocateInitCluster_12

        call computeLogSectorNumber                     ; cluster -> sector number
        stordarg _sector, cx, dx                        ; save sector #

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  allocate a buffer/ init
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        call selBuffer                                  ; allocate a buffer
        push di                                         ; pointer to header
        lea di, offset [ ccbData ][ di ]                ; point to data buffer
        clearMemory sizeCCBData                         ; clear to zeroes
        pop di                                          ; pointer to header

AllocateInitCluster_08:
        call updateChangedCCB                           ; MUST force write updated buffer

        add word ptr es:[ ccbLBN. _low  ][ di ], 0001
        adc word ptr es:[ ccbLBN. _high ][ di ], 0000
        dec word ptr [ _secPerCluster  ][ bp ]          ; continuous write of directory 
        jnz AllocateInitCluster_08

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        getdarg cx, dx, _sector
        mov word ptr es:[ ccbLBN. _low  ][ di ], dx
        mov word ptr es:[ ccbLBN. _high ][ di ], cx
        or cx, cx                                       ; no carry.

AllocateInitCluster_12:
        getarg ax, _drive
        getdarg cx, dx, _cluster
        restoreRegisters bx, si
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Append A Cluster                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster to append                                    ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  new cluster value                                    ;
        ;...............................................................;

AppendCluster:

        Entry
        def _drive, ax
        ddef _cluster, cx, dx
        ddef _allocatedCluster

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  allocate a free cluster / sector
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

AppendCluster_06:
        push bx
        call AllocateCluster                            ; allocate a cluster
        jc appendCluster_08                             ; if error -->

        stordarg _allocatedCluster, cx, dx              ; allocated cluster

        push word ptr [ _allocatedCluster. _high ][ bp ]
        push word ptr [ _allocatedCluster. _low  ][ bp ]
        getdarg cx, dx, _cluster
        call updateClusterValue                         ; update cluster value

        getdarg cx, dx, _allocatedCluster               ; restore allocated cluster
        or dx, dx                                       ; clear carry

appendCluster_08:
        pop bx
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Get Max Clusters                                             ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  max clusters for drive                               ;
        ;...............................................................;

GetMaxClusters:

        push es
        push bx
        call getAddrDPB
        jc GetMaxClusters_20

        xor cx, cx
        mov dx, word ptr es:[ _dpbMaxClusterNumber   ][ bx ]
        or dx, dx
        jnz GetMaxClusters_20

        mov dx, word ptr es:[ _dpbxMaxCluster. _low  ][ bx ]
        mov cx, word ptr es:[ _dpbxMaxCluster. _high ][ bx ]

GetMaxClusters_20:
        pop bx
        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  See If Max Clusters                                          ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster address                                      ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  max clusters                                         ;
        ;   cy     cluster address below min or above max               ;
        ;                                                               ;
        ;...............................................................;

SeeIfMaxClusters:

        Entry
        ddef _clusters, cx, dx
        ddef _maxclusters

        or cx, cx 
        jnz SeeIfMaxClusters_08                         ; if above MIN -->
        cmp dx, MINCLUSTER
        jc SeeIfMaxClusters_Return                      ; if less than MIN -->

SeeIfMaxClusters_08:
        call GetMaxClusters
        stordarg _maxclusters, cx, dx

        cmp cx, word ptr [ _clusters. _high ][ bp ]
        jnz SeeIfMaxClusters_Return                     ; if carry, cluster address is too high ->
        cmp dx, word ptr [ _clusters. _low ][ bp ]

SeeIfMaxClusters_Return:
        getdarg cx, dx, _maxclusters
        Return

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Is End Of FAT32 Chain                                        ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   cx:dx  cluster number                                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   zr     if end of chain (0x0FFFFFFF)                         ;
        ;...............................................................;

isEndOfFAT32:

        push ax
        mov ax, cx
        and ax, 0FFFh
        cmp ax, 0FFFh                                   ; end link ?
        jnz isEndOfFAT32_Return                         ; no -->

        mov ax, dx
        and ax, 0FFF8h                                  ; FAT value, 12 bit entries.
        cmp ax, 0FFF8h                                  ; end of chain ?

isEndOfFAT32_Return:
        clc
        pop ax
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Compute Logical Sector Number                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ax     drive                                                ;
        ;   cx:dx  cluster number                                       ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   ax     drive                                                ;
        ;   cx:dx  logical sector number                                ;
        ;...............................................................;

computeLogSectorNumber:

        push es
        push bx
        push ax                                         ; drive

        call getAddrDPB                                 ; get first sector address from DPB
        cmp word ptr es:[ _dpbMaxClusterNumber ][ bx ], 0000
        jnz computeLogSectorNumber_40                   ; if NOT FAT32 -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; FAT 32 drive
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, dx
        mov dx, cx

        or cx, ax                                       ; is it root directory ?
        jnz computeLogSectorNumber_16                   ; no -->
        mov ax, word ptr es:[ _dpbxRootCluster. _low  ][ bx ]
        mov dx, word ptr es:[ _dpbxRootCluster. _high ][ bx ]

computeLogSectorNumber_16:
        sub ax, 0002
        sbb dx, 0000                                    ; adj clusters by 2 (min cluster 2)
        mov cx, 1
        add cl, byte ptr es:[ _dpbClusterSizeMask ][ bx ]
        call _mul32

        add ax, word ptr es:[ _dpbxFirstSector. _low ][ bx ]
        adc dx, word ptr es:[ _dpbxFirstSector. _high ][ bx ]

        mov cx, dx                                      ; high part
        mov dx, ax                                      ; low part
        jmp short computeLogSectorNumber_Return

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; FAT 16 drive
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

computeLogSectorNumber_40:
        mov ax, dx                                      ; cluster
        mov dx, word ptr es:[ _dpbFirstDirSector ][ bx ]
        xor cx, cx                                      ; if cluster is zero
        or ax, ax                                       ; if reading cluster 0000
        jz computeLogSectorNumber_Return                ; LSN is zero -->

        dec ax
        dec ax
        mov cx, 1
        add cl, byte ptr es:[ _dpbClusterSizeMask ][ bx ]
        mul cx                                          ; compute sector
        add ax, word ptr es:[ _dpbFirstDataSector ][ bx ]
        adc dx, 0000
        mov cx, dx                                      ; high part
        mov dx, ax                                      ; low part

computeLogSectorNumber_Return:
        pop ax                                          ; restore drive
        pop bx
        pop es
        ret

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Read Random Buffer FAT File                                  ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  Input:                                                       ;
        ;   ss:bx  fat access control block                             ;
        ;                                                               ;
        ;  Output:                                                      ;
        ;   es:bx  pointer in block buffer to data                      ;
        ;   cx     remaining bytes in block                             ;
        ;   zr     means end of file or wrong address                   ;
        ;...............................................................;

_FATReadRandom:

        Entry
        ddef _accessControl, ss, bx
        ddef _endcluster
        ddef _reqOffset
        def  _bytesPerCluster
        ddef _DPBPointer
        ddef _BufferAddress
        def  _bytesremain, 0000
        def  _isnotFAT32                                ; zero means FAT32

        saveRegisters ds, di, si, dx, ax
        and word ptr [ diskAcOptions ][ bx ], NOT (DISKAC_RESULT_READMULTIPLESECTORS)

        mov ax, word ptr [ diskAcDrive ][ bx ]
        call getAddrDPB                                 ; (es:bx) Device Parameter Block
        stordarg _DPBPointer, es, bx
        ifc _FATReadRandom_Return                       ; if error, exit -->

        mov ax, word ptr es:[ _dpbSectorsPerFat ][ bx ] ; is FAT32 drive ?
        storarg _isnotFAT32, ax                         ; zero means FAT32

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if request is before start of buffer
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        setDS ss
        mov bx, word ptr [ _accessControl. _pointer ][ bp ]
        mov ax, word ptr [ diskAcPosition. _low  ][ bx ]
        mov dx, word ptr [ diskAcPosition. _high ][ bx ]
        stordarg _reqOffset, dx, ax

        sub ax, word ptr [ diskAcOffAtBegCluster. _low  ][ bx ]        
        sbb dx, word ptr [ diskAcOffAtBegCluster. _high ][ bx ]
        jge _FATReadRandom_16

        xor ax, ax
        mov word ptr [ diskAcOffAtBegBuffer. _low   ][ bx ], ax
        mov word ptr [ diskAcOffAtBegBuffer. _high  ][ bx ], ax
        mov word ptr [ diskAcOffAtBegCluster. _low  ][ bx ], ax
        mov word ptr [ diskAcOffAtBegCluster. _high ][ bx ], ax
        mov word ptr [ diskAcCurCluster. _low       ][ bx ], ax
        mov word ptr [ diskAcCurCluster. _high      ][ bx ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  locate starting cluster and sector
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FATReadRandom_16:
        mov dx, word ptr [ diskAcCurCluster. _low  ][ bx ]
        or dx, word ptr [ diskAcCurCluster. _high ][ bx ]
        jnz _FATReadRandom_18
        mov dx, word ptr [ diskAcBegCluster. _low  ][ bx ]
        mov cx, word ptr [ diskAcBegCluster. _high ][ bx ]
        mov word ptr [ diskAcCurCluster. _low  ][ bx ], dx
        mov word ptr [ diskAcCurCluster. _high ][ bx ], cx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  compute a fake sector size if addressing cluster 0 (root dir).
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FATReadRandom_18:
        getdarg es, si, _DPBPointer
        mov ax, word ptr es:[ _dpbBytesPerSector ][ si ]
        mov cl, byte ptr es:[ _dpbClusterSizeShift ][ si ]
        shl ax, cl                                      ; bytes per cluster

        mov dx, word ptr [ diskAcBegCluster. _low ][ bx ]
        or dx, word ptr [ diskAcBegCluster. _high ][ bx ]
        jnz _FATReadRandom_20
        cmp word ptr [ _isnotFAT32 ][ bp ], 0000        ; is FAT32 drive ?
        jz _FATReadRandom_20                            ; if FAT32 drive, keep value in ax -->

        mov ax, word ptr es:[ _dpbMaxAllocRootDir ][ si ]
        mov cx, sizeDIRENTRY
        mul cx

_FATReadRandom_20:
        mov word ptr [ _bytesPerCluster ][ bp ], ax

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if within current cluster
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FATReadRandom_22:
        mov dx, word ptr [ _reqOffset. _low  ][ bp ]
        mov cx, word ptr [ _reqOffset. _high ][ bp ]
        sub dx, word ptr [ diskAcOffAtBegCluster. _low  ][ bx ]
        sbb cx, word ptr [ diskAcOffAtBegCluster. _high ][ bx ]
        jnz _FATReadRandom_24                           ; if have to scan forward -->
        cmp dx, word ptr [ _bytesPerCluster ][ bp ]     ; beyond cluster size ?
        jc _FATReadRandom_32                            ; no, within current cluster -->

_FATReadRandom_24:
        mov dx, word ptr [ diskAcCurCluster. _low  ][ bx ]
        mov cx, word ptr [ diskAcCurCluster. _high ][ bx ]

        mov ax, cx
        or ax, dx                                       ; zero cluster ?
        jnz _FATReadRandom_26                           ; no, go scan clusters -->

        cmp word ptr [ _isnotFAT32 ][ bp ], 0000        ; is non FAT32 drive and root directory ?
        ifnz _FATReadRandom_Return                      ; yes, quit -->

_FATReadRandom_26:
        mov di, bx                                      ; dskAccess to ss:di
        call getNextClusterDskAccess                    ; get next cluster
        ifz _FATReadRandom_Return                       ; if end of cluster chain -->

        mov word ptr [ diskAcCurCluster. _low  ][ bx ], dx
        mov word ptr [ diskAcCurCluster. _high ][ bx ], cx

        mov cx, word ptr [ _bytesPerCluster  ][ bp ]
        add word ptr [ diskAcOffAtBegCluster. _low  ][ bx ], cx
        adc word ptr [ diskAcOffAtBegCluster. _high ][ bx ], 0000
        jmp _FATReadRandom_22

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  see if within current sector
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FATReadRandom_32:
        lds bx, dword ptr [ _accessControl ][ bp ]
        mov ax, word ptr [ diskAcDrive ][ bx ]
        mov dx, word ptr [ diskAcCurCluster. _low  ][ bx ]
        mov cx, word ptr [ diskAcCurCluster. _high ][ bx ]
        call computeLogSectorNumber                     ; cluster -> sector number

        push cx
        push dx                                         ; save logical sector number

        mov ax, word ptr [ _reqOffset. _low  ][ bp ]
        mov dx, word ptr [ _reqOffset. _high ][ bp ]
        sub ax, word ptr [ diskAcOffAtBegCluster. _low  ][ bx ]
        sbb dx, word ptr [ diskAcOffAtBegCluster. _high ][ bx ]
        div word ptr  es:[ _dpbBytesPerSector ][ si ]

        mov cx, word ptr [ _reqOffset. _low ][ bp ]
        sub cx, dx                                      ; subtract out remainder
        mov word ptr [ diskAcOffAtBegBuffer. _low ][ bx ], cx
        mov cx, word ptr [ _reqOffset. _high ][ bp ]
        mov word ptr [ diskAcOffAtBegBuffer. _high ][ bx ], cx

        pop dx
        pop cx                                          ; restore lsn
        add dx, ax                                      ; real sector offset
        adc cx, 0

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; read sector
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov ax, word ptr [ diskAcDrive ][ bx ]
        mov word ptr [ diskAcCurSector. _low ][ bx ], dx
        mov word ptr [ diskAcCurSector. _high ][ bx ], cx

        test word ptr [ _reqOffset. _low ][ bp ], (SIZESECTOR - 1)
        ifnz _FATReadRandom_52                          ; if not on sector boundary ->
        test word ptr [ diskAcOptions ][ bx ], DISKAC_READ_MULTIPLESECTORS
        ifz _FATReadRandom_52                           ; if not multiple sectors ->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; identify max forward scan clusters
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        mov es, word ptr [ diskAcBufferPtr. _segment ][ bx ]
        mov di, word ptr [ diskAcBufferPtr. _pointer ][ bx ]
        NormalizeBuffer es, di                          ; normalize buffer pointer
        stordarg _BufferAddress, es, di

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; compute clusters to read (can't round up)
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        xor dx, dx
        mov ax, word ptr [ _bytesPerCluster ][ bp ]     ; wrap up by adding 
        dec ax                                          ; bytes per cluster - 1
        add ax, word ptr [ diskAcBytesToRead ][ bx ]    ; allow max requested
        adc dx, 0000
        div word ptr [ _bytesPerCluster ][ bp ]         ; compute max clusters to scan

        push ax                                         ; max clusters to scan
        mov ax, word ptr [ diskAcDrive ][ bx ]
        mov dx, word ptr [ diskAcCurCluster. _low  ][ bx ]
        mov cx, word ptr [ diskAcCurCluster. _high ][ bx ]
        call scanClusterMap                             ; number contiguous clusters
        stordarg _endcluster, cx, dx

        xor dx, dx
        mul word ptr [ _bytesPerCluster ][ bp ]         ; compute total bytes to read

        or dx, dx
        jnz _FATReadRandom_40                           ; if > 64k, drop down to actual request
        cmp ax, word ptr [ diskAcBytesToRead ][ bx ]    ; more than requested ?
        jc _FATReadRandom_42                            ; no -->

_FATReadRandom_40:
        mov ax, word ptr [ diskAcBytesToRead ][ bx ]    ; limit to requested size

_FATReadRandom_42:
        xor dx, dx
        mov cx, SIZESECTOR                              ; into sectors
        div cx                                          ; sectors to read

        getdarg es, di, _BufferAddress
        or word ptr [ diskAcOptions ][ bx ], DISKAC_RESULT_READMULTIPLESECTORS
        mov dx, word ptr [ diskAcCurSector. _low ][ bx ]
        mov cx, word ptr [ diskAcCurSector. _high ][ bx ]
        mov bx, word ptr [ diskAcDrive ][ bx ]
        xchg ax, bx
        call DevRead

        mov ax, SIZESECTOR                              ; sector size
        mul cx                                          ; actual sectors read
        storarg _bytesremain, ax                        ; actual bytes returned

        getdarg es, di, _BufferAddress
        jmp short _FATReadRandom_Return                 ; return bytes read

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  read sector (unoptimized read )
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FATReadRandom_52:
        mov ax, word ptr [ diskAcDrive ][ bx ]
        mov dx, word ptr [ diskAcCurSector. _low ][ bx ]
        mov cx, word ptr [ diskAcCurSector. _high ][ bx ]
        call ReadBuffer                                 ; read sector
        mov ax, word ptr [ diskAcOptions ][ bx ]
        or byte ptr es:[ ccbStatus ][ di ], al          ; type of block

        lea di, offset [ ccbData ][ di ]                ; point to data 
        mov word ptr [ diskAcBufferPtr. _segment ][ bx ], es
        mov word ptr [ diskAcBufferPtr. _pointer ][ bx ], di

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return pointer
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FATReadRandom_60:
        lds bx, dword ptr [ _accessControl ][ bp ]
        les si, dword ptr [ _DPBPointer    ][ bp ]
        mov cx, word ptr es:[ _dpbBytesPerSector ][ si ]

        mov ax, word ptr [ _reqOffset. _low  ][ bp ]
        mov dx, word ptr [ _reqOffset. _high ][ bp ]
        sub ax, word ptr [ diskAcOffAtBegBuffer. _low  ][ bx ]
        sbb dx, word ptr [ diskAcOffAtBegBuffer. _high ][ bx ]

        les bx, dword ptr [ diskAcBufferPtr ][ bx ]
        add bx, ax                                      ; offset into buffer
        sub cx, ax                                      ; bytes remaining in buffer
        storarg _bytesremain, cx

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  return
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_FATReadRandom_Return:
        restoreRegisters ax, dx, si, di, ds
        getarg cx, _bytesremain
        or cx, cx
        Return

RxDOS   ENDS
        END
