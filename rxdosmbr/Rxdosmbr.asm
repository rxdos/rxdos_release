        TITLE   'RxDOS Boot Sector Program'
        PAGE 59, 132
        .LALL

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  RxDOS Boot Sector Program                                    ;
        ;...............................................................;

        ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''';
        ;  Real Time Dos                                                ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;                                                               ;
        ;  This product is distributed  AS IS and contains no warranty  ;
        ;  whatsoever,   including  warranty  of   merchantability  or  ;
        ;  fitness for a particular purpose.                            ;
        ;                                                               ;
        ;  (c) Copyright 1990, 1999. Api Software and Mike Podanoffsky  ;
        ;      All Rights Reserved Worldwide.                           ;
        ;                                                               ;
        ;  This product is protected under copyright laws and  may not  ;
        ;  be reproduced  in whole  or in part, in any form  or media,  ;
        ;  included but not limited to source listing, facsimile, data  ;
        ;  transmission, cd-rom, or  floppy disk without the expressed  ;
        ;  written consent of the author.                               ;
        ;                                                               ;
        ;  License  for  distribution  for commercial  use  or  resale  ;
        ;  required from:                                               ;
        ;                                                               ;
        ;  Api Software                                                 ;
        ;  25 St Anthony Drive                                          ;
        ;  Hudson,  NH   03051                                          ;
        ;                                                               ;
        ;  internet: mikep@rxdos.com                                    ;
        ;                                                               ;
        ;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -;
        ;  Compile with MASM 5.1                                        ;
        ;...............................................................;

        include rxdosmac.inc
        include rxdosdef.inc

RxDOSMBRBOOT SEGMENT PUBLIC 'CODE'
        assume cs:RxDOSMBRBOOT, ds:RxDOSMBRBOOT, es:RxDOSMBRBOOT, ss:RxDOSMBRBOOT

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; reserved on stack
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

_diskPartitionTable             dw ?
_stackReserved                  equ ($-_diskPartitionTable) + 40h

PART_BOOT                       equ 80h
PART_NULLBOOT                   equ 00h

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; the RxDOS boot process begins here
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                                org 7C00h

RxDOS_MASTERBOOTLOAD:
        cli                                             ; no interrupts
        cld                                             ; all that we need to init

        mov ax, cs
        mov ds, ax                                      ; set segments
        mov es, ax                                      ; 
        mov ss, ax                                      ; set stack
        mov sp, 0600h - _stackReserved
        mov bp, sp
        sti                                             ; re-enable interrupts

   ; relocate MBR to 0060: 0000
        mov di, 0600h                                   ; 0060:0000 
        mov cx, (sizeSector / 2)                        ; size of sector in words
        rep movsw                                       ; copy sector
        JMP_FAR 60h, RxDOS_MASTERBOOTLOAD_08            ; jmp FAR RxDOS_MASTERBOOTLOAD_08

RxDOS_MASTERBOOTLOAD_08:
        mov cx, 4                                       ; # arguments
        mov bx, offset RxDOS_MASTERBOOT_PARTITIONTABLE

RxDOS_MASTERBOOTLOAD_10:
        cmp byte ptr [ _ptBootable ][ bx ], PART_BOOT   ; [0000] == 80h
        jz RxDOS_MASTERBOOTLOAD_22                      ; yes -->
        cmp byte ptr [ _ptBootable ][ bx ], PART_NULLBOOT  ; [0000] == 00h
        jnz RxDOS_MASTERBOOTLOAD_CorruptTableError      ; table is corrupted -->

        add bx, sizePARTITION
        loop RxDOS_MASTERBOOTLOAD_10

        int 18h                                         ; if error 

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  if boot partition located
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
RxDOS_MASTERBOOTLOAD_22:
        mov si, bx                                      ; save pointer to this entry
        mov word ptr _diskPartitionTable[ bp ], bx

RxDOS_MASTERBOOTLOAD_24:
        add bx, sizePARTITION                           ; next entry
        dec cx                                          ; see if more entries
        jz RxDOS_MASTERBOOTLOAD_32                      ; no more -->

      ; only one entry in table can be a boot entry
      ; -------------------------------------------

        cmp byte ptr [ _ptBootable ][ bx ], PART_NULLBOOT  ; [0000] == 00h
        jz RxDOS_MASTERBOOTLOAD_24                      ; see if all entries scanned ->
        jmp RxDOS_MASTERBOOTLOAD_CorruptTableError      ; table is corrupted -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  boot partion located
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
RxDOS_MASTERBOOTLOAD_32:
        mov dx, word ptr [ _ptBootable ][ si ]          ; get bootable/ head
        mov cx, word ptr [ _ptBeginSector ][ si ]       ; get sector/ cylinder
        mov di, 5                                       ; # retries

RxDOS_MASTERBOOTLOAD_36:
        mov bx, 7C00h                                   ; where to load
        mov ax, 0201h                                   ; BIOS command
        push di                                         ; save retries
        int 13h                                         ; try reading disk
        pop di                                          ; retries
        jnc RxDOS_MASTERBOOTLOAD_44                     ; ok -->

        xor ax, ax                                      ; reset disk on error
        int 13h                                         ; 
        dec di                                          ; more retires to go
        jnz RxDOS_MASTERBOOTLOAD_36                     ; retries -->
     ;  jmp short RxDOS_MASTERBOOTLOAD_LoadError

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Error Messages
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
RxDOS_MASTERBOOTLOAD_LoadError:
        mov si, offset RxDOS_MASTERBOOT_MsgErrorWhenLoading
        jmp short RxDOS_MASTERBOOT_DisplayMessage

RxDOS_MASTERBOOTLOAD_CorruptTableError:
        mov si, offset RxDOS_MASTERBOOT_MsgTableIsCorrupted
        jmp short RxDOS_MASTERBOOT_DisplayMessage

RxDOS_MASTERBOOTLOAD_OSMissingError:
        mov si, offset RxDOS_MASTERBOOT_MsgOSMissing
     ;  jmp short RxDOS_MASTERBOOT_DisplayMessage

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Display Message
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
RxDOS_MASTERBOOT_DisplayMessage:
        lodsb
        cmp al, 00
        jz RxDOS_MASTERBOOTLOAD_WaitLoop

        push si
        mov bx, 0007h                                   ; attributes
        mov ah, 0Eh                                     ; display character
        int 10h
        pop si
        jmp RxDOS_MASTERBOOT_DisplayMessage

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Wait Loop
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
RxDOS_MASTERBOOTLOAD_WaitLoop:
        jmp RxDOS_MASTERBOOTLOAD_WaitLoop               ; self loop -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  boot partion located
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
RxDOS_MASTERBOOTLOAD_44:
        mov di, offset (7C00h + sizeSector - 2)
        cmp word ptr [ di ], RxDOS_PARTITIONSIGNATURE   ; AA55
        jnz RxDOS_MASTERBOOTLOAD_OSMissingError

        mov si, word ptr _diskPartitionTable[ bp ]
        JMP_FAR RxDOS_MASTERBOOTLOAD, 7C00h             ; jmp FAR to 7C00 -->

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  Error Messages
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

RxDOS_MASTERBOOT_MsgTableIsCorrupted:
        db "Master Boot Table is corrupted.", 0

RxDOS_MASTERBOOT_MsgErrorWhenLoading:
        db "Error when loading.", 0

RxDOS_MASTERBOOT_MsgOSMissing:
        db "Operating System missing", 0

;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; partition table
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        ORG 0600H + sizeSector - 2 - 4 * sizePARTITION

RxDOS_MASTERBOOT_PARTITIONTABLE:
        db 0                                            ; partition table patched here

        ORG 7C00h + sizeSector - 2
        db 55h, 0AAh

RxDOSMBRBOOT    ENDS
                END  RxDOS_MASTERBOOTLOAD


